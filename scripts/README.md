# Transposer API Scripts

## kafka_get_active_brokers

`kafka_get_active_brokers` queries the Kafka API to get the active brokers in the cluster. If the cluster is not reachable or has no active brokers, an empty string will be returned. On success it returns the list of brokers, one on each line.

### Parameters

The script gets configured by a `.env` file in the same directory.

**Mandatory parameters**:
* `KAFKA_BROKER_API_VERSIOS_SH` Full path to the `kafka-broker-api-versions.sh` script of the Kafka installtion
* `KAFKA_BOOTSTRAP_SERVER` The server to query. Only one of the servers in the cluster is necessary.


**Optional parameters**:
* `ACTIVE_BROKERS_DATA`: Path the a target file. If omitted, the result will be printed on the command line.
