#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ========================================================================================
# 
# Copyright 2023 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import os
import sys
import types
import typing
import shutil
import importlib

BIN_DIR		         = os.path.dirname(os.path.realpath(__file__))
PROJECT_DIR	         = os.path.realpath(os.path.join(BIN_DIR, '../'))
CONFIG_DIR	         = os.path.realpath(os.path.join(PROJECT_DIR, 'etc'))
MODULES_DIR	         = os.path.realpath(os.path.join(PROJECT_DIR, 'modules'))
LOG_DIR		         = os.path.realpath(os.path.join(PROJECT_DIR, 'var/log'))
WORK_DIR	         = os.path.realpath(os.path.join(PROJECT_DIR, 'var/run'))
TMP_DIR	             = os.path.realpath(os.path.join(PROJECT_DIR, 'var/tmp'))

SHARED_LIB_DIR   = os.path.realpath(os.path.join(PROJECT_DIR, '../shared-lib'))
SHARED_MODULES_DIR   = os.path.realpath(os.path.join(SHARED_LIB_DIR, 'modules'))

sys.path.insert(0, MODULES_DIR)
if os.path.isdir(SHARED_MODULES_DIR):
    sys.path.insert(0, SHARED_MODULES_DIR)

if os.path.isdir('/var/lib/transposer/modules'):
    sys.path.insert(0, '/var/lib/transposer/modules')

user_lib_path = os.path.expanduser('~/.local/lib/transposer/modules')
if os.path.isdir(user_lib_path):
    sys.path.insert(0, user_lib_path)


import logging
import getopt

import re
import json

from websockets.sync.client import connect

from dotenv import load_dotenv

from utils import (
    RE_TRUE,
    RE_FALSE,
    RE_NONE,
    RE_NEGATED,
    RE_NUMBER,
    RE_INT,
    RE_FLOAT,
    RE_PRIMITIVE,
    RE_FRACTION,
    RE_SPLIT_FRACTION,
    RE_JSON_OBJECT_START,
    RE_JSON_OBJECT_END,
    RE_JSON_ARRAY_START,
    RE_JSON_ARRAY_END,
    RE_COMMA_SEPARATOR,
    RE_COMMA_TRAILING,
    RE_NEWLINE_SINGLE,
    RE_NEWLINE_MULTI,
    RE_WHITESPACE,
    RE_WHITESPACE_START,
    RE_WHITESPACE_END,
    RE_STARTSWITH_SLASH,
    RE_STARTSWITH_TILDE,
    
    get_uuid,
    is_true,
    to_int
)



class RpcClient:
    def __init__(self, host:str = 'localhost', port:int = 8080) -> None:
        self._sock = None
        self._address = (host, port)
    
    def connect(self):
        try:
            self._sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self._sock.connect(self._address)
        except EOFError as err:
            print(err)
            raise Exception('Client was not able to connect.')
    
    def disconnect(self):
        try:
            self._sock.close()
        except:
            pass





### Help
def show_help():
    print("""Usage: rpc_ws_client.py [OPTIONS]

    --help             Print this help
-h  --host             Hostname or IP address
-p  --port             Port number
""")
    sys.exit(0)





# Get CLI options
getopt_types = {
    '--help': {
        'name': 'help',
        'convert': lambda val: True
    },
    
    '-h': {
        'name': 'host',
        'convert': lambda val: val if isinstance(val, str) else None
    },
    '--host': {
        'name': 'host',
        'convert': lambda val: val if isinstance(val, str) else None
    },
    
    '-p': {
        'name': 'port',
        'convert': lambda val: to_int(val) if isinstance(val, (str, int)) else None
    },
    '--port': {
        'name': 'port',
        'convert': lambda val: to_int(val) if isinstance(val, (str, int)) else None
    }
}

try:
    opts, args = getopt.getopt(sys.argv[1:], 'h:p:', [
        'help',
        'host=',
        'port='
    ])
except getopt.GetoptError:
    show_help()

opts = { getopt_types[x[0]]['name']:getopt_types[x[0]]['convert'](x[1]) for x in opts };
if 'help' in opts:
    show_help()
if 'host' not in opts or 'port' not in opts:
    show_help()





with connect(f"ws://{opts['host']}:{opts['port']}/wsrpc") as websocket:
    # Send text message
    # Expect error
    print(f"Send text message (Expect error) ...")
    websocket.send("Hello world!")
    message = websocket.recv()
    print(f"Received: {message}")
    
    # Send JSON message to echo method
    print(f"Send JSON message to echo method ...")
    websocket.send(json.dumps({
        'method': 'echo',
        'args': ['Hello world!']
    }))
    message = websocket.recv()
    print(f"Received: {message}")
    
    # Request constant
    print(f"Request constant ...")
    websocket.send(json.dumps({
        'id': 'hello'
    }))
    message = websocket.recv()
    print(f"Received: {message}")


