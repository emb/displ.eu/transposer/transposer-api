from typing import Any, List

from typing_extensions import Annotated

from pydantic import BaseModel, ValidationError
from pydantic.functional_validators import BeforeValidator, AfterValidator

import re


RE_INT = re.compile(r'^(\d+)$', re.I)
RE_FLOAT = re.compile(r'^(\d+(?:\.\d+)?)$', re.I)
RE_MMSSMS = re.compile(r'^([0-9]{2}):([0-9]{2})\.([0-9]{3})$', re.I)
RE_HHMMSSMS = re.compile(r'^([0-9]{2}):([0-9]{2}):([0-9]{2})\.([0-9]{3})$', re.I)


def check_squares(v: int) -> int:
    assert v**0.5 % 1 == 0, f'{v} is not a square number'
    return v


def double(v: Any) -> Any:
    return v * 2


def time_to_int(v: Any) -> Any:
    if isinstance(v, int):
        return v
    if isinstance(v, float):
        return int(v)
    if not isinstance(v, str):
        raise AssertionError('No convertible value')
    v = v.strip()

    result = RE_INT.match(v)
    if result:
        return int(result.group(1))
    result = RE_FLOAT.match(v)
    if result:
        return int(float(result.group(1)))

    result = RE_HHMMSSMS.match(v)
    if result:
        return (
            (int(result.group(1)) * 3600000) + 
            (int(result.group(2)) * 60000) + 
            (int(result.group(3)) * 1000) + 
            int(result.group(4))
        )
    result = RE_MMSSMS.match(v)
    if result:
        return (
            (int(result.group(1)) * 60000) + 
            (int(result.group(2)) * 1000) + 
            int(result.group(3))
        )
    raise AssertionError('Invalid value')


MyNumber = Annotated[int, AfterValidator(double), AfterValidator(check_squares)]

MyTime = Annotated[int, BeforeValidator(time_to_int)]


class DemoModel(BaseModel):
    number: List[MyNumber]


class Type1Model(BaseModel):
    value1: str
    value2: MyTime
    value3: str

class Type2Model(BaseModel):
    host: str
    port: int
    label: str


val1 = {
    'value1': 'hello',
    'value2': '01:00.234',
    'value3': 'world'
}
val2 = {
    'host': 'hello',
    'port': '10',
    'label': 'world'
}


print('test val1: test 1')
try:
    result = Type1Model(**val1)
    print('result:', result)
except ValidationError as e:
    print(e)


print('test val1: test 2')
try:
    result = Type2Model(**val1)
    print('result:', result)
except ValidationError as e:
    print(e)


print('test val2: test 1')
try:
    result = Type1Model(**val2)
    print('result:', result)
except ValidationError as e:
    print(e)


print('test val2: test 2')
try:
    result = Type2Model(**val2)
    print('result:', result)
except ValidationError as e:
    print(e)



