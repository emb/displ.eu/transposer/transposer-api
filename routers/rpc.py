# ========================================================================================
# 
# Copyright 2023 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import os
import sys
import types
import typing

import logging

import re
import json

import asyncio
from fastapi import (
    APIRouter,
    Cookie,
    Depends,
    FastAPI,
    HTTPException,
    Query,
    WebSocket,
    WebSocketException,
    WebSocketDisconnect,
    status
)
from fastapi.security import OAuth2PasswordBearer
from fastapi.responses import HTMLResponse
from fastapi.templating import Jinja2Templates

#from fastapi_third_party_auth import KeycloakIDToken

from pydantic import BaseModel

from rpc_server import (
    RpcServer,
    RpcRequestItem,
    RpcDataRequestItem,
    RpcResponseItem
)

from apiutils import (
    setup_fastapi_jinja_templates
)
import trp_config



# Constants
ROUTER_PATH_PREFIX = '/rpc'
ROUTER_TAGS = [ 'rpc' ]
TEMPLATES_PATH_SUFFIX = 'rpc'
TEMPLATES_PATH_BASE = 'templates/html'



# Connection manager for websocket connections
class ConnectionManager:
    def __init__(self):
        self.active_connections: list[WebSocket] = []

    async def connect(self, websocket: WebSocket):
        await websocket.accept()
        self.active_connections.append(websocket)

    def disconnect(self, websocket: WebSocket):
        try:
            self.active_connections.remove(websocket)
        except ValueError:
            pass

    async def send_message(self, message: str, websocket: WebSocket):
        await websocket.send_text(message)

    async def broadcast(self, message: str):
        for connection in self.active_connections:
            await connection.send_text(message)



# Data container
class MData:
    router: APIRouter
    app: typing.Any
    config: trp_config.TrpConfig | None = None
    server: typing.Any = None
    auth: typing.Any = None
    templates: Jinja2Templates | None = None
    connection_manager: ConnectionManager | None = None
    
    @classmethod
    def init(
        cls,
        app: typing.Any,
        config: trp_config.TrpConfig,
        auth: typing.Any = None
    ) -> None:
        """Initialize module
        
            Args:
                app (FastAPI): FastAPI app
                config (trp_config.TrpConfig): Methods paths
                auth (typing.Any): Auth
        """
        cls.app = app
        cls.config = config
        cls.auth = auth
        cls.connection_manager = ConnectionManager()
        
        cls.server = None
        dirs = config.get('appdirs').get('methods_dirs')
        if isinstance(dirs, list):
            cls.server = RpcServer(dirs)
        
        setup_fastapi_jinja_templates(
            cls,
            TEMPLATES_PATH_SUFFIX,
            TEMPLATES_PATH_BASE
        )
        
        
        ### Routes Handling ###

        # Get constant
        async def rpc_get(cls, id) -> RpcResponseItem:
            logging.debug("'/rpc' GET called")
            if not cls.server:
                raise HTTPException(status_code=500, detail="RPC Server not initialized")
            return await cls.server.get(id)
        cls.rpc_get = classmethod(rpc_get)


        # Call method
        async def rpc_call(cls, item: RpcRequestItem) -> RpcResponseItem:
            logging.debug("'/rpc' POST called")
            if not cls.server:
                raise HTTPException(status_code=500, detail="RPC Server not initialized")
            return await cls.server.call(item)
        cls.rpc_call = classmethod(rpc_call)


        # Communicate via websocket
        async def websocket_rpc_endpoint(cls, websocket: WebSocket):
            await cls.connection_manager.connect(websocket)
            try:
                while True:
                    data = await websocket.receive_text()
                    if not data:
                        continue
                    if data == 'close':
                        await websocket.close()
                        cls.connection_manager.disconnect(websocket)
                        break
                    
                    try:
                        data = json.loads(data)
                        
                    except Exception as err:
                        err_msg = json.dumps(str(err))
                        logging.error(err_msg)
                        await websocket.send_json(RpcResponseItem(
                            success=False,
                            message=err_msg
                        ).model_dump())
                        continue
                    
                    is_method_request = False
                    try:
                        item = RpcRequestItem(**data)
                        is_method_request = True
                    except:
                        try:
                            item = RpcDataRequestItem(**data)
                        except:
                            err_msg = 'Invalid request data structure'
                            logging.error(err_msg)
                            await websocket.send_json(RpcResponseItem(
                                success=False,
                                message=err_msg
                            ).model_dump())
                            continue
                    
                    if is_method_request:
                        if not cls.server:
                            err_msg = 'RPC Server not initialized'
                            logging.error(err_msg)
                            await websocket.send_json(RpcResponseItem(
                                success=False,
                                message=err_msg
                            ).model_dump())
                            continue
                        
                        try:
                            result = await cls.server.call(item)
                            
                        except Exception as err:
                            err_msg = json.dumps(str(err))
                            logging.error(err_msg)
                            await websocket.send_json(RpcResponseItem(
                                success=False,
                                message=err_msg
                            ).model_dump())
                            continue
                        
                        if isinstance(result, RpcResponseItem):
                            await websocket.send_json(result.model_dump())
                            continue
                        
                        if result is None:
                            await websocket.send_json(RpcResponseItem(
                                success=True
                            ).model_dump())
                            continue
                        
                        try:
                            await websocket.send_json(RpcResponseItem(
                                success=True,
                                payload=result
                            ).model_dump())
                            continue
                        
                        except Exception as err:
                            err_msg = json.dumps(str(err))
                            logging.error(err_msg)
                            await websocket.send_json(RpcResponseItem(
                                success=False,
                                message=err_msg
                            ).model_dump())
                            continue
                    
                    if not cls.server:
                        err_msg = 'RPC Server not initialized'
                        logging.error(err_msg)
                        await websocket.send_json(RpcResponseItem(
                            success=False,
                            message=err_msg
                        ).model_dump())
                        continue
                    
                    try:
                        result = await cls.server.get(item.id)
                    except Exception as err:
                        err_msg = json.dumps(str(err))
                        logging.error(err_msg)
                        await websocket.send_json(RpcResponseItem(
                            success=False,
                            message=err_msg
                        ).model_dump())
                        continue
                    
                    if isinstance(result, RpcResponseItem):
                        await websocket.send_json(result.model_dump())
                        continue
                    
                    if result is None:
                        await websocket.send_json(RpcResponseItem(
                            success=True
                        ).model_dump())
                        continue
                    
                    try:
                        await websocket.send_json(RpcResponseItem(
                            success=True,
                            payload=result
                        ).model_dump())
                        continue
                    
                    except Exception as err:
                        err_msg = json.dumps(str(err))
                        logging.error(err_msg)
                        await websocket.send_json(RpcResponseItem(
                            success=False,
                            message=err_msg
                        ).model_dump())
                        continue
            
            except WebSocketDisconnect:
                cls.connection_manager.disconnect(websocket)
        cls.websocket_rpc_endpoint = classmethod(websocket_rpc_endpoint)
        
        
        ### Setup router ###
        cls.router = APIRouter(
            prefix=ROUTER_PATH_PREFIX,
            tags=ROUTER_TAGS,
            responses={
                404: { 'description': 'Not found' }
            }
        )
        
        
        ### Add routes ###
        cls.router.add_api_route('/{id}',   cls.rpc_get,                methods=['GET'])
        cls.router.add_api_route('/',       cls.rpc_call,               methods=['POST'])
        cls.router.add_api_route('/ws',     cls.websocket_rpc_endpoint, methods=['WEBSOCKET'])



### Init ###

def init(
    app: typing.Any,
    config: trp_config.TrpConfig,
    auth: typing.Any = None
) -> None:
    """Initialize module
    
        Args:
            app (FastAPI): FastAPI app
            config (trp_config.TrpConfig): Methods paths
            auth (typing.Any): Auth
    """
    MData.init(
        app,
        config,
        auth
    )


def get_router() -> APIRouter:
    return MData.router



### Testing ###

if __name__ == '__main__':
    init(
        trp_config.TrpConfig(),
        None
    )

