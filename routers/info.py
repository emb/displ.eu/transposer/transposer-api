# ========================================================================================
# 
# Copyright 2023 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import os
import sys
import types
import typing

import logging

from fastapi import (
    FastAPI,
    APIRouter,
    HTTPException
)
from fastapi.security import OAuth2PasswordBearer
from fastapi.templating import Jinja2Templates

#from fastapi_third_party_auth import KeycloakIDToken

from pydantic import BaseModel

from apiutils import (
    setup_fastapi_jinja_templates
)
import trp_config



# Constants
ROUTER_PATH_PREFIX = '/info'
ROUTER_TAGS = [ 'info' ]
TEMPLATES_PATH_SUFFIX = 'info'
TEMPLATES_PATH_BASE = 'templates/html'



# API classes
class GeneralInfoItem(BaseModel):
    success: bool = True
    payload: str = 'This is the Transposer API. Please refer to the documentation for further information.'

class EndpointsItem(BaseModel):
    success: bool = True
    payload: list[dict[str, str]] = [
        {
            'endpoint': '/info',
            'description': 'General information',
            'method': 'GET'
        },
        {
            'endpoint': '/info/endpoints',
            'description': 'List of endpoints',
            'method': 'GET'
        },
        {
            'endpoint': '/cluster/brokers',
            'description': 'List of active brokers',
            'method': 'GET'
        },
        {
            'endpoint': '/cluster/active',
            'description': 'Flag indicating if cluster is active',
            'method': 'GET'
        },
        {
            'endpoint': r'/rpc/{id}',
            'description': 'Get constant value',
            'method': 'GET'
        },
        {
            'endpoint': '/rpc',
            'description': 'Call method',
            'method': 'POST'
        },
        {
            'endpoint': '/rpc/ws',
            'description': 'Call method over websocket',
            'method': 'WEBSOCKET'
        }
    ]



# Data container
class MData:
    router: APIRouter
    app: typing.Any
    config: trp_config.TrpConfig | None = None
    server: typing.Any = None
    auth: typing.Any = None
    templates: Jinja2Templates | None = None
    
    @classmethod
    def init(
        cls,
        app: typing.Any,
        config: trp_config.TrpConfig,
        auth: typing.Any = None
    ) -> None:
        """Initialize module
        
            Args:
                app (FastAPI): FastAPI app
                config (trp_config.TrpConfig): Methods paths
                auth (typing.Any): Auth
        """
        cls.app = app
        cls.config = config
        cls.auth = auth
        cls.server = None
        
        setup_fastapi_jinja_templates(
            cls,
            TEMPLATES_PATH_SUFFIX,
            TEMPLATES_PATH_BASE
        )
        
        
        ### Routes Handling ###

        # Get default page
        async def get_general_info(cls) -> GeneralInfoItem:
            logging.debug("'/info' called")
            return GeneralInfoItem()
        cls.get_general_info = classmethod(get_general_info)


        # Get endpoints
        async def get_endpoints(cls) -> EndpointsItem:
            logging.debug("'/info/endpoints' called")
            return EndpointsItem()
        cls.get_endpoints = classmethod(get_endpoints)
        
        
        ### Setup router ###
        cls.router = APIRouter(
            prefix=ROUTER_PATH_PREFIX,
            tags=ROUTER_TAGS,
            responses={
                404: { 'description': 'Not found' }
            }
        )
        
        
        ### Add routes ###
        cls.router.add_api_route('/',           cls.get_general_info,   methods=['GET'])
        cls.router.add_api_route('/endpoints',  cls.get_endpoints,      methods=['GET'])



### Init ###

def init(
    app: typing.Any,
    config: trp_config.TrpConfig,
    auth: typing.Any = None
) -> None:
    """Initialize module
    
        Args:
            app (FastAPI): FastAPI app
            config (trp_config.TrpConfig): Methods paths
            auth (typing.Any): Auth
    """
    MData.init(
        app,
        config,
        auth
    )


def get_router() -> APIRouter:
    return MData.router



### Testing ###

if __name__ == '__main__':
    init(
        trp_config.TrpConfig(),
        None
    )

