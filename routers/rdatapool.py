# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import os
import typing
import logging
import re
import json

from fastapi import (
    APIRouter,
    Request
)
from fastapi.security import (
    OAuth2PasswordBearer,
    OAuth2PasswordRequestForm
)
from fastapi.responses import HTMLResponse
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates

from pydantic import BaseModel

import sqlalchemy

from apiutils import (
    setup_fastapi_jinja_templates
)
import trp_config

from datapool.routers import (
    AccessTokenRouter,
    BearerTokenRouter,
    CaptionRouter,
    ContentRouter,
    ContentOptionRouter,
    DocumentRouter,
    GroupRouter,
    JobRouter,
    MediaRouter,
    PeerRouter,
    PeerAccessTokenRouter,
    PeerBearerTokenRouter,
    PeerConnectionRouter,
    PeerConnectionPathRouter,
    PermissionRouter,
    PipelineConnectorRouter,
    PipelineConsumerRouter,
    PipelineConsumerOptionRouter,
    PipelineProducerRouter,
    PipelineProducerOptionRouter,
    PipelineServerRouter,
    PipelineRouter,
    PipelineOptionRouter,
    RequestRouter,
    RoleRouter,
    SettingRouter,
    TranscriptionRouter,
    TranscriptionLanguageProbRouter,
    TranscriptionSegmentRouter,
    TranscriptionSpeechActivityRouter,
    TranscriptionWordRouter,
    UserRouter,
    WorkflowStepRouter,
    WorkflowTemplateRouter,
    WorkflowRouter
)

from datapool.pipeline import (
    PipelineConsumerManager
)



# Constants
ROUTER_PATH_PREFIX = '/datapool'
ROUTER_TAGS = [ 'datapool' ]
TEMPLATES_PATH_SUFFIX = 'datapool'
TEMPLATES_PATH_BASE = 'templates/html'



RE_CSV = re.compile(r'\.csv$', re.I)
RE_SPLIT_TO_LIST = re.compile(r'\s*,\s*', re.I)
RE_VALUE_LT = re.compile(r'^\s*<\s*(\d+)\s*$', re.I)
RE_VALUE_GT = re.compile(r'^\s*(\d+)\s*>\s*$', re.I)
RE_VALUE_BT = re.compile(r'^\s*(\d+)\s*>\s*(\d+)\s*$', re.I)



OAUTH2_SCHEME = OAuth2PasswordBearer(tokenUrl='token')





# Add config files to container
def add_config_files(container: list, dir: str, name: str) -> None:
    if not os.path.isdir(dir):
        return
    file_path = os.path.join(dir, f'{name}.defaults.ini')
    if os.path.isfile(file_path):
        container.append(file_path)
    file_path = os.path.join(dir, f'{name}.ini')
    if os.path.isfile(file_path):
        container.append(file_path)





###
# Data container
###

class MData:
    router: APIRouter
    app: typing.Any
    config: trp_config.TrpConfig | None = None
    server: typing.Any = None
    auth: typing.Any = None
    templates: Jinja2Templates | None = None
    
    @classmethod
    def init(
        cls,
        app: typing.Any,
        config: trp_config.TrpConfig,
        auth: typing.Any = None
    ) -> None:
        """Initialize module
        
            Args:
                app (FastAPI): FastAPI app
                config (trp_config.TrpConfig): Methods paths
                auth (typing.Any): Auth
        """
        cls.app = app
        cls.config = config
        cls.auth = auth
        cls.server = None

        # Load datapool config
        app_dirs = config.get('appdirs')
        config_dirs = app_dirs.get('config_dirs')
        app_dirs = app_dirs.options
        app_dirs['methods_dirs'] = json.dumps(app_dirs['methods_dirs'])
        app_dirs['routers_dirs'] = json.dumps(app_dirs['routers_dirs'])
        app_dirs['config_dirs'] = json.dumps(app_dirs['config_dirs'])

        #print('app_dirs:', app_dirs)
        #print('config_dirs:', config_dirs)

        datapool_config_files = []
        pipeline_config_files = []
        #producer_config_files = []
        #consumer_config_files = []
        for d in config_dirs:
            add_config_files(datapool_config_files, d, 'config')
            add_config_files(datapool_config_files, d, 'datapool')
            add_config_files(pipeline_config_files, d, 'pipeline')
            #add_config_files(producer_config_files, d, 'pipeline_producer')
            #add_config_files(consumer_config_files, d, 'pipeline_consumer')
        pipeline_config_files = datapool_config_files + pipeline_config_files
        #producer_config_files = pipeline_config_files + producer_config_files
        #consumer_config_files = pipeline_config_files + consumer_config_files

        #print('datapool_config_files:', datapool_config_files)

        # datapool config
        datapool_config = trp_config.TrpConfig({
            'defaults': {
                'dir': app_dirs['config'],
                'file': 'defaults.ini',
                'data': {
                    'appdirs': app_dirs
                }
            },
            'files': datapool_config_files
        }, True, 'TRP_API_DATAPOOL')

        # pipeline config
        pipeline_config = trp_config.TrpConfig({
            'defaults': {
                'dir': app_dirs['config'],
                'file': 'defaults.ini',
                'data': {
                    'appdirs': app_dirs
                }
            },
            'files': pipeline_config_files
        }, True, ['TRP_API_DATAPOOL', 'TRP_API_PIPELINE'])

        # # pipeline producer config
        # producer_config = trp_config.TrpConfig({
        #     'defaults': {
        #         'dir': app_dirs['config'],
        #         'file': 'defaults.ini',
        #         'data': {
        #             'appdirs': app_dirs
        #         }
        #     },
        #     'files': producer_config_files
        # }, True, ['TRP_API_DATAPOOL', 'TRP_API_PIPELINE', 'TRP_API_PIPELINE_PRODUCER'])

        # # pipeline consumer config
        # consumer_config = trp_config.TrpConfig({
        #     'defaults': {
        #         'dir': app_dirs['config'],
        #         'file': 'defaults.ini',
        #         'data': {
        #             'appdirs': app_dirs
        #         }
        #     },
        #     'files': consumer_config_files
        # }, True, ['TRP_API_DATAPOOL', 'TRP_API_PIPELINE', 'TRP_API_PIPELINE_CONSUMER'])



        # print('datapool_config:', datapool_config)
        # print('pipeline_config:', pipeline_config)
        # print('producer_config:', producer_config)
        # print('consumer_config:', consumer_config)

        # print('consumer_config appdirs:', consumer_config.get('appdirs').options)
        # print('consumer_config datapool:', consumer_config.get('datapool').options)
        # print('datapool_config database:', datapool_config.get('database').options)
        # print('consumer_config database:', consumer_config.get('database').options)


        print('datapool_config something:', datapool_config.get('something').options)
        print('pipeline_config something_else:', pipeline_config.get('something_else').options)
        print('pipeline_config - consumer - value:', pipeline_config.get('consumer').options)


        # if not config.has('datapool'):
        #     raise ValueError('Missing "datapool" section in config')
        # if not config.get('datapool').has('db_host'):
        #     raise ValueError('Missing "datapool.db_host" in config')
        # if not config.get('datapool').has('db_name'):
        #     raise ValueError('Missing "datapool.db_name" in config')
        # if not config.get('datapool').has('db_user'):
        #     raise ValueError('Missing "datapool.db_user" in config')
        # if not config.get('datapool').has('db_password'):
        #     raise ValueError('Missing "datapool.db_password" in config')

        # cls.db_engine = sqlalchemy.create_engine(
        #     f"postgresql://{config.get('datapool').get('db_user')}:{config.get('datapool').get('db_password')}@{config.get('datapool').get('db_host')}/{config.get('datapool').get('db_name')}",
        #     echo=True
        # )

        if not datapool_config.has('database'):
            raise ValueError('Missing "database" section in datapool config')
        if not datapool_config.get('database').has('host'):
            raise ValueError('Missing "database.host" in datapool config')
        if not datapool_config.get('database').has('name'):
            raise ValueError('Missing "database.name" in datapool config')
        if not datapool_config.get('database').has('user'):
            raise ValueError('Missing "database.user" in datapool config')
        if not datapool_config.get('database').has('password'):
            raise ValueError('Missing "database.password" in datapool config')

        cls.db_engine = sqlalchemy.create_engine(
            f"postgresql://{datapool_config.get('database').get('user')}:{datapool_config.get('database').get('password')}@{datapool_config.get('database').get('host')}/{datapool_config.get('database').get('name')}",
            echo=True
        )

        setup_fastapi_jinja_templates(
            cls,
            TEMPLATES_PATH_SUFFIX,
            TEMPLATES_PATH_BASE
        )


        
        ###### Routes Handler ######

        # Get index page
        async def get_index_page(
            cls,
            request: Request#,
            #token: typing.Annotated[str, Depends(OAUTH2_SCHEME)]
        ):
            return cls.templates.TemplateResponse('index.html', {
                'request': request
            })
        cls.get_index_page = classmethod(get_index_page)


        ### Setup router ###
        cls.router = APIRouter(
            prefix=ROUTER_PATH_PREFIX,
            tags=ROUTER_TAGS,
            responses={
                404: { 'success': False, 'description': 'Not found' }
            }
        )

        ### Add sub-routers ###
        UserRouter(cls.db_engine, cls.app, cls.router, datapool_config, OAUTH2_SCHEME)
        GroupRouter(cls.db_engine, cls.app, cls.router, datapool_config, OAUTH2_SCHEME)
        RoleRouter(cls.db_engine, cls.app, cls.router, datapool_config, OAUTH2_SCHEME)
        PermissionRouter(cls.db_engine, cls.app, cls.router, datapool_config, OAUTH2_SCHEME)
        
        AccessTokenRouter(cls.db_engine, cls.app, cls.router, datapool_config, OAUTH2_SCHEME)
        BearerTokenRouter(cls.db_engine, cls.app, cls.router, datapool_config, OAUTH2_SCHEME)
        
        JobRouter(cls.db_engine, cls.app, cls.router, datapool_config, OAUTH2_SCHEME)
        RequestRouter(cls.db_engine, cls.app, cls.router, datapool_config, OAUTH2_SCHEME)

        CaptionRouter(cls.db_engine, cls.app, cls.router, datapool_config, OAUTH2_SCHEME)
        ContentOptionRouter(cls.db_engine, cls.app, cls.router, datapool_config, OAUTH2_SCHEME)
        ContentRouter(cls.db_engine, cls.app, cls.router, datapool_config, OAUTH2_SCHEME)
        DocumentRouter(cls.db_engine, cls.app, cls.router, datapool_config, OAUTH2_SCHEME)
        MediaRouter(cls.db_engine, cls.app, cls.router, datapool_config, OAUTH2_SCHEME)
        
        PeerAccessTokenRouter(cls.db_engine, cls.app, cls.router, datapool_config, OAUTH2_SCHEME)
        PeerBearerTokenRouter(cls.db_engine, cls.app, cls.router, datapool_config, OAUTH2_SCHEME)
        PeerConnectionPathRouter(cls.db_engine, cls.app, cls.router, datapool_config, OAUTH2_SCHEME)
        PeerConnectionRouter(cls.db_engine, cls.app, cls.router, datapool_config, OAUTH2_SCHEME)
        PeerRouter(cls.db_engine, cls.app, cls.router, datapool_config, OAUTH2_SCHEME)
        
        TranscriptionLanguageProbRouter(cls.db_engine, cls.app, cls.router, datapool_config, OAUTH2_SCHEME)
        TranscriptionSegmentRouter(cls.db_engine, cls.app, cls.router, datapool_config, OAUTH2_SCHEME)
        TranscriptionSpeechActivityRouter(cls.db_engine, cls.app, cls.router, datapool_config, OAUTH2_SCHEME)
        TranscriptionWordRouter(cls.db_engine, cls.app, cls.router, datapool_config, OAUTH2_SCHEME)
        TranscriptionRouter(cls.db_engine, cls.app, cls.router, datapool_config, OAUTH2_SCHEME)
        
        SettingRouter(cls.db_engine, cls.app, cls.router, datapool_config, OAUTH2_SCHEME)

        PipelineConsumerOptionRouter(cls.db_engine, cls.app, cls.router, pipeline_config, OAUTH2_SCHEME)
        PipelineProducerOptionRouter(cls.db_engine, cls.app, cls.router, pipeline_config, OAUTH2_SCHEME)
        PipelineOptionRouter(cls.db_engine, cls.app, cls.router, pipeline_config, OAUTH2_SCHEME)

        PipelineConnectorRouter(cls.db_engine, cls.app, cls.router, pipeline_config, OAUTH2_SCHEME)
        PipelineConsumerRouter(cls.db_engine, cls.app, cls.router, pipeline_config, OAUTH2_SCHEME)
        PipelineProducerRouter(cls.db_engine, cls.app, cls.router, pipeline_config, OAUTH2_SCHEME)
        PipelineServerRouter(cls.db_engine, cls.app, cls.router, pipeline_config, OAUTH2_SCHEME)
        PipelineRouter(cls.db_engine, cls.app, cls.router, pipeline_config, OAUTH2_SCHEME)
        
        WorkflowTemplateRouter(cls.db_engine, cls.app, cls.router, pipeline_config, OAUTH2_SCHEME)
        WorkflowStepRouter(cls.db_engine, cls.app, cls.router, pipeline_config, OAUTH2_SCHEME)
        WorkflowRouter(cls.db_engine, cls.app, cls.router, pipeline_config, OAUTH2_SCHEME)





### Init ###

def init(
    app: typing.Any,
    config: trp_config.TrpConfig,
    auth: typing.Any = None
) -> None:
    """Initialize module
    
        Args:
            app (FastAPI): FastAPI app
            config (trp_config.TrpConfig): Methods paths
            auth (typing.Any): Auth
    """
    MData.init(
        app,
        config,
        auth
    )


def get_router() -> APIRouter:
    return MData.router





### Testing ###

if __name__ == '__main__':
    init(
        trp_config.TrpConfig(),
        None
    )


