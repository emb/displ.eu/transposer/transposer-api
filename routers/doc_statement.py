# ========================================================================================
# 
# Copyright 2023 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import os
import sys
import types
import typing

import logging

import re
import json
import csv
import random

import asyncio
import aiofiles

from fastapi import (
    APIRouter,
    Cookie,
    Depends,
    FastAPI,
    File,
    HTTPException,
    Query,
    Request,
    UploadFile,
    WebSocket,
    WebSocketException,
    WebSocketDisconnect,
    status
)
from fastapi.security import OAuth2PasswordBearer
from fastapi.responses import HTMLResponse
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates

#from fastapi_third_party_auth import KeycloakIDToken

from pydantic import BaseModel

from utils import (
    RE_TRUE,
    RE_FALSE,
    RE_NONE,
    RE_NEGATED,
    RE_NUMBER,
    RE_INT,
    RE_FLOAT,
    RE_PRIMITIVE,
    RE_FRACTION,
    RE_SPLIT_FRACTION,
    RE_JSON_OBJECT_START,
    RE_JSON_OBJECT_END,
    RE_JSON_ARRAY_START,
    RE_JSON_ARRAY_END,
    RE_COMMA_SEPARATOR,
    RE_COMMA_TRAILING,
    RE_NEWLINE_SINGLE,
    RE_NEWLINE_MULTI,
    RE_WHITESPACE,
    RE_WHITESPACE_START,
    RE_WHITESPACE_END,
    
    UTILS_FUNCTION_MAP,
    
    to_int,
    to_float,
    to_abs,
    to_list,
    to_msecs,
    frames_to_msecs,
    str_is_empty,
    to_date,
    to_time,
    to_datetime,
    date_to_str,
    time_to_str,
    datetime_to_str
)
from apiutils import (
    setup_fastapi_jinja_templates
)
import trp_config
import statement_transformer





# Constants
ROUTER_PATH_PREFIX = '/doc/statement'
ROUTER_TAGS = [ 'doc', 'statement' ]
TEMPLATES_PATH_SUFFIX = 'doc/statement'
TEMPLATES_PATH_BASE = 'templates/html'



RE_CSV = re.compile(r'\.csv$', re.I)



# API classes
class DocRequestItem(BaseModel):
    method: str
    args: list | None = None
    kwargs: dict | None = None

class DocResponseItem(BaseModel):
    success: bool = False
    message: str | None = None
    payload: typing.Any = None



# Data container
class MData:
    router: APIRouter
    app: typing.Any
    config: trp_config.TrpConfig | None = None
    server: typing.Any = None
    auth: typing.Any = None
    templates: Jinja2Templates | None = None
    data_dir: str = 'var/tmp'
    config_dir: str = 'etc'
    transformer: statement_transformer.StatementTransformer | None = None
    
    @classmethod
    def init(
        cls,
        app: typing.Any,
        config: trp_config.TrpConfig,
        auth: typing.Any = None
    ) -> None:
        """Initialize module
        
            Args:
                app (FastAPI): FastAPI app
                config (trp_config.TrpConfig): Methods paths
                auth (typing.Any): Auth
        """
        cls.app = app
        cls.config = config
        cls.auth = auth
        cls.server = None
        
        setup_fastapi_jinja_templates(
            cls,
            TEMPLATES_PATH_SUFFIX,
            TEMPLATES_PATH_BASE
        )
        
        if config.has('doc_statement'):
            cls.data_dir = config.get('doc_statement').get('data_dir', 'var/tmp')
            cls.config_dir = config.get('doc_statement').get('config_dir', 'etc')
        
        if cls.data_dir.startswith('~'):
            cls.data_dir = os.path.expanduser(cls.data_dir)
        if not cls.data_dir.startswith('/'):
            cls.data_dir = os.path.join(
                config.get('appdirs').get('project'),
                cls.data_dir
            )
        
        if cls.config_dir.startswith('~'):
            cls.config_dir = os.path.expanduser(cls.config_dir)
        if not cls.config_dir.startswith('/'):
            cls.config_dir = os.path.join(
                config.get('appdirs').get('project'),
                cls.config_dir
            )
        
        print('data_dir:', cls.data_dir)
        print('config_dir:', cls.config_dir)
        
        cls.transformer = statement_transformer.StatementTransformer(
            cls.config_dir
        )
        
        print('transformer:', cls.transformer)
        
        
        ### Routes Handler ###

        # Get upload page
        async def get_upload_page(
            cls,
            request: Request#,
            #token: typing.Annotated[str, Depends(auth)]
        ):
            return cls.templates.TemplateResponse('index.html', {
                'request': request
            })
        cls.get_upload_page = classmethod(get_upload_page)
        

        # Transform document
        async def transform_statement(
            cls,
            files: typing.Annotated[
                list[UploadFile], File(...)
            ]
        ) -> DocResponseItem:
            logging.debug("'/transform' POST called")
            if not files:
                return DocResponseItem(
                    success=False,
                    message='File missing'
                )
            status = []
            for file in files:
                print('file:', file.filename)
                if not RE_CSV.search(file.filename):
                    status.append({
                        'filename': file.filename,
                        'success': False,
                        'message': 'Only CSV files are supported'
                    })
                    continue
                
                fname = os.path.join(cls.data_dir, file.filename)
                async with aiofiles.open(fname, 'wb') as out_file:
                    while content := await file.read(1024):  # async read file chunk
                        await out_file.write(content)  # async write file chunk
                status.append({
                    'filename': file.filename,
                    #'success': False if random.random() < 0.5 else True,
                    'success': True,
                    'message': 'File uploaded'
                })
            return DocResponseItem(
                success=True,
                payload=status
            )
        cls.transform_statement = classmethod(transform_statement)


        ### Setup router ###
        cls.router = APIRouter(
            prefix=ROUTER_PATH_PREFIX,
            tags=ROUTER_TAGS,
            responses={
                404: { 'description': 'Not found' }
            }
        )
        
        
        ### Add routes ###
        cls.router.add_api_route('/',           cls.get_upload_page,        methods=['GET'])
        cls.router.add_api_route('/transform',  cls.transform_statement,    methods=['POST'])
        
        print('doc_statement ... tmp:', config.get('doc_statement').get('tmp'))



### Init ###

def init(
    app: typing.Any,
    config: trp_config.TrpConfig,
    auth: typing.Any = None
) -> None:
    """Initialize module
    
        Args:
            app (FastAPI): FastAPI app
            config (trp_config.TrpConfig): Methods paths
            auth (typing.Any): Auth
    """
    MData.init(
        app,
        config,
        auth
    )


def get_router() -> APIRouter:
    return MData.router





### Testing ###

if __name__ == '__main__':
    init(
        trp_config.TrpConfig(),
        None
    )

