# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import os
import sys

if __name__ == '__main__':
    BIN_DIR		         = os.path.dirname(os.path.realpath(__file__))
    PROJECT_DIR	         = os.path.realpath(os.path.join(BIN_DIR, '../'))
    CONFIG_DIR	         = os.path.realpath(os.path.join(PROJECT_DIR, 'etc'))
    MODULES_DIR	         = os.path.realpath(os.path.join(PROJECT_DIR, 'modules'))
    METHODS_DIR	         = os.path.realpath(os.path.join(PROJECT_DIR, 'methods'))
    ROUTERS_DIR	         = os.path.realpath(os.path.join(PROJECT_DIR, 'routers'))

    SHARED_LIB_DIR   = os.path.realpath(os.path.join(PROJECT_DIR, '../shared-lib'))
    SHARED_MODULES_DIR   = os.path.realpath(os.path.join(SHARED_LIB_DIR, 'modules'))
    SHARED_METHODS_DIR   = os.path.realpath(os.path.join(SHARED_LIB_DIR, 'methods'))
    SHARED_ROUTERS_DIR   = os.path.realpath(os.path.join(SHARED_LIB_DIR, 'routers'))
    
    def add_sub_search_dirs(path: str) -> None:
        for root, dirs, files in os.walk(path):
            for d in dirs:
                if d.startswith('.'):
                    continue
                if d.startswith('_'):
                    continue
                s_path = os.path.join(root, d)
                for s_root, s_dirs, s_files in os.walk(s_path):
                    if '__init__.py' in s_files:
                        break
                    sys.path.insert(0, s_path)
                    break
            break

    def add_search_dirs(paths: list) -> None:
        for modules_path, methods_path, routers_path in paths:
            if os.path.isdir(modules_path):
                sys.path.insert(0, modules_path)
                add_sub_search_dirs(modules_path)
            if os.path.isdir(methods_path):
                sys.path.insert(0, methods_path)
                add_sub_search_dirs(methods_path)
            if os.path.isdir(routers_path):
                sys.path.insert(0, routers_path)
                add_sub_search_dirs(routers_path)

    add_search_dirs([
        (MODULES_DIR, METHODS_DIR, ROUTERS_DIR),
        (SHARED_MODULES_DIR, SHARED_METHODS_DIR, SHARED_ROUTERS_DIR)
    ])

import types
import typing

import logging

import re
import json
import csv

import asyncio
from fastapi import (
    APIRouter,
    Cookie,
    Depends,
    FastAPI,
    File,
    HTTPException,
    Query,
    Request,
    Security,
    UploadFile,
    WebSocket,
    WebSocketException,
    WebSocketDisconnect,
    status
)
from fastapi.security import OAuth2PasswordBearer
from fastapi.responses import HTMLResponse
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates

from fastapi_third_party_auth import KeycloakIDToken

from pydantic import BaseModel

from utils import (
    RE_TRUE,
    RE_FALSE,
    RE_NONE,
    RE_NEGATED,
    RE_NUMBER,
    RE_INT,
    RE_FLOAT,
    RE_PRIMITIVE,
    RE_FRACTION,
    RE_SPLIT_FRACTION,
    RE_JSON_OBJECT_START,
    RE_JSON_OBJECT_END,
    RE_JSON_ARRAY_START,
    RE_JSON_ARRAY_END,
    RE_COMMA_SEPARATOR,
    RE_COMMA_TRAILING,
    RE_NEWLINE_SINGLE,
    RE_NEWLINE_MULTI,
    RE_WHITESPACE,
    RE_WHITESPACE_START,
    RE_WHITESPACE_END,
    
    UTILS_FUNCTION_MAP,
    
    to_int,
    to_float,
    to_abs,
    to_list,
    to_msecs,
    frames_to_msecs,
    str_is_empty,
    to_date,
    to_time,
    to_datetime,
    date_to_str,
    time_to_str,
    datetime_to_str
)
from apiutils import (
    setup_fastapi_jinja_templates
)
import trp_config



# Constants
ROUTER_PATH_PREFIX = '/auth'
ROUTER_TAGS = [ 'auth' ]
TEMPLATES_PATH_SUFFIX = 'auth'
TEMPLATES_PATH_BASE = 'templates/html'



# API classes
class AuthRequestItem(BaseModel):
    method: str
    args: list | None = None
    kwargs: dict | None = None

class AuthResponseItem(BaseModel):
    success: bool = False
    message: str | None = None
    payload: typing.Any = None



# Data container
class MData:
    router: APIRouter
    app: typing.Any
    config: trp_config.TrpConfig | None = None
    server: typing.Any = None
    auth: typing.Any = None
    templates: Jinja2Templates | None = None
    
    @classmethod
    def init(
        cls,
        app: typing.Any,
        config: trp_config.TrpConfig,
        auth: typing.Any = None
    ) -> None:
        """Initialize module
        
            Args:
                app (FastAPI): FastAPI app
                config (trp_config.TrpConfig): Methods paths
                auth (typing.Any): Auth
        """
        cls.app = app
        cls.config = config
        cls.auth = auth
        cls.server = None
        
        setup_fastapi_jinja_templates(
            cls,
            TEMPLATES_PATH_SUFFIX,
            TEMPLATES_PATH_BASE
        )
        
        
        ### Routes Handling ###

        # Get login page
        async def get_login_page(
            cls,
            token: typing.Annotated[str, Depends(auth)]
        ):
            #print('get_login_page ... request:', request)
            print('get_login_page ... token:', token)
            
            return {'token': token}
        cls.get_login_page = classmethod(get_login_page)


        # Verify token
        async def token_callback(
            cls,
            token: typing.Annotated[str, Depends(auth)]
        ):
            #print('token_callback ... request:', request)
            print('token_callback ... token:', token)
            
            return {'token': token}
        cls.token_callback = classmethod(token_callback)
        
        
        ### Setup router ###
        cls.router = APIRouter(
            prefix=ROUTER_PATH_PREFIX,
            tags=ROUTER_TAGS,
            responses={
                404: { 'description': 'Not found' }
            }
        )
        
        
        ### Add routes ###
        cls.router.add_api_route('/',                           cls.get_login_page, methods=['GET'])
        cls.router.add_api_route('/callback/belowtoxic.cloud',  cls.token_callback, methods=['GET'])



### Init ###

def init(
    app: typing.Any,
    config: trp_config.TrpConfig,
    auth: typing.Any = None
) -> None:
    """Initialize module
    
        Args:
            app (FastAPI): FastAPI app
            config (trp_config.TrpConfig): Methods paths
            auth (typing.Any): Auth
    """
    MData.init(
        app,
        config,
        auth
    )


def get_router() -> APIRouter:
    return MData.router



### Testing ###

if __name__ == '__main__':
    oauth2_scheme = OAuth2PasswordBearer(
        tokenUrl='auth/login'
    )
    init(
        trp_config.TrpConfig(),
        oauth2_scheme
    )

