# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import os
import sys
import types
import typing

import logging

from fastapi import (
    FastAPI,
    APIRouter,
    HTTPException
)
from fastapi.security import OAuth2PasswordBearer
from fastapi.templating import Jinja2Templates

#from fastapi_third_party_auth import KeycloakIDToken

from cluster_api import (
    ClusterApi,
    ActivityItem,
    ServersItem
)
from apiutils import (
    setup_fastapi_jinja_templates
)
import trp_config



# Constants
ROUTER_PATH_PREFIX = '/cluster'
ROUTER_TAGS = [ 'cluster' ]
TEMPLATES_PATH_SUFFIX = 'cluster'
TEMPLATES_PATH_BASE = 'templates/html'



# Data container
class MData:
    router: APIRouter
    app: typing.Any
    config: trp_config.TrpConfig | None = None
    server: typing.Any = None
    auth: typing.Any = None
    templates: Jinja2Templates | None = None
    
    @classmethod
    def init(
        cls,
        app: typing.Any,
        config: trp_config.TrpConfig,
        auth: typing.Any = None
    ) -> None:
        """Initialize module
        
            Args:
                app (FastAPI): FastAPI app
                config (trp_config.TrpConfig): Methods paths
                auth (typing.Any): Auth
        """
        cls.app = app
        cls.config = config
        cls.auth = auth
        cls.server = ClusterApi(config)
        
        setup_fastapi_jinja_templates(
            cls,
            TEMPLATES_PATH_SUFFIX,
            TEMPLATES_PATH_BASE
        )
        
        
        ### Routes Handler ###

        # Get default page
        async def cluster_brokers(cls) -> ServersItem:
            cls,
            logging.debug("'/cluster/brokers' called")
            if not cls.server:
                raise HTTPException(status_code=500, detail="Cluster API not initialized")
            return await cls.server.get_brokers()
        cls.cluster_brokers = classmethod(cluster_brokers)

        
        async def cluster_active(cls) -> ActivityItem:
            cls,
            logging.debug("'/cluster/active' called")
            if not cls.server:
                raise HTTPException(status_code=500, detail="Cluster API not initialized")
            return await cls.server.is_active()
        cls.cluster_active = classmethod(cluster_active)


        ### Setup router ###
        cls.router = APIRouter(
            prefix=ROUTER_PATH_PREFIX,
            tags=ROUTER_TAGS,
            responses={
                404: { 'description': 'Not found' }
            }
        )
        
        
        ### Add routes ###
        cls.router.add_api_route('/brokers',    cls.cluster_brokers,    methods=['GET'])
        cls.router.add_api_route('/active',     cls.cluster_active,     methods=['GET'])



### Init ###

def init(
    app: typing.Any,
    config: trp_config.TrpConfig,
    auth: typing.Any = None
) -> None:
    """Initialize module
    
        Args:
            app (FastAPI): FastAPI app
            config (trp_config.TrpConfig): Methods paths
            auth (typing.Any): Auth
    """
    MData.init(
        app,
        config,
        auth
    )


def get_router() -> APIRouter:
    return MData.router




### Testing ###

if __name__ == '__main__':
    init(
        trp_config.TrpConfig(),
        None
    )

