# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import os
import sys
import types
import typing

import logging

import re
import json
import csv

import asyncio
from fastapi import (
    APIRouter,
    Cookie,
    Depends,
    FastAPI,
    File,
    HTTPException,
    Query,
    Request,
    Security,
    UploadFile,
    WebSocket,
    WebSocketException,
    WebSocketDisconnect,
    status
)
from fastapi.security import OAuth2PasswordBearer
from fastapi.responses import HTMLResponse
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates

#from fastapi_third_party_auth import KeycloakIDToken

from pydantic import BaseModel

from utils import (
    RE_TRUE,
    RE_FALSE,
    RE_NONE,
    RE_NEGATED,
    RE_NUMBER,
    RE_INT,
    RE_FLOAT,
    RE_PRIMITIVE,
    RE_FRACTION,
    RE_SPLIT_FRACTION,
    RE_JSON_OBJECT_START,
    RE_JSON_OBJECT_END,
    RE_JSON_ARRAY_START,
    RE_JSON_ARRAY_END,
    RE_COMMA_SEPARATOR,
    RE_COMMA_TRAILING,
    RE_NEWLINE_SINGLE,
    RE_NEWLINE_MULTI,
    RE_WHITESPACE,
    RE_WHITESPACE_START,
    RE_WHITESPACE_END,
    
    UTILS_FUNCTION_MAP,
    
    to_int,
    to_float,
    to_abs,
    to_list,
    to_msecs,
    frames_to_msecs,
    str_is_empty,
    to_date,
    to_time,
    to_datetime,
    date_to_str,
    time_to_str,
    datetime_to_str
)
from apiutils import (
    setup_fastapi_jinja_templates
)
import trp_config



# Constants
ROUTER_PATH_PREFIX = '/user'
ROUTER_TAGS = [ 'user' ]
TEMPLATES_PATH_SUFFIX = 'user'
TEMPLATES_PATH_BASE = 'templates/html'



# API classes
class UserRequestItem(BaseModel):
    method: str
    args: list | None = None
    kwargs: dict | None = None

class UserResponseItem(BaseModel):
    success: bool = False
    message: str | None = None
    payload: typing.Any = None



# Data container
class MData:
    router: APIRouter
    app: typing.Any
    config: trp_config.TrpConfig | None = None
    server: typing.Any = None
    auth: typing.Any = None
    templates: Jinja2Templates | None = None
    
    @classmethod
    def init(
        cls,
        app: typing.Any,
        config: trp_config.TrpConfig,
        auth: typing.Any = None
    ) -> None:
        """Initialize module
        
            Args:
                app (FastAPI): FastAPI app
                config (trp_config.TrpConfig): Methods paths
                auth (typing.Any): Auth
        """
        cls.app = app
        cls.config = config
        cls.auth = auth
        cls.server = None
        
        setup_fastapi_jinja_templates(
            cls,
            TEMPLATES_PATH_SUFFIX,
            TEMPLATES_PATH_BASE
        )
        
        
        ### Routes Handling ###

        # Get list of users
        async def get_users(
            cls,
            token: typing.Annotated[str, Depends(auth)]
        ):
            #print('get_users ... request:', request)
            print('get_users ... token:', token)
            
            return {'token': token}
        cls.get_users = classmethod(get_users)
        
        
        # Get user
        async def get_user(
            cls,
            token: typing.Annotated[str, Depends(auth)]
        ):
            #print('get_user ... request:', request)
            print('get_user ... token:', token)
            
            return {'token': token}
        cls.get_user = classmethod(get_user)
        
        
        # Get user profile
        async def get_user_profile(
            cls,
            token: typing.Annotated[str, Depends(auth)]
        ):
            #print('get_user_profile ... request:', request)
            print('get_user_profile ... token:', token)
            
            return {'token': token}
        cls.get_user_profile = classmethod(get_user_profile)
        
        
        ### Setup router ###
        cls.router = APIRouter(
            prefix=ROUTER_PATH_PREFIX,
            tags=ROUTER_TAGS,
            responses={
                404: { 'description': 'Not found' }
            }
        )
        
        
        ### Add routes ###
        cls.router.add_api_route('/',               cls.get_users,          methods=['GET'])
        cls.router.add_api_route('/{id}',           cls.get_user,           methods=['GET'])
        cls.router.add_api_route('/{id}/profile',   cls.get_user_profile,   methods=['GET'])



### Init ###

def init(
    app: typing.Any,
    config: trp_config.TrpConfig,
    auth: typing.Any = None
) -> None:
    """Initialize module
    
        Args:
            app (FastAPI): FastAPI app
            config (trp_config.TrpConfig): Methods paths
            auth (typing.Any): Auth
    """
    MData.init(
        app,
        config,
        auth
    )


def get_router() -> APIRouter:
    return MData.router



### Testing ###

if __name__ == '__main__':
    init(
        trp_config.TrpConfig(),
        None
    )

