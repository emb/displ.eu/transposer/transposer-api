import re
import asyncio


export = {
    'methods': [
        'add',
        'sub',
        'mul',
        
        'SomeClass.method2'
    ]
}


RE_SOME_CONSTANT = re.compile(r'^SOME_CONSTANT$')

hello = 'world'


def add(a, b):
    return a + b

async def sub(a, b):
    return a - b

async def mul(a, b):
    return a * b


class SomeClass:
    def method1():
        return 1
    
    async def method2():
        return 2

