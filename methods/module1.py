import asyncio


export = {
    'methods': [
        'add',
        'sub',
        'mul'
    ],
    'constants': [] 
}


def add(a, b):
    return a + b

async def sub(a, b):
    return a - b

async def mul(a, b):
    return a * b

