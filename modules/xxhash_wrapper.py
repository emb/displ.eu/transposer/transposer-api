# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import os
import re
import subprocess



BIN = '/usr/bin/xxhsum'
ALGORITHMS = {
    0: True,
    1: True,
    2: True,
    3: True,
    32: True,
    64: True,
    128: True
}
RE_HEX = re.compile(r'=\s*([0-9a-fA-F]+)$')



def xxhsum(filepath: str, algorithm: int = 3, binary: str = BIN) -> str:
    """Calculate the xxHash of a file using the xxhsum command line tool.

    Args:
        filepath (str): The path to the file to hash.
        algorithm (int): The algorithm to use for the hash calculation.
            0: XXH32
            1: XXH64
            2: XXH128
            3: XXH3
        binary (str): The path to the xxhsum binary.
    
    Returns:
        str: The hash of the file.
    """
    # Test if xxhsum is installed
    if not os.path.exists(binary):
        raise Exception('xxhsum is not installed or not found in the system path')

    # Test if the file exists
    if not filepath:
        raise Exception('File path is required')
    if not os.path.exists(filepath):
        raise Exception(f'File not found: {filepath}')
    
    # Test if the algorithm is valid
    if algorithm not in ALGORITHMS:
        raise Exception(f'Invalid algorithm: {algorithm}')
    
    try:
        # Call the xxhsum command with the specified algorithm and file
        result = subprocess.run([binary, f'-H{algorithm}', '-q', filepath], text=True, capture_output=True)
        if result.returncode != 0:
            raise Exception(f'{result.stderr}')
        result = RE_HEX.search(result.stdout.strip())
        return result.group(1)
    except Exception as e:
        raise Exception(f'An error occurred: {e}')





#########
# Testing
#
if __name__ == '__main__':
    
    # Example usage:

    # Replace this with your file path
    #file_path = '/home/transposer/media/The Lonely Island - Jizz In My Pants (Official Music Video).mp4'
    file_path = '/home/transposer/media/Color Burst HDR Dolby Vision™ 12K 60FPS.mp4'
    algorithm = 3  # 0, 1, 2, 3, 32, 64 or 128 for XXH32, XXH64, XXH128, or XXH3

    print(f'The hash of the file is: {xxhsum(file_path, algorithm)}')


