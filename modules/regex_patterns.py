# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import re



# Constants
RE_SPLIT_TO_LIST = re.compile(r'\s*,\s*', re.I)

RE_INT_VALUE = re.compile(r'^\s*(\d+)\s*$', re.I)
RE_INT_VALUE_LT = re.compile(r'^\s*<\s*(\d+)\s*$', re.I)
RE_INT_VALUE_GT = re.compile(r'^\s*(\d+)\s*>\s*$', re.I)
RE_INT_VALUE_BT = re.compile(r'^\s*(\d+)\s*[<>]\s*(\d+)\s*$', re.I)

RE_FLOAT_VALUE = re.compile(r'^\s*(\d+(?:\.\d+)?)\s*$', re.I)
RE_FLOAT_VALUE_LT = re.compile(r'^\s*<\s*(\d+(?:\.\d+)?)\s*$', re.I)
RE_FLOAT_VALUE_GT = re.compile(r'^\s*(\d+(?:\.\d+)?)\s*>\s*$', re.I)
RE_FLOAT_VALUE_BT = re.compile(r'^\s*(\d+(?:\.\d+)?)\s*[<>]\s*(\d+(?:\.\d+)?)\s*$', re.I)

RE_TRUE = re.compile(r'^\s*(?:1|true|yes|ja|t|y|j)\s*$', re.I)

RE_LANG_CODE = re.compile(r'^([a-z]{2,3})$', re.I)
RE_FILE_EXTENSION = re.compile(r'^([a-z0-9]{1,16})$', re.I)
RE_FILE_PURPOSE = re.compile(r'^(original|transcription)$', re.I)

RE_ALPHANUM_TERM = re.compile(r'\s*([\w \'"\.-]+)\s*', re.I)
RE_ALPHANUM_NAME = re.compile(r'([\w\.-]+)', re.I)
RE_ALPHANUM_ASCII = re.compile(r'([a-z0-9_\.-]+)', re.I)
RE_ALPHANUM_ASCII_WSPACE = re.compile(r'([a-z0-9](?:[a-z0-9_\. -]*[a-z0-9])?)', re.I)
RE_FIELD_NAME = re.compile(r'((?!.*\.\.)(?!.*--)(?!.*-\.)(?!.*-_)(?!.*\.-)(?!.*_-)[a-z0-9][a-z0-9_\.-]*[a-z0-9])', re.I)
RE_ORDER_FIELD_NAME = re.compile(r'^((?!.*\.\.)(?!.*--)(?!.*-\.)(?!.*-_)(?!.*\.-)(?!.*_-)[a-z0-9][a-z0-9_\.-]*?[a-z0-9])(\.(?:asc|desc))?$', re.I)

RE_UUID4 = re.compile(r'([0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12})', re.I)
RE_UUID4_SHORT = re.compile(r'([0-9a-f]{8}-[0-9a-f]{4})', re.I)

#RE_DOMAIN = re.compile(r'((?:(?:[a-z0-9]{1,2}|(?:[a-z0-9][a-z0-9-]{1,61}[a-z0-9]))\.)*(?:(?:[a-z][a-z0-9])|(?:[a-z][a-z0-9-]{1,61}[a-z0-9]))\.[a-z]{2,63})', re.I)
RE_DOMAIN = re.compile(r'((?:(?:(?:25[0-9])|(?:2[0-4][0-9])|(?:1[0-9][0-9])|(?:[1-9][0-9])|[1-9])\.(?:(?:25[0-9])|(?:2[0-4][0-9])|(?:1[0-9][0-9])|(?:[1-9][0-9])|[0-9])\.(?:(?:25[0-9])|(?:2[0-4][0-9])|(?:1[0-9][0-9])|(?:[1-9][0-9])|[0-9])\.(?:(?:25[0-9])|(?:2[0-4][0-9])|(?:1[0-9][0-9])|(?:[1-9][0-9])|[0-9]))|(?:(?:(?:[0-9a-f]{1,4}:){8}})|(?:(?:[0-9a-f]{1,4}:){2,6}:[0-9a-f]{1,4})|(?:(?:[0-9a-f]{1,4}:){2,5}:[0-9a-f]{1,4}:[0-9a-f]{1,4})|(?:(?:[0-9a-f]{1,4}:){2,4}:(?:[0-9a-f]{1,4}:){2}[0-9a-f]{1,4})|(?:(?:[0-9a-f]{1,4}:){2,3}:(?:[0-9a-f]{1,4}:){3}[0-9a-f]{1,4}))|(?:(?:[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?\.)+[a-z]{2,64}))', re.I)
RE_IPV4 = re.compile(r'((?:(?:25[0-9])|(?:2[0-4][0-9])|(?:1[0-9][0-9])|(?:[1-9][0-9])|[1-9])\.(?:(?:25[0-9])|(?:2[0-4][0-9])|(?:1[0-9][0-9])|(?:[1-9][0-9])|[0-9])\.(?:(?:25[0-9])|(?:2[0-4][0-9])|(?:1[0-9][0-9])|(?:[1-9][0-9])|[0-9])\.(?:(?:25[0-9])|(?:2[0-4][0-9])|(?:1[0-9][0-9])|(?:[1-9][0-9])|[0-9]))', re.I)
RE_IPV4_HOST = re.compile(r'((?:\[(?:(?:25[0-9])|(?:2[0-4][0-9])|(?:1[0-9][0-9])|(?:[1-9][0-9])|[1-9])\.(?:(?:25[0-9])|(?:2[0-4][0-9])|(?:1[0-9][0-9])|(?:[1-9][0-9])|[0-9])\.(?:(?:25[0-9])|(?:2[0-4][0-9])|(?:1[0-9][0-9])|(?:[1-9][0-9])|[0-9])\.(?:(?:25[0-9])|(?:2[0-4][0-9])|(?:1[0-9][0-9])|(?:[1-9][0-9])|[0-9]))\])', re.I)
RE_IPV6 = re.compile(r'((?:(?:[0-9a-f]{1,4}:){8}})|(?:(?:[0-9a-f]{1,4}:){2,6}:[0-9a-f]{1,4})|(?:(?:[0-9a-f]{1,4}:){2,5}:[0-9a-f]{1,4}:[0-9a-f]{1,4})|(?:(?:[0-9a-f]{1,4}:){2,4}:(?:[0-9a-f]{1,4}:){2}[0-9a-f]{1,4})|(?:(?:[0-9a-f]{1,4}:){2,3}:(?:[0-9a-f]{1,4}:){3}[0-9a-f]{1,4}))', re.I)
RE_IPV6_HOST = re.compile(r'(\[(?:ipv6:)?(?:(?:(?:[0-9a-f]{1,4}:){8}})|(?:(?:[0-9a-f]{1,4}:){2,6}:[0-9a-f]{1,4})|(?:(?:[0-9a-f]{1,4}:){2,5}:[0-9a-f]{1,4}:[0-9a-f]{1,4})|(?:(?:[0-9a-f]{1,4}:){2,4}:(?:[0-9a-f]{1,4}:){2}[0-9a-f]{1,4})|(?:(?:[0-9a-f]{1,4}:){2,3}:(?:[0-9a-f]{1,4}:){3}[0-9a-f]{1,4}))\])', re.I)
RE_EMAIL = re.compile(r'([a-z][a-z0-9_\.+-]*[a-z0-9_]@(?:(?:[a-z0-9]{1,2}|(?:[a-z0-9][a-z0-9-]{1,61}[a-z0-9]))\.)*(?:(?:[a-z][a-z0-9])|(?:[a-z][a-z0-9-]{1,61}[a-z0-9]))\.[a-z]{2,63})', re.I)
RE_USERNAME = re.compile(r'((?!.*\.\.)(?!.*--)(?!.*\+\+)[a-z0-9_][a-z0-9_\.+-]+[a-z0-9_])', re.I)
RE_SLUG = re.compile(r'((?!.*--)(?!.*__)(?!.*_-)(?!.*-_)[a-z0-9][a-z0-9_-]*[a-z0-9])', re.I)
RE_PASSWORD = re.compile(r'((?=.*[A-Z])(?=.*[a-z])(?=.*\d)(?=.*[_\.,<>\(\)\[\]\{\}\\/#\^|`\'":;%@&$!?=~*+-])[a-zA-Z0-9_\.,<>\(\)\[\]\{\}\\/#^|`\'":;%@&$!?=~*+-]{6,})')
RE_HASH_XX = re.compile(r'([a-z0-9_-]+)', re.I)
RE_TOKEN = re.compile(r'([a-z0-9_\.=*+-]+)', re.I)
RE_HEX = re.compile(r'([0-9a-f]+)', re.I)
RE_SHA256_HEX = re.compile(r'([0-9a-f]{64})', re.I)
RE_SHAKE256_KEY_HEX = re.compile(r'([0-9a-f]{20})', re.I)
RE_XXHASH_CID_HEX = re.compile(r'([0-9a-f]{16})', re.I)

RE_URI_SCHEMES = re.compile(r'^\s*(about|afp|apt|attachment|blob|cid|cvs|dat|data|dav|did|dns|embedded|feed|file|filesystem|finger|ftp|geo|go|graph|http|https|icon|im|imap|info|ipfs|ipn|ipns|irc|ircs|jar|ldap|ldaps|magnet|mailto|matrix|message|mongodb|nfs|oid|openid|proxy|query|quic-transport|redis|rediss|rsync|rtmp|rtsp|rtsps|service|session|sftp|sip|sips|skype|smb|sms|smtp|snmp|ssh|steam|stun|stuns|submit|svn|tag|tel|telnet|tftp|tool|turn|turns|vnc|web3|udp|urn|webcal|wifi|ws|wss|xmpp|aaa|aaas|acap|acct|acd|acr|adiumxtra|adt|afs|aim|amss|android|appdata|ar|ark|at|aw|barion|bb|beshare|bitcoin|bitcoincash|bolo|brid|browserext|cabal|calculator|callto|cap|cast|casts|chrome|chrome-extension|coap|coap\+tcp|coap\+ws|coaps|coaps\+tcp|coaps+ws|com-eventbrite-attendee|content|content-type|crid|cstr|dab|dhttp|diaspora|dict|dis|dlna-playcontainer|dlna-playsingle|dntp|doi|dpp|drm|drop|dtmi|dtn|dvb|dvx|dweb|ed2k|eid|elsi|ens|ethereum|example|facetime|fax|feedready|fido|first-run-pen-experience|fish|fm|fuchsia-pkg|gg|git|gitoid|gizmoproject|gopher|grd|gtalk|h323|ham|hcap|hcp|hxxp|hxxps|hydrazone|hyper|iax|icap|iotdisco|ipp|ipps|irc6|iris|iris\.beep|iris\.lwz|iris\.xpc|iris\.xpcs|isostore|itms|jabber|jms|keyparc|lastfm|lbry|leaptofrogans|lid|lorawan|lpa|lvlt|mailserver|maps|market|mid|mms|modem|moz|ms-access|ms-appinstaller|ms-browser-extension|ms-calculator|ms-drive-to|ms-enrollment|ms-excel|ms-eyecontrolspeech|ms-gamebarservices|ms-gamingoverlay|ms-getoffice|ms-help|ms-infopath|ms-inputapp|ms-launchremotedesktop|ms-lockscreencomponent-config|ms-media-stream-id|ms-meetnow|ms-mixedrealitycapture|ms-mobileplans|ms-newsandinterests|ms-officeapp|ms-people|ms-project|ms-powerpoint|ms-publisher|ms-remotedesktop|ms-remotedesktop-launch|ms-restoretabcompanion|ms-screenclip|ms-screensketch|ms-search|ms-search-repair|ms-secondary-screen-controller|ms-secondary-screen-setup|ms-settings|ms-settings-airplanemode|ms-settings-bluetooth|ms-settings-camera|ms-settings-cellular|ms-settings-cloudstorage|ms-settings-connectabledevices|ms-settings-displays-topology|ms-settings-emailandaccounts|ms-settings-language|ms-settings-location|ms-settings-lock|ms-settings-nfctransactions|ms-settings-notifications|ms-settings-power|ms-settings-privacy|ms-settings-proximity|ms-settings-screenrotation|ms-settings-wifi|ms-settings-workplace|ms-spd|ms-stickers|ms-sttoverlay|ms-transit-to|ms-useractivityset|ms-virtualtouchpad|ms-visio|ms-walk-to|ms-whiteboard|ms-whiteboard-cmd|ms-word|msnim|msrp|msrps|mss|mt|mtqp|mumble|mupdate|mvn|mvrp|mvrps|news|ni|nih|nntp|notes|num|ocf|onenote|onenote-cmd|opaquelocktoken|openpgp4fpr|otpauth|p1|pack|palm|paparazzi|payment|payto|pkcs11|platform|pop|pres|prospero|pwid|psyc|pttp|qb|reload|res|resource|rmi|rtmfp|rtspu|sarif|secondlife|secret-token|sgn|shc|sieve|simpleledger|simplex|smp|snews|soap.beep|soap.beeps|soldat|spiffe|spotify|ssb|starknet|swh|swid|swidpath|taler|teamspeak|teliaeid|things|thismessage|tip|tn3270|tv|unreal|upt|ut2004|uuid-in-package|v-event|vemmi|ventrilo|ves|videotex|view-source|vscode|vscode-insiders|vsls|w3|wais|wcr|web\+ap|wpid|wtai|wyciwyg|xcon|xcon-userid|xfire|xmlrpc\.beep|xmlrpc\.beeps|xftp|xrcp|xri|ymsgr|z39\.50|z39\.50r|z39\.50s)', re.I)
RE_URI_SCHEME_SEPARATOR = re.compile(r':/{,2}', re.I)

RE_HOST = re.compile(r'((afp|apt|blob|cid|cvs|dat|data|dav|did|dns|feed|file|ftp|geo|graph|http|https|icon|imap|info|ipfs|ipn|ipns|irc|ircs|jar|ldap|ldaps|magnet|mailto|matrix|message|mongodb|nfs|oid|openid|proxy|query|quic-transport|redis|rediss|rsync|rtmp|rtsp|rtsps|service|session|sftp|sip|sips|skype|smb|sms|smtp|snmp|ssh|steam|stun|stuns|submit|svn|tag|tel|telnet|tftp|tool|turn|turns|vnc|web3|udp|urn|webcal|wifi|ws|wss|xmpp|aaa|aaas|acap|acct|acd|acr|afs|aim|amss|android|appdata|ar|bitcoin|bitcoincash|browserext|cabal|calculator|callto|cap|cast|casts|chrome|chrome-extension|coap|coap\+tcp|coap\+ws|coaps|coaps\+tcp|coaps+ws|com-eventbrite-attendee|content|content-type|crid|cstr|dab|dhttp|diaspora|dict|dis|dlna-playcontainer|dlna-playsingle|dntp|doi|dpp|drm|drop|dtmi|dtn|dvb|dvx|dweb|ed2k|eid|elsi|ens|ethereum|example|facetime|fax|feedready|fido|first-run-pen-experience|fish|fm|fuchsia-pkg|gg|git|gitoid|gizmoproject|gopher|grd|gtalk|h323|ham|hcap|hcp|hxxp|hxxps|hydrazone|hyper|iax|icap|iotdisco|ipp|ipps|irc6|iris|iris\.beep|iris\.lwz|iris\.xpc|iris\.xpcs|isostore|itms|jabber|jms|keyparc|lastfm|lbry|leaptofrogans|lid|lorawan|lpa|lvlt|mailserver|maps|market|mid|mms|modem|moz|mumble|mupdate|mvn|mvrp|mvrps|news|ni|nih|nntp|notes|num|ocf|onenote|onenote-cmd|opaquelocktoken|openpgp4fpr|otpauth|p1|pack|palm|paparazzi|payment|payto|pkcs11|platform|pop|pres|prospero|pwid|psyc|pttp|qb|reload|res|resource|rmi|rtmfp|rtspu|smp|spiffe|spotify|swid|teamspeak|things|tip|tv|unreal|upt|ventrilo|videotex|view-source|vscode|vsls|w3|web\+ap|xfire|xmlrpc|xftp)://((?:(?:(?:[a-z0-9]{1,2}|(?:[a-z0-9][a-z0-9-]{1,61}[a-z0-9]))\.)*(?:(?:[a-z][a-z0-9])|(?:[a-z][a-z0-9-]{1,61}[a-z0-9]))\.[a-z]{2,63})|(?:(?:(?:25[0-9])|(?:2[0-4][0-9])|(?:1[0-9][0-9])|(?:[1-9][0-9])|[1-9])\.(?:(?:25[0-9])|(?:2[0-4][0-9])|(?:1[0-9][0-9])|(?:[1-9][0-9])|[0-9])\.(?:(?:25[0-9])|(?:2[0-4][0-9])|(?:1[0-9][0-9])|(?:[1-9][0-9])|[0-9])\.(?:(?:25[0-9])|(?:2[0-4][0-9])|(?:1[0-9][0-9])|(?:[1-9][0-9])|[0-9]))|(?:(?:(?:[0-9a-f]{1,4}:){8}})|(?:(?:[0-9a-f]{1,4}:){2,6}:[0-9a-f]{1,4})|(?:(?:[0-9a-f]{1,4}:){2,5}:[0-9a-f]{1,4}:[0-9a-f]{1,4})|(?:(?:[0-9a-f]{1,4}:){2,4}:(?:[0-9a-f]{1,4}:){2}[0-9a-f]{1,4})|(?:(?:[0-9a-f]{1,4}:){2,3}:(?:[0-9a-f]{1,4}:){3}[0-9a-f]{1,4}))))', re.I)
RE_HOST_PATH = re.compile(r"((?:/[0-9a-z_$@\.,:;=!+*'\(\)-]+)+/?(?:\?(?:[0-9a-z_$@\.,:;!+*'\(\)-]+=[0-9a-z_$@\.,:;!+*'\(\)-]+(?:&[0-9a-z_$@\.,:;!+*'\(\)-]+=[0-9a-z_$@\.,:;!+*'\(\)-]+)*)?)?)", re.I)

RE_MIME_BASE_TYPES = re.compile(r'^\s*(application|audio|font|image|message|model|multipart|text|video)', re.I)

RE_DATETIME_GENERAL = re.compile(r'(?:[12][9012][0-9]{2}(?:-[0-9]{1,2}(?:-[0-9]{1,2}(?:[ T][0-9]{1,2}(?::[0-9]{1,2}(?::[0-9]{1,2}(?:\.[0-9]+(?:[+-][0-9]{2}(?::[0-9]{2})?)?)?)?)?)?)?)?)', re.I)
RE_DATETIME_FULL = re.compile(r'(([12][9012][0-9]{2})-([0-9]{1,2})-([0-9]{1,2})[ T]([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2})\.([0-9]+)([+-](?:[0-9]{2}(?::[0-9]{2}(?:\.[0-9]+)))))', re.I)
RE_DATETIME_YYYYMMDD_HHMMSS_MS = re.compile(r'(([12][9012][0-9]{2})-([0-9]{1,2})-([0-9]{1,2})[ T]([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2})\.([0-9]+))', re.I)
RE_DATETIME_YYYYMMDD_HHMMSS = re.compile(r'(([12][9012][0-9]{2})-([0-9]{1,2})-([0-9]{1,2})[ T]([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2}))', re.I)
RE_DATETIME_YYYYMMDD_HHMM = re.compile(r'(([12][9012][0-9]{2})-([0-9]{1,2})-([0-9]{1,2})[ T]([0-9]{1,2}):([0-9]{1,2}))', re.I)
RE_DATETIME_YYYYMMDD_HH = re.compile(r'(([12][9012][0-9]{2})-([0-9]{1,2})-([0-9]{1,2})[ T]([0-9]{1,2}))', re.I)
RE_DATETIME_YYYYMMDD = re.compile(r'(([12][9012][0-9]{2})-([0-9]{1,2})-([0-9]{1,2}))', re.I)
RE_DATETIME_YYYYMM = re.compile(r'(([12][9012][0-9]{2})-([0-9]{1,2}))', re.I)
RE_DATETIME_YYYY = re.compile(r'(([12][9012][0-9]{2}))', re.I)
RE_DATETIME_TZINFO_1 = re.compile(r'([+-][0-9]{2})', re.I)
RE_DATETIME_TZINFO_2 = re.compile(r'([+-][0-9]{4})', re.I)
RE_DATETIME_TZINFO_3 = re.compile(r'([+-][0-9]{2}:[0-9]{2})', re.I)

#RE_DATETIME_LT = re.compile(r'^\s*<\s*([0-9]{4}(?:-[0-9-]*[0-9]{2}(?: [0-9]{2}(?::[0-9:]*[0-9]{2}(?:\.[0-9]{6}(?:[+-][0-9]{2})?)?)?)?)?)', re.I)
#RE_DATETIME_GT = re.compile(r'^\s*([0-9]{4}(?:-[0-9-]*[0-9]{2}(?: [0-9]{2}(?::[0-9:]*[0-9]{2}(?:\.[0-9]{6}(?:[+-][0-9]{2})?)?)?)?)?)\s*>\s*$', re.I)
#RE_DATETIME_BT = re.compile(r'^\s*([0-9]{4}(?:-[0-9-]*[0-9]{2}(?: [0-9]{2}(?::[0-9:]*[0-9]{2}(?:\.[0-9]{6}(?:[+-][0-9]{2})?)?)?)?)?)\s*[<>]\s*([0-9]{4}(?:-[0-9-]*[0-9]{2}(?: [0-9]{2}(?::[0-9:]*[0-9]{2}(?:\.[0-9]{6}(?:[+-][0-9]{2})?)?)?)?)?)\s*$', re.I)

RE_DATETIME_LT_1 = re.compile(r'^\s*>\s*([12][9012][0-9]{2}(?:-[0-9]{1,2}(?:-[0-9]{1,2}(?:[ T][0-9]{1,2}(?::[0-9]{1,2}(?::[0-9]{1,2}(?:\.[0-9]+(?:[+-][0-9]{2}(?::[0-9]{2})?)?)?)?)?)?)?)?)\s*$', re.I)
RE_DATETIME_LT_2 = re.compile(r'^\s*([12][9012][0-9]{2}(?:-[0-9]{1,2}(?:-[0-9]{1,2}(?:[ T][0-9]{1,2}(?::[0-9]{1,2}(?::[0-9]{1,2}(?:\.[0-9]+(?:[+-][0-9]{2}(?::[0-9]{2})?)?)?)?)?)?)?)?)\s*<\s*$', re.I)
RE_DATETIME_GT_1 = re.compile(r'^\s*<\s*([12][9012][0-9]{2}(?:-[0-9]{1,2}(?:-[0-9]{1,2}(?:[ T][0-9]{1,2}(?::[0-9]{1,2}(?::[0-9]{1,2}(?:\.[0-9]+(?:[+-][0-9]{2}(?::[0-9]{2})?)?)?)?)?)?)?)?)\s*$', re.I)
RE_DATETIME_GT_2 = re.compile(r'^\s*([12][9012][0-9]{2}(?:-[0-9]{1,2}(?:-[0-9]{1,2}(?:[ T][0-9]{1,2}(?::[0-9]{1,2}(?::[0-9]{1,2}(?:\.[0-9]+(?:[+-][0-9]{2}(?::[0-9]{2})?)?)?)?)?)?)?)?)\s*>\s*$', re.I)
RE_DATETIME_BT = re.compile(r'^\s*([12][9012][0-9]{2}(?:-[0-9]{1,2}(?:-[0-9]{1,2}(?:[ T][0-9]{1,2}(?::[0-9]{1,2}(?::[0-9]{1,2}(?:\.[0-9]+(?:[+-][0-9]{2}(?::[0-9]{2})?)?)?)?)?)?)?)?)\s*[<>]\s*([12][9012][0-9]{2}(?:-[0-9]{1,2}(?:-[0-9]{1,2}(?:[ T][0-9]{1,2}(?::[0-9]{1,2}(?::[0-9]{1,2}(?:\.[0-9]+(?:[+-][0-9]{2}(?::[0-9]{2})?)?)?)?)?)?)?)?)\s*$', re.I)

RE_FLEX_DATETIME_LT_1 = re.compile(r'^\s*>\s*((?:[1-9][0-9]{4,9}(?:\.[0-9]+)?)|(?:[12][9012][0-9]{2}(?:-[0-9]{1,2}(?:-[0-9]{1,2}(?:[ T][0-9]{1,2}(?::[0-9]{1,2}(?::[0-9]{1,2}(?:\.[0-9]+(?:[+-][0-9]{2}(?::[0-9]{2})?)?)?)?)?)?)?)?))\s*$', re.I)
RE_FLEX_DATETIME_LT_2 = re.compile(r'^\s*((?:[1-9][0-9]{4,9}(?:\.[0-9]+)?)|(?:[12][9012][0-9]{2}(?:-[0-9]{1,2}(?:-[0-9]{1,2}(?:[ T][0-9]{1,2}(?::[0-9]{1,2}(?::[0-9]{1,2}(?:\.[0-9]+(?:[+-][0-9]{2}(?::[0-9]{2})?)?)?)?)?)?)?)?))\s*<\s*$', re.I)
RE_FLEX_DATETIME_GT_1 = re.compile(r'^\s*<\s*((?:[1-9][0-9]{4,9}(?:\.[0-9]+)?)|(?:[12][9012][0-9]{2}(?:-[0-9]{1,2}(?:-[0-9]{1,2}(?:[ T][0-9]{1,2}(?::[0-9]{1,2}(?::[0-9]{1,2}(?:\.[0-9]+(?:[+-][0-9]{2}(?::[0-9]{2})?)?)?)?)?)?)?)?))\s*$', re.I)
RE_FLEX_DATETIME_GT_2 = re.compile(r'^\s*((?:[1-9][0-9]{4,9}(?:\.[0-9]+)?)|(?:[12][9012][0-9]{2}(?:-[0-9]{1,2}(?:-[0-9]{1,2}(?:[ T][0-9]{1,2}(?::[0-9]{1,2}(?::[0-9]{1,2}(?:\.[0-9]+(?:[+-][0-9]{2}(?::[0-9]{2})?)?)?)?)?)?)?)?))\s*>\s*$', re.I)
RE_FLEX_DATETIME_BT = re.compile(r'^\s*((?:[1-9][0-9]{4,9}(?:\.[0-9]+)?)|(?:[12][9012][0-9]{2}(?:-[0-9]{1,2}(?:-[0-9]{1,2}(?:[ T][0-9]{1,2}(?::[0-9]{1,2}(?::[0-9]{1,2}(?:\.[0-9]+(?:[+-][0-9]{2}(?::[0-9]{2})?)?)?)?)?)?)?)?))\s*[<>]\s*((?:[1-9][0-9]{4,9}(?:\.[0-9]+)?)|(?:[12][9012][0-9]{2}(?:-[0-9]{1,2}(?:-[0-9]{1,2}(?:[ T][0-9]{1,2}(?::[0-9]{1,2}(?::[0-9]{1,2}(?:\.[0-9]+(?:[+-][0-9]{2}(?::[0-9]{2})?)?)?)?)?)?)?)?))\s*$', re.I)

RE_TIME_SSMS = re.compile(r'^\s*([0-9]+)[\.,]([0-9]{3})\s*$', re.I)
RE_TIME_SSUS = re.compile(r'^\s*([0-9]+)[\.,]([0-9]{6})\s*$', re.I)
RE_TIME_SSNS = re.compile(r'^\s*([0-9]+)[\.,]([0-9]{9})\s*$', re.I)
RE_TIME_SSALL = re.compile(r'^\s*([0-9]+)[\.,]([0-9]+)\s*$', re.I)
RE_TIME_MMSSMS = re.compile(r'^\s*([0-9]+):([0-9]{2})[\.,]([0-9]{3})\s*$', re.I)
RE_TIME_MMSSUS = re.compile(r'^\s*([0-9]+):([0-9]{2})[\.,]([0-9]{6})\s*$', re.I)
RE_TIME_MMSSNS = re.compile(r'^\s*([0-9]+):([0-9]{2})[\.,]([0-9]{9})\s*$', re.I)
RE_TIME_MMSSALL = re.compile(r'^\s*([0-9]+):([0-9]{2})[\.,]([0-9]+)\s*$', re.I)
RE_TIME_HHMMSSMS = re.compile(r'^\s*([0-9]+):([0-9]{2}):([0-9]{2})[\.,]([0-9]{3})\s*$', re.I)
RE_TIME_HHMMSSUS = re.compile(r'^\s*([0-9]+):([0-9]{2}):([0-9]{2})[\.,]([0-9]{6})\s*$', re.I)
RE_TIME_HHMMSSNS = re.compile(r'^\s*([0-9]+):([0-9]{2}):([0-9]{2})[\.,]([0-9]{9})\s*$', re.I)
RE_TIME_HHMMSSALL = re.compile(r'^\s*([0-9]+):([0-9]{2}):([0-9]{2})[\.,]([0-9]+)\s*$', re.I)

RE_FLEX_TIME_SSXX = re.compile(r'^\s*([0-9]+)[\.,]([0-9]+)\s*$', re.I)
RE_FLEX_TIME_MMSSXX = re.compile(r'^\s*([0-9]+):([0-9]{2})[\.,]([0-9]+)\s*$', re.I)
RE_FLEX_TIME_HHMMSSXX = re.compile(r'^\s*([0-9]+):([0-9]{2}):([0-9]{2})[\.,]([0-9]+)\s*$', re.I)


