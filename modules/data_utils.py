# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import os
import sys
import errno

import typing

import re
import nh3
import datetime
import calendar
import base64
import json

import validators
from pathvalidate import (
    sanitize_filepath as sanitize_filepath_func,
    sanitize_filename as sanitize_filename_func
)

from regex_patterns import (
    RE_SPLIT_TO_LIST,
    RE_INT_VALUE, RE_INT_VALUE_LT, RE_INT_VALUE_GT, RE_INT_VALUE_BT,
    RE_FLOAT_VALUE, RE_FLOAT_VALUE_LT, RE_FLOAT_VALUE_GT, RE_FLOAT_VALUE_BT,
    RE_TRUE,
    RE_LANG_CODE,
    RE_ALPHANUM_TERM, RE_ALPHANUM_NAME, RE_ALPHANUM_ASCII, RE_FIELD_NAME, RE_ORDER_FIELD_NAME,
    RE_UUID4,
    RE_DOMAIN, RE_IPV4, RE_IPV4_HOST, RE_IPV6, RE_IPV6_HOST, RE_EMAIL, RE_USERNAME, RE_SLUG, RE_PASSWORD, RE_HASH_XX,
    RE_URI_SCHEMES, RE_URI_SCHEME_SEPARATOR,
    RE_MIME_BASE_TYPES,
    RE_DATETIME_GENERAL,
    RE_DATETIME_FULL, RE_DATETIME_YYYYMMDD_HHMMSS_MS, RE_DATETIME_YYYYMMDD_HHMMSS,
    RE_DATETIME_YYYYMMDD_HHMM, RE_DATETIME_YYYYMMDD_HH, RE_DATETIME_YYYYMMDD,
    RE_DATETIME_YYYYMM, RE_DATETIME_YYYY,
    #RE_DATETIME_LT, RE_DATETIME_GT, RE_DATETIME_BT,
    RE_FLEX_DATETIME_LT_1, RE_FLEX_DATETIME_LT_2,
    RE_FLEX_DATETIME_GT_1, RE_FLEX_DATETIME_GT_2,
    RE_FLEX_DATETIME_BT,
    RE_DATETIME_TZINFO_1, RE_DATETIME_TZINFO_2, RE_DATETIME_TZINFO_3
)
# from datapool.models.base import (
#     validate_and_convert_timestamp
# )



MIME_BASE_TYPES_MAP = {
    'application': True,
    'audio': True,
    'font': True,
    'image': True,
    'text': True,
    'video': True
}
MIME_TYPES_MAP = {
    'application/epub+zip': '.epub',
    'application/gzip': '.gz',
    'application/java-archive': '.jar',
    'application/json': '.json',
    'application/ld+json': '.jsonld',
    'application/msword': '.doc',
    'application/octet-stream': '.bin',
    'application/ogg': '.ogx',
    'application/pdf': '.pdf',
    'application/php': '.php',
    'application/rtf': '.rtf',
    'application/vnd.amazon.ebook': '.azw',
    'application/vnd.apple.installer+xml': '.mpkg',
    'application/vnd.mozilla.xul+xml': '.xul',
    'application/vnd.ms-excel': '.xls',
    'application/vnd.ms-fontobject': '.eot',
    'application/vnd.ms-powerpoint': '.ppt',
    'application/vnd.oasis.opendocument.presentation': '.odp',
    'application/vnd.oasis.opendocument.spreadsheet': '.ods',
    'application/vnd.oasis.opendocument.text': '.odt',
    'application/vnd.openxmlformats-officedocument.presentationml.presentation': '.pptx',
    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet': '.xlsx',
    'application/vnd.openxmlformats-officedocument.wordprocessingml.document': '.docx',
    'application/vnd.rar': '.rar',
    'application/vnd.visio': '.vsd',
    'application/x-7z-compressed': '.7z',
    'application/x-csh': '.csh',
    'application/x-freearc': '.arc',
    'application/x-httpd-php': '.php',
    'application/x-sh': '.sh',
    'application/x-shockwave-flash': '.swf',
    'application/x-tar': '.tar',
    'application/xhtml+xml': '.xhtml',
    'application/xml': '.xml',
    'application/zip': '.zip',
    'audio/3gpp': '.3gp',
    'audio/3gpp2': '.3g2',
    'audio/aac': '.aac',
    'audio/midi': '.midi',
    'audio/mpeg': '.mp3',
    'audio/ogg': '.ogg',
    'audio/opus': '.opus',
    'audio/wav': '.wav',
    'audio/webm': '.webm',
    'audio/x-midi': '.midi',
    'font/otf': '.otf',
    'font/ttf': '.ttf',
    'font/woff': '.woff',
    'font/woff2': '.woff2',
    'image/bmp': '.bmp',
    'image/gif': '.gif',
    'image/jpeg': '.jpg',
    'image/png': '.png',
    'image/svg+xml': '.svg',
    'image/tiff': '.tiff',
    'image/vnd.microsoft.icon': '.ico',
    'image/webp': '.webp',
    'text/calendar': '.ics',
    'text/css': '.css',
    'text/csv': '.csv',
    'text/html': '.html',
    'text/javascript': '.js',
    'text/plain': '.txt',
    'text/xml': '.xml',
    'video/3gpp': '.3gp',
    'video/3gpp2': '.3g2',
    'video/mp4': '.mp4',
    'video/mpeg': '.mpeg',
    'video/ogg': '.ogv',
    'video/webm': '.webm',
    'video/x-msvideo': '.avi'
}





### Utils

def is_empty(
    value: typing.Any
) -> bool:
    #print('is_empty() ... value:', value, value is None)
    if value is None:
        return True
    if isinstance(value, str):
        value = value.strip()
        return value == ''
    if isinstance(value, (list, dict, tuple)):
        return len(value) == 0
    if isinstance(value, bool):
        return not value
    return False



def is_true(
    value: typing.Any
) -> bool:
    if value is None:
        False
    if isinstance(value, bool):
        return value
    if isinstance(value, (int, float)):
        return value > 0
    if isinstance(value, str):
        return RE_TRUE.match(value) is not None
    return False



def sanitize_text(
    value: str | None
) -> str | None:
    if is_empty(value):
        return None
    value = nh3.clean(value)
    if is_empty(value):
        return None
    return value



def split_terms(
    value: str | None,
    regex: re.Pattern,
    prefix: str,
    suffix: str
) -> list:
    if not value:
        return []
    value = value.strip()
    if not value:
        return []
    value = RE_SPLIT_TO_LIST.split(value)
    tmp = []
    for t in value:
        result = regex.match(t)
        if not result:
            continue
        tmp.append(f'{prefix}{result.group(1).strip()}{suffix}')
    return tmp



def validate_slug(
    value: str | None
) -> bool:
    if not value:
        return False
    value = value.strip()
    if not value:
        return False
    result = RE_SLUG.match(value)
    if result:
        return result.slug(value)

def split_slugs(
    value: str | None,
    prefix: str,
    suffix: str
) -> list:
    if not value:
        return []
    value = value.strip()
    if not value:
        return []
    value = RE_SPLIT_TO_LIST.split(value)
    tmp = []
    for t in value:
        if not validators.slug(t):
            continue
        tmp.append(f'{prefix}{t}{suffix}')
    return tmp



def validate_hostname(
    value: str | None
) -> bool:
    if not value:
        return False
    value = value.strip()
    if not value:
        return False
    return validators.hostname(value)

def split_hostnames(
    value: str | None,
    prefix: str,
    suffix: str
) -> list:
    if not value:
        return []
    value = value.strip()
    if not value:
        return []
    value = RE_SPLIT_TO_LIST.split(value)
    tmp = []
    for t in value:
        if not validators.hostname(t):
            continue
        tmp.append(f'{prefix}{t}{suffix}')
    return tmp



def validate_domain(
    value: str | None
) -> bool:
    if not value:
        return False
    value = value.strip()
    if not value:
        return False
    result = RE_DOMAIN.match(value)
    if result:
        return validators.domain(value)
    return False

def split_domains(
    value: str | None,
    prefix: str,
    suffix: str
) -> list:
    if not value:
        return []
    value = value.strip()
    if not value:
        return []
    value = RE_SPLIT_TO_LIST.split(value)
    tmp = []
    for t in value:
        if not validate_domain(t):
            continue
        tmp.append(f'{prefix}{t}{suffix}')
    return tmp



def validate_email(
    value: str | None
) -> bool:
    if not value:
        return False
    value = value.strip()
    if not value:
        return False
    return validators.email(value)

def split_emails(
    value: str | None,
    prefix: str,
    suffix: str
) -> list:
    if not value:
        return []
    value = value.strip()
    if not value:
        return []
    value = RE_SPLIT_TO_LIST.split(value)
    tmp = []
    for t in value:
        if not validators.email(t):
            continue
        tmp.append(f'{prefix}{t}{suffix}')
    return tmp



def validate_uri(
    value: str | None
) -> bool:
    if not value:
        return False
    value = value.strip()
    if not value:
        return False
    return validators.url(value)

def split_uris(
    value: str | None,
    prefix: str,
    suffix: str
) -> list:
    if not value:
        return []
    value = value.strip()
    if not value:
        return []
    value = RE_SPLIT_TO_LIST.split(value)
    tmp = []
    for t in value:
        if not validators.url(t):
            continue
        tmp.append(f'{prefix}{t}{suffix}')
    return tmp



def split_numeric(
    value: int | float | str | None,
    rounding: int | float | None = None
) -> list:
    if value is None:
        return []
    
    if isinstance(value, (int, float)):
        if rounding is None:
            return [value]
        value = int(value / rounding) * rounding
        return [(value, '>=', value + rounding, '<=')]
    
    if not isinstance(value, str):
        return []
    
    value = value.strip()
    if not value:
        return []
    value = RE_SPLIT_TO_LIST.split(value)
    tmp = []
    for t in value:
        result = RE_INT_VALUE_BT.match(t)
        if result:
            val1 = int(result.group(1))
            val2 = int(result.group(2))
            if val1 > val2:
                val1, val2 = val2, val1
            tmp.append((val1, '>=', val2, '<='))
            continue
        result = RE_INT_VALUE_LT.match(t)
        if result:
            tmp.append((int(result.group(1)), '<='))
            continue
        result = RE_INT_VALUE_GT.match(t)
        if result:
            tmp.append((int(result.group(1)), '>='))
            continue
        result = RE_INT_VALUE.match(t)
        if result:
            if rounding is None:
                tmp.append(int(result.group(1)))
                continue
            val1 = int(result.group(1))
            val1 = int(int(val1 / rounding) * rounding)
            tmp.append((val1, '>=', val1 + rounding, '<='))
            continue

        result = RE_FLOAT_VALUE_BT.match(t)
        if result:
            val1 = float(result.group(1))
            val2 = float(result.group(2))
            if val1 > val2:
                val1, val2 = val2, val1
            tmp.append((val1, '>=', val2, '<='))
            continue
        result = RE_FLOAT_VALUE_LT.match(t)
        if result:
            tmp.append((float(result.group(1)), '<='))
            continue
        result = RE_FLOAT_VALUE_GT.match(t)
        if result:
            tmp.append((float(result.group(1)), '>='))
            continue
        result = RE_FLOAT_VALUE.match(t)
        if result:
            if rounding is None:
                tmp.append(float(result.group(1)))
                continue
            val1 = float(result.group(1))
            val1 = int(val1 / rounding) * rounding
            tmp.append((val1, '>=', val1 + rounding, '<='))
            continue
    
    return tmp



def split_port(
    value: str | None,
    rounding: int | None = None
) -> list:
    if not value:
        return []
    value = value.strip()
    if not value:
        return []
    value = RE_SPLIT_TO_LIST.split(value)
    tmp = []
    for t in value:
        result = RE_INT_VALUE_BT.match(t)
        if result:
            val1 = int(result.group(1))
            if val1 < 1 or val1 > 65535:
                continue
            val2 = int(result.group(2))
            if val2 < 1 or val2 > 65535:
                continue
            if val1 > val2:
                val1, val2 = val2, val1
            tmp.append((val1, '>=', val2, '<='))
            continue
        result = RE_INT_VALUE_LT.match(t)
        if result:
            val1 = int(result.group(1))
            if val1 < 1 or val1 > 65535:
                continue
            tmp.append((val1, '<='))
            continue
        result = RE_INT_VALUE_GT.match(t)
        if result:
            val1 = int(result.group(1))
            if val1 < 1 or val1 > 65535:
                continue
            tmp.append((val1, '>='))
            continue
        result = RE_INT_VALUE.match(t)
        if result:
            if rounding is None:
                tmp.append(int(result.group(1)))
                continue
            val1 = int(result.group(1))
            val1 = int(int(val1 / rounding) * rounding)
            if val1 < 1 or val1 > 65535:
                continue
            val2 = val1 + rounding
            if val2 < 1 or val2 > 65535:
                continue
            tmp.append((val1, '>=', val2, '<='))
            continue

        result = RE_FLOAT_VALUE_BT.match(t)
        if result:
            val1 = int(result.group(1))
            if val1 < 1 or val1 > 65535:
                continue
            val2 = int(result.group(2))
            if val2 < 1 or val2 > 65535:
                continue
            if val1 > val2:
                val1, val2 = val2, val1
            tmp.append((val1, '>=', val2, '<='))
            continue
        result = RE_FLOAT_VALUE_LT.match(t)
        if result:
            val1 = int(result.group(1))
            if val1 < 1 or val1 > 65535:
                continue
            tmp.append((val1, '<='))
            continue
        result = RE_FLOAT_VALUE_GT.match(t)
        if result:
            val1 = int(result.group(1))
            if val1 < 1 or val1 > 65535:
                continue
            tmp.append((val1, '>='))
            continue
        result = RE_FLOAT_VALUE.match(t)
        if result:
            val1 = int(result.group(1))
            if val1 < 1 or val1 > 65535:
                continue
            if rounding is None:
                tmp.append(val1)
                continue
            val1 = int(val1 / rounding) * rounding
            if val1 < 1 or val1 > 65535:
                continue
            val2 = val1 + rounding
            if val2 < 1 or val2 > 65535:
                continue
            tmp.append((val1, '>=', val2, '<='))
            continue
    
    return tmp



def validate_datetime(
    value: str | None
) -> bool:
    if RE_DATETIME_FULL.match(value) is not None:
        return True
    if RE_DATETIME_YYYYMMDD_HHMMSS_MS.match(value) is not None:
        return True
    if RE_DATETIME_YYYYMMDD_HHMMSS.match(value) is not None:
        return True
    if RE_DATETIME_YYYYMMDD_HHMM.match(value) is not None:
        return True
    if RE_DATETIME_YYYYMMDD_HH.match(value) is not None:
        return True
    if RE_DATETIME_YYYYMMDD.match(value) is not None:
        return True
    if RE_DATETIME_YYYYMM.match(value) is not None:
        return True
    if RE_DATETIME_YYYY.match(value) is not None:
        return True
    return False


def number_to_datetime_obj(value: int | float) -> str | None:
    # We interpret integers and floats as timestamps
    if value < 0:
        # We do not allow negative values
        return None
    
    if value > 9999999999:
        # We interpret this integer as timestamps in milliseconds
        value = value * 0.001
    
    if value > 9999999999:
        # If we still exceed the maximum value,
        # we interpret the value as timestamps in micoseconds
        value = value * 0.001
    
    if value > 9999999999:
        # If we still exceed the maximum value,
        # we interpret the value as timestamps in nanoseconds
        value = value * 0.001
    
    if value > 9999999999:
        # If we still exceed the maximum value,
        # we return None
        return None

    try:
        # Convert to datetime object
        return datetime.datetime.fromtimestamp(value)
    except Exception as e:
        # If we encounter an error, we return None
        return None


def number_to_datetime_string(value: int | float) -> str | None:
    # Convert to datetime object
    value = number_to_datetime_obj(value)
    if value is None:
        return None
    # Convert to string
    return f"{value.strftime('%Y-%m-%d %H:%M:%S.%f')}+00:00"


def get_datetime_tz_str(value: str) -> str:
    result = RE_DATETIME_TZINFO_1.match(value)
    if result is not None:
        return f'{result.group(1)}:00'
    result = RE_DATETIME_TZINFO_2.match(value)
    if result is not None:
        return f'{result.group(1)[0:3]}:{result.group(1)[3:5]}'
    result = RE_DATETIME_TZINFO_3.match(value)
    if result is not None:
        return result.group(1)
    return '+00:00'


def get_datetime_str(
    value: int | float | str | datetime.datetime | datetime.date | None,
    upper: bool = False
) -> str | None:
    if isinstance(value, datetime.datetime):
        if value.tzinfo is None:
            return f"{value.strftime('%Y-%m-%d %H:%M:%S.%f')}+00:00"
        return value.strftime('%Y-%m-%d %H:%M:%S.%f%z')
    
    if isinstance(value, datetime.date):
        dt = datetime.datetime(value.year, value.month, value.day)
        if upper:
            return dt.replace(hour=23, minute=59, second=59, microsecond=999999)
        return f"{dt.strftime('%Y-%m-%d %H:%M:%S.%f')}+00:00"
    
    if isinstance(value, (int, float)):
        return number_to_datetime_string(value)
    
    if not isinstance(value, str):
        return None

    result = RE_INT_VALUE.match(value)
    if result is not None:
        return number_to_datetime_string(int(result.group(1)))

    result = RE_FLOAT_VALUE.match(value)
    if result is not None:
        return number_to_datetime_string(float(result.group(1)))

    result = RE_DATETIME_FULL.match(value)
    if result is not None:
        return f"{result.group(2)}-{result.group(3).rjust(2, '0')}-{result.group(4).rjust(2, '0')} {result.group(5).rjust(2, '0')}:{result.group(6).rjust(2, '0')}:{result.group(7).rjust(2, '0')}.{result.group(8).ljust(6, '0')}{get_datetime_tz_str(result.group(9))}"
    result = RE_DATETIME_YYYYMMDD_HHMMSS_MS.match(value)
    if result is not None:
        return f"{result.group(2)}-{result.group(3).rjust(2, '0')}-{result.group(4).rjust(2, '0')} {result.group(5).rjust(2, '0')}:{result.group(6).rjust(2, '0')}:{result.group(7).rjust(2, '0')}.{result.group(8).ljust(6, '0')}+00:00"
    result = RE_DATETIME_YYYYMMDD_HHMMSS.match(value)
    if result is not None:
        if upper:
            return f"{result.group(2)}-{result.group(3).rjust(2, '0')}-{result.group(4).rjust(2, '0')} {result.group(5).rjust(2, '0')}:{result.group(6).rjust(2, '0')}:{result.group(7).rjust(2, '0')}.999999+00:00"
        return f"{result.group(2)}-{result.group(3).rjust(2, '0')}-{result.group(4).rjust(2, '0')} {result.group(5).rjust(2, '0')}:{result.group(6).rjust(2, '0')}:{result.group(7).rjust(2, '0')}.000000+00:00"
    result = RE_DATETIME_YYYYMMDD_HHMM.match(value)
    if result is not None:
        if upper:
            return f"{result.group(2)}-{result.group(3).rjust(2, '0')}-{result.group(4).rjust(2, '0')} {result.group(5).rjust(2, '0')}:{result.group(6).rjust(2, '0')}:59.999999+00:00"
        return f"{result.group(2)}-{result.group(3).rjust(2, '0')}-{result.group(4).rjust(2, '0')} {result.group(5).rjust(2, '0')}:{result.group(6).rjust(2, '0')}:00.000000+00:00"
    result = RE_DATETIME_YYYYMMDD_HH.match(value)
    if result is not None:
        if upper:
            return f"{result.group(2)}-{result.group(3).rjust(2, '0')}-{result.group(4).rjust(2, '0')} {result.group(5).rjust(2, '0')}:59:59.999999+00:00"
        return f"{result.group(2)}-{result.group(3).rjust(2, '0')}-{result.group(4).rjust(2, '0')} {result.group(5).rjust(2, '0')}:00:00.000000+00:00"
    result = RE_DATETIME_YYYYMMDD.match(value)
    if result is not None:
        if upper:
            return f"{result.group(2)}-{result.group(3).rjust(2, '0')}-{result.group(4).rjust(2, '0')} 23:59:59.999999+00:00"
        return f"{result.group(2)}-{result.group(3).rjust(2, '0')}-{result.group(4).rjust(2, '0')} 00:00:00.000000+00:00"
    result = RE_DATETIME_YYYYMM.match(value)
    if result is not None:
        if upper:
            num_days = calendar.monthrange(result.group(2), result.group(3))[1]
            return f"{result.group(2)}-{result.group(3).rjust(2, '0')}-{num_days} 23:59:59.999999+00:00"
        return f"{result.group(2)}-{result.group(3).rjust(2, '0')}-01 00:00:00.000000+00:00"
    result = RE_DATETIME_YYYY.match(value)
    if result is not None:
        if upper:
            return f"{result.group(2)}-12-31 23:59:59.999999+00:00"
        return f"{result.group(2)}-01-01 00:00:00.000000+00:00"
    return None

def get_datetime_obj(
    value: int | float | str | datetime.datetime | datetime.date | None,
    upper: bool = False
) -> datetime.datetime | None:
    value = get_datetime_str(value, upper)
    if value is None:
        return None
    return datetime.datetime.fromisoformat(value)


def split_datetime(
    value: str | None
) -> list:
    if not value:
        return []
    value = value.strip()
    if not value:
        return []
    value = RE_SPLIT_TO_LIST.split(value)
    tmp = []
    for t in value:
        result = RE_FLEX_DATETIME_BT.match(t)
        if result:
            val1 = result.group(1)
            val2 = result.group(2)
            obj1 = get_datetime_obj(val1)
            if obj1 is None:
                continue
            obj2 = get_datetime_obj(val2)
            if obj2 is None:
                continue
            if obj1 > obj2:
                val1, val2 = val2, val1
            val1 = get_datetime_str(val1)
            val2 = get_datetime_str(val2, True)
            tmp.append((val1, '>=', val2, '<='))
            continue
        
        result = RE_FLEX_DATETIME_LT_1.match(t)
        if result is None:
            result = RE_FLEX_DATETIME_LT_2.match(t)
        if result:
            val = get_datetime_str(result.group(1), True)
            if val is None:
                continue
            tmp.append((val, '<='))
            continue
        
        result = RE_FLEX_DATETIME_GT_1.match(t)
        if result is None:
            result = RE_FLEX_DATETIME_GT_2.match(t)
        if result:
            val = get_datetime_str(result.group(1))
            if val is None:
                continue
            tmp.append((val, '>='))
            continue
        
        result = RE_DATETIME_GENERAL.match(t)
        if result:
            val1 = result.group(1)
            val2 = get_datetime_str(val1, True)
            if val2 is None:
                continue
            val1 = get_datetime_str(val1)
            if val1 == val2:
                tmp.append((val1, '=='))
                continue
            tmp.append((val1, '>=', val2, '<='))
            continue
    
    return tmp



def is_valid_mime_type(
    value: str
) -> list:
    if is_empty(value):
        return False
    value = value.strip()
    if is_empty(value):
        return False
    value = value.lower()
    if value in MIME_TYPES_MAP:
        return True
    return False


def split_mime_types(
    value: str | None,
    prefix: str = '',
    suffix: str = ''
) -> list:
    if not value:
        return []
    value = value.strip()
    if not value:
        return []
    value = RE_SPLIT_TO_LIST.split(value)
    tmp = []
    for t in value:
        result = RE_MIME_BASE_TYPES.match(t)
        if result:
            tmp.append(f'{prefix}{result.group(1)}{suffix}')
            continue
        if value not in MIME_TYPES_MAP:
            continue
        tmp.append(f'{prefix}{value}{suffix}')
    return tmp



def is_valid_filepath(
    value: str
) -> bool:
    if is_empty(value) or not isinstance(value, str):
        return False
    value = value.strip()
    if is_empty(value):
        return False
    if '%' in value:
        return False
    if sanitize_filepath_func(value) == value:
        return True
    return False


def sanitize_filepath(
    value: str
) -> str | None:
    if is_empty(value) or not isinstance(value, str):
        return None
    value = value.strip()
    if is_empty(value):
        return None
    if '%' in value:
        return None
    sanitized_value = sanitize_filepath_func(value)
    sanitized_value = sanitized_value.strip()
    if is_empty(sanitized_value):
        return None
    return sanitized_value


def split_filepaths(
    value: str | None,
    prefix: str = '',
    suffix: str = ''
) -> list:
    if not value:
        return []
    value = value.strip()
    if not value:
        return []
    value = RE_SPLIT_TO_LIST.split(value)
    tmp = []
    for t in value:
        if not is_valid_filepath(t):
            continue
        tmp.append(f'{prefix}{value}{suffix}')
    return tmp



def is_valid_filename(
    value: str
) -> bool:
    if is_empty(value) or not isinstance(value, str):
        return False
    value = value.strip()
    if is_empty(value):
        return False
    if '%' in value:
        return False
    if sanitize_filename_func(value) == value:
        return True
    return False


def sanitize_filename(
    value: str
) -> str | None:
    if is_empty(value) or not isinstance(value, str):
        return None
    value = value.strip()
    if is_empty(value):
        return None
    if '%' in value:
        return None
    sanitized_value = sanitize_filename_func(value)
    sanitized_value = sanitized_value.strip()
    if is_empty(sanitized_value):
        return None
    return sanitized_value


def split_filenames(
    value: str | None,
    prefix: str = '',
    suffix: str = ''
) -> list:
    if not value:
        return []
    value = value.strip()
    if not value:
        return []
    value = RE_SPLIT_TO_LIST.split(value)
    tmp = []
    for t in value:
        if not is_valid_filename(t):
            continue
        tmp.append(f'{prefix}{value}{suffix}')
    return tmp



### base64 encode / decode

def data_2_base64(value: typing.Any | None) -> str | None:
    # Convert input to string
    if isinstance(value, (int, float, bool, dict, list, tuple)):
        try:
            value = json.dumps(value)
        except:
            return None
    elif not isinstance(value, str):
        return None

    # Encode to UTF-8
    try:
        value = value.encode(encoding='UTF-8')
    except:
        return None

    # Encode to base64
    try:
        return base64.b64encode(value).decode('ascii')
    except:
        return None


def base64_2_data(value: str | None) -> typing.Any:
    if not value:
        return None
    
    # Decode to base64
    try:
        value = base64.b64decode(value)
    except:
        return None
    
    # Try to decode byte-string
    try:
        value = value.decode('utf-8')
    except:
        return None

    # Try to JSON parse the resulting string
    try:
        return json.loads(value)
    except:
        # If JSON decoding throws an error,
        # we assume that the string should
        # be used as-is
        return value







################################################################
# Testing
#
if __name__ == '__main__':
    print('Running tests...')

    print("\n'data_2_base64' and 'base64_2_data' with: int ".ljust(80, '-'))
    val = 10
    print('Input:', val)
    result = data_2_base64(val)
    print("'data_2_base64' result:", result)
    result = base64_2_data(result)
    print("'base64_2_data' result, type:", result, type(result))
    
    print("\n'data_2_base64' and 'base64_2_data' with: float ".ljust(80, '-'))
    val = 9.9999
    print('Input:', val)
    result = data_2_base64(val)
    print("'data_2_base64' result:", result)
    result = base64_2_data(result)
    print("'base64_2_data' result, type:", result, type(result))
    
    print("\n'data_2_base64' and 'base64_2_data' with: bool ".ljust(80, '-'))
    val = True
    print('Input:', val)
    result = data_2_base64(val)
    print("'data_2_base64' result:", result)
    result = base64_2_data(result)
    print("'base64_2_data' result, type:", result, type(result))
    
    print("\n'data_2_base64' and 'base64_2_data' with: list ".ljust(80, '-'))
    val = [1, 2, 'hello', 'world', True]
    print('Input:', val)
    result = data_2_base64(val)
    print("'data_2_base64' result:", result)
    result = base64_2_data(result)
    print("'base64_2_data' result, type:", result, type(result))
    
    print("\n'data_2_base64' and 'base64_2_data' with: dict ".ljust(80, '-'))
    val = {'hello': 'world', 1: 2, 'somewhere': True}
    print('Input:', val)
    result = data_2_base64(val)
    print("'data_2_base64' result:", result)
    result = base64_2_data(result)
    print("'base64_2_data' result, type:", result, type(result))
    
    print("\n'data_2_base64' and 'base64_2_data' with: string ".ljust(80, '-'))
    val = "J'ai commencé le basket à l'âge de 13, 14 ans"
    print('Input:', val)
    result = data_2_base64(val)
    print("'data_2_base64' result:", result)
    result = base64_2_data(result)
    print("'base64_2_data' result, type:", result, type(result))
    
    print("\nconvert data ".ljust(80, '-'))
    val = """
WEBVTT

04:02.500 --> 04:05.000
J'ai commencé le basket à l'âge de 13, 14 ans

04:05.001 --> 04:07.800
Sur les <i.foreignphrase><lang en>playground</lang></i>, ici à Montpellier
"""
    print('Input:', val)
    result = data_2_base64(val)
    print("'data_2_base64' result:", result)
    result = base64_2_data(result)
    print("'base64_2_data' result, type:", result, type(result))




