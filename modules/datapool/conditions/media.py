# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import typing

from regex_patterns import (
    RE_INT_VALUE,
    RE_FLOAT_VALUE,
    RE_UUID4,
    RE_ALPHANUM_TERM,
    RE_HASH_XX,
    RE_FILE_EXTENSION,
    RE_FILE_PURPOSE
)
from data_utils import (
    is_empty, is_true,
    split_terms,
    split_numeric,
    split_datetime,
    sanitize_text,
    validate_uri as validate_uri_func,
    split_mime_types,
    split_filepaths,
    split_filenames
)
from ..filter import (
    create_filter
)
from ..models.media import MediaOrm





def add_media_conditions(
    container: list,
    obj: typing.Any,
    use_and: bool = False
) -> bool:
    condition_added = False
    
    # id
    val = obj.id
    if isinstance(val, int):
        val = str(val)
    val = create_filter(
        MediaOrm.id,
        [(x, '==') for x in split_terms(val, RE_INT_VALUE, '', '')],
        use_and
    )
    if val is not None:
        condition_added = True
        container.append(val)
    
    # uuid
    val = obj.uuid
    val = create_filter(
        MediaOrm.uuid,
        [(x, '==') for x in split_terms(val, RE_UUID4, '', '')],
        use_and
    )
    if val is not None:
        condition_added = True
        container.append(val)
    
    # created_at
    val = obj.created_at
    val = create_filter(
        MediaOrm.created_at,
        split_datetime(val),
        use_and
    )
    if val is not None:
        condition_added = True
        container.append(val)
    
    # document_id
    val = obj.document_id
    if isinstance(val, int):
        val = str(val)
    val = create_filter(
        MediaOrm.document_id,
        [(x, '==') for x in split_terms(val, RE_INT_VALUE, '', '')],
        use_and
    )
    if val is not None:
        condition_added = True
        container.append(val)
    
    # uri
    val = obj.uri
    val = create_filter(
        MediaOrm.uri,
        [(f'%{val}%', 'like')] if validate_uri_func(val) else [],
        use_and
    )
    if val is not None:
        condition_added = True
        container.append(val)
    
    # path
    val = obj.path
    val = create_filter(
        MediaOrm.path,
        [(x, 'like') for x in split_filepaths(val, '%', '%')],
        use_and
    )
    if val is not None:
        condition_added = True
        container.append(val)
    
    # filename
    val = obj.filename
    val = create_filter(
        MediaOrm.filename,
        [(x, 'like') for x in split_filenames(val, '%', '%')],
        use_and
    )
    if val is not None:
        condition_added = True
        container.append(val)
    
    # title
    val = obj.title
    val = create_filter(
        MediaOrm.title,
        [(x, 'like') for x in split_filenames(val, '%', '%')],
        use_and
    )
    if val is not None:
        condition_added = True
        container.append(val)
    
    # cid
    val = obj.cid
    val = create_filter(
        MediaOrm.cid,
        [(x, '==') for x in split_terms(val, RE_HASH_XX, '', '')],
        use_and
    )
    if val is not None:
        condition_added = True
        container.append(val)
    
    # mime
    val = obj.mime
    val = create_filter(
        MediaOrm.mime,
        [(x, 'like') for x in split_mime_types(val, '%', '%')],
        use_and
    )
    if val is not None:
        condition_added = True
        container.append(val)
    
    # extension
    val = obj.extension
    val = create_filter(
        MediaOrm.extension,
        [(x, '==') for x in split_terms(val, RE_FILE_EXTENSION, '', '')],
        use_and
    )
    if val is not None:
        condition_added = True
        container.append(val)
    
    # size
    val = obj.size
    if isinstance(val, int):
        val = str(val)
    val = create_filter(
        MediaOrm.size,
        split_numeric(val),
        use_and
    )
    if val is not None:
        condition_added = True
        container.append(val)
    
    # duration
    val = obj.duration
    if isinstance(val, (int, float)):
        val = str(val)
    val = create_filter(
        MediaOrm.duration,
        split_numeric(val),
        use_and
    )
    if val is not None:
        condition_added = True
        container.append(val)
    
    # purpose
    val = obj.purpose
    val = create_filter(
        MediaOrm.purpose,
        [(x, '==') for x in split_terms(val, RE_FILE_PURPOSE, '', '')],
        use_and
    )
    if val is not None:
        condition_added = True
        container.append(val)
    
    # status
    val = obj.status
    if isinstance(val, int):
        val = str(val)
    val = create_filter(
        MediaOrm.status,
        split_numeric(val),
        use_and
    )
    if val is not None:
        condition_added = True
        container.append(val)
    
    # reason
    val = obj.reason
    val = create_filter(
        MediaOrm.reason,
        [(x, 'like') for x in split_terms(val, RE_ALPHANUM_TERM, '%', '%')],
        use_and
    )
    if val is not None:
        condition_added = True
        container.append(val)
    
    return condition_added


