# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import typing

from regex_patterns import (
    RE_INT_VALUE,
    RE_UUID4,
    RE_ALPHANUM_TERM,
    RE_EMAIL,
    RE_USERNAME,
    RE_SLUG,
    RE_PASSWORD,
    RE_HASH_XX
)
from data_utils import (
    is_empty, is_true,
    validate_email,
    split_emails,
    split_terms,
    split_numeric,
    split_datetime,
    sanitize_text
)
from ..filter import (
    create_filter
)
from ..models.user import UserOrm





def add_user_conditions(
    container: list,
    obj: typing.Any,
    use_and: bool = False
) -> bool:
    condition_added = False
    
    # id
    val = obj.id
    if isinstance(val, int):
        val = str(val)
    val = create_filter(
        UserOrm.id,
        [(x, '==') for x in split_terms(val, RE_INT_VALUE, '', '')],
        use_and
    )
    if val is not None:
        condition_added = True
        container.append(val)
    
    # uuid
    val = obj.uuid
    val = create_filter(
        UserOrm.uuid,
        [(x, '==') for x in split_terms(val, RE_UUID4, '', '')],
        use_and
    )
    if val is not None:
        condition_added = True
        container.append(val)
    
    # created_at
    val = obj.created_at
    val = create_filter(
        UserOrm.created_at,
        split_datetime(val),
        use_and
    )
    if val is not None:
        condition_added = True
        container.append(val)
    
    # modified_at
    val = obj.modified_at
    val = create_filter(
        UserOrm.modified_at,
        split_datetime(val),
        use_and
    )
    if val is not None:
        condition_added = True
        container.append(val)
    
    # username
    val = obj.username
    val = create_filter(
        UserOrm.username,
        [(x, 'like') for x in split_terms(val, RE_USERNAME, '%', '%')],
        use_and
    )
    if val is not None:
        condition_added = True
        container.append(val)
    
    # email
    val = obj.email
    val = create_filter(
        UserOrm.email,
        [(x, 'like') for x in split_emails(val, '%', '%')],
        use_and
    )
    if val is not None:
        condition_added = True
        container.append(val)
    
    # password
    # val = obj.password
    # val = create_filter(
    #     UserOrm.password,
    #     [(x, 'like') for x in split_terms(val, RE_PASSWORD, '%', '%')],
    #     use_and
    # )
    # if val is not None:
    #     condition_added = True
    #     container.append(val)
    
    # salt
    # val = obj.salt
    # val = create_filter(
    #     UserOrm.salt,
    #     [(x, 'like') for x in split_terms(val, RE_PASSWORD, '%', '%')],
    #     use_and
    # )
    # if val is not None:
    #     condition_added = True
    #     container.append(val)
    
    # display_name
    val = obj.display_name
    val = create_filter(
        UserOrm.display_name,
        [(x, 'like') for x in split_terms(val, RE_ALPHANUM_TERM, '%', '%')],
        use_and
    )
    if val is not None:
        condition_added = True
        container.append(val)
    
    # is_active
    val = obj.is_active
    if val is not None:
        val = create_filter(
            UserOrm.is_active,
            [(val, '==')],
            use_and
        )
    if val is not None:
        condition_added = True
        container.append(val)
    
    return condition_added


