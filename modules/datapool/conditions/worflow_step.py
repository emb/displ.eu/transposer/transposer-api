# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import typing

from regex_patterns import (
    RE_INT_VALUE,
    RE_LANG_CODE,
    RE_UUID4,
    RE_ALPHANUM_TERM,
    RE_SLUG
)
from data_utils import (
    is_empty,
    split_terms,
    split_datetime,
    split_numeric
)
from ..filter import (
    create_filter
)
from ..models.workflow_step import WorkflowStepOrm





def add_workflow_step_conditions(
    container: list,
    obj: typing.Any,
    use_and: bool = False
) -> bool:
    condition_added = False

    # id
    val = obj.id
    if isinstance(val, int):
        val = str(val)
    val = create_filter(
        WorkflowStepOrm.id,
        [(x, '==') for x in split_terms(val, RE_INT_VALUE, '', '')],
        use_and
    )
    if val is not None:
        condition_added = True
        container.append(val)

    # uuid
    val = obj.uuid
    val = create_filter(
        WorkflowStepOrm.uuid,
        [(x, '==') for x in split_terms(val, RE_UUID4, '', '')],
        use_and
    )
    if val is not None:
        condition_added = True
        container.append(val)
    
    # workflow_id
    val = obj.workflow_id
    if isinstance(val, int):
        val = str(val)
    val = create_filter(
        WorkflowStepOrm.workflow_id,
        [(x, '==') for x in split_terms(val, RE_INT_VALUE, '', '')],
        use_and
    )
    if val is not None:
        condition_added = True
        container.append(val)
    
    # pos
    val = obj.pos
    val = create_filter(
        WorkflowStepOrm.pos,
        split_numeric(val),
        use_and
    )
    if val is not None:
        condition_added = True
        container.append(val)
    
    # allow_if_previous_failed
    val = obj.allow_if_previous_failed
    if val is not None:
        val = create_filter(
            WorkflowStepOrm.allow_if_previous_failed,
            [(val, '==')],
            use_and
        )
    if val is not None:
        condition_added = True
        container.append(val)
    
    # action
    val = obj.action
    val = create_filter(
        WorkflowStepOrm.action,
        [(x, 'like') for x in split_terms(val, RE_SLUG, '%', '%')],
        use_and
    )
    if val is not None:
        condition_added = True
        container.append(val)
    
    # topic_send
    val = obj.topic_send
    val = create_filter(
        WorkflowStepOrm.topic_send,
        [(x, 'like') for x in split_terms(val, RE_SLUG, '%', '%')],
        use_and
    )
    if val is not None:
        condition_added = True
        container.append(val)
    
    # topic_receive
    val = obj.topic_receive
    val = create_filter(
        WorkflowStepOrm.topic_receive,
        [(x, 'like') for x in split_terms(val, RE_SLUG, '%', '%')],
        use_and
    )
    if val is not None:
        condition_added = True
        container.append(val)
    
    # progress
    val = obj.progress
    if isinstance(val, (int, float)):
        val = str(val)
    val = create_filter(
        WorkflowStepOrm.progress,
        split_numeric(val),
        use_and
    )
    if val is not None:
        condition_added = True
        container.append(val)
    
    # status
    val = obj.status
    if isinstance(val, int):
        val = str(val)
    val = create_filter(
        WorkflowStepOrm.status,
        split_numeric(val),
        use_and
    )
    if val is not None:
        condition_added = True
        container.append(val)
    
    # reason
    val = obj.reason
    val = create_filter(
        WorkflowStepOrm.reason,
        [(x, 'like') for x in split_terms(val, RE_ALPHANUM_TERM, '%', '%')],
        use_and
    )
    if val is not None:
        condition_added = True
        container.append(val)
    
    return condition_added


