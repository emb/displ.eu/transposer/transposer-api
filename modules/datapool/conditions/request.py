# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import typing

from regex_patterns import (
    RE_INT_VALUE,
    RE_UUID4,
    RE_ALPHANUM_TERM,
    RE_HASH_XX
)
from data_utils import (
    is_empty, is_true,
    split_terms,
    split_numeric,
    split_datetime,
    sanitize_text,
    validate_uri
)
from ..filter import (
    create_filter
)
from ..models.request import RequestOrm





def add_request_conditions(
    container: list,
    obj: typing.Any,
    use_and: bool = False
) -> bool:
    condition_added = False
    
    # id
    val = obj.id
    if isinstance(val, int):
        val = str(val)
    val = create_filter(
        RequestOrm.id,
        [(x, '==') for x in split_terms(val, RE_INT_VALUE, '', '')],
        use_and
    )
    if val is not None:
        condition_added = True
        container.append(val)
    
    # created_at
    val = obj.created_at
    val = create_filter(
        RequestOrm.created_at,
        split_datetime(val),
        use_and
    )
    if val is not None:
        condition_added = True
        container.append(val)
    
    # owner_id
    val = obj.owner_id
    if isinstance(val, int):
        val = str(val)
    val = create_filter(
        RequestOrm.owner_id,
        [(x, '==') for x in split_terms(val, RE_INT_VALUE, '', '')],
        use_and
    )
    if val is not None:
        condition_added = True
        container.append(val)
    
    # peer_args
    # peer_args is a JSON field. For now we ignore it.
    val = obj.peer_args
    
    # is_async
    val = obj.is_async
    if val is not None:
        val = create_filter(
            RequestOrm.is_async,
            [(val, '==')],
            use_and
        )
    if val is not None:
        condition_added = True
        container.append(val)
    
    # status
    val = obj.status
    if isinstance(val, int):
        val = str(val)
    val = create_filter(
        RequestOrm.status,
        split_numeric(val),
        use_and
    )
    if val is not None:
        condition_added = True
        container.append(val)
    
    # reason
    val = obj.reason
    val = create_filter(
        RequestOrm.reason,
        [(x, 'like') for x in split_terms(val, RE_ALPHANUM_TERM, '%', '%')],
        use_and
    )
    if val is not None:
        condition_added = True
        container.append(val)
    
    return condition_added


