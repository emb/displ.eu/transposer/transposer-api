# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import typing

from regex_patterns import (
    RE_INT_VALUE,
    RE_LANG_CODE,
    RE_UUID4,
    RE_SLUG,
    RE_ALPHANUM_TERM
)
from data_utils import (
    is_empty,
    split_terms,
    split_datetime,
    split_numeric
)
from ..filter import (
    create_filter
)
from ..models.workflow_template import WorkflowTemplateOrm





def add_workflow_template_conditions(
    container: list,
    obj: typing.Any,
    use_and: bool = False
) -> bool:
    condition_added = False

    # id
    val = obj.id
    if isinstance(val, int):
        val = str(val)
    val = create_filter(
        WorkflowTemplateOrm.id,
        [(x, '==') for x in split_terms(val, RE_INT_VALUE, '', '')],
        use_and
    )
    if val is not None:
        condition_added = True
        container.append(val)

    # uuid
    val = obj.uuid
    val = create_filter(
        WorkflowTemplateOrm.uuid,
        [(x, '==') for x in split_terms(val, RE_UUID4, '', '')],
        use_and
    )
    if val is not None:
        condition_added = True
        container.append(val)
    
    # created_at
    val = obj.created_at
    val = create_filter(
        WorkflowTemplateOrm.created_at,
        split_datetime(val),
        use_and
    )
    if val is not None:
        condition_added = True
        container.append(val)
    
    # setting_id
    val = obj.setting_id
    val = create_filter(
        WorkflowTemplateOrm.setting_id,
        [(x, '==') for x in split_terms(val, RE_SLUG, '', '')],
        use_and
    )
    if val is not None:
        condition_added = True
        container.append(val)
    
    # template
    # template is a JSON field. For now we ignore it.
    val = obj.template
    
    return condition_added


