# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import typing

from regex_patterns import (
    RE_INT_VALUE,
    RE_UUID4,
    RE_ALPHANUM_TERM,
    RE_TOKEN,
    RE_HASH_XX
)
from data_utils import (
    is_empty, is_true,
    split_terms,
    split_numeric,
    split_datetime,
    sanitize_text,
    get_datetime_obj
)
from ..filter import (
    create_filter
)
from ..models.peer_bearer_token import PeerBearerTokenOrm





def add_peer_bearer_token_conditions(
    container: list,
    obj: typing.Any,
    use_and: bool = False
) -> bool:
    condition_added = False
    
    # id
    val = obj.id
    if isinstance(val, int):
        val = str(val)
    val = create_filter(
        PeerBearerTokenOrm.id,
        [(x, '==') for x in split_terms(val, RE_INT_VALUE, '', '')],
        use_and
    )
    if val is not None:
        condition_added = True
        container.append(val)
    
    # created_at
    val = obj.created_at
    val = create_filter(
        PeerBearerTokenOrm.created_at,
        split_datetime(val),
        use_and
    )
    if val is not None:
        condition_added = True
        container.append(val)
    
    # peer_id
    val = obj.peer_id
    if isinstance(val, int):
        val = str(val)
    val = create_filter(
        PeerBearerTokenOrm.peer_id,
        [(x, '==') for x in split_terms(val, RE_INT_VALUE, '', '')],
        use_and
    )
    if val is not None:
        condition_added = True
        container.append(val)
    
    # token
    val = obj.token
    val = create_filter(
        PeerBearerTokenOrm.token,
        [(x, 'like') for x in split_terms(val, RE_TOKEN, '%', '%')],
        use_and
    )
    if val is not None:
        condition_added = True
        container.append(val)
    
    # is_active
    val = obj.is_active
    if val is not None:
        val = create_filter(
            PeerBearerTokenOrm.is_active,
            [(val, '==')],
            use_and
        )
    if val is not None:
        condition_added = True
        container.append(val)
    
    # valid_until
    val = obj.valid_until
    val = create_filter(
        PeerBearerTokenOrm.valid_until,
        split_datetime(val),
        use_and
    )
    if val is not None:
        condition_added = True
        container.append(val)
    
    return condition_added


