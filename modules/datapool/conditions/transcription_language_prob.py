# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import typing

from regex_patterns import (
    RE_INT_VALUE,
    RE_FLOAT_VALUE,
    RE_LANG_CODE
)
from data_utils import (
    is_empty, is_true,
    split_terms,
    split_numeric,
    split_datetime
)
from ..filter import (
    create_filter
)
from ..models.transcription_language_prob import TranscriptionLanguageProbOrm





def add_transcription_language_prob_conditions(
    container: list,
    obj: typing.Any,
    use_and: bool = False
) -> bool:
    condition_added = False

    # id
    val = obj.id
    if isinstance(val, int):
        val = str(val)
    val = create_filter(
        TranscriptionLanguageProbOrm.id,
        [(x, '==') for x in split_terms(val, RE_INT_VALUE, '', '')],
        use_and
    )
    if val is not None:
        condition_added = True
        container.append(val)

    # transcription_id
    val = obj.transcription_id
    val = create_filter(
        TranscriptionLanguageProbOrm.transcription_id,
        split_datetime(val),
        use_and
    )
    if val is not None:
        condition_added = True
        container.append(val)
    
    # lang
    val = obj.lang
    val = create_filter(
        TranscriptionLanguageProbOrm.lang,
        [(x, 'like') for x in split_terms(val, RE_LANG_CODE, '%', '%')],
        use_and
    )
    if val is not None:
        condition_added = True
        container.append(val)
    
    # probability
    val = obj.probability
    val = create_filter(
        TranscriptionLanguageProbOrm.probability,
        split_numeric(val),
        use_and
    )
    if val is not None:
        condition_added = True
        container.append(val)
    
    return condition_added


