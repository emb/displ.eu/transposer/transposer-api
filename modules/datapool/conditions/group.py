# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import typing

from regex_patterns import (
    RE_INT_VALUE,
    RE_ALPHANUM_TERM
)
from data_utils import (
    split_terms,
    sanitize_text
)
from ..filter import (
    create_filter
)
from ..models.group import GroupOrm





def add_group_conditions(
    container: list,
    obj: typing.Any,
    use_and: bool = False
) -> bool:
    condition_added = False

    # id
    val = obj.id
    if isinstance(val, int):
        val = str(val)
    val = create_filter(
        GroupOrm.id,
        [(x, '==') for x in split_terms(val, RE_INT_VALUE, '', '')],
        use_and
    )
    if val is not None:
        condition_added = True
        container.append(val)

    # name
    val = obj.name
    val = create_filter(
        GroupOrm.name,
        [(x, 'like') for x in split_terms(val, RE_ALPHANUM_TERM, '%', '%')],
        use_and
    )
    if val is not None:
        condition_added = True
        container.append(val)
    
    return condition_added


