# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import re
import typing
from .orm import TranscriptionSpeechActivityOrm





TRANSCRIPTION_SPEECH_ACTIVITY__FIELDS_MAP = {
    'transcription_speech_activity': {
        'id': TranscriptionSpeechActivityOrm.id,
        'transcription': TranscriptionSpeechActivityOrm.transcription_id,
        'transcription_id': TranscriptionSpeechActivityOrm.transcription_id,
        'pos': TranscriptionSpeechActivityOrm.pos,
        'start': TranscriptionSpeechActivityOrm.start,
        'end': TranscriptionSpeechActivityOrm.end
    },
    'transcription_speech_activity_id': TranscriptionSpeechActivityOrm.id,
    'transcription_speech_activity_transcription': TranscriptionSpeechActivityOrm.transcription_id,
    'transcription_speech_activity_transcription_id': TranscriptionSpeechActivityOrm.transcription_id,
    'transcription_speech_activity_pos': TranscriptionSpeechActivityOrm.pos,
    'transcription_speech_activity_start': TranscriptionSpeechActivityOrm.start,
    'transcription_speech_activity_end': TranscriptionSpeechActivityOrm.end,

    'transcription_speech_activity.id': TranscriptionSpeechActivityOrm.id,
    'transcription_speech_activity.transcription': TranscriptionSpeechActivityOrm.transcription_id,
    'transcription_speech_activity.transcription_id': TranscriptionSpeechActivityOrm.transcription_id,
    'transcription_speech_activity.pos': TranscriptionSpeechActivityOrm.pos,
    'transcription_speech_activity.start': TranscriptionSpeechActivityOrm.start,
    'transcription_speech_activity.end': TranscriptionSpeechActivityOrm.end
}

TRANSCRIPTION_SPEECH_ACTIVITY__VARIANTS_MAP = {
    'transcription_speech_activity': {
        'id': 'transcription_speech_activity.id',
        'transcription': 'transcription_speech_activity.transcription_id',
        'transcription_id': 'transcription_speech_activity.transcription_id',
        'pos': 'transcription_speech_activity.pos',
        'start': 'transcription_speech_activity.start',
        'end': 'transcription_speech_activity.end'
    },
    'transcription_speech_activity_id': 'transcription_speech_activity.id',
    'transcription_speech_activity_transcription': 'transcription_speech_activity.transcription_id',
    'transcription_speech_activity_transcription_id': 'transcription_speech_activity.transcription_id',
    'transcription_speech_activity_pos': 'transcription_speech_activity.pos',
    'transcription_speech_activity_start': 'transcription_speech_activity.start',
    'transcription_speech_activity_end': 'transcription_speech_activity.end',

    'transcription_speech_activity.id': 'transcription_speech_activity.id',
    'transcription_speech_activity.transcription': 'transcription_speech_activity.transcription_id',
    'transcription_speech_activity.transcription_id': 'transcription_speech_activity.transcription_id',
    'transcription_speech_activity.pos': 'transcription_speech_activity.pos',
    'transcription_speech_activity.start': 'transcription_speech_activity.start',
    'transcription_speech_activity.end': 'transcription_speech_activity.end'
}



############
### Primary

TRANSCRIPTION_SPEECH_ACTIVITY__PRIMARY_FIELDS_MAP = {
    'id': TranscriptionSpeechActivityOrm.id,
    'transcription': TranscriptionSpeechActivityOrm.transcription_id,
    'transcription_id': TranscriptionSpeechActivityOrm.transcription_id,
    'pos': TranscriptionSpeechActivityOrm.pos,
    'start': TranscriptionSpeechActivityOrm.start,
    'end': TranscriptionSpeechActivityOrm.end
}

TRANSCRIPTION_SPEECH_ACTIVITY__PRIMARY_VARIANTS_MAP = {
    'id': 'transcription_speech_activity.id',
    'transcription': 'transcription_speech_activity.transcription_id',
    'transcription_id': 'transcription_speech_activity.transcription_id',
    'pos': 'transcription_speech_activity.pos',
    'start': 'transcription_speech_activity.start',
    'end': 'transcription_speech_activity.end'
}


