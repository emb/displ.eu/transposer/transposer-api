# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import typing

import sqlalchemy
from sqlalchemy.orm import (
    Mapped,
    mapped_column,
    relationship,
    validates
)

from regex_patterns import (
    RE_INT_VALUE,
    RE_FLOAT_VALUE
)
from data_utils import (
    is_empty,
    split_terms,
    split_numeric,
    split_datetime
)
from ..base import BaseOrm





class TranscriptionSpeechActivityOrm(BaseOrm):
    __tablename__ = 'transcription_speech_activities'
    id: Mapped[int] = mapped_column(
        sqlalchemy.Integer, primary_key=True
    )
    transcription_id: Mapped[int] = mapped_column(sqlalchemy.ForeignKey('transcriptions.id'))
    pos: Mapped[int] = mapped_column(sqlalchemy.Integer, default=0)
    start: Mapped[float] = mapped_column(sqlalchemy.Float, default=-1.0)
    end: Mapped[float] = mapped_column(sqlalchemy.Float, default=-1.0)

    transcription: Mapped['TranscriptionOrm'] = relationship(back_populates='speech_activities')
    
    def __repr__(self):
        return (
            f'<TranscriptionSpeechActivity('
            f'id={self.id}, '
            f'transcription_id={self.transcription_id}, '
            f'pos={self.pos}, '
            f'start={self.start}, '
            f'end={self.end})>'
        )
    
    def __str__(self):
        return (
            f'TranscriptionSpeechActivity('
            f'id={self.id}, '
            f'transcription_id={self.transcription_id}, '
            f'pos={self.pos}, '
            f'start={self.start}, '
            f'end={self.end})'
        )
    
    def __json__(self):
        return {
            'id': self.id,
            'transcription_id': self.transcription_id,
            'pos': self.pos,
            'start': self.start,
            'end': self.end
        }
    
    def _fields(self) -> tuple:
        return (
            ['id', 'transcription_id', 'pos', 'start', 'end'],
            [('transcription', False, None)]
        )
    

    @validates('transcription_id')
    def validate_transcription_id(self, key, value):
        if is_empty(value):
            raise ValueError("Transcription ID is required")
        if isinstance(value, int):
            return value
        if isinstance(value, str):
            result = RE_INT_VALUE.match(value)
            if result is None:
                raise ValueError("Invalid transcription ID")
            return int(result.group(1))
        raise ValueError("Invalid transcription ID")

    @validates('pos')
    def validate_pos(self, key, value):
        if is_empty(value):
            raise ValueError("Position is required")
        if isinstance(value, int):
            return value
        if isinstance(value, str):
            result = RE_INT_VALUE.match(value)
            if result is None:
                raise ValueError("Invalid position")
            return int(result.group(1))
        raise ValueError("Invalid position")

    @validates('start')
    def validate_start(self, key, value):
        if is_empty(value):
            raise ValueError("Start is required")
        if isinstance(value, (int, float)):
            return float(value)
        if isinstance(value, str):
            result = RE_FLOAT_VALUE.match(value)
            if result is None:
                raise ValueError("Invalid start")
            return float(result.group(1))
        raise ValueError("Invalid start")

    @validates('end')
    def validate_end(self, key, value):
        if is_empty(value):
            raise ValueError("End is required")
        if isinstance(value, (int, float)):
            return float(value)
        if isinstance(value, str):
            result = RE_FLOAT_VALUE.match(value)
            if result is None:
                raise ValueError("Invalid end")
            return float(result.group(1))
        raise ValueError("Invalid end")




