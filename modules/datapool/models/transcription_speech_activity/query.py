# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import typing

from regex_patterns import (
    RE_INT_VALUE
)
from data_utils import (
    split_terms,
    split_numeric
)
from ..base import BaseQueryParams
from .orm import TranscriptionSpeechActivityOrm





class TranscriptionSpeechActivityQueryParams(BaseQueryParams):
    __slots__ = [
        '_id',
        '_transcription_id',
        '_pos',
        '_start',
        '_end',

        '_id_stmt',
        '_transcription_id_stmt',
        '_pos_stmt',
        '_start_stmt',
        '_end_stmt'
    ]

    def __init__(self, 
        id: int | str | None = None,
        transcription_id: int | str | None = None,
        pos: int | str | None = None,
        start: int | float | str | None = None,
        end: int | float | str | None = None
    ):
        super().__init__()

        # Store raw values
        self._id = id
        self._transcription_id = transcription_id
        self._pos = pos
        self._start = start
        self._end = end

        # Init statement values
        self._id_stmt = None
        self._transcription_id_stmt = None
        self._pos_stmt = None
        self._start_stmt = None
        self._end_stmt = None


    def add_conditions(self, container: list, use_and: bool = False) -> bool:

        # id
        if self._id_stmt is not None:
            container.append(self._id_stmt)
        else:
            val = self._id
            if isinstance(val, int):
                val = str(val)
            val = self.create_filter(
                TranscriptionSpeechActivityOrm.id,
                [(x, '==') for x in split_terms(val, RE_INT_VALUE, '', '')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._id_stmt = val
                container.append(val)

        # transcription_id
        if self._transcription_id_stmt is not None:
            container.append(self._transcription_id_stmt)
        else:
            val = self._transcription_id
            if isinstance(val, int):
                val = str(val)
            val = self.create_filter(
                TranscriptionSpeechActivityOrm.transcription_id,
                [(x, '==') for x in split_terms(val, RE_INT_VALUE, '', '')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._transcription_id_stmt = val
                container.append(val)
        
        # pos
        if self._pos_stmt is not None:
            container.append(self._pos_stmt)
        else:
            val = self._pos
            if isinstance(val, int):
                val = str(val)
            val = self.create_filter(
                TranscriptionSpeechActivityOrm.pos,
                split_numeric(val),
                use_and
            )
            if val is not None:
                self._has_params = True
                self._pos_stmt = val
                container.append(val)
        
        # start
        if self._start_stmt is not None:
            container.append(self._start_stmt)
        else:
            val = self._start
            if isinstance(val, (int, float)):
                val = str(val)
            val = self.create_filter(
                TranscriptionSpeechActivityOrm.start,
                split_numeric(val),
                use_and
            )
            if val is not None:
                self._has_params = True
                self._start_stmt = val
                container.append(val)
        
        # end
        if self._end_stmt is not None:
            container.append(self._end_stmt)
        else:
            val = self._end
            if isinstance(val, (int, float)):
                val = str(val)
            val = self.create_filter(
                TranscriptionSpeechActivityOrm.end,
                split_numeric(val),
                use_and
            )
            if val is not None:
                self._has_params = True
                self._end_stmt = val
                container.append(val)

        # Return True if conditions were added
        return self._has_params
    

    # Getters for raw values

    @property
    def id(self) -> int | str | None:
        return self._id
    
    @property
    def transcription_id(self) -> int | str | None:
        return self._transcription_id
    
    @property
    def pos(self) -> int | str | None:
        return self._pos
    
    @property
    def start(self) -> int | float | str | None:
        return self._start
    
    @property
    def end(self) -> int | float | str | None:
        return self._end


    # Getters for statements
    
    @property
    def id_stmt(self) -> typing.Any:
        return self._id_stmt
    
    @property
    def transcription_id_stmt(self) -> typing.Any:
        return self._transcription_id_stmt
    
    @property
    def pos_stmt(self) -> typing.Any:
        return self._pos_stmt
    
    @property
    def start_stmt(self) -> typing.Any:
        return self._start_stmt
    
    @property
    def end_stmt(self) -> typing.Any:
        return self._end_stmt



### Define Interfaces

class TranscriptionSpeechActivityQueryParamsPrimary(TranscriptionSpeechActivityQueryParams):
    def __init__(self,
        id: int | str | None = None,
        transcription: int | str | None = None,
        transcription_id: int | str | None = None,
        pos: int | str | None = None,
        start: int | float | str | None = None,
        end: int | float | str | None = None
    ):
        super().__init__(
            id,
            transcription_id if transcription_id is not None else transcription,
            pos,
            start,
            end
        )


class TranscriptionSpeechActivityQueryParamsGeneral(TranscriptionSpeechActivityQueryParams):
    def __init__(self,
        transcription_speech_activity_id: int | str | None = None,
        transcription_speech_activity_transcription: int | str | None = None,
        transcription_speech_activity_transcription_id: int | str | None = None,
        transcription_speech_activity_pos: int | str | None = None,
        transcription_speech_activity_start: int | float | str | None = None,
        transcription_speech_activity_end: int | float | str | None = None
    ):
        super().__init__(
            transcription_speech_activity_id,
            transcription_speech_activity_transcription_id if transcription_speech_activity_transcription_id is not None else transcription_speech_activity_transcription,
            transcription_speech_activity_pos,
            transcription_speech_activity_start,
            transcription_speech_activity_end
        )


