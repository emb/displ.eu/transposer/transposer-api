# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

from .orm import PipelineConsumerOptionOrm





PIPELINE_CONSUMER_OPTION__FIELDS_MAP = {
    'pipeline_consumer_option': {
        'id': PipelineConsumerOptionOrm.id,
        'parent': PipelineConsumerOptionOrm.parent_id,
        'parent_id': PipelineConsumerOptionOrm.parent_id,
        'key': PipelineConsumerOptionOrm.key,
        'value': PipelineConsumerOptionOrm.value,
        'datatype': PipelineConsumerOptionOrm.datatype
    },
    'pipeline_consumer_option_id': PipelineConsumerOptionOrm.id,
    'pipeline_consumer_option_parent': PipelineConsumerOptionOrm.parent_id,
    'pipeline_consumer_option_parent_id': PipelineConsumerOptionOrm.parent_id,
    'pipeline_consumer_option_key': PipelineConsumerOptionOrm.key,
    'pipeline_consumer_option_value': PipelineConsumerOptionOrm.value,
    'pipeline_consumer_option_datatype': PipelineConsumerOptionOrm.datatype,

    'pipeline_consumer_option.id': PipelineConsumerOptionOrm.id,
    'pipeline_consumer_option.parent': PipelineConsumerOptionOrm.parent_id,
    'pipeline_consumer_option.parent_id': PipelineConsumerOptionOrm.parent_id,
    'pipeline_consumer_option.key': PipelineConsumerOptionOrm.key,
    'pipeline_consumer_option.value': PipelineConsumerOptionOrm.value,
    'pipeline_consumer_option.datatype': PipelineConsumerOptionOrm.datatype
}

PIPELINE_CONSUMER_OPTION__VARIANTS_MAP = {
    'pipeline_consumer_option': {
        'id': 'pipeline_consumer_option.id',
        'parent': 'pipeline_consumer_option.parent_id',
        'parent_id': 'pipeline_consumer_option.parent_id',
        'key': 'pipeline_consumer_option.key',
        'value': 'pipeline_consumer_option.value',
        'datatype': 'pipeline_consumer_option.datatype'
    },
    'pipeline_consumer_option_id': 'pipeline_consumer_option.id',
    'pipeline_consumer_option_parent': 'pipeline_consumer_option.parent_id',
    'pipeline_consumer_option_parent_id': 'pipeline_consumer_option.parent_id',
    'pipeline_consumer_option_key': 'pipeline_consumer_option.key',
    'pipeline_consumer_option_value': 'pipeline_consumer_option.value',
    'pipeline_consumer_option_datatype': 'pipeline_consumer_option.datatype',

    'pipeline_consumer_option.id': 'pipeline_consumer_option.id',
    'pipeline_consumer_option.parent': 'pipeline_consumer_option.parent_id',
    'pipeline_consumer_option.parent_id': 'pipeline_consumer_option.parent_id',
    'pipeline_consumer_option.key': 'pipeline_consumer_option.key',
    'pipeline_consumer_option.value': 'pipeline_consumer_option.value',
    'pipeline_consumer_option.datatype': 'pipeline_consumer_option.datatype'
}



############
### Primary

PIPELINE_CONSUMER_OPTION__PRIMARY_FIELDS_MAP = {
    'id': PipelineConsumerOptionOrm.id,
    'parent': PipelineConsumerOptionOrm.parent_id,
    'parent_id': PipelineConsumerOptionOrm.parent_id,
    'key': PipelineConsumerOptionOrm.key,
    'value': PipelineConsumerOptionOrm.value,
    'datatype': PipelineConsumerOptionOrm.datatype
}

PIPELINE_CONSUMER_OPTION__PRIMARY_VARIANTS_MAP = {
    'id': 'pipeline_consumer_option.id',
    'parent': 'pipeline_consumer_option.parent_id',
    'parent_id': 'pipeline_consumer_option.parent_id',
    'key': 'pipeline_consumer_option.key',
    'value': 'pipeline_consumer_option.value',
    'datatype': 'pipeline_consumer_option.datatype'
}


