# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

from .orm import WorkflowOrm





WORKFLOW__FIELDS_MAP = {
    'workflow': {
        'id': WorkflowOrm.id,
        'uuid': WorkflowOrm.uuid,
        'created_at': WorkflowOrm.created_at,
        'document': WorkflowOrm.document_id,
        'document_id': WorkflowOrm.document_id,
        'progress': WorkflowOrm.progress,
        'status': WorkflowOrm.status,
        'reason': WorkflowOrm.reason
    },
    'workflow_id': WorkflowOrm.id,
    'workflow_uuid': WorkflowOrm.uuid,
    'workflow_created_at': WorkflowOrm.created_at,
    'workflow_document': WorkflowOrm.document_id,
    'workflow_document_id': WorkflowOrm.document_id,
    'workflow_progress': WorkflowOrm.progress,
    'workflow_status': WorkflowOrm.status,
    'workflow_reason': WorkflowOrm.reason,

    'workflow.id': WorkflowOrm.id,
    'workflow.uuid': WorkflowOrm.uuid,
    'workflow.created_at': WorkflowOrm.created_at,
    'workflow.document': WorkflowOrm.document_id,
    'workflow.document_id': WorkflowOrm.document_id,
    'workflow.progress': WorkflowOrm.progress,
    'workflow.status': WorkflowOrm.status,
    'workflow.reason': WorkflowOrm.reason
}

WORKFLOW__VARIANTS_MAP = {
    'workflow': {
        'id': 'workflow.id',
        'uuid': 'workflow.uuid',
        'created_at': 'workflow.created_at',
        'document': 'workflow.document_id',
        'document_id': 'workflow.document_id',
        'progress': 'workflow.progress',
        'status': 'workflow.status',
        'reason': 'workflow.reason'
    },
    'workflow_id': 'workflow.id',
    'workflow_uuid': 'workflow.uuid',
    'workflow_created_at': 'workflow.created_at',
    'workflow_document': 'workflow.document_id',
    'workflow_document_id': 'workflow.document_id',
    'workflow_progress': 'workflow.progress',
    'workflow_status': 'workflow.status',
    'workflow_reason': 'workflow.reason',

    'workflow.id': 'workflow.id',
    'workflow.uuid': 'workflow.uuid',
    'workflow.created_at': 'workflow.created_at',
    'workflow.document': 'workflow.document_id',
    'workflow.document_id': 'workflow.document_id',
    'workflow.progress': 'workflow.progress',
    'workflow.status': 'workflow.status',
    'workflow.reason': 'workflow.reason'
}



############
### Primary

WORKFLOW__PRIMARY_FIELDS_MAP = {
    'id': WorkflowOrm.id,
    'uuid': WorkflowOrm.uuid,
    'created_at': WorkflowOrm.created_at,
    'document': WorkflowOrm.document_id,
    'document_id': WorkflowOrm.document_id,
    'progress': WorkflowOrm.progress,
    'status': WorkflowOrm.status,
    'reason': WorkflowOrm.reason
}

WORKFLOW__PRIMARY_VARIANTS_MAP = {
    'id': 'workflow.id',
    'uuid': 'workflow.uuid',
    'created_at': 'workflow.created_at',
    'document': 'workflow.document_id',
    'document_id': 'workflow.document_id',
    'progress': 'workflow.progress',
    'status': 'workflow.status',
    'reason': 'workflow.reason'
}


