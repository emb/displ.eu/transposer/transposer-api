# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import typing
import datetime

from regex_patterns import (
    RE_INT_VALUE,
    RE_LANG_CODE,
    RE_UUID4,
    RE_ALPHANUM_TERM
)
from data_utils import (
    split_terms,
    split_datetime,
    split_numeric
)
from ..base import (
    BaseQueryParams
)
from .orm import WorkflowOrm





class WorkflowQueryParams(BaseQueryParams):
    __slots__ = [
        '_id',
        '_uuid',
        '_created_at',
        '_document_id',
        '_progress',
        '_status',
        '_reason',

        '_id_stmt',
        '_uuid_stmt',
        '_created_at_stmt',
        '_document_id_stmt',
        '_progress_stmt',
        '_status_stmt',
        '_reason_stmt'
    ]

    def __init__(self, 
        id: int | str | None = None,
        uuid: str | None = None,
        created_at: int | float | str | None = None,
        document_id: int | str | None = None,
        progress: int | float | str | None = None,
        status: int | str | None = None,
        reason: str | None = None
    ):
        super().__init__()

        # Store raw values
        self._id = id
        self._uuid = uuid
        self._created_at = created_at
        self._document_id = document_id
        self._progress = progress
        self._status = status
        self._reason = reason

        # Init statement values
        self._id_stmt = None
        self._uuid_stmt = None
        self._created_at_stmt = None
        self._document_id_stmt = None
        self._progress_stmt = None
        self._status_stmt = None
        self._reason_stmt = None


    def add_conditions(self, container: list, use_and: bool = False) -> bool:

        # id
        if self._id_stmt is not None:
            container.append(self._id_stmt)
        else:
            val = self._id
            if isinstance(val, int):
                val = str(val)
            val = self.create_filter(
                WorkflowOrm.id,
                [(x, '==') for x in split_terms(val, RE_INT_VALUE, '', '')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._id_stmt = val
                container.append(val)

        # uuid
        if self._uuid_stmt is not None:
            container.append(self._uuid_stmt)
        else:
            val = self._uuid
            val = self.create_filter(
                WorkflowOrm.uuid,
                [(x, '==') for x in split_terms(val, RE_UUID4, '', '')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._uuid_stmt = val
                container.append(val)
        
        # created_at
        if self._created_at_stmt is not None:
            container.append(self._created_at_stmt)
        else:
            val = self._created_at
            val = self.create_filter(
                WorkflowOrm.created_at,
                split_datetime(val),
                use_and
            )
            if val is not None:
                self._has_params = True
                self._created_at_stmt = val
                container.append(val)

        # document_id
        if self._document_id_stmt is not None:
            container.append(self._document_id_stmt)
        else:
            val = self._document_id
            if isinstance(val, int):
                val = str(val)
            val = self.create_filter(
                WorkflowOrm.document_id,
                [(x, '==') for x in split_terms(val, RE_INT_VALUE, '', '')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._document_id_stmt = val
                container.append(val)

        # progress
        if self._progress_stmt is not None:
            container.append(self._progress_stmt)
        else:
            val = self._progress
            if isinstance(val, (int, float)):
                val = str(val)
            val = self.create_filter(
                WorkflowOrm.progress,
                split_numeric(val),
                use_and
            )
            if val is not None:
                self._has_params = True
                self._progress_stmt = val
                container.append(val)
        
        # status
        if self._status_stmt is not None:
            container.append(self._status_stmt)
        else:
            val = self._status
            if isinstance(val, int):
                val = str(val)
            val = self.create_filter(
                WorkflowOrm.status,
                split_numeric(val),
                use_and
            )
            if val is not None:
                self._has_params = True
                self._status_stmt = val
                container.append(val)
        
        # reason
        if self._reason_stmt is not None:
            container.append(self._reason_stmt)
        else:
            val = self._reason
            val = self.create_filter(
                WorkflowOrm.reason,
                [(x, 'like') for x in split_terms(val, RE_ALPHANUM_TERM, '%', '%')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._reason_stmt = val
                container.append(val)

        # Return True if conditions were added
        return self._has_params
    

    # Getters for raw values

    @property
    def id(self) -> int | str | None:
        return self._id
    
    @property
    def uuid(self) -> str | None:
        return self._uuid
    
    @property
    def created_at(self) -> int | float | str | None:
        return self._created_at

    @property
    def document_id(self) -> int | str | None:
        return self._document_id
    
    @property
    def progress(self) -> int | float | str | None:
        return self._progress
    
    @property
    def status(self) -> int | str | None:
        return self._status
    
    @property
    def reason(self) -> str | None:
        return self._reason
    

    # Getters for statements
    
    @property
    def id_stmt(self) -> typing.Any:
        return self._id_stmt
    
    @property
    def uuid_stmt(self) -> typing.Any:
        return self._uuid_stmt
    
    @property
    def created_at_stmt(self) -> typing.Any:
        return self._created_at_stmt
    
    @property
    def document_id_stmt(self) -> typing.Any:
        return self._document_id_stmt
    
    @property
    def progress_stmt(self) -> typing.Any:
        return self._progress_stmt
    
    @property
    def status_stmt(self) -> typing.Any:
        return self._status_stmt
    
    @property
    def reason_stmt(self) -> typing.Any:
        return self._reason_stmt



### Define Interfaces

class WorkflowQueryParamsPrimary(WorkflowQueryParams):
    def __init__(self,
        id: int | str | None = None,
        uuid: str | None = None,
        created_at: int | float | str | None = None,
        document: int | str | None = None,
        document_id: int | str | None = None,
        progress: int | float | str | None = None,
        status: int | str | None = None,
        reason: str | None = None
    ):
        super().__init__(
            id,
            uuid,
            created_at,
            document_id if document_id is not None else document,
            progress,
            status,
            reason
        )


class WorkflowQueryParamsGeneral(WorkflowQueryParams):
    def __init__(self,
        workflow_id: int | str | None = None,
        workflow_uuid: str | None = None,
        workflow_created_at: int | float | str | None = None,
        workflow_document: int | str | None = None,
        workflow_document_id: int | str | None = None,
        workflow_progress: int | float | str | None = None,
        workflow_status: int | str | None = None,
        workflow_reason: str | None = None
    ):
        super().__init__(
            workflow_id,
            workflow_uuid,
            workflow_created_at,
            workflow_document_id if workflow_document_id is not None else workflow_document,
            workflow_progress,
            workflow_status,
            workflow_reason
        )


