# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import typing

import sqlalchemy
from sqlalchemy.orm import (
    Mapped,
    mapped_column,
    relationship,
    validates
)

from regex_patterns import (
    RE_INT_VALUE,
    RE_ALPHANUM_TERM
)
from data_utils import (
    is_empty,
    split_terms,
    sanitize_text
)
from ..base import BaseOrm
from ..links import (
    user_group_link_table,
    group_role_link_table
)





class GroupOrm(BaseOrm):
    __tablename__ = 'groups'
    id: Mapped[int] = mapped_column(
        sqlalchemy.Integer, primary_key=True
    )
    name: Mapped[str] = mapped_column(sqlalchemy.String)

    users: Mapped[typing.List['UserOrm']] = relationship(
        'UserOrm',
        secondary=user_group_link_table,
        back_populates='groups'
    )
    roles: Mapped[typing.List['RoleOrm']] = relationship(
        'RoleOrm',
        secondary=group_role_link_table,
        back_populates='groups'
    )
    
    def __repr__(self):
        return (
            f'<Group('
            f'id={self.id}, '
            f'name={self.name})>'
        )
    
    def __str__(self):
        return (
            f'Group('
            f'id={self.id}, '
            f'name={self.name})'
        )
    
    def __json__(self):
        return {
            'id': self.id,
            'name': self.name
        }
    
    def _fields(self) -> tuple:
        return (
            ['id', 'name'],
            [('users', True, None), ('roles', True, None)]
        )
    

    @validates('name')
    def validate_name(self, key, value):
        if is_empty(value):
            raise ValueError("Name is required")
        if isinstance(value, str):
            result = RE_ALPHANUM_TERM.match(value)
            if result is None:
                raise ValueError("Invalid Name")
            return result.group(1)
        raise ValueError("Invalid Name")



