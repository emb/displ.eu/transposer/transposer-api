# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import re
import typing
from .orm import GroupOrm





GROUP__FIELDS_MAP = {
    'group': {
        'id': GroupOrm.id,
        'name': GroupOrm.name
    },
    'group_id': GroupOrm.id,
    'group_name': GroupOrm.name,

    'group.id': GroupOrm.id,
    'group.name': GroupOrm.name
}

GROUP__VARIANTS_MAP = {
    'group': {
        'id': 'group.id',
        'name': 'group.name'
    },
    'group_id': 'group.id',
    'group_name': 'group.name',

    'group.id': 'group.id',
    'group.name': 'group.name'
}



############
### Primary

GROUP__PRIMARY_FIELDS_MAP = {
    'id': GroupOrm.id,
    'name': GroupOrm.name
}

GROUP__PRIMARY_VARIANTS_MAP = {
    'id': 'group.id',
    'name': 'group.name'
}


