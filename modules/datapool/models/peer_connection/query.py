# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import typing
import datetime

from regex_patterns import (
    RE_INT_VALUE,
    RE_SLUG
)
from data_utils import (
    split_terms,
    split_datetime
)
from ..base import (
    BaseQueryParams
)
from .orm import PeerConnectionOrm





class PeerConnectionQueryParams(BaseQueryParams):
    __slots__ = [
        '_id',
        '_created_at',
        '_peer_id',
        '_key',

        '_id_stmt',
        '_created_at_stmt',
        '_peer_id_stmt',
        '_key_stmt'
    ]

    def __init__(self, 
        id: int | str | None = None,
        created_at: int | float | str | None = None,
        peer_id: str | None = None,
        key: str | None = None
    ):
        super().__init__()

        # Store raw values
        self._id = id
        self._created_at = created_at
        self._peer_id = peer_id
        self._key = key

        # Init statement values
        self._id_stmt = None
        self._created_at_stmt = None
        self._peer_id_stmt = None
        self._key_stmt = None


    def add_conditions(self, container: list, use_and: bool = False) -> bool:

        # id
        if self._id_stmt is not None:
            container.append(self._id_stmt)
        else:
            val = self._id
            if isinstance(val, int):
                val = str(val)
            val = self.create_filter(
                PeerConnectionOrm.id,
                [(x, '==') for x in split_terms(val, RE_INT_VALUE, '', '')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._id_stmt = val
                container.append(val)
        
        # created_at
        if self._created_at_stmt is not None:
            container.append(self._created_at_stmt)
        else:
            val = self._created_at
            val = self.create_filter(
                PeerConnectionOrm.created_at,
                split_datetime(val),
                use_and
            )
            if val is not None:
                self._has_params = True
                self._created_at_stmt = val
                container.append(val)

        # peer_id
        if self._peer_id_stmt is not None:
            container.append(self._peer_id_stmt)
        else:
            val = self._peer_id
            if isinstance(val, int):
                val = str(val)
            val = self.create_filter(
                PeerConnectionOrm.peer_id,
                [(x, '==') for x in split_terms(val, RE_INT_VALUE, '', '')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._peer_id_stmt = val
                container.append(val)

        # key
        if self._key_stmt is not None:
            container.append(self._key_stmt)
        else:
            val = self._key
            val = self.create_filter(
                PeerConnectionOrm.key,
                [(x, 'like') for x in split_terms(val, RE_SLUG, '%', '%')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._key_stmt = val
                container.append(val)

        # Return True if conditions were added
        return self._has_params
    

    # Getters for raw values

    @property
    def id(self) -> int | str | None:
        return self._id
    
    @property
    def created_at(self) -> int | float | str | None:
        return self._created_at
    
    @property
    def peer_id(self) -> int | str | None:
        return self._peer_id
    
    @property
    def key(self) -> str | None:
        return self._key
    

    # Getters for statements
    
    @property
    def id_stmt(self) -> typing.Any:
        return self._id_stmt
    
    @property
    def created_at_stmt(self) -> typing.Any:
        return self._created_at_stmt
    
    @property
    def peer_id_stmt(self) -> typing.Any:
        return self._peer_id_stmt
    
    @property
    def key_stmt(self) -> typing.Any:
        return self._key_stmt



### Define Interfaces

class PeerConnectionQueryParamsPrimary(PeerConnectionQueryParams):
    def __init__(self,
        id: int | str | None = None,
        created_at: int | float | str | None = None,
        peer_id: int | str | None = None,
        key: str | None = None
    ):
        super().__init__(
            id,
            created_at,
            peer_id,
            key
        )


class PeerConnectionQueryParamsGeneral(PeerConnectionQueryParams):
    def __init__(self,
        peer_connection_id: int | str | None = None,
        peer_connection_created_at: int | float | str | None = None,
        peer_connection_peer_id: int | str | None = None,
        peer_connection_key: str | None = None
    ):
        super().__init__(
            peer_connection_id,
            peer_connection_created_at,
            peer_connection_peer_id,
            peer_connection_key
        )


