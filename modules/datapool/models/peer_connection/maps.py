# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import re
import typing
from .orm import PeerConnectionOrm





PEER_CONNECTION__FIELDS_MAP = {
    'peer_connection': {
        'id': PeerConnectionOrm.id,
        'created_at': PeerConnectionOrm.created_at,
        'peer': PeerConnectionOrm.peer_id,
        'peer_id': PeerConnectionOrm.peer_id,
        'key': PeerConnectionOrm.key
    },
    'peer_connection_id': PeerConnectionOrm.id,
    'peer_connection_created_at': PeerConnectionOrm.created_at,
    'peer_connection_peer': PeerConnectionOrm.peer_id,
    'peer_connection_peer_id': PeerConnectionOrm.peer_id,
    'peer_connection_key': PeerConnectionOrm.key,

    'peer_connection.id': PeerConnectionOrm.id,
    'peer_connection.created_at': PeerConnectionOrm.created_at,
    'peer_connection.peer': PeerConnectionOrm.peer_id,
    'peer_connection.peer_id': PeerConnectionOrm.peer_id,
    'peer_connection.key': PeerConnectionOrm.key
}

PEER_CONNECTION__VARIANTS_MAP = {
    'peer_connection': {
        'id': 'peer_connection.id',
        'created_at': 'peer_connection.created_at',
        'peer': 'peer_connection.peer_id',
        'peer_id': 'peer_connection.peer_id',
        'key': 'peer_connection.key'
    },
    'peer_connection_id': 'peer_connection.id',
    'peer_connection_created_at': 'peer_connection.created_at',
    'peer_connection_peer': 'peer_connection.peer_id',
    'peer_connection_peer_id': 'peer_connection.peer_id',
    'peer_connection_key': 'peer_connection.key',

    'peer_connection.id': 'peer_connection.id',
    'peer_connection.created_at': 'peer_connection.created_at',
    'peer_connection.peer': 'peer_connection.peer_id',
    'peer_connection.peer_id': 'peer_connection.peer_id',
    'peer_connection.key': 'peer_connection.key'
}



############
### Primary

PEER_CONNECTION__PRIMARY_FIELDS_MAP = {
    'id': PeerConnectionOrm.id,
    'created_at': PeerConnectionOrm.created_at,
    'peer': PeerConnectionOrm.peer_id,
    'peer_id': PeerConnectionOrm.peer_id,
    'key': PeerConnectionOrm.key
}

PEER_CONNECTION__PRIMARY_VARIANTS_MAP = {
    'id': 'peer_connection.id',
    'created_at': 'peer_connection.created_at',
    'peer': 'peer_connection.peer_id',
    'peer_id': 'peer_connection.peer_id',
    'key': 'peer_connection.key'
}


