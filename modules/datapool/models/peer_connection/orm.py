# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import typing
import datetime

import sqlalchemy
from sqlalchemy.orm import (
    Mapped,
    mapped_column,
    relationship,
    validates
)

from regex_patterns import (
    RE_INT_VALUE,
    RE_SLUG,
    RE_URI_SCHEMES,
    RE_DOMAIN,
    RE_IPV4,
    RE_IPV6,
    RE_FIELD_NAME
)
from data_utils import (
    is_empty,
    split_terms,
    split_datetime
)
from ..base import BaseOrm
from ..links import request_connection_link_table
from ..links import job_connection_link_table





class PeerConnectionOrm(BaseOrm):
    __tablename__ = 'peer_connections'
    id: Mapped[int] = mapped_column(
        sqlalchemy.Integer, primary_key=True
    )
    created_at: Mapped[datetime.datetime] = mapped_column(
        sqlalchemy.DateTime, default=sqlalchemy.sql.functions.now()
    )
    peer_id: Mapped[int] = mapped_column(sqlalchemy.ForeignKey('peers.id'))
    key: Mapped[str] = mapped_column(sqlalchemy.String)

    peer: Mapped['PeerOrm'] = relationship(
        back_populates='connections'
    )
    paths: Mapped[typing.List['PeerConnectionPathOrm']] = relationship(
        back_populates='connection',
        primaryjoin="(PeerConnectionOrm.id==PeerConnectionPathOrm.connection_id)"
    )
    requests: Mapped[typing.List['RequestOrm']] = relationship(
        'RequestOrm',
        secondary=request_connection_link_table,
        back_populates='connections'
    )
    jobs: Mapped[typing.List['JobOrm']] = relationship(
        'JobOrm',
        secondary=job_connection_link_table,
        back_populates='connections'
    )
    
    def __repr__(self):
        return (
            f'<PeerConnection('
            f'id={self.id}, '
            f'peer_id={self.peer_id}, '
            f'key={self.key})>'
        )
    
    def __str__(self):
        return (
            f'PeerConnection('
            f'id={self.id}, '
            f'peer_id={self.peer_id}, '
            f'key={self.key})'
        )
    
    def __json__(self):
        return {
            'id': self.id,
            'created_at': self.created_at,
            'peer_id': self.peer_id,
            'key': self.key
        }
    
    def _fields(self) -> tuple:
        return (
            ['id', 'created_at', 'peer_id', 'key'],
            [
                ('peer', False, None),
                ('paths', True, None),
                ('requests', True, None),
                ('jobs', True, None)
            ]
        )
    

    @validates('peer_id')
    def validate_peer_id(self, key, value):
        if is_empty(value):
            raise ValueError("Peer ID is required")
        if isinstance(value, int):
            return value
        if isinstance(value, str):
            result = RE_INT_VALUE.match(value)
            if result is None:
                raise ValueError("Invalid peer ID")
            return int(result.group(1))
        raise ValueError("Invalid peer ID")
    
    @validates('key')
    def validate_key(self, key, value):
        if is_empty(value):
            raise ValueError("Key is required")
        
        if RE_URI_SCHEMES.match(value) is None:
            raise ValueError("Invalid key")
        if (
            RE_DOMAIN.match(value) is None and
            RE_IPV4.match(value) is None and
            RE_IPV6.match(value) is None
        ):
            raise ValueError("Invalid key")
        return value



