# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

from pydantic import BaseModel

from ..peer_connection_path import (
    NewPeerConnectionPathPartialModel,
    UpdatePeerConnectionPathPartialModel
)





class PeerConnectionModel(BaseModel):
    id: int
    peer: int
    key: str


class NewPeerConnectionPartialModel(BaseModel):
    key: str

class NewPeerConnectionModel(NewPeerConnectionPartialModel):
    peer: int

class NewPeerConnectionStructurePartialModel(NewPeerConnectionPartialModel):
    paths: list[NewPeerConnectionPathPartialModel] | None = None

class NewPeerConnectionStructureModel(NewPeerConnectionModel):
    paths: list[NewPeerConnectionPathPartialModel] | None = None


class UpdatePeerConnectionModel(BaseModel):
    key: str | None = None

class UpdatePeerConnectionPartialModel(UpdatePeerConnectionModel):
    id: int | None = None # Required for update, missing id performs add

class UpdatePeerConnectionStructureModel(UpdatePeerConnectionModel):
    paths: list[UpdatePeerConnectionPathPartialModel] | None = None

class UpdatePeerConnectionStructurePartialModel(UpdatePeerConnectionPartialModel):
    paths: list[UpdatePeerConnectionPathPartialModel] | None = None

