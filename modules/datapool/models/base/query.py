# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import typing
import sqlalchemy





class BaseQueryParams:
    __slots__ = [
        '_has_params'
    ]

    def __init__(self):
        self._has_params = False



    def add_conditions(self, container: list) -> bool:
        """
        Add conditions to the container
        
        Args:
            container (list): The container to add the conditions to
            
        Returns:
            bool: True if conditions were added, False otherwise
        """
        return False


    # Getters for raw values

    @property
    def has_params(self) -> bool:
        return self._has_params


    ### Filtering

    def create_filter(self,
        field: typing.Any,
        values: list | tuple | None,
        use_and: bool = False
    ) -> typing.Any:
        """
        Create a filter statement
        
        Args:
            field (typing.Any): The field to filter on
            values (list | tuple | None): The values to filter by
            use_and (bool): Use AND instead of OR
        
        Returns:
            typing.Any: The filter statement
        """
        if not values:
            return None
        if isinstance(values, tuple):
            return self._render_filter_tuple(field, values)
        if not isinstance(values, list):
            return None
        num_elems = len(values)
        if num_elems == 0:
            return None
        container = []
        for elem in values:
            elem = self._render_filter_tuple(field, elem)
            if elem is None:
                continue
            container.append(elem)
        num_grps = len(container)
        if num_grps == 0:
            return None
        if num_grps == 1:
            return container[0]
        if use_and:
            return sqlalchemy.and_(*container)
        return sqlalchemy.or_(*container)


    def _render_filter_elem(self,
        field: typing.Any,
        value: typing.Any,
        comparator: str
    ) -> typing.Any:
        """
        Render a filter element
        
        Args:
            field (typing.Any): The field to filter on
            value (typing.Any): The value to filter by
            comparator (str): The comparator to use
        
        Returns:
            typing.Any: The filter statement
        """
        if comparator == '==':
            return field == value
        if comparator == '!=':
            return field != value
        if comparator == '>':
            return field > value
        if comparator == '<':
            return field < value
        if comparator == '>=':
            return field >= value
        if comparator == '<=':
            return field <= value
        if comparator == 'like':
            return field.ilike(value)
        return None


    def _render_filter_tuple(self,
        field: typing.Any,
        values: tuple | None
    ) -> typing.Any:
        """
        Render a filter tuple
        
        Args:
            field (typing.Any): The field to filter on
            values (tuple | None): The values to filter by
        
        Returns:
            typing.Any: The filter statement
        """
        if not values:
            return None
        num_grps = int(len(values) * 0.5)
        if num_grps == 0:
            return None
        if num_grps == 1:
            return self._render_filter_elem(
                field,
                values[0],
                values[1]
            )
        container = []
        for i in range(num_grps):
            start_i = i * 2
            elem = self._render_filter_elem(
                field,
                values[start_i],
                values[start_i + 1]
            )
            if elem is None:
                continue
            container.append(elem)
        num_grps = len(container)
        if num_grps == 0:
            return None
        if num_grps == 1:
            return container[0]
        return sqlalchemy.and_(*container)


    def _add_stmt_to_container(self,
        container: list,
        key: str,
        value: typing.Any
    ) -> None:
        if value is None:
            return
        self._has_params = True
        setattr(self, f'_{key}_stmt', value)
        container.append(value)


