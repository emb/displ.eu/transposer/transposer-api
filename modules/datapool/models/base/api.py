# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import datetime
import typing
from typing_extensions import Annotated

from pydantic import BaseModel, ValidationError
from pydantic.functional_validators import BeforeValidator, AfterValidator

from regex_patterns import (
    RE_INT_VALUE,
    RE_FLOAT_VALUE,
    RE_UUID4,
    RE_EMAIL,
    RE_USERNAME,
    RE_SLUG,
    RE_ALPHANUM_TERM,
    RE_HOST,
    RE_HASH_XX,
    RE_XXHASH_CID_HEX,
    RE_SHA256_HEX,
    RE_SHAKE256_KEY_HEX,

    RE_TIME_SSMS,
    RE_TIME_SSUS,
    RE_TIME_SSNS,
    RE_TIME_SSALL,
    RE_TIME_MMSSMS,
    RE_TIME_MMSSUS,
    RE_TIME_MMSSNS,
    RE_TIME_MMSSALL,
    RE_TIME_HHMMSSMS,
    RE_TIME_HHMMSSUS,
    RE_TIME_HHMMSSNS,
    RE_TIME_HHMMSSALL,

    RE_FLEX_TIME_SSXX,
    RE_FLEX_TIME_MMSSXX,
    RE_FLEX_TIME_HHMMSSXX,

    RE_DATETIME_FULL,
    RE_DATETIME_YYYY,
    RE_DATETIME_YYYYMM,
    RE_DATETIME_YYYYMMDD,
    RE_DATETIME_YYYYMMDD_HH,
    RE_DATETIME_YYYYMMDD_HHMM,
    RE_DATETIME_YYYYMMDD_HHMMSS,
    RE_DATETIME_YYYYMMDD_HHMMSS_MS
)





### Types and Validators ###

# Convert a time value to an integer, representing seconds
def time_to_int(value: typing.Any, positions: int = 12) -> typing.Any:
    """
    Convert a time value to an integer, representing seconds

    Args:
        value (Any): The value to convert

    Returns:
        Any: The converted value
    """

    if isinstance(value, int):
        # Already an integer
        return value
    
    multiply = int('1'.ljust(positions + 1, '0')) if positions else 1

    if isinstance(value, float):
        # Convert float to int
        # We assume that the float value represents seconds
        return int(value * multiply)

    if isinstance(value, datetime.time):
        # Value is time object
        if multiply > 1:
            return (
                (value.hour * 3600 * multiply) +
                (value.minute * 60 * multiply) +
                (value.second * multiply) +
                int(str(value.microsecond).ljust(positions, '0')[0:positions])
            )
        return (
            (value.hour * 3600) +
            (value.minute * 60) +
            value.second
        )
    
    if not isinstance(value, str):
        # If value is not a string, it is not convertible
        raise AssertionError('Not convertible value')
    
    # Test the value against several regular expressions

    # INT
    result = RE_INT_VALUE.match(value)
    if result:
        return int(result.group(1))
    
    # FLOAT
    result = RE_FLOAT_VALUE.match(value)
    if result:
        return int(float(result.group(1)) * multiply)

    # HH:MM:SS.xxx
    result = RE_FLEX_TIME_HHMMSSXX.match(value)
    if result:
        if multiply > 1:
            return (
                (int(result.group(1)) * 3600 * multiply) + 
                (int(result.group(2)) * 60 * multiply) + 
                (int(result.group(3)) * multiply) + 
                int(result.group(4).ljust(positions, '0')[0:positions])
            )
        return (
            (int(result.group(1)) * 3600) + 
            (int(result.group(2)) * 60) + 
            int(result.group(3))
        )
    
    # MM:SS.xxx
    result = RE_FLEX_TIME_MMSSXX.match(value)
    if result:
        if multiply > 1:
            return (
                (int(result.group(1)) * 60 * multiply) + 
                (int(result.group(2)) * multiply) + 
                int(result.group(3).ljust(positions, '0')[0:positions])
            )
        return (
            (int(result.group(1)) * 60) + 
            int(result.group(2))
        )
    
    # SS.xxx
    result = RE_FLEX_TIME_SSXX.match(value)
    if result:
        if multiply > 1:
            return (
                (int(result.group(1)) * multiply) + 
                int(result.group(2).ljust(positions, '0')[0:positions])
            )
        return (
            int(result.group(1))
        )
    
    # Invalid value
    raise AssertionError('Invalid value')


# Convert a time value to an integer, representing seconds
def time_to_secs(value: typing.Any) -> typing.Any:
    """
    Convert a time value to an integer, representing seconds

    Args:
        value (Any): The value to convert

    Returns:
        Any: The converted value
    """
    return time_to_int(value, 0)


# Convert a time value to an integer, representing milliseconds
def time_to_millisecs(value: typing.Any) -> typing.Any:
    """
    Convert a time value to an integer, representing milliseconds

    Args:
        value (Any): The value to convert

    Returns:
        Any: The converted value
    """
    return time_to_int(value, 3)


# Convert a time value to an integer, representing microseconds
def time_to_microsecs(value: typing.Any) -> typing.Any:
    """
    Convert a time value to an integer, representing microseconds

    Args:
        value (Any): The value to convert

    Returns:
        Any: The converted value
    """
    return time_to_int(value, 6)


# Convert a time value to an integer, representing nanoseconds
def time_to_nanosecs(value: typing.Any) -> typing.Any:
    """
    Convert a time value to an integer, representing nanoseconds

    Args:
        value (Any): The value to convert

    Returns:
        Any: The converted value
    """
    return time_to_int(value, 9)



# Convert a time value to a float, representing seconds
def time_to_secs_float(value: typing.Any) -> typing.Any:
    """
    Convert a time value to a float, representing seconds

    Args:
        value (Any): The value to convert

    Returns:
        Any: The converted value
    """

    if isinstance(value, int):
        # Convert int to float
        # We assume that the int value represents seconds
        return float(value)
    
    if isinstance(value, float):
        # Already a float
        # We assume that the float value represents seconds
        return int(value)

    if isinstance(value, datetime.time):
        # Value is time object
        return (
            (value.hour * 3600) +
            (value.minute * 60) +
            value.second + 
            (value.microsecond * 0.000001)
        )
    
    if not isinstance(value, str):
        # If value is not a string, it is not convertible
        raise AssertionError('Not convertible value')
    
    # Test the value against several regular expressions

    # INT
    result = RE_INT_VALUE.match(value)
    if result:
        return float(result.group(1))
    
    # FLOAT
    result = RE_FLOAT_VALUE.match(value)
    if result:
        return float(result.group(1))

    # HH:MM:SS.xxx
    result = RE_FLEX_TIME_HHMMSSXX.match(value)
    if result:
        return (
            (int(result.group(1)) * 3600) + 
            (int(result.group(2)) * 60) + 
            (int(result.group(3))) + 
            float('0.' + result.group(4))
        )
    
    # MM:SS.xxx
    result = RE_FLEX_TIME_MMSSXX.match(value)
    if result:
        return (
            (int(result.group(1)) * 60) + 
            int(result.group(2)) + 
            float('0.' + result.group(3))
        )
    
    # SS.xxx
    result = RE_FLEX_TIME_SSXX.match(value)
    if result:
        return (
            int(result.group(1)) + 
            float('0.' + result.group(2))
        )
    
    # Invalid value
    raise AssertionError('Invalid value')


# Create a type for a time value
TrpTimeSec = Annotated[int | float | str, BeforeValidator(time_to_secs)]
TrpTimeMSec = Annotated[int | float | str, BeforeValidator(time_to_millisecs)]
TrpTimeUSec = Annotated[int | float | str, BeforeValidator(time_to_microsecs)]
TrpTimeNSec = Annotated[int | float | str, BeforeValidator(time_to_nanosecs)]
TrpTimeSecFloat = Annotated[int | float | str, BeforeValidator(time_to_secs_float)]





# Validate and convert datetime values
def number_to_datetime_string(value: typing.Any) -> typing.Any:
    # We interpret integers and floats as timestamps
    if value < 0:
        # We do not allow negative values
        raise AssertionError('Not convertible value')
    
    if value > 9999999999:
        # We interpret this integer as timestamps in milliseconds
        value = value * 0.001
    
    if value > 9999999999:
        # If we still exceed the maximum value,
        # we interpret the value as timestamps in micoseconds
        value = value * 0.001
    
    if value > 9999999999:
        # If we still exceed the maximum value,
        # we interpret the value as timestamps in nanoseconds
        value = value * 0.001
    
    if value > 9999999999:
        # If we still exceed the maximum value,
        # we throw an error
        raise AssertionError('Value out of range')

    try:
        # Convert to datetime object
        value = datetime.datetime.fromtimestamp(value)
    except Exception as e:
        raise AssertionError(f'Value out of range: {str(e)}')

    # Convert to string
    return f"{value.strftime('%Y-%m-%d %H:%M:%S.%f')}+00:00"


def validate_and_convert_timestamp(value: typing.Any) -> typing.Any:
    """
    Validate and convert timestamp/datetime values

    Args:
        value (Any): The value to convert

    Returns:
        Any: The converted value
    """

    if isinstance(value, (int, float)):
        return number_to_datetime_string(value)

    if isinstance(value, datetime.datetime):
        if value.tzinfo is None:
            return f"{value.strftime('%Y-%m-%d %H:%M:%S.%f')}+00:00"
        return value.strftime('%Y-%m-%d %H:%M:%S.%f%z')

    if isinstance(value, datetime.date):
        return value.strftime(f'{value.isoformat()} 00:00:00.000000+00:00')
    
    if not isinstance(value, str):
        # If value is not a string, it is not convertible
        raise AssertionError('Not convertible value')
    
    # Test the value against several regular expressions

    # INT
    result = RE_INT_VALUE.match(value)
    if result:
        return number_to_datetime_string(int(result.group(1)))
    
    # FLOAT
    result = RE_FLOAT_VALUE.match(value)
    if result:
        return number_to_datetime_string(float(result.group(1)))

    # DATETIME FULL
    result = RE_DATETIME_FULL.match(value)
    if result:
        return f'{result.group(1)}-{result.group(2)}-{result.group(3)} {result.group(4)}:{result.group(5)}:{result.group(6)}.{result.group(7)}{result.group(8)}'

    # YYYY-MM-DD HH:MM:SS.MSMSMS
    result = RE_DATETIME_YYYYMMDD_HHMMSS_MS.match(value)
    if result:
        return f'{result.group(1)}-{result.group(2)}-{result.group(3)} {result.group(4)}:{result.group(5)}:{result.group(6)}.{result.group(7)}+00:00'

    # YYYY-MM-DD HH:MM:SS
    result = RE_DATETIME_YYYYMMDD_HHMMSS.match(value)
    if result:
        return f'{result.group(1)}-{result.group(2)}-{result.group(3)} {result.group(4)}:{result.group(5)}:{result.group(6)}.000000+00:00'

    # YYYY-MM-DD HH:MM
    result = RE_DATETIME_YYYYMMDD_HHMM.match(value)
    if result:
        return f'{result.group(1)}-{result.group(2)}-{result.group(3)} {result.group(4)}:{result.group(5)}:00.000000+00:00'

    # YYYY-MM-DD HH
    result = RE_DATETIME_YYYYMMDD_HH.match(value)
    if result:
        return f'{result.group(1)}-{result.group(2)}-{result.group(3)} {result.group(4)}:00:00.000000+00:00'

    # YYYY-MM-DD
    result = RE_DATETIME_YYYYMMDD.match(value)
    if result:
        return f'{result.group(1)}-{result.group(2)}-{result.group(3)} 00:00:00.000000+00:00'

    # YYYY-MM
    result = RE_DATETIME_YYYYMM.match(value)
    if result:
        return f'{result.group(1)}-{result.group(2)}-01 00:00:00.000000+00:00'

    # YYYY
    result = RE_DATETIME_YYYY.match(value)
    if result:
        return f'{result.group(1)}-01-01 00:00:00.000000+00:00'

    # Invalid value
    raise AssertionError('Invalid value')


# Create a type for a timestamp value
TrpTimestamp = Annotated[int | float | str, BeforeValidator(validate_and_convert_timestamp)]





# Validate a mixed id for user: id, uuid, email, username
def validate_mixed__user_id(value: typing.Any) -> typing.Any:
    """
    Validate a mixed id for user: id, uuid, email, username

    Args:
        value (Any): The value to validate

    Returns:
        Any: The validated value
    """

    if isinstance(value, int):
        # Already an integer. Probably an id
        return value
    
    if not isinstance(value, str):
        # If value is not a string, it is not convertible
        raise AssertionError('Not a valid user id (id, uuid, email or username)')
    
    # Test the value against several regular expressions

    # INT
    result = RE_INT_VALUE.match(value)
    if result:
        return int(result.group(1))
    
    # UUID
    result = RE_UUID4.match(value)
    if result:
        return result.group(1)
    
    # EMAIL
    result = RE_EMAIL.match(value)
    if result:
        return result.group(1)
    
    # USERNAME
    result = RE_USERNAME.match(value)
    if result:
        return result.group(1)
    
    # Invalid value
    raise AssertionError('Not a valid user id (id, uuid, email or username)')


# Create a type for a user id value
TrpPrimaryKey_UserID = Annotated[int | str, BeforeValidator(validate_mixed__user_id)]





# Validate a mixed id for media: id, uuid, cid
def validate_mixed__media_id(value: typing.Any) -> typing.Any:
    """
    Validate a mixed id for media: id, uuid, cid

    Args:
        value (Any): The value to validate

    Returns:
        Any: The validated value
    """

    if isinstance(value, int):
        # Already an integer. Probably an id
        return value
    
    if not isinstance(value, str):
        # If value is not a string, it is not convertible
        raise AssertionError('Not a valid media id (id, uuid, cid)')
    
    # Test the value against several regular expressions

    # INT
    result = RE_INT_VALUE.match(value)
    if result:
        return int(result.group(1))
    
    # UUID
    result = RE_UUID4.match(value)
    if result:
        return result.group(1)
    
    # CID
    result = RE_XXHASH_CID_HEX.match(value)
    if result:
        return result.group(1)
    
    # Invalid value
    raise AssertionError('Not a valid media id (id, uuid, cid)')


# Create a type for a media id value
TrpPrimaryKey_MediaID = Annotated[int | str, BeforeValidator(validate_mixed__media_id)]





# Validate a mixed id: id or uuid
def validate_mixed__id_uuid(value: typing.Any) -> typing.Any:
    """
    Validate a mixed id: id or uuid

    Args:
        value (Any): The value to validate

    Returns:
        Any: The validated value
    """

    if isinstance(value, int):
        # Already an integer. Probably an id
        return value
    
    if not isinstance(value, str):
        # If value is not a string, it is not convertible
        raise AssertionError('Not a valid id or uuid')
    
    # Test the value against several regular expressions

    # INT
    result = RE_INT_VALUE.match(value)
    if result:
        return int(result.group(1))
    
    # UUID
    result = RE_UUID4.match(value)
    if result:
        return result.group(1)
    
    # Invalid value
    raise AssertionError('Not a valid id or uuid')


# Create a type for a mixed id value
TrpPrimaryKey_ID_UUID = Annotated[int | str, BeforeValidator(validate_mixed__id_uuid)]





# Validate a mixed id: id, token or key
def validate_mixed__id_token_key(value: typing.Any) -> typing.Any:
    """
    Validate a mixed id: id, token or key

    Args:
        value (Any): The value to validate

    Returns:
        Any: The validated value
    """

    if isinstance(value, int):
        # Already an integer. Probably an id
        return value
    
    if not isinstance(value, str):
        # If value is not a string, it is not convertible
        raise AssertionError('Not a valid id, token or key')
    
    # Test the value against several regular expressions

    # INT
    result = RE_INT_VALUE.match(value)
    if result:
        return int(result.group(1))
    
    # TOKEN
    result = RE_SHA256_HEX.match(value)
    if result:
        return result.group(1)
    
    # KEY
    result = RE_SHAKE256_KEY_HEX.match(value)
    if result:
        return result.group(1)
    
    # Invalid value
    raise AssertionError('Not a valid id, token or key')


# Create a type for a mixed id value
TrpPrimaryKey_ID_TOKEN_KEY = Annotated[int | str, BeforeValidator(validate_mixed__id_token_key)]





# Validate a mixed id: id or token
def validate_mixed__id_token(value: typing.Any) -> typing.Any:
    """
    Validate a mixed id: id or token

    Args:
        value (Any): The value to validate

    Returns:
        Any: The validated value
    """

    if isinstance(value, int):
        # Already an integer. Probably an id
        return value
    
    if not isinstance(value, str):
        # If value is not a string, it is not convertible
        raise AssertionError('Not a valid id or token')
    
    # Test the value against several regular expressions

    # INT
    result = RE_INT_VALUE.match(value)
    if result:
        return int(result.group(1))
    
    # TOKEN
    result = RE_HASH_XX.match(value)
    if result:
        return result.group(1)
    
    # Invalid value
    raise AssertionError('Not a valid id or token')


# Create a type for a mixed id value
TrpPrimaryKey_ID_TOKEN = Annotated[int | str, BeforeValidator(validate_mixed__id_token)]





# Validate a mixed id: id or hash
def validate_mixed__id_hash(value: typing.Any) -> typing.Any:
    """
    Validate a mixed id: id or hash

    Args:
        value (Any): The value to validate

    Returns:
        Any: The validated value
    """

    if isinstance(value, int):
        # Already an integer. Probably an id
        return value
    
    if not isinstance(value, str):
        # If value is not a string, it is not convertible
        raise AssertionError('Not a valid id or hash')
    
    # Test the value against several regular expressions

    # INT
    result = RE_INT_VALUE.match(value)
    if result:
        return int(result.group(1))
    
    # HASH
    result = RE_HASH_XX.match(value)
    if result:
        return result.group(1)
    
    # Invalid value
    raise AssertionError('Not a valid id or hash')


# Create a type for a mixed id value
TrpPrimaryKey_ID_HASH = Annotated[int | str, BeforeValidator(validate_mixed__id_hash)]





# Validate a mixed id: id or alphanum
def validate_mixed__id_alphanum(value: typing.Any) -> typing.Any:
    """
    Validate a mixed id: id or alphanum

    Args:
        value (Any): The value to validate

    Returns:
        Any: The validated value
    """

    if isinstance(value, int):
        # Already an integer. Probably an id
        return value
    
    if not isinstance(value, str):
        # If value is not a string, it is not convertible
        raise AssertionError('Not a valid id or alphanum')
    
    # Test the value against several regular expressions

    # INT
    result = RE_INT_VALUE.match(value)
    if result:
        return int(result.group(1))
    
    # ALPHANUM_TERM
    result = RE_ALPHANUM_TERM.match(value)
    if result:
        return result.group(1)
    
    # Invalid value
    raise AssertionError('Not a valid id or alphanum')


# Create a type for a mixed id value
TrpPrimaryKey_ID_ALPHANUM = Annotated[int | str, BeforeValidator(validate_mixed__id_alphanum)]





# Validate a mixed id: id or slug
def validate_mixed__id_slug(value: typing.Any) -> typing.Any:
    """
    Validate a mixed id: id or slug

    Args:
        value (Any): The value to validate

    Returns:
        Any: The validated value
    """

    if isinstance(value, int):
        # Already an integer. Probably an id
        return value
    
    if not isinstance(value, str):
        # If value is not a string, it is not convertible
        raise AssertionError('Not a valid id or slug')
    
    # Test the value against several regular expressions

    # INT
    result = RE_INT_VALUE.match(value)
    if result:
        return int(result.group(1))
    
    # SLUG
    result = RE_SLUG.match(value)
    if result:
        return result.group(1)
    
    # Invalid value
    raise AssertionError('Not a valid id or slug')


# Create a type for a mixed id value
TrpPrimaryKey_ID_SLUG = Annotated[int | str, BeforeValidator(validate_mixed__id_slug)]





# Validate a mixed id: id or host
def validate_mixed__id_host(value: typing.Any) -> typing.Any:
    """
    Validate a mixed id: id or host

    Args:
        value (Any): The value to validate

    Returns:
        Any: The validated value
    """

    if isinstance(value, int):
        # Already an integer. Probably an id
        return value
    
    if not isinstance(value, str):
        # If value is not a string, it is not convertible
        raise AssertionError('Not a valid id or host')
    
    # Test the value against several regular expressions

    # INT
    result = RE_INT_VALUE.match(value)
    if result:
        return int(result.group(1))
    
    # HOST
    result = RE_HOST.match(value)
    if result:
        return result.group(1)
    
    # Invalid value
    raise AssertionError('Not a valid id or host')


# Create a type for a mixed id value
TrpPrimaryKey_ID_HOST = Annotated[int | str, BeforeValidator(validate_mixed__id_host)]









### Pydantic Models ###

class SingleStringModel(BaseModel):
    value: str

class TokenModel(BaseModel):
    token: str


class ApiRequestModel(BaseModel):
    method: str
    args: list | None = None
    kwargs: dict | None = None

class ApiResponseModel(BaseModel):
    success: bool = False
    message: str | None = None
    payload: typing.Any = None

class ApiResponseWithCountModel(ApiResponseModel):
    count: int | None = None
    total: int | None = None


