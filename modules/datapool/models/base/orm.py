# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import typing
import enum
import json
import uuid
import hashlib
import logging

from sqlalchemy.orm import (
    DeclarativeBase
)
from sqlalchemy.ext.declarative import declarative_base

from regex_patterns import (
    RE_TRUE
)





### ENUM

# Property_Datatype
class Property_Datatype(enum.Enum):
    STRING = 'string'
    INTEGER = 'integer'
    FLOAT = 'float'
    BOOL = 'bool'
    JSON = 'json'

# Media_Purpose
class Media_Purpose(enum.Enum):
    ORIGINAL = 'original'
    TRANSCRIBE = 'transcribe'

# Job_Action
class Job_Action(enum.Enum):
    TRANSCRIBE = 'transcribe'
    TRANSLATE = 'translate'

# Record_Status
class Record_Status(enum.Enum):
    UNDEFINED = 'undefined'
    CREATED = 'created'
    PROCESSING = 'processing'
    CANCELED = 'canceled'
    SUCCESS = 'success'
    INCOMPLETE = 'incomplete'
    ERROR = 'error'





### TRANSFORMERS

# Type Transformers
TYPE_TRANSFORMERS = {
    'STRING': lambda x: str(x),
    'INTEGER': lambda x: int(x),
    'FLOAT': lambda x: float(x),
    'BOOL': lambda x: RE_TRUE.match(str(x)) is not None,
    'JSON': lambda x: json.loads(x)
}





### CLASSES

class BaseOrm(DeclarativeBase):
    def to_json(self):
        return json.dumps(self.to_dict())
    
    def to_json_pretty(self):
        return json.dumps(self.to_dict(), indent=4, sort_keys=True)
    
    def to_json_pretty_print(self):
        print(json.dumps(self.to_dict(), indent=4, sort_keys=True))
    

    def to_dict(self, include: dict | None = None):
        if not isinstance(include, dict) or len(include) == 0:
            return self.__json__()

        current_fields, foreign_fields = self._fields()

        container = {}
        if 'current' in include:
            container = self.__json__()
        else:
            for key in current_fields:
                if key in include:
                    container[key] = getattr(self, key)

        for key, multi, transformer in foreign_fields:
            if key in include:
                if callable(transformer):
                    try:
                        if multi:
                            container[key] = transformer([item.to_dict(include[key]) for item in getattr(self, key)])
                            continue
                        container[key] = transformer(getattr(self, key).to_dict(include[key]))
                    except Exception as e:
                        logging.error(f'Error applying transformer: {e}')
                    finally:
                        continue
                if multi:
                    container[key] = [item.to_dict(include[key]) for item in getattr(self, key)]
                    continue
                container[key] = getattr(self, key).to_dict(include[key])

        return container


    def _fields(self) -> tuple:
        return (
            [],
            []
        )
    

    def _transform_options(self, options: typing.Any):
        """
        Transforms the options list into a dictionary
        """
        if not isinstance(options, list) or len(options) == 0:
            return options
        opt = options[0]
        if not isinstance(opt, dict) or len(opt) == 0:
            return options
        if 'key' not in opt or 'value' in opt or 'type' not in opt:
            return options
        
        container = {}
        for opt in options:
            val = None
            try:
                val = TYPE_TRANSFORMERS[opt['datatype']](opt['value'])
            except Exception as e:
                continue

            if opt['key'] in container:
                if not isinstance(container[opt['key']], list):
                    container[opt['key']] = [container[opt['key']]]
                container[opt['key']].append(val)
                continue
            
            container[opt['key']] = val

        return container



    @staticmethod
    def get_uuid4():
        return str(uuid.uuid4())
    
    @staticmethod
    def get_sha256(data: str):
        h = hashlib.sha256()
        h.update(data.encode('ascii'))
        return h.hexdigest()
    
    @staticmethod
    def get_random_sha256():
        h = hashlib.sha256()
        h.update(str(uuid.uuid4()).encode('ascii'))
        return h.hexdigest()
    
    @staticmethod
    def get_random_shake256(length: int = 32):
        h = hashlib.shake_256(str(uuid.uuid4()).encode('ascii'))
        return h.hexdigest(length)



SIMPLE_BASE = declarative_base()
SIMPLE_BASE_METADATA = SIMPLE_BASE.metadata


