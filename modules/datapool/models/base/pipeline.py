# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import typing
import json

from regex_patterns import (
    RE_TRUE
)
from .orm import BaseOrm



TYPE_TRANSFORMERS = [
    lambda x: str(x),
    lambda x: int(x),
    lambda x: float(x),
    lambda x: RE_TRUE.match(str(x)) is not None,
    lambda x: json.loads(x)
]



class PipelineBaseOrm(BaseOrm):
    __abstract__ = True
    def _transform_options(self, options: typing.Any):
        """
        Transforms the options list into a dictionary
        """
        if not isinstance(options, list) or len(options) == 0:
            return options
        opt = options[0]
        if not isinstance(opt, dict) or len(opt) == 0:
            return options
        if 'key' not in opt or 'value' in opt or 'type' not in opt:
            return options
        
        container = {}
        for opt in options:
            val = None
            try:
                val = TYPE_TRANSFORMERS[opt['type']](opt['value'])
            except Exception as e:
                continue

            if opt['key'] in container:
                if not isinstance(container[opt['key']], list):
                    container[opt['key']] = [container[opt['key']]]
                container[opt['key']].append(val)
                continue
            
            container[opt['key']] = val

        return container


