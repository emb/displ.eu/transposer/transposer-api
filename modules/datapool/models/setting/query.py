# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import typing
import datetime

from regex_patterns import (
    RE_INT_VALUE,
    RE_LANG_CODE,
    RE_UUID4,
    RE_ALPHANUM_TERM,
    RE_SLUG,
    RE_ALPHANUM_ASCII_WSPACE
)
from data_utils import (
    split_terms,
    split_datetime,
    split_numeric
)
from ..base import (
    BaseQueryParams
)
from .orm import SettingOrm





class SettingQueryParams(BaseQueryParams):
    __slots__ = [
        '_id',
        '_created_at',
        '_modified_at',
        '_name',
        '_default_lang',
        '_default_ttl',

        '_id_stmt',
        '_created_at_stmt',
        '_modified_at_stmt',
        '_name_stmt',
        '_default_lang_stmt',
        '_default_ttl_stmt'
    ]

    def __init__(self, 
        id: str | None = None,
        created_at: int | float | str | None = None,
        modified_at: int | float | str | None = None,
        name: str | None = None,
        default_lang: str | None = None,
        default_ttl: int | str | None = None
    ):
        super().__init__()

        # Store raw values
        self._id = id
        self._created_at = created_at
        self._modified_at = modified_at
        self._name = name
        self._default_lang = default_lang
        self._default_ttl = default_ttl

        # Init statement values
        self._id_stmt = None
        self._created_at_stmt = None
        self._modified_at_stmt = None
        self._name_stmt = None
        self._default_lang_stmt = None
        self._default_ttl_stmt = None


    def add_conditions(self, container: list, use_and: bool = False) -> bool:

        # id
        if self._id_stmt is not None:
            container.append(self._id_stmt)
        else:
            val = self._id
            val = self.create_filter(
                SettingOrm.id,
                [(x, '==') for x in split_terms(val, RE_SLUG, '', '')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._id_stmt = val
                container.append(val)

        # created_at
        if self._created_at_stmt is not None:
            container.append(self._created_at_stmt)
        else:
            val = self._created_at
            val = self.create_filter(
                SettingOrm.created_at,
                split_datetime(val),
                use_and
            )
            if val is not None:
                self._has_params = True
                self._created_at_stmt = val
                container.append(val)
        
        # modified_at
        if self._modified_at_stmt is not None:
            container.append(self._modified_at_stmt)
        else:
            val = self._modified_at
            val = self.create_filter(
                SettingOrm.modified_at,
                split_datetime(val),
                use_and
            )
            if val is not None:
                self._has_params = True
                self._modified_at_stmt = val
                container.append(val)

        # name
        if self._name_stmt is not None:
            container.append(self._name_stmt)
        else:
            val = self._name
            val = self.create_filter(
                SettingOrm.name,
                [(x, 'like') for x in split_terms(val, RE_ALPHANUM_ASCII_WSPACE, '%', '%')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._name_stmt = val
                container.append(val)

        # default_lang
        if self._default_lang_stmt is not None:
            container.append(self._default_lang_stmt)
        else:
            val = self._default_lang
            val = self.create_filter(
                SettingOrm.default_lang,
                [(x, '==') for x in split_terms(val, RE_LANG_CODE, '', '')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._default_lang_stmt = val
                container.append(val)
        
        # default_ttl
        if self._default_ttl_stmt is not None:
            container.append(self._default_ttl_stmt)
        else:
            val = self._default_ttl
            if isinstance(val, int):
                val = str(val)
            val = self.create_filter(
                SettingOrm.default_ttl,
                split_numeric(val),
                use_and
            )
            if val is not None:
                self._has_params = True
                self._default_ttl_stmt = val
                container.append(val)

        # Return True if conditions were added
        return self._has_params
    

    # Getters for raw values

    @property
    def id(self) -> str | None:
        return self._id
    
    @property
    def created_at(self) -> int | float | str | None:
        return self._created_at
    
    @property
    def modified_at(self) -> int | float | str | None:
        return self._modified_at

    @property
    def name(self) -> str | None:
        return self._name
    
    @property
    def default_lang(self) -> str | None:
        return self._default_lang
    
    @property
    def default_ttl(self) -> int | str | None:
        return self._default_ttl
    

    # Getters for statements
    
    @property
    def id_stmt(self) -> typing.Any:
        return self._id_stmt
    
    @property
    def created_at_stmt(self) -> typing.Any:
        return self._created_at_stmt
    
    @property
    def modified_at_stmt(self) -> typing.Any:
        return self._modified_at_stmt
    
    @property
    def name_stmt(self) -> typing.Any:
        return self._name_stmt
    
    @property
    def default_lang_stmt(self) -> typing.Any:
        return self._default_lang_stmt
    
    @property
    def default_ttl_stmt(self) -> typing.Any:
        return self._default_ttl_stmt



### Define Interfaces

class SettingQueryParamsPrimary(SettingQueryParams):
    def __init__(self,
        id: str | None = None,
        created_at: int | float | str | None = None,
        modified_at: int | float | str | None = None,
        name: str | None = None,
        default_lang: str | None = None,
        default_ttl: int | str | None = None
    ):
        super().__init__(
            id,
            created_at,
            modified_at,
            name,
            default_lang,
            default_ttl
        )


class SettingQueryParamsGeneral(SettingQueryParams):
    def __init__(self,
        setting_id: str | None = None,
        setting_created_at: int | float | str | None = None,
        setting_modified_at: int | float | str | None = None,
        setting_name: str | None = None,
        setting_default_lang: str | None = None,
        setting_default_ttl: int | str | None = None
    ):
        super().__init__(
            setting_id,
            setting_created_at,
            setting_modified_at,
            setting_name,
            setting_default_lang,
            setting_default_ttl
        )


