# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import typing
import datetime

import sqlalchemy
from sqlalchemy.orm import (
    Mapped,
    mapped_column,
    relationship,
    validates
)

from regex_patterns import (
    RE_INT_VALUE,
    RE_FLOAT_VALUE,
    RE_LANG_CODE,
    RE_UUID4,
    RE_SLUG,
    RE_ALPHANUM_ASCII_WSPACE
)
from data_utils import (
    is_empty,
    split_terms,
    split_datetime,
    sanitize_text
)
from ..base import BaseOrm





class SettingOrm(BaseOrm):
    __tablename__ = 'settings'
    id: Mapped[str] = mapped_column(
        sqlalchemy.String, primary_key=True
    )
    created_at: Mapped[datetime.datetime] = mapped_column(
        sqlalchemy.DateTime, default=sqlalchemy.sql.functions.now()
    )
    modified_at: Mapped[datetime.datetime] = mapped_column(
        sqlalchemy.DateTime, default=sqlalchemy.sql.functions.now()
    )
    name: Mapped[typing.Optional[str]] = mapped_column(sqlalchemy.String, default=None, nullable=True)
    default_lang: Mapped[str] = mapped_column(sqlalchemy.String, default='en')
    default_ttl: Mapped[int] = mapped_column(sqlalchemy.Integer, default=3600000)

    pipelines: Mapped[typing.List['PipelineOrm']] = relationship(
        back_populates='setting',
        primaryjoin="(SettingOrm.id==PipelineOrm.setting_id)"
    )
    workflow_templates: Mapped[typing.List['WorkflowTemplateOrm']] = relationship(
        back_populates='setting',
        primaryjoin="(SettingOrm.id==WorkflowTemplateOrm.setting_id)"
    )
    
    def __repr__(self):
        return (
            f'<Setting('
            f'id={self.id}, '
            f'created_at={self.created_at}, '
            f'modified_at={self.modified_at}, '
            f'name={self.name}, '
            f'default_lang={self.default_lang}, '
            f'default_ttl={self.default_ttl})>'
        )
    
    def __str__(self):
        return (
            f'Setting('
            f'id={self.id}, '
            f'created_at={self.created_at}, '
            f'modified_at={self.modified_at}, '
            f'name={self.name}, '
            f'default_lang={self.default_lang}, '
            f'default_ttl={self.default_ttl})'
        )
    
    def __json__(self):
        return {
            'id': self.id,
            'created_at': self.created_at,
            'modified_at': self.modified_at,
            'name': self.name,
            'default_lang': self.default_lang,
            'default_ttl': self.default_ttl
        }
    
    def _fields(self) -> tuple:
        return (
            ['id', 'created_at', 'modified_at', 'name', 'default_lang', 'default_ttl'],
            [('pipelines', True, None), ('workflow_templates', True, None)]
        )
    

    @validates('id')
    def validate_id(self, key, value):
        if is_empty(value):
            raise ValueError("Key is required")
        if isinstance(value, str):
            result = RE_SLUG.match(value)
            if result is None:
                raise ValueError("Invalid ID")
            return result.group(1)
        raise ValueError("Invalid ID")
    
    @validates('name')
    def validate_name(self, key, value):
        if is_empty(value):
            return None
        if isinstance(value, str):
            result = RE_ALPHANUM_ASCII_WSPACE.match(value)
            if result is None:
                raise ValueError("Invalid name")
            return result.group(1)
        raise ValueError("Invalid name")
    
    @validates('default_lang')
    def validate_default_lang(self, key, value):
        if is_empty(value):
            raise ValueError("Default language is required")
        if isinstance(value, str):
            result = RE_LANG_CODE.match(value)
            if result is None:
                raise ValueError("Invalid default language")
            return result.group(1)
        raise ValueError("Invalid default language")

    @validates('default_ttl')
    def validate_default_ttl(self, key, value):
        if is_empty(value):
            raise ValueError("Default TTL is required")
        if isinstance(value, int):
            return value
        if isinstance(value, str):
            result = RE_INT_VALUE.match(value)
            if result is None:
                raise ValueError("Invalid default TTL")
            return int(result.group(1))
        raise ValueError("Invalid default TTL")


