# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

from .orm import SettingOrm





SETTING__FIELDS_MAP = {
    'setting': {
        'id': SettingOrm.id,
        'created_at': SettingOrm.created_at,
        'modified_at': SettingOrm.modified_at,
        'name': SettingOrm.name,
        'default_lang': SettingOrm.default_lang,
        'default_ttl': SettingOrm.default_ttl
    },
    'setting_id': SettingOrm.id,
    'setting_created_at': SettingOrm.created_at,
    'setting_modified_at': SettingOrm.modified_at,
    'setting_name': SettingOrm.name,
    'setting_default_lang': SettingOrm.default_lang,
    'setting_default_ttl': SettingOrm.default_ttl,

    'setting.id': SettingOrm.id,
    'setting.created_at': SettingOrm.created_at,
    'setting.modified_at': SettingOrm.modified_at,
    'setting.name': SettingOrm.name,
    'setting.default_lang': SettingOrm.default_lang,
    'setting.default_ttl': SettingOrm.default_ttl
}

SETTING__VARIANTS_MAP = {
    'setting': {
        'id': 'setting.id',
        'created_at': 'setting.created_at',
        'modified_at': 'setting.modified_at',
        'name': 'setting.name',
        'default_lang': 'setting.default_lang',
        'default_ttl': 'setting.default_ttl'
    },
    'setting_id': 'setting.id',
    'setting_created_at': 'setting.created_at',
    'setting_modified_at': 'setting.modified_at',
    'setting_name': 'setting.name',
    'setting_default_lang': 'setting.default_lang',
    'setting_default_ttl': 'setting.default_ttl',

    'setting.id': 'setting.id',
    'setting.created_at': 'setting.created_at',
    'setting.modified_at': 'setting.modified_at',
    'setting.name': 'setting.name',
    'setting.default_lang': 'setting.default_lang',
    'setting.default_ttl': 'setting.default_ttl'
}



############
### Primary

SETTING__PRIMARY_FIELDS_MAP = {
    'id': SettingOrm.id,
    'created_at': SettingOrm.created_at,
    'modified_at': SettingOrm.modified_at,
    'name': SettingOrm.name,
    'default_lang': SettingOrm.default_lang,
    'default_ttl': SettingOrm.default_ttl
}

SETTING__PRIMARY_VARIANTS_MAP = {
    'id': 'setting.id',
    'created_at': 'setting.created_at',
    'modified_at': 'setting.modified_at',
    'name': 'setting.name',
    'default_lang': 'setting.default_lang',
    'default_ttl': 'setting.default_ttl'
}


