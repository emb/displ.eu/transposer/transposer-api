# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

from .orm import WorkflowStepOrm





WORKFLOW_STEP__FIELDS_MAP = {
    'workflow_step': {
        'id': WorkflowStepOrm.id,
        'uuid': WorkflowStepOrm.uuid,
        'workflow': WorkflowStepOrm.workflow_id,
        'workflow_id': WorkflowStepOrm.workflow_id,
        'pos': WorkflowStepOrm.pos,
        'allow_if_previous_failed': WorkflowStepOrm.allow_if_previous_failed,
        'action': WorkflowStepOrm.action,
        'topic_send': WorkflowStepOrm.topic_send,
        'topic_receive': WorkflowStepOrm.topic_receive,
        'progress': WorkflowStepOrm.progress,
        'status': WorkflowStepOrm.status,
        'reason': WorkflowStepOrm.reason
    },
    'workflow_step_id': WorkflowStepOrm.id,
    'workflow_step_uuid': WorkflowStepOrm.uuid,
    'workflow_step_workflow': WorkflowStepOrm.workflow_id,
    'workflow_step_workflow_id': WorkflowStepOrm.workflow_id,
    'workflow_step_pos': WorkflowStepOrm.pos,
    'workflow_step_allow_if_previous_failed': WorkflowStepOrm.allow_if_previous_failed,
    'workflow_step_action': WorkflowStepOrm.action,
    'workflow_step_topic_send': WorkflowStepOrm.topic_send,
    'workflow_step_topic_receive': WorkflowStepOrm.topic_receive,
    'workflow_step_progress': WorkflowStepOrm.progress,
    'workflow_step_status': WorkflowStepOrm.status,
    'workflow_step_reason': WorkflowStepOrm.reason,

    'workflow_step.id': WorkflowStepOrm.id,
    'workflow_step.uuid': WorkflowStepOrm.uuid,
    'workflow_step.workflow': WorkflowStepOrm.workflow_id,
    'workflow_step.workflow_id': WorkflowStepOrm.workflow_id,
    'workflow_step.pos': WorkflowStepOrm.pos,
    'workflow_step.allow_if_previous_failed': WorkflowStepOrm.allow_if_previous_failed,
    'workflow_step.action': WorkflowStepOrm.action,
    'workflow_step.topic_send': WorkflowStepOrm.topic_send,
    'workflow_step.topic_receive': WorkflowStepOrm.topic_receive,
    'workflow_step.progress': WorkflowStepOrm.progress,
    'workflow_step.status': WorkflowStepOrm.status,
    'workflow_step.reason': WorkflowStepOrm.reason
}

WORKFLOW_STEP__VARIANTS_MAP = {
    'workflow_step': {
        'id': 'workflow_step.id',
        'uuid': 'workflow_step.uuid',
        'workflow': 'workflow_step.workflow_id',
        'workflow_id': 'workflow_step.workflow_id',
        'pos': 'workflow_step.pos',
        'allow_if_previous_failed': 'workflow_step.allow_if_previous_failed',
        'action': 'workflow_step.action',
        'topic_send': 'workflow_step.topic_send',
        'topic_receive': 'workflow_step.topic_receive',
        'progress': 'workflow_step.progress',
        'status': 'workflow_step.status',
        'reason': 'workflow_step.reason'
    },
    'workflow_step_id': 'workflow_step.id',
    'workflow_step_uuid': 'workflow_step.uuid',
    'workflow_step_workflow': 'workflow_step.workflow_id',
    'workflow_step_workflow_id': 'workflow_step.workflow_id',
    'workflow_step_pos': 'workflow_step.pos',
    'workflow_step_allow_if_previous_failed': 'workflow_step.allow_if_previous_failed',
    'workflow_step_action': 'workflow_step.action',
    'workflow_step_topic_send': 'workflow_step.topic_send',
    'workflow_step_topic_receive': 'workflow_step.topic_receive',
    'workflow_step_progress': 'workflow_step.progress',
    'workflow_step_status': 'workflow_step.status',
    'workflow_step_reason': 'workflow_step.reason',

    'workflow_step.id': 'workflow_step.id',
    'workflow_step.uuid': 'workflow_step.uuid',
    'workflow_step.workflow': 'workflow_step.workflow_id',
    'workflow_step.workflow_id': 'workflow_step.workflow_id',
    'workflow_step.pos': 'workflow_step.pos',
    'workflow_step.allow_if_previous_failed': 'workflow_step.allow_if_previous_failed',
    'workflow_step.action': 'workflow_step.action',
    'workflow_step.topic_send': 'workflow_step.topic_send',
    'workflow_step.topic_receive': 'workflow_step.topic_receive',
    'workflow_step.progress': 'workflow_step.progress',
    'workflow_step.status': 'workflow_step.status',
    'workflow_step.reason': 'workflow_step.reason'
}



############
### Primary

WORKFLOW_STEP__PRIMARY_FIELDS_MAP = {
    'id': WorkflowStepOrm.id,
    'uuid': WorkflowStepOrm.uuid,
    'workflow': WorkflowStepOrm.workflow_id,
    'workflow_id': WorkflowStepOrm.workflow_id,
    'pos': WorkflowStepOrm.pos,
    'allow_if_previous_failed': WorkflowStepOrm.allow_if_previous_failed,
    'action': WorkflowStepOrm.action,
    'topic_send': WorkflowStepOrm.topic_send,
    'topic_receive': WorkflowStepOrm.topic_receive,
    'progress': WorkflowStepOrm.progress,
    'status': WorkflowStepOrm.status,
    'reason': WorkflowStepOrm.reason
}

WORKFLOW_STEP__PRIMARY_VARIANTS_MAP = {
    'id': 'workflow_step.id',
    'uuid': 'workflow_step.uuid',
    'workflow': 'workflow_step.workflow_id',
    'workflow_id': 'workflow_step.workflow_id',
    'pos': 'workflow_step.pos',
    'allow_if_previous_failed': 'workflow_step.allow_if_previous_failed',
    'action': 'workflow_step.action',
    'topic_send': 'workflow_step.topic_send',
    'topic_receive': 'workflow_step.topic_receive',
    'progress': 'workflow_step.progress',
    'status': 'workflow_step.status',
    'reason': 'workflow_step.reason'
}


