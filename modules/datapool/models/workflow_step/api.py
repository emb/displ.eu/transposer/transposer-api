# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

from pydantic import BaseModel
from ..base import (
    TrpPrimaryKey_ID_UUID
)




class WorkflowStepModel(BaseModel):
    id: int
    uuid: str
    workflow: TrpPrimaryKey_ID_UUID
    pos: int = 0
    allow_if_previous_failed: bool = False
    action: str
    topic_send: str | None = None
    topic_receive: str | None = None
    progress: int | float | str = 0.0
    status: int = 0
    reason: str | None = None


class NewWorkflowStepPartialModel(BaseModel):
    pos: int = 0
    allow_if_previous_failed: bool = False
    action: str
    topic_send: str | None = None
    topic_receive: str | None = None
    progress: int | float | str = 0.0
    status: int = 1

class NewWorkflowStepModel(NewWorkflowStepPartialModel):
    workflow: TrpPrimaryKey_ID_UUID


class UpdateWorkflowStepModel(BaseModel):
    pos: int | None = None
    allow_if_previous_failed: bool | None = None
    action: str | None = None
    topic_send: str | None = None
    topic_receive: str | None = None
    progress: int | float | str | None = None
    status: int | None = None
    reason: str | None = None

class UpdateWorkflowStepPartialModel(UpdateWorkflowStepModel):
    id: int | None = None
    uuid: str | None = None


