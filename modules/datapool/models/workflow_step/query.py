# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import typing
import datetime

from regex_patterns import (
    RE_INT_VALUE,
    RE_LANG_CODE,
    RE_UUID4,
    RE_ALPHANUM_TERM,
    RE_SLUG
)
from data_utils import (
    split_terms,
    split_datetime,
    split_numeric
)
from ..base import BaseQueryParams
from .orm import WorkflowStepOrm





class WorkflowStepQueryParams(BaseQueryParams):
    __slots__ = [
        '_id',
        '_uuid',
        '_workflow_id',
        '_pos',
        '_allow_if_previous_failed',
        '_action',
        '_topic_send',
        '_topic_receive',
        '_progress',
        '_status',
        '_reason',

        '_id_stmt',
        '_uuid_stmt',
        '_workflow_id_stmt',
        '_pos_stmt',
        '_allow_if_previous_failed_stmt',
        '_action_stmt',
        '_topic_send_stmt',
        '_topic_receive_stmt',
        '_progress_stmt',
        '_status_stmt',
        '_reason_stmt'
    ]

    def __init__(self, 
        id: int | str | None = None,
        uuid: int | str | None = None,
        workflow_id: int | str | None = None,
        pos: int | None = None,
        allow_if_previous_failed: bool | None = None,
        action: str | None = None,
        topic_send: str | None = None,
        topic_receive: str | None = None,
        progress: int | float | str | None = None,
        status: int | str | None = None,
        reason: str | None = None
    ):
        super().__init__()

        # Store raw values
        self._id = id
        self._uuid = uuid
        self._workflow_id = workflow_id
        self._pos = pos
        self._allow_if_previous_failed = allow_if_previous_failed
        self._action = action
        self._topic_send = topic_send
        self._topic_receive = topic_receive
        self._progress = progress
        self._status = status
        self._reason = reason

        # Init statement values
        self._id_stmt = None
        self._uuid_stmt = None
        self._workflow_id_stmt = None
        self._pos_stmt = None
        self._allow_if_previous_failed_stmt = None
        self._action_stmt = None
        self._topic_send_stmt = None
        self._topic_receive_stmt = None
        self._progress_stmt = None
        self._status_stmt = None
        self._reason_stmt = None


    def add_conditions(self, container: list, use_and: bool = False) -> bool:

        # id
        if self._id_stmt is not None:
            container.append(self._id_stmt)
        else:
            val = self._id
            if isinstance(val, int):
                val = str(val)
            val = self.create_filter(
                WorkflowStepOrm.id,
                [(x, '==') for x in split_terms(val, RE_INT_VALUE, '', '')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._id_stmt = val
                container.append(val)

        # uuid
        if self._uuid_stmt is not None:
            container.append(self._uuid_stmt)
        else:
            val = self._uuid
            val = self.create_filter(
                WorkflowStepOrm.uuid,
                [(x, '==') for x in split_terms(val, RE_UUID4, '', '')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._uuid_stmt = val
                container.append(val)
        
        # workflow_id
        if self._workflow_id_stmt is not None:
            container.append(self._workflow_id_stmt)
        else:
            val = self._workflow_id
            if isinstance(val, int):
                val = str(val)
            val = self.create_filter(
                WorkflowStepOrm.workflow_id,
                [(x, '==') for x in split_terms(val, RE_INT_VALUE, '', '')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._workflow_id_stmt = val
                container.append(val)

        # pos
        if self._pos_stmt is not None:
            container.append(self._pos_stmt)
        else:
            val = self._pos
            if isinstance(val, int):
                val = str(val)
            val = self.create_filter(
                WorkflowStepOrm.pos,
                split_numeric(val),
                use_and
            )
            if val is not None:
                self._has_params = True
                self._pos_stmt = val
                container.append(val)
        
        # allow_if_previous_failed
        if self._allow_if_previous_failed_stmt is not None:
            container.append(self._allow_if_previous_failed_stmt)
        else:
            val = self._allow_if_previous_failed
            if val is not None:
                val = self.create_filter(
                    WorkflowStepOrm.allow_if_previous_failed,
                    [(val, '==')],
                    use_and
                )
                if val is not None:
                    self._has_params = True
                    self._allow_if_previous_failed_stmt = val
                    container.append(val)

        # action
        if self._action_stmt is not None:
            container.append(self._action_stmt)
        else:
            val = self._action
            val = self.create_filter(
                WorkflowStepOrm.action,
                [(x, 'like') for x in split_terms(val, RE_SLUG, '%', '%')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._action_stmt = val
                container.append(val)
        
        # topic_send
        if self._topic_send_stmt is not None:
            container.append(self._topic_send_stmt)
        else:
            val = self._topic_send
            val = self.create_filter(
                WorkflowStepOrm.topic_send,
                [(x, 'like') for x in split_terms(val, RE_SLUG, '%', '%')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._topic_send_stmt = val
                container.append(val)
        
        # topic_receive
        if self._topic_receive_stmt is not None:
            container.append(self._topic_receive_stmt)
        else:
            val = self._topic_receive
            val = self.create_filter(
                WorkflowStepOrm.topic_receive,
                [(x, 'like') for x in split_terms(val, RE_SLUG, '%', '%')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._topic_receive_stmt = val
                container.append(val)

        # Return True if conditions were added
        return self._has_params
    

    # Getters for raw values

    @property
    def id(self) -> int | str | None:
        return self._id
    
    @property
    def uuid(self) -> str | None:
        return self._uuid
    
    @property
    def workflow_id(self) -> int | str | None:
        return self._workflow_id
    
    @property
    def pos(self) -> int | None:
        return self._pos
    
    @property
    def allow_if_previous_failed(self) -> bool | None:
        return self._allow_if_previous_failed
    
    @property
    def action(self) -> str | None:
        return self._action
    
    @property
    def topic_send(self) -> str | None:
        return self._topic_send
    
    @property
    def topic_receive(self) -> str | None:
        return self._topic_receive
    
    @property
    def progress(self) -> int | float | str | None:
        return self._progress
    
    @property
    def status(self) -> int | str | None:
        return self._status
    
    @property
    def reason(self) -> str | None:
        return self._reason
    

    # Getters for statements
    
    @property
    def id_stmt(self) -> typing.Any:
        return self._id_stmt
    
    @property
    def uuid_stmt(self) -> typing.Any:
        return self._uuid_stmt
    
    @property
    def workflow_id_stmt(self) -> typing.Any:
        return self._workflow_id_stmt
    
    @property
    def pos_stmt(self) -> typing.Any:
        return self._pos_stmt
    
    @property
    def allow_if_previous_failed_stmt(self) -> typing.Any:
        return self._allow_if_previous_failed_stmt
    
    @property
    def action_stmt(self) -> typing.Any:
        return self._action_stmt
    
    @property
    def topic_send_stmt(self) -> typing.Any:
        return self._topic_send_stmt
    
    @property
    def topic_receive_stmt(self) -> typing.Any:
        return self._topic_receive_stmt
    
    @property
    def progress_stmt(self) -> typing.Any:
        return self._progress_stmt
    
    @property
    def status_stmt(self) -> typing.Any:
        return self._status_stmt
    
    @property
    def reason_stmt(self) -> typing.Any:
        return self._reason_stmt



### Define Interfaces

class WorkflowStepQueryParamsPrimary(WorkflowStepQueryParams):
    def __init__(self,
        id: int | str | None = None,
        uuid: str | None = None,
        workflow: int | str | None = None,
        workflow_id: int | str | None = None,
        pos: int | None = None,
        allow_if_previous_failed: bool | None = None,
        action: str | None = None,
        topic_send: str | None = None,
        topic_receive: str | None = None,
        progress: int | float | str | None = None,
        status: int | str | None = None,
        reason: str | None = None
    ):
        super().__init__(
            id,
            uuid,
            workflow_id if workflow_id is not None else workflow,
            pos,
            allow_if_previous_failed,
            action,
            topic_send,
            topic_receive,
            progress,
            status,
            reason
        )


class WorkflowStepQueryParamsGeneral(WorkflowStepQueryParams):
    def __init__(self,
        workflow_step_id: int | str | None = None,
        workflow_step_uuid: str | None = None,
        workflow_step_workflow: int | str | None = None,
        workflow_step_workflow_id: int | str | None = None,
        workflow_step_pos: int | None = None,
        workflow_step_allow_if_previous_failed: bool | None = None,
        workflow_step_action: str | None = None,
        workflow_step_topic_send: str | None = None,
        workflow_step_topic_receive: str | None = None,
        workflow_step_progress: int | float | str | None = None,
        workflow_step_status: int | str | None = None,
        workflow_step_reason: str | None = None
    ):
        super().__init__(
            workflow_step_id,
            workflow_step_uuid,
            workflow_step_workflow_id if workflow_step_workflow_id is not None else workflow_step_workflow,
            workflow_step_pos,
            workflow_step_allow_if_previous_failed,
            workflow_step_action,
            workflow_step_topic_send,
            workflow_step_topic_receive,
            workflow_step_progress,
            workflow_step_status,
            workflow_step_reason
        )


