# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import typing
import datetime

import sqlalchemy
from sqlalchemy.orm import (
    Mapped,
    mapped_column,
    relationship,
    validates
)

from regex_patterns import (
    RE_INT_VALUE,
    RE_FLOAT_VALUE,
    RE_LANG_CODE,
    RE_UUID4,
    RE_SLUG
)
from data_utils import (
    is_empty,
    is_true,
    split_terms,
    split_datetime,
    sanitize_text
)
from ..base import BaseOrm





class WorkflowStepOrm(BaseOrm):
    __tablename__ = 'workflow_steps'
    id: Mapped[int] = mapped_column(
        sqlalchemy.Integer, primary_key=True
    )
    uuid: Mapped[str] = mapped_column(sqlalchemy.String)
    workflow_id: Mapped[int] = mapped_column(sqlalchemy.ForeignKey('workflows.id'))
    pos: Mapped[int] = mapped_column(sqlalchemy.Integer, default=0)
    allow_if_previous_failed: Mapped[bool] = mapped_column(sqlalchemy.Boolean, default=False)
    action: Mapped[str] = mapped_column(sqlalchemy.String)
    topic_send: Mapped[typing.Optional[str]] = mapped_column(sqlalchemy.String, default=None, nullable=True)
    topic_receive: Mapped[typing.Optional[str]] = mapped_column(sqlalchemy.String, default=None, nullable=True)
    progress: Mapped[float] = mapped_column(sqlalchemy.Float, default=0.0)
    status: Mapped[int] = mapped_column(sqlalchemy.Integer, default=0)
    reason: Mapped[typing.Optional[str]] = mapped_column(sqlalchemy.String, default=None, nullable=True)

    workflow: Mapped['WorkflowOrm'] = relationship(
        back_populates='steps'
    )
    
    def __repr__(self):
        return (
            f'<WorkflowStep('
            f'id={self.id}, '
            f'uuid={self.uuid}, '
            f'workflow_id={self.workflow_id}, '
            f'pos={self.pos}, '
            f'allow_if_previous_failed={self.allow_if_previous_failed}, '
            f'action={self.action}, '
            f'topic_send={self.topic_send}, '
            f'topic_receive={self.topic_receive}, '
            f'progress={self.progress}, '
            f'status={self.status}, '
            f'reason={self.reason})>'
        )
    
    def __str__(self):
        return (
            f'WorkflowStep('
            f'id={self.id}, '
            f'uuid={self.uuid}, '
            f'workflow_id={self.workflow_id}, '
            f'pos={self.pos}, '
            f'allow_if_previous_failed={self.allow_if_previous_failed}, '
            f'action={self.action}, '
            f'topic_send={self.topic_send}, '
            f'topic_receive={self.topic_receive}, '
            f'progress={self.progress}, '
            f'status={self.status}, '
            f'reason={self.reason})'
        )
    
    def __json__(self):
        return {
            'id': self.id,
            'uuid': self.uuid,
            'workflow_id': self.workflow_id,
            'pos': self.pos,
            'allow_if_previous_failed': self.allow_if_previous_failed,
            'action': self.action,
            'topic_send': self.topic_send,
            'topic_receive': self.topic_receive,
            'progress': self.progress,
            'status': self.status,
            'reason': self.reason
        }
    
    def _fields(self) -> tuple:
        return (
            ['id', 'uuid', 'workflow_id', 'pos', 'allow_if_previous_failed', 'action', 'topic_send', 'topic_receive', 'progress', 'status', 'reason'],
            [('workflow', False, None)]
        )
    

    @validates('uuid')
    def validate_uuid(self, key, value):
        if is_empty(value):
            raise ValueError("UUID is required")
        if isinstance(value, str):
            result = RE_UUID4.match(value)
            if result is None:
                raise ValueError("Invalid UUID")
            return result.group(1)
        raise ValueError("Invalid UUID")
    
    @validates('workflow_id')
    def validate_workflow_id(self, key, value):
        if is_empty(value):
            raise ValueError("Workflow ID is required")
        if isinstance(value, int):
            return value
        if isinstance(value, str):
            result = RE_INT_VALUE.match(value)
            if result is None:
                raise ValueError("Invalid Workflow ID")
            return int(result.group(1))
        raise ValueError("Invalid Workflow ID")

    @validates('pos')
    def validate_pos(self, key, value):
        if is_empty(value):
            raise ValueError("Position is required")
        if isinstance(value, int):
            return value
        if isinstance(value, str):
            result = RE_INT_VALUE.match(value)
            if result is None:
                raise ValueError("Invalid position")
            return int(result.group(1))
        raise ValueError("Invalid position")

    @validates('allow_if_previous_failed')
    def validate_allow_if_previous_failed(self, key, value):
        return is_true(value)

    @validates('action')
    def validate_action(self, key, value):
        if is_empty(value):
            raise ValueError("Action is required")
        if isinstance(value, str):
            result = RE_SLUG.match(value)
            if result is None:
                raise ValueError("Invalid action")
            return result.group(1)
        raise ValueError("Invalid action")
    
    @validates('topic_send')
    def validate_topic_send(self, key, value):
        if is_empty(value):
            return None
        if isinstance(value, str):
            result = RE_SLUG.match(value)
            if result is None:
                raise ValueError("Invalid sending topic")
            return result.group(1)
        raise ValueError("Invalid sending topic")
    
    @validates('topic_receive')
    def validate_topic_receive(self, key, value):
        if is_empty(value):
            return None
        if isinstance(value, str):
            result = RE_SLUG.match(value)
            if result is None:
                raise ValueError("Invalid receiving topic")
            return result.group(1)
        raise ValueError("Invalid receiving topic")

    @validates('progress')
    def validate_progress(self, key, value):
        if is_empty(value):
            raise ValueError("Progress is required")
        if isinstance(value, float):
            return value
        if isinstance(value, int):
            return float(value)
        if isinstance(value, str):
            result = RE_FLOAT_VALUE.match(value)
            if result is None:
                result = RE_INT_VALUE.match(value)
                if result is None:
                    raise ValueError("Invalid progress")
                return float(result.group(1))
            return float(result.group(1))
        raise ValueError("Invalid progress")

    @validates('status')
    def validate_status(self, key, value):
        if is_empty(value):
            raise ValueError("Status is required")
        if isinstance(value, int):
            return value
        if isinstance(value, str):
            result = RE_INT_VALUE.match(value)
            if result is None:
                raise ValueError("Invalid status")
            return int(result.group(1))
        raise ValueError("Invalid status")

    @validates('reason')
    def validate_reason(self, key, value):
        return sanitize_text(value)


