# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import re
import typing
from .orm import AccessTokenOrm





ACCESS_TOKEN__FIELDS_MAP = {
    'access_token': {
        'id': AccessTokenOrm.id,
        'created_at': AccessTokenOrm.created_at,
        'owner': AccessTokenOrm.owner_id,
        'owner_id': AccessTokenOrm.owner_id,
        'name': AccessTokenOrm.name,
        'key': AccessTokenOrm.key,
        'token': AccessTokenOrm.token,
        'is_active': AccessTokenOrm.is_active,
        'valid_until': AccessTokenOrm.valid_until
    },
    'access_token_id': AccessTokenOrm.id,
    'access_token_created_at': AccessTokenOrm.created_at,
    'access_token_owner': AccessTokenOrm.owner_id,
    'access_token_owner_id': AccessTokenOrm.owner_id,
    'access_token_name': AccessTokenOrm.name,
    'access_token_key': AccessTokenOrm.key,
    'access_token_token': AccessTokenOrm.token,
    'access_token_is_active': AccessTokenOrm.is_active,
    'access_token_valid_until': AccessTokenOrm.valid_until,

    'access_token.id': AccessTokenOrm.id,
    'access_token.created_at': AccessTokenOrm.created_at,
    'access_token.owner': AccessTokenOrm.owner_id,
    'access_token.owner_id': AccessTokenOrm.owner_id,
    'access_token.name': AccessTokenOrm.name,
    'access_token.key': AccessTokenOrm.key,
    'access_token.token': AccessTokenOrm.token,
    'access_token.is_active': AccessTokenOrm.is_active,
    'access_token.valid_until': AccessTokenOrm.valid_until
}

ACCESS_TOKEN__VARIANTS_MAP = {
    'access_token': {
        'id': 'access_token.id',
        'created_at': 'access_token.created_at',
        'owner': 'access_token.owner_id',
        'owner_id': 'access_token.owner_id',
        'name': 'access_token.name',
        'key': 'access_token.key',
        'token': 'access_token.token',
        'is_active': 'access_token.is_active',
        'valid_until': 'access_token.valid_until'
    },
    'access_token_id': 'access_token.id',
    'access_token_created_at': 'access_token.created_at',
    'access_token_owner': 'access_token.owner_id',
    'access_token_owner_id': 'access_token.owner_id',
    'access_token_name': 'access_token.name',
    'access_token_key': 'access_token.key',
    'access_token_token': 'access_token.token',
    'access_token_is_active': 'access_token.is_active',
    'access_token_valid_until': 'access_token.valid_until',

    'access_token.id': 'access_token.id',
    'access_token.created_at': 'access_token.created_at',
    'access_token.owner': 'access_token.owner_id',
    'access_token.owner_id': 'access_token.owner_id',
    'access_token.name': 'access_token.name',
    'access_token.key': 'access_token.key',
    'access_token.token': 'access_token.token',
    'access_token.is_active': 'access_token.is_active',
    'access_token.valid_until': 'access_token.valid_until'
}



############
### Primary

ACCESS_TOKEN__PRIMARY_FIELDS_MAP = {
    'id': AccessTokenOrm.id,
    'created_at': AccessTokenOrm.created_at,
    'owner': AccessTokenOrm.owner_id,
    'owner_id': AccessTokenOrm.owner_id,
    'name': AccessTokenOrm.name,
    'key': AccessTokenOrm.key,
    'token': AccessTokenOrm.token,
    'is_active': AccessTokenOrm.is_active,
    'valid_until': AccessTokenOrm.valid_until
}

ACCESS_TOKEN__PRIMARY_VARIANTS_MAP = {
    'id': 'access_token.id',
    'created_at': 'access_token.created_at',
    'owner': 'access_token.owner_id',
    'owner_id': 'access_token.owner_id',
    'name': 'access_token.name',
    'key': 'access_token.key',
    'token': 'access_token.token',
    'is_active': 'access_token.is_active',
    'valid_until': 'access_token.valid_until'
}


