# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import typing
import datetime

from regex_patterns import (
    RE_INT_VALUE,
    RE_ALPHANUM_TERM,
    RE_SHA256_HEX,
    RE_SHAKE256_KEY_HEX
)
from data_utils import (
    split_terms,
    split_datetime
)
from ..base import (
    BaseQueryParams
)
from .orm import AccessTokenOrm





class AccessTokenQueryParams(BaseQueryParams):
    __slots__ = [
        '_id',
        '_created_at',
        '_owner_id',
        '_name',
        '_key',
        '_token',
        '_is_active',
        '_valid_until',

        '_id_stmt',
        '_created_at_stmt',
        '_owner_id_stmt',
        '_name_stmt',
        '_key_stmt',
        '_token_stmt',
        '_is_active_stmt',
        '_valid_until_stmt'
    ]

    def __init__(self, 
        id: int | str | None = None,
        created_at: int | float | str | None = None,
        owner_id: int | str | None = None,
        name: str | None = None,
        key: str | None = None,
        token: str | None = None,
        is_active: bool | None = None,
        valid_until: int | float | str | None = None
    ):
        super().__init__()

        # Store raw values
        self._id = id
        self._created_at = created_at
        self._owner_id = owner_id
        self._name = name
        self._key = key
        self._token = token
        self._is_active = is_active
        self._valid_until = valid_until

        # Init statement values
        self._id_stmt = None
        self._created_at_stmt = None
        self._owner_id_stmt = None
        self._name_stmt = None
        self._key_stmt = None
        self._token_stmt = None
        self._is_active_stmt = None
        self._valid_until_stmt = None


    def add_conditions(self, container: list, use_and: bool = False) -> bool:

        # id
        if self._id_stmt is not None:
            container.append(self._id_stmt)
        else:
            val = self._id
            if isinstance(val, int):
                val = str(val)
            val = self.create_filter(
                AccessTokenOrm.id,
                [(x, '==') for x in split_terms(val, RE_INT_VALUE, '', '')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._id_stmt = val
                container.append(val)
        
        # created_at
        if self._created_at_stmt is not None:
            container.append(self._created_at_stmt)
        else:
            val = self._created_at
            val = self.create_filter(
                AccessTokenOrm.created_at,
                split_datetime(val),
                use_and
            )
            if val is not None:
                self._has_params = True
                self._created_at_stmt = val
                container.append(val)

        # owner_id
        if self._owner_id_stmt is not None:
            container.append(self._owner_id_stmt)
        else:
            val = self._owner_id
            if isinstance(val, int):
                val = str(val)
            val = self.create_filter(
                AccessTokenOrm.owner_id,
                [(x, '==') for x in split_terms(val, RE_INT_VALUE, '', '')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._owner_id_stmt = val
                container.append(val)
        
        # name
        if self._name_stmt is not None:
            container.append(self._name_stmt)
        else:
            val = self._name
            val = self.create_filter(
                AccessTokenOrm.token,
                [(x, 'like') for x in split_terms(val, RE_ALPHANUM_TERM, '%', '%')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._name_stmt = val
                container.append(val)
        
        # key
        if self._key_stmt is not None:
            container.append(self._key_stmt)
        else:
            val = self._key
            val = self.create_filter(
                AccessTokenOrm.token,
                [(x, 'like') for x in split_terms(val, RE_SHAKE256_KEY_HEX, '%', '%')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._key_stmt = val
                container.append(val)
        
        # token
        if self._token_stmt is not None:
            container.append(self._token_stmt)
        else:
            val = self._token
            val = self.create_filter(
                AccessTokenOrm.token,
                [(x, 'like') for x in split_terms(val, RE_SHA256_HEX, '%', '%')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._token_stmt = val
                container.append(val)
        
        # is_active
        if self._is_active_stmt is not None:
            container.append(self._is_active_stmt)
        else:
            val = self._is_active
            if val is not None:
                val = self.create_filter(
                    AccessTokenOrm.is_active,
                    [(val, '==')],
                    use_and
                )
                if val is not None:
                    self._has_params = True
                    self._is_active_stmt = val
                    container.append(val)
        
        # valid_until
        if self._valid_until_stmt is not None:
            container.append(self._valid_until_stmt)
        else:
            val = self._valid_until
            val = self.create_filter(
                AccessTokenOrm.valid_until,
                split_datetime(val),
                use_and
            )
            if val is not None:
                self._has_params = True
                self._valid_until_stmt = val
                container.append(val)

        # Return True if conditions were added
        return self._has_params
    

    # Getters for raw values

    @property
    def id(self) -> int | str | None:
        return self._id
    
    @property
    def created_at(self) -> int | float | str | None:
        return self._created_at
    
    @property
    def owner_id(self) -> int | str | None:
        return self._owner_id
    
    @property
    def name(self) -> str | None:
        return self._name
    
    @property
    def key(self) -> str | None:
        return self._key
    
    @property
    def token(self) -> str | None:
        return self._token
    
    @property
    def is_active(self) -> bool | None:
        return self._is_active
    
    @property
    def valid_until(self) -> int | float | str | None:
        return self._valid_until
    

    # Getters for statements
    
    @property
    def id_stmt(self) -> typing.Any:
        return self._id_stmt
    
    @property
    def created_at_stmt(self) -> typing.Any:
        return self._created_at_stmt
    
    @property
    def owner_id_stmt(self) -> typing.Any:
        return self._owner_id_stmt
    
    @property
    def name_stmt(self) -> typing.Any:
        return self._name_stmt
    
    @property
    def key_stmt(self) -> typing.Any:
        return self._key_stmt
    
    @property
    def token_stmt(self) -> typing.Any:
        return self._token_stmt
    
    @property
    def is_active_stmt(self) -> typing.Any:
        return self._is_active_stmt
    
    @property
    def valid_until_stmt(self) -> typing.Any:
        return self._valid_until_stmt



### Define Interfaces

class AccessTokenQueryParamsPrimary(AccessTokenQueryParams):
    def __init__(self,
        id: int | str | None = None,
        created_at: int | float | str | None = None,
        owner: int | str | None = None,
        owner_id: int | str | None = None,
        name: str | None = None,
        key: str | None = None,
        token: str | None = None,
        is_active: bool | None = None,
        valid_until: int | float | str | None = None
    ):
        super().__init__(
            id,
            created_at,
            owner_id if owner_id is not None else owner,
            name,
            key,
            token,
            is_active,
            valid_until
        )


class AccessTokenQueryParamsGeneral(AccessTokenQueryParams):
    def __init__(self,
        access_token_id: int | str | None = None,
        access_token_created_at: int | float | str | None = None,
        access_token_owner: int | str | None = None,
        access_token_owner_id: int | str | None = None,
        access_token_name: str | None = None,
        access_token_key: str | None = None,
        access_token_token: str | None = None,
        access_token_is_active: bool | None = None,
        access_token_valid_until: int | float | str | None = None
    ):
        super().__init__(
            access_token_id,
            access_token_created_at,
            access_token_owner_id if access_token_owner_id is not None else access_token_owner,
            access_token_name,
            access_token_key,
            access_token_token,
            access_token_is_active,
            access_token_valid_until
        )


