# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import typing
import datetime

from regex_patterns import (
    RE_INT_VALUE,
    RE_LANG_CODE,
    RE_UUID4,
    RE_ALPHANUM_TERM,
    RE_SLUG,
    RE_ALPHANUM_ASCII_WSPACE
)
from data_utils import (
    split_terms,
    split_datetime,
    split_numeric
)
from ..base import (
    BaseQueryParams
)
from .orm import PipelineConsumerOrm





class PipelineConsumerQueryParams(BaseQueryParams):
    __slots__ = [
        '_id',
        '_uuid',
        '_created_at',
        '_pipeline_id',
        '_name',
        '_client_id',
        '_group_id',

        '_id_stmt',
        '_uuid_stmt',
        '_created_at_stmt',
        '_pipeline_id_stmt',
        '_name_stmt',
        '_client_id_stmt',
        '_group_id_stmt'
    ]

    def __init__(self, 
        id: int | str | None = None,
        uuid: str | None = None,
        created_at: int | float | str | None = None,
        pipeline_id: str | None = None,
        name: str | None = None,
        client_id: str | None = None,
        group_id: str | None = None
    ):
        super().__init__()

        # Store raw values
        self._id = id
        self._uuid = uuid
        self._created_at = created_at
        self._pipeline_id = pipeline_id
        self._name = name
        self._client_id = client_id
        self._group_id = group_id

        # Init statement values
        self._id_stmt = None
        self._uuid_stmt = None
        self._created_at_stmt = None
        self._pipeline_id_stmt = None
        self._name_stmt = None
        self._client_id_stmt = None
        self._group_id_stmt = None


    def add_conditions(self, container: list, use_and: bool = False) -> bool:

        # id
        if self._id_stmt is not None:
            container.append(self._id_stmt)
        else:
            val = self._id
            if isinstance(val, int):
                val = str(val)
            val = self.create_filter(
                PipelineConsumerOrm.id,
                [(x, '==') for x in split_terms(val, RE_INT_VALUE, '', '')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._id_stmt = val
                container.append(val)

        # uuid
        if self._uuid_stmt is not None:
            container.append(self._uuid_stmt)
        else:
            val = self._uuid
            val = self.create_filter(
                PipelineConsumerOrm.uuid,
                [(x, '==') for x in split_terms(val, RE_UUID4, '', '')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._uuid_stmt = val
                container.append(val)
        
        # created_at
        if self._created_at_stmt is not None:
            container.append(self._created_at_stmt)
        else:
            val = self._created_at
            val = self.create_filter(
                PipelineConsumerOrm.created_at,
                split_datetime(val),
                use_and
            )
            if val is not None:
                self._has_params = True
                self._created_at_stmt = val
                container.append(val)

        # pipeline_id
        if self._pipeline_id_stmt is not None:
            container.append(self._pipeline_id_stmt)
        else:
            val = self._pipeline_id
            if isinstance(val, int):
                val = str(val)
            val = self.create_filter(
                PipelineConsumerOrm.pipeline_id,
                [(x, '==') for x in split_terms(val, RE_INT_VALUE, '', '')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._pipeline_id_stmt = val
                container.append(val)

        # name
        if self._name_stmt is not None:
            container.append(self._name_stmt)
        else:
            val = self._name
            val = self.create_filter(
                PipelineConsumerOrm.name,
                [(x, 'like') for x in split_terms(val, RE_ALPHANUM_ASCII_WSPACE, '%', '%')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._name_stmt = val
                container.append(val)

        # client_id
        if self._client_id_stmt is not None:
            container.append(self._client_id_stmt)
        else:
            val = self._client_id
            val = self.create_filter(
                PipelineConsumerOrm.client_id,
                [(x, 'like') for x in split_terms(val, RE_SLUG, '%', '%')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._client_id_stmt = val
                container.append(val)

        # group_id
        if self._group_id_stmt is not None:
            container.append(self._group_id_stmt)
        else:
            val = self._group_id
            val = self.create_filter(
                PipelineConsumerOrm.group_id,
                [(x, 'like') for x in split_terms(val, RE_SLUG, '%', '%')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._group_id_stmt = val
                container.append(val)

        # Return True if conditions were added
        return self._has_params
    

    # Getters for raw values

    @property
    def id(self) -> int | str | None:
        return self._id
    
    @property
    def uuid(self) -> str | None:
        return self._uuid
    
    @property
    def created_at(self) -> int | float | str | None:
        return self._created_at

    @property
    def pipeline_id(self) -> int | str | None:
        return self._pipeline_id
    
    @property
    def name(self) -> str | None:
        return self._name
    
    @property
    def client_id(self) -> str | None:
        return self._client_id
    
    @property
    def group_id(self) -> str | None:
        return self._group_id
    

    # Getters for statements
    
    @property
    def id_stmt(self) -> typing.Any:
        return self._id_stmt
    
    @property
    def uuid_stmt(self) -> typing.Any:
        return self._uuid_stmt
    
    @property
    def created_at_stmt(self) -> typing.Any:
        return self._created_at_stmt
    
    @property
    def pipeline_id_stmt(self) -> typing.Any:
        return self._pipeline_id_stmt
    
    @property
    def name_stmt(self) -> typing.Any:
        return self._name_stmt
    
    @property
    def client_id_stmt(self) -> typing.Any:
        return self._client_id_stmt
    
    @property
    def group_id_stmt(self) -> typing.Any:
        return self._group_id_stmt



### Define Interfaces

class PipelineConsumerQueryParamsPrimary(PipelineConsumerQueryParams):
    def __init__(self,
        id: int | str | None = None,
        uuid: str | None = None,
        created_at: int | float | str | None = None,
        pipeline: str | None = None,
        pipeline_id: str | None = None,
        name: str | None = None,
        client_id: str | None = None,
        group_id: str | None = None
    ):
        super().__init__(
            id,
            uuid,
            created_at,
            pipeline_id if pipeline_id is not None else pipeline,
            name,
            client_id,
            group_id
        )


class PipelineConsumerQueryParamsGeneral(PipelineConsumerQueryParams):
    def __init__(self,
        pipeline_consumer_id: int | str | None = None,
        pipeline_consumer_uuid: str | None = None,
        pipeline_consumer_created_at: int | float | str | None = None,
        pipeline_consumer_pipeline: str | None = None,
        pipeline_consumer_pipeline_id: str | None = None,
        pipeline_consumer_name: str | None = None,
        pipeline_consumer_client_id: str | None = None,
        pipeline_consumer_group_id: str | None = None
    ):
        super().__init__(
            pipeline_consumer_id,
            pipeline_consumer_uuid,
            pipeline_consumer_created_at,
            pipeline_consumer_pipeline_id if pipeline_consumer_pipeline_id is not None else pipeline_consumer_pipeline,
            pipeline_consumer_name,
            pipeline_consumer_client_id,
            pipeline_consumer_group_id
        )


