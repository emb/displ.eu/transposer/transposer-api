# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

from .orm import PipelineConsumerOrm





PIPELINE_CONSUMER__FIELDS_MAP = {
    'pipeline_consumer': {
        'id': PipelineConsumerOrm.id,
        'uuid': PipelineConsumerOrm.uuid,
        'created_at': PipelineConsumerOrm.created_at,
        'pipeline': PipelineConsumerOrm.pipeline_id,
        'pipeline_id': PipelineConsumerOrm.pipeline_id,
        'name': PipelineConsumerOrm.name,
        'client': PipelineConsumerOrm.client_id,
        'client_id': PipelineConsumerOrm.client_id,
        'group': PipelineConsumerOrm.group_id,
        'group_id': PipelineConsumerOrm.group_id
    },
    'pipeline_consumer_id': PipelineConsumerOrm.id,
    'pipeline_consumer_uuid': PipelineConsumerOrm.uuid,
    'pipeline_consumer_created_at': PipelineConsumerOrm.created_at,
    'pipeline_consumer_pipeline': PipelineConsumerOrm.pipeline_id,
    'pipeline_consumer_pipeline_id': PipelineConsumerOrm.pipeline_id,
    'pipeline_consumer_name': PipelineConsumerOrm.name,
    'pipeline_consumer_client': PipelineConsumerOrm.client_id,
    'pipeline_consumer_client_id': PipelineConsumerOrm.client_id,
    'pipeline_consumer_group': PipelineConsumerOrm.group_id,
    'pipeline_consumer_group_id': PipelineConsumerOrm.group_id,

    'pipeline_consumer.id': PipelineConsumerOrm.id,
    'pipeline_consumer.uuid': PipelineConsumerOrm.uuid,
    'pipeline_consumer.created_at': PipelineConsumerOrm.created_at,
    'pipeline_consumer.pipeline': PipelineConsumerOrm.pipeline_id,
    'pipeline_consumer.pipeline_id': PipelineConsumerOrm.pipeline_id,
    'pipeline_consumer.name': PipelineConsumerOrm.name,
    'pipeline_consumer.client': PipelineConsumerOrm.client_id,
    'pipeline_consumer.client_id': PipelineConsumerOrm.client_id,
    'pipeline_consumer.group': PipelineConsumerOrm.group_id,
    'pipeline_consumer.group_id': PipelineConsumerOrm.group_id
}

PIPELINE_CONSUMER__VARIANTS_MAP = {
    'pipeline_consumer': {
        'id': 'pipeline_consumer.id',
        'uuid': 'pipeline_consumer.uuid',
        'created_at': 'pipeline_consumer.created_at',
        'pipeline': 'pipeline_consumer.pipeline_id',
        'pipeline_id': 'pipeline_consumer.pipeline_id',
        'name': 'pipeline_consumer.name',
        'client': 'pipeline_consumer.client_id',
        'client_id': 'pipeline_consumer.client_id',
        'group': 'pipeline_consumer.group_id',
        'group_id': 'pipeline_consumer.group_id'
    },
    'pipeline_consumer_id': 'pipeline_consumer.id',
    'pipeline_consumer_uuid': 'pipeline_consumer.uuid',
    'pipeline_consumer_created_at': 'pipeline_consumer.created_at',
    'pipeline_consumer_pipeline': 'pipeline_consumer.pipeline_id',
    'pipeline_consumer_pipeline_id': 'pipeline_consumer.pipeline_id',
    'pipeline_consumer_name': 'pipeline_consumer.name',
    'pipeline_consumer_client': 'pipeline_consumer.client_id',
    'pipeline_consumer_client_id': 'pipeline_consumer.client_id',
    'pipeline_consumer_group': 'pipeline_consumer.group_id',
    'pipeline_consumer_group_id': 'pipeline_consumer.group_id',

    'pipeline_consumer.id': 'pipeline_consumer.id',
    'pipeline_consumer.uuid': 'pipeline_consumer.uuid',
    'pipeline_consumer.created_at': 'pipeline_consumer.created_at',
    'pipeline_consumer.pipeline': 'pipeline_consumer.pipeline_id',
    'pipeline_consumer.pipeline_id': 'pipeline_consumer.pipeline_id',
    'pipeline_consumer.name': 'pipeline_consumer.name',
    'pipeline_consumer.client': 'pipeline_consumer.client_id',
    'pipeline_consumer.client_id': 'pipeline_consumer.client_id',
    'pipeline_consumer.group': 'pipeline_consumer.group_id',
    'pipeline_consumer.group_id': 'pipeline_consumer.group_id'
}



############
### Primary

PIPELINE_CONSUMER__PRIMARY_FIELDS_MAP = {
    'id': PipelineConsumerOrm.id,
    'uuid': PipelineConsumerOrm.uuid,
    'created_at': PipelineConsumerOrm.created_at,
    'pipeline': PipelineConsumerOrm.pipeline_id,
    'pipeline_id': PipelineConsumerOrm.pipeline_id,
    'name': PipelineConsumerOrm.name,
    'client': PipelineConsumerOrm.client_id,
    'client_id': PipelineConsumerOrm.client_id,
    'group': PipelineConsumerOrm.group_id,
    'group_id': PipelineConsumerOrm.group_id
}

PIPELINE_CONSUMER__PRIMARY_VARIANTS_MAP = {
    'id': 'pipeline_consumer.id',
    'uuid': 'pipeline_consumer.uuid',
    'created_at': 'pipeline_consumer.created_at',
    'pipeline': 'pipeline_consumer.pipeline_id',
    'pipeline_id': 'pipeline_consumer.pipeline_id',
    'name': 'pipeline_consumer.name',
    'client': 'pipeline_consumer.client_id',
    'client_id': 'pipeline_consumer.client_id',
    'group': 'pipeline_consumer.group_id',
    'group_id': 'pipeline_consumer.group_id'
}


