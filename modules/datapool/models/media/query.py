# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import typing
import datetime

from regex_patterns import (
    RE_INT_VALUE,
    RE_ALPHANUM_TERM,
    RE_HASH_XX,
    RE_XXHASH_CID_HEX,
    RE_UUID4,
    RE_FILE_EXTENSION,
    RE_FILE_PURPOSE
)
from data_utils import (
    split_terms,
    split_numeric,
    split_datetime,
    validate_uri as validate_uri_func,
    is_valid_mime_type,
    split_mime_types,
    sanitize_filepath,
    sanitize_filename,
    is_valid_filepath,
    is_valid_filename
)
from ..base import (
    BaseQueryParams
)
from .orm import MediaOrm





class MediaQueryParams(BaseQueryParams):
    __slots__ = [
        '_id',
        '_uuid',
        '_created_at',
        '_document_id',
        '_uri',
        '_path',
        '_filename',
        '_title',
        '_cid',
        '_mime',
        '_extension',
        '_size',
        '_duration',
        '_purpose',
        '_status',
        '_reason',

        '_id_stmt',
        '_uuid_stmt',
        '_created_at_stmt',
        '_document_id_stmt',
        '_uri_stmt',
        '_path_stmt',
        '_filename_stmt',
        '_title_stmt',
        '_cid_stmt',
        '_mime_stmt',
        '_extension_stmt',
        '_size_stmt',
        '_duration_stmt',
        '_purpose_stmt',
        '_status_stmt',
        '_reason_stmt'
    ]

    def __init__(self, 
        id: int | str | None = None,
        uuid: str | None = None,
        created_at: int | float | str | None = None,
        document_id: int | str | None = None,
        uri: str | None = None,
        path: str | None = None,
        filename: str | None = None,
        title: str | None = None,
        cid: str | None = None,
        mime: str | None = None,
        extension: str | None = None,
        size: int | str | None = None,
        duration: int | float | str | None = None,
        purpose: str | None = None,
        status: int | str | None = None,
        reason: str | None = None
    ):
        super().__init__()

        # Store raw values
        self._id = id
        self._uuid = uuid
        self._created_at = created_at
        self._document_id = document_id
        self._uri = uri
        self._path = path
        self._filename = filename
        self._title = title
        self._cid = cid
        self._mime = mime
        self._extension = extension
        self._size = size
        self._duration = duration
        self._purpose = purpose
        self._status = status
        self._reason = reason

        # Init statement values
        self._id_stmt = None
        self._uuid_stmt = None
        self._created_at_stmt = None
        self._document_id_stmt = None
        self._uri_stmt = None
        self._path_stmt = None
        self._filename_stmt = None
        self._title_stmt = None
        self._cid_stmt = None
        self._mime_stmt = None
        self._extension_stmt = None
        self._size_stmt = None
        self._duration_stmt = None
        self._purpose_stmt = None
        self._status_stmt = None
        self._reason_stmt = None


    def add_conditions(self, container: list, use_and: bool = False) -> bool:

        # id
        if self._id_stmt is not None:
            container.append(self._id_stmt)
        else:
            val = self._id
            if isinstance(val, int):
                val = str(val)
            val = self.create_filter(
                MediaOrm.id,
                [(x, '==') for x in split_terms(val, RE_INT_VALUE, '', '')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._id_stmt = val
                container.append(val)
        
        # uuid
        if self._uuid_stmt is not None:
            container.append(self._uuid_stmt)
        else:
            val = self._uuid
            val = self.create_filter(
                MediaOrm.uuid,
                [(x, '==') for x in split_terms(val, RE_UUID4, '', '')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._uuid_stmt = val
                container.append(val)
        
        # created_at
        if self._created_at_stmt is not None:
            container.append(self._created_at_stmt)
        else:
            val = self._created_at
            val = self.create_filter(
                MediaOrm.created_at,
                split_datetime(val),
                use_and
            )
            if val is not None:
                self._has_params = True
                self._created_at_stmt = val
                container.append(val)

        # document_id
        if self._document_id_stmt is not None:
            container.append(self._document_id_stmt)
        else:
            val = self._document_id
            if isinstance(val, int):
                val = str(val)
            val = self.create_filter(
                MediaOrm.document_id,
                [(x, '==') for x in split_terms(val, RE_INT_VALUE, '', '')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._document_id_stmt = val
                container.append(val)
    
        # uri
        if self._uri_stmt is not None:
            container.append(self._uri_stmt)
        else:
            val = self._uri
            val = self.create_filter(
                MediaOrm.uri,
                [(f'%{val}%', 'like')] if validate_uri_func(val) else [],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._uri_stmt = val
                container.append(val)
    
        # path
        if self._path_stmt is not None:
            container.append(self._path_stmt)
        else:
            val = self._path
            val = self.create_filter(
                MediaOrm.path,
                [(f'%{val}%', 'like')] if is_valid_filepath(val) else [],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._path_stmt = val
                container.append(val)
    
        # filename
        if self._filename_stmt is not None:
            container.append(self._filename_stmt)
        else:
            val = self._filename
            val = self.create_filter(
                MediaOrm.filename,
                [(f'%{val}%', 'like')] if is_valid_filename(val) else [],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._filename_stmt = val
                container.append(val)
    
        # title
        if self._title_stmt is not None:
            container.append(self._title_stmt)
        else:
            val = self._title
            val = self.create_filter(
                MediaOrm.title,
                [(f'%{val}%', 'like')] if is_valid_filename(val) else [],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._title_stmt = val
                container.append(val)
        
        # cid
        if self._cid_stmt is not None:
            container.append(self._cid_stmt)
        else:
            val = self._cid
            val = self.create_filter(
                MediaOrm.cid,
                [(x, '==') for x in split_terms(val, RE_HASH_XX, '', '')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._cid_stmt = val
                container.append(val)
        
        # mime
        if self._mime_stmt is not None:
            container.append(self._mime_stmt)
        else:
            val = self._mime
            val = self.create_filter(
                MediaOrm.mime,
                [(x, 'like') for x in split_mime_types(val, '%', '%')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._mime_stmt = val
                container.append(val)
        
        # extension
        if self._extension_stmt is not None:
            container.append(self._extension_stmt)
        else:
            val = self._extension
            val = self.create_filter(
                MediaOrm.extension,
                [(x, '==') for x in split_terms(val, RE_FILE_EXTENSION, '', '')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._extension_stmt = val
                container.append(val)
        
        # size
        if self._size_stmt is not None:
            container.append(self._size_stmt)
        else:
            val = self._size
            if isinstance(val, int):
                val = str(val)
            val = self.create_filter(
                MediaOrm.size,
                split_numeric(val),
                use_and
            )
            if val is not None:
                self._has_params = True
                self._size_stmt = val
                container.append(val)
        
        # duration
        if self._duration_stmt is not None:
            container.append(self._duration_stmt)
        else:
            val = self._duration
            if isinstance(val, (int, float)):
                val = str(val)
            val = self.create_filter(
                MediaOrm.duration,
                split_numeric(val),
                use_and
            )
            if val is not None:
                self._has_params = True
                self._duration_stmt = val
                container.append(val)
        
        # purpose
        if self._purpose_stmt is not None:
            container.append(self._purpose_stmt)
        else:
            val = self._purpose
            val = self.create_filter(
                MediaOrm.purpose,
                [(x, '==') for x in split_terms(val, RE_FILE_PURPOSE, '', '')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._purpose_stmt = val
                container.append(val)
        
        # status
        if self._status_stmt is not None:
            container.append(self._status_stmt)
        else:
            val = self._status
            if isinstance(val, int):
                val = str(val)
            val = self.create_filter(
                MediaOrm.status,
                split_numeric(val),
                use_and
            )
            if val is not None:
                self._has_params = True
                self._status_stmt = val
                container.append(val)
        
        # reason
        if self._reason_stmt is not None:
            container.append(self._reason_stmt)
        else:
            val = self._reason
            val = self.create_filter(
                MediaOrm.reason,
                [(x, 'like') for x in split_terms(val, RE_ALPHANUM_TERM, '%', '%')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._reason_stmt = val
                container.append(val)

        # Return True if conditions were added
        return self._has_params
    

    # Getters for raw values

    @property
    def id(self) -> int | str | None:
        return self._id
    
    @property
    def uuid(self) -> str | None:
        return self._uuid
    
    @property
    def created_at(self) -> int | float | str | None:
        return self._created_at
    
    @property
    def document_id(self) -> int | str | None:
        return self._document_id
    
    @property
    def uri(self) -> str | None:
        return self._uri
    
    @property
    def path(self) -> str | None:
        return self._path
    
    @property
    def filename(self) -> str | None:
        return self._filename
    
    @property
    def title(self) -> str | None:
        return self._title
    
    @property
    def cid(self) -> str | None:
        return self._cid
    
    @property
    def mime(self) -> str | None:
        return self._mime
    
    @property
    def extension(self) -> str | None:
        return self._extension
    
    @property
    def size(self) -> int | str | None:
        return self._size
    
    @property
    def duration(self) -> int | float | str | None:
        return self._duration
    
    @property
    def purpose(self) -> str | None:
        return self._purpose
    
    @property
    def status(self) -> int | str | None:
        return self._status
    
    @property
    def reason(self) -> str | None:
        return self._reason
    

    # Getters for statements
    
    @property
    def id_stmt(self) -> typing.Any:
        return self._id_stmt
    
    @property
    def uuid_stmt(self) -> typing.Any:
        return self._uuid_stmt
    
    @property
    def created_at_stmt(self) -> typing.Any:
        return self._created_at_stmt
    
    @property
    def document_id_stmt(self) -> typing.Any:
        return self._document_id_stmt
    
    @property
    def uri_stmt(self) -> typing.Any:
        return self._uri_stmt
    
    @property
    def path_stmt(self) -> typing.Any:
        return self._path_stmt
    
    @property
    def filename_stmt(self) -> typing.Any:
        return self._filename_stmt
    
    @property
    def title_stmt(self) -> typing.Any:
        return self._title_stmt
    
    @property
    def cid_stmt(self) -> typing.Any:
        return self._cid_stmt
    
    @property
    def mime_stmt(self) -> typing.Any:
        return self._mime_stmt
    
    @property
    def extension_stmt(self) -> typing.Any:
        return self._extension_stmt
    
    @property
    def size_stmt(self) -> typing.Any:
        return self._size_stmt
    
    @property
    def duration_stmt(self) -> typing.Any:
        return self._duration_stmt
    
    @property
    def purpose_stmt(self) -> typing.Any:
        return self._purpose_stmt
    
    @property
    def status_stmt(self) -> typing.Any:
        return self._status_stmt
    
    @property
    def reason_stmt(self) -> typing.Any:
        return self._reason_stmt



### Define Interfaces

class MediaQueryParamsPrimary(MediaQueryParams):
    def __init__(self,
        id: int | str | None = None,
        uuid: str | None = None,
        created_at: int | float | str | None = None,
        document: int | str | None = None,
        document_id: int | str | None = None,
        uri: str | None = None,
        path: str | None = None,
        filename: str | None = None,
        title: str | None = None,
        cid: str | None = None,
        mime: str | None = None,
        extension: str | None = None,
        size: int | str | None = None,
        duration: int | float | str | None = None,
        purpose: str | None = None,
        status: int | str | None = None,
        reason: str | None = None
    ):
        super().__init__(
            id,
            uuid,
            created_at,
            document_id if document_id is not None else document,
            uri,
            path,
            filename,
            title,
            cid,
            mime,
            extension,
            size,
            duration,
            purpose,
            status,
            reason
        )


class MediaQueryParamsGeneral(MediaQueryParams):
    def __init__(self,
        media_id: int | str | None = None,
        media_uuid: str | None = None,
        media_created_at: int | float | str | None = None,
        media_document: int | str | None = None,
        media_document_id: int | str | None = None,
        media_uri: str | None = None,
        media_path: str | None = None,
        media_filename: str | None = None,
        media_title: str | None = None,
        media_cid: str | None = None,
        media_mime: str | None = None,
        media_extension: str | None = None,
        media_size: int | str | None = None,
        media_duration: int | float | str | None = None,
        media_purpose: str | None = None,
        media_status: int | str | None = None,
        media_reason: str | None = None
    ):
        super().__init__(
            media_id,
            media_uuid,
            media_created_at,
            media_document_id if media_document_id is not None else media_document,
            media_uri,
            media_path,
            media_filename,
            media_title,
            media_cid,
            media_mime,
            media_extension,
            media_size,
            media_duration,
            media_purpose,
            media_status,
            media_reason
        )


