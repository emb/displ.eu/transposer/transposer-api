# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import typing
import datetime

import sqlalchemy
from sqlalchemy.orm import (
    Mapped,
    mapped_column,
    relationship,
    validates
)

from regex_patterns import (
    RE_INT_VALUE,
    RE_FLOAT_VALUE,
    RE_UUID4,
    RE_ALPHANUM_TERM,
    RE_HASH_XX,
    RE_XXHASH_CID_HEX,
    RE_FILE_EXTENSION,
    RE_FILE_PURPOSE
)
from data_utils import (
    is_empty, is_true,
    split_terms,
    split_numeric,
    split_datetime,
    sanitize_text,
    validate_uri as validate_uri_func,
    is_valid_mime_type,
    sanitize_filepath,
    sanitize_filename
)
from ..base import BaseOrm





class MediaOrm(BaseOrm):
    __tablename__ = 'medias'
    id: Mapped[int] = mapped_column(
        sqlalchemy.Integer, primary_key=True
    )
    uuid: Mapped[str] = mapped_column(sqlalchemy.String)
    created_at: Mapped[datetime.datetime] = mapped_column(
        sqlalchemy.DateTime, default=sqlalchemy.sql.functions.now()
    )
    document_id: Mapped[int] = mapped_column(sqlalchemy.ForeignKey('documents.id'))
    uri: Mapped[typing.Optional[str]] = mapped_column(sqlalchemy.String, default=None, nullable=True)
    path: Mapped[typing.Optional[str]] = mapped_column(sqlalchemy.String, default=None, nullable=True)
    filename: Mapped[str] = mapped_column(sqlalchemy.String)
    title: Mapped[str] = mapped_column(sqlalchemy.String)
    cid: Mapped[str] = mapped_column(sqlalchemy.String)
    mime: Mapped[str] = mapped_column(sqlalchemy.String)
    extension: Mapped[str] = mapped_column(sqlalchemy.String)
    size: Mapped[int] = mapped_column(sqlalchemy.Integer, default=0)
    duration: Mapped[float] = mapped_column(sqlalchemy.Float, default=0.0)
    purpose: Mapped[str] = mapped_column(sqlalchemy.String, default='original')
    status: Mapped[int] = mapped_column(sqlalchemy.Integer, default=0)
    reason: Mapped[typing.Optional[str]] = mapped_column(sqlalchemy.String, default=None, nullable=True)

    document: Mapped['DocumentOrm'] = relationship(back_populates='medias')
    transcriptions: Mapped[typing.List['TranscriptionOrm']] = relationship(
        back_populates='media',
        primaryjoin="(MediaOrm.id==TranscriptionOrm.media_id)"
    )
    
    def __repr__(self):
        return (
            f'<Media('
            f'id={self.id}, '
            f'uuid={self.uuid}, '
            f'document_id={self.document_id}, '
            f'uri={self.uri}, '
            f'path={self.path}, '
            f'filename={self.filename}, '
            f'title={self.title}, '
            f'cid={self.cid}, '
            f'mime={self.mime}, '
            f'extension={self.extension}, '
            f'size={self.size}, '
            f'duration={self.duration}, '
            f'purpose={self.purpose})>'
        )
    
    def __str__(self):
        return (
            f'Media('
            f'id={self.id}, '
            f'uuid={self.uuid}, '
            f'document_id={self.document_id}, '
            f'uri={self.uri}, '
            f'path={self.path}, '
            f'filename={self.filename}, '
            f'title={self.title}, '
            f'cid={self.cid}, '
            f'mime={self.mime}, '
            f'extension={self.extension}, '
            f'size={self.size}, '
            f'duration={self.duration}, '
            f'purpose={self.purpose})'
        )
    
    def __json__(self):
        return {
            'id': self.id,
            'uuid': self.uuid,
            'created_at': self.created_at,
            'document_id': self.document_id,
            'uri': self.uri,
            'path': self.path,
            'filename': self.filename,
            'title': self.title,
            'cid': self.cid,
            'mime': self.mime,
            'extension': self.extension,
            'size': self.size,
            'duration': self.duration,
            'purpose': self.purpose,
            'status': self.status,
            'reason': self.reason
        }
    
    def _fields(self) -> tuple:
        return (
            ['id', 'uuid', 'created_at', 'document_id', 'uri', 'path', 'filename', 'title', 'cid', 'mime', 'extension', 'size', 'duration', 'purpose', 'status', 'reason'],
            [('document', False, None), ('transcriptions', True, None)]
        )
    

    @validates('uuid')
    def validate_uuid(self, key, value):
        if is_empty(value):
            raise ValueError('UUID required')
        if isinstance(value, str):
            result = RE_UUID4.match(value)
            if result is None:
                raise ValueError('Invalid UUID')
            return result.group(1)
        raise ValueError('Invalid UUID')

    @validates('document_id')
    def validate_document_id(self, key, value):
        if is_empty(value):
            raise ValueError('Document ID is required')
        if isinstance(value, int):
            return value
        if isinstance(value, str):
            value = value.strip()
            result = RE_INT_VALUE.match(value)
            if result is None:
                raise ValueError('Invalid document ID')
            return int(result.group(1))
        raise ValueError('Invalid document ID')

    @validates('uri')
    def validate_uri(self, key, value):
        if is_empty(value):
            return None
        if isinstance(value, str) and validate_uri_func(value):
            return value
        raise ValueError('Invalid uri')

    @validates('path')
    def validate_path(self, key, value):
        if is_empty(value):
            return None
        if not isinstance(value, str):
            raise ValueError('Invalid path')
        value = value.strip()
        if is_empty(value):
            return None
        sanitized_value = sanitize_filepath(value)
        if sanitized_value is None or sanitized_value != value:
            raise ValueError('Invalid path')
        return sanitized_value

    @validates('filename')
    def validate_filename(self, key, value):
        if is_empty(value):
            raise ValueError('Filename is required')
        if not isinstance(value, str):
            raise ValueError('Invalid filename')
        value = value.strip()
        if is_empty(value):
            raise ValueError('Invalid filename')
        sanitized_value = sanitize_filename(value)
        if sanitized_value is None or sanitized_value != value:
            raise ValueError('Invalid filename')
        return sanitized_value

    @validates('title')
    def validate_title(self, key, value):
        if is_empty(value):
            raise ValueError('Title required')
        if not isinstance(value, str):
            raise ValueError('Invalid title')
        value = value.strip()
        if is_empty(value):
            raise ValueError('Invalid title')
        sanitized_value = sanitize_filename(value)
        if sanitized_value is None or sanitized_value != value:
            raise ValueError('Invalid title')
        return sanitized_value

    @validates('cid')
    def validate_cid(self, key, value):
        if is_empty(value):
            raise ValueError('CID is required')
        if isinstance(value, str):
            result = RE_XXHASH_CID_HEX.match(value)
            if result is None:
                raise ValueError('Invalid CID')
            return result.group(1)
        raise ValueError('Invalid CID')

    @validates('mime')
    def validate_mime(self, key, value):
        if is_empty(value):
            raise ValueError('MIME type is required')
        if is_valid_mime_type(value):
            return value
        raise ValueError('Invalid MIME type')

    @validates('extension')
    def validate_extension(self, key, value):
        if is_empty(value):
            raise ValueError('File extension is required')
        if isinstance(value, str):
            result = RE_FILE_EXTENSION.match(value)
            if result is None:
                raise ValueError('Invalid file extension')
            return result.group(1)
        raise ValueError('Invalid file extension')

    @validates('size')
    def validate_size(self, key, value):
        if is_empty(value):
            raise ValueError('Size is required')
        if isinstance(value, int):
            return value
        if isinstance(value, str):
            result = RE_INT_VALUE.match(value)
            if result is None:
                raise ValueError('Invalid size')
            return int(result.group(1))
        raise ValueError('Invalid size')

    @validates('duration')
    def validate_duration(self, key, value):
        if is_empty(value):
            raise ValueError('Duration is required')
        if isinstance(value, (int, float)):
            return float(value)
        if isinstance(value, str):
            result = RE_FLOAT_VALUE.match(value)
            if result is None:
                result = RE_INT_VALUE.match(value)
                if result is None:
                    raise ValueError('Invalid duration')
                return float(result.group(1))
            return float(result.group(1))
        raise ValueError('Invalid duration')

    @validates('purpose')
    def validate_purpose(self, key, value):
        if is_empty(value):
            raise ValueError('File purpose is required')
        if isinstance(value, str):
            result = RE_FILE_PURPOSE.match(value)
            if result is None:
                raise ValueError('Invalid file purpose')
            return result.group(1)
        raise ValueError('Invalid file purpose')

    @validates('status')
    def validate_status(self, key, value):
        if is_empty(value):
            raise ValueError('Status is required')
        if isinstance(value, int):
            return value
        if isinstance(value, str):
            result = RE_INT_VALUE.match(value)
            if result is None:
                raise ValueError('Invalid status')
            return int(result.group(1))
        raise ValueError('Invalid status')

    @validates('reason')
    def validate_reason(self, key, value):
        return sanitize_text(value)


