# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import re
import typing
from .orm import MediaOrm





MEDIA__FIELDS_MAP = {
    'media': {
        'id': MediaOrm.id,
        'uuid': MediaOrm.uuid,
        'created_at': MediaOrm.created_at,
        'document': MediaOrm.document_id,
        'document_id': MediaOrm.document_id,
        'uri': MediaOrm.uri,
        'path': MediaOrm.path,
        'filename': MediaOrm.filename,
        'title': MediaOrm.title,
        'cid': MediaOrm.cid,
        'mime': MediaOrm.mime,
        'extension': MediaOrm.extension,
        'size': MediaOrm.size,
        'duration': MediaOrm.duration,
        'purpose': MediaOrm.purpose,
        'status': MediaOrm.status,
        'reason': MediaOrm.reason
    },
    'media_id': MediaOrm.id,
    'media_uuid': MediaOrm.uuid,
    'media_created_at': MediaOrm.created_at,
    'media_document': MediaOrm.document_id,
    'media_document_id': MediaOrm.document_id,
    'media_uri': MediaOrm.uri,
    'media_path': MediaOrm.path,
    'media_filename': MediaOrm.filename,
    'media_title': MediaOrm.title,
    'media_cid': MediaOrm.cid,
    'media_mime': MediaOrm.mime,
    'media_extension': MediaOrm.extension,
    'media_size': MediaOrm.size,
    'media_duration': MediaOrm.duration,
    'media_purpose': MediaOrm.purpose,
    'media_status': MediaOrm.status,
    'media_reason': MediaOrm.reason,

    'media.id': MediaOrm.id,
    'media.uuid': MediaOrm.uuid,
    'media.created_at': MediaOrm.created_at,
    'media.document': MediaOrm.document_id,
    'media.document_id': MediaOrm.document_id,
    'media.uri': MediaOrm.uri,
    'media.path': MediaOrm.path,
    'media.filename': MediaOrm.filename,
    'media.title': MediaOrm.title,
    'media.cid': MediaOrm.cid,
    'media.mime': MediaOrm.mime,
    'media.extension': MediaOrm.extension,
    'media.size': MediaOrm.size,
    'media.duration': MediaOrm.duration,
    'media.purpose': MediaOrm.purpose,
    'media.status': MediaOrm.status,
    'media.reason': MediaOrm.reason
}

MEDIA__VARIANTS_MAP = {
    'media': {
        'id': 'media.id',
        'uuid': 'media.uuid',
        'created_at': 'media.created_at',
        'document': 'media.document_id',
        'document_id': 'media.document_id',
        'uri': 'media.uri',
        'path': 'media.path',
        'filename': 'media.filename',
        'title': 'media.title',
        'cid': 'media.cid',
        'mime': 'media.mime',
        'extension': 'media.extension',
        'size': 'media.size',
        'duration': 'media.duration',
        'purpose': 'media.purpose',
        'status': 'media.status',
        'reason': 'media.reason'
    },
    'media_id': 'media.id',
    'media_uuid': 'media.uuid',
    'media_created_at': 'media.created_at',
    'media_document': 'media.document_id',
    'media_document_id': 'media.document_id',
    'media_uri': 'media.uri',
    'media_path': 'media.path',
    'media_filename': 'media.filename',
    'media_title': 'media.title',
    'media_cid': 'media.cid',
    'media_mime': 'media.mime',
    'media_extension': 'media.extension',
    'media_size': 'media.size',
    'media_duration': 'media.duration',
    'media_purpose': 'media.purpose',
    'media_status': 'media.status',
    'media_reason': 'media.reason',

    'media.id': 'media.id',
    'media.uuid': 'media.uuid',
    'media.created_at': 'media.created_at',
    'media.document': 'media.document_id',
    'media.document_id': 'media.document_id',
    'media.uri': 'media.uri',
    'media.path': 'media.path',
    'media.filename': 'media.filename',
    'media.title': 'media.title',
    'media.cid': 'media.cid',
    'media.mime': 'media.mime',
    'media.extension': 'media.extension',
    'media.size': 'media.size',
    'media.duration': 'media.duration',
    'media.purpose': 'media.purpose',
    'media.status': 'media.status',
    'media.reason': 'media.reason'
}



############
### Primary

MEDIA__PRIMARY_FIELDS_MAP = {
    'id': MediaOrm.id,
    'uuid': MediaOrm.uuid,
    'created_at': MediaOrm.created_at,
    'document': MediaOrm.document_id,
    'document_id': MediaOrm.document_id,
    'uri': MediaOrm.uri,
    'path': MediaOrm.path,
    'filename': MediaOrm.filename,
    'title': MediaOrm.title,
    'cid': MediaOrm.cid,
    'mime': MediaOrm.mime,
    'extension': MediaOrm.extension,
    'size': MediaOrm.size,
    'duration': MediaOrm.duration,
    'purpose': MediaOrm.purpose,
    'status': MediaOrm.status,
    'reason': MediaOrm.reason
}

MEDIA__PRIMARY_VARIANTS_MAP = {
    'id': 'media.id',
    'uuid': 'media.uuid',
    'created_at': 'media.created_at',
    'document': 'media.document_id',
    'document_id': 'media.document_id',
    'uri': 'media.uri',
    'path': 'media.path',
    'filename': 'media.filename',
    'title': 'media.title',
    'cid': 'media.cid',
    'mime': 'media.mime',
    'extension': 'media.extension',
    'size': 'media.size',
    'duration': 'media.duration',
    'purpose': 'media.purpose',
    'status': 'media.status',
    'reason': 'media.reason'
}



########################################################################
### MIME Types

MIME_TYPES_MAP = {
    'application/epub+zip': 'epub',
    'application/gzip': 'gz',
    'application/java-archive': 'jar',
    'application/json': 'json',
    'application/ld+json': 'jsonld',
    'application/msword': 'doc',
    'application/octet-stream': 'bin',
    'application/ogg': 'ogx',
    'application/pdf': 'pdf',
    'application/php': 'php',
    'application/rtf': 'rtf',
    'application/vnd.amazon.ebook': 'azw',
    'application/vnd.apple.installer+xml': 'mpkg',
    'application/vnd.mozilla.xul+xml': 'xul',
    'application/vnd.ms-excel': 'xls',
    'application/vnd.ms-fontobject': 'eot',
    'application/vnd.ms-powerpoint': 'ppt',
    'application/vnd.oasis.opendocument.presentation': 'odp',
    'application/vnd.oasis.opendocument.spreadsheet': 'ods',
    'application/vnd.oasis.opendocument.text': 'odt',
    'application/vnd.openxmlformats-officedocument.presentationml.presentation': 'pptx',
    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet': 'xlsx',
    'application/vnd.openxmlformats-officedocument.wordprocessingml.document': 'docx',
    'application/vnd.rar': 'rar',
    'application/vnd.visio': 'vsd',
    'application/x-7z-compressed': '7z',
    'application/x-csh': 'csh',
    'application/x-freearc': 'arc',
    'application/x-httpd-php': 'php',
    'application/x-sh': 'sh',
    'application/x-shockwave-flash': 'swf',
    'application/x-tar': 'tar',
    'application/xhtml+xml': 'xhtml',
    'application/xml': 'xml',
    'application/zip': 'zip',
    'audio/3gpp': '3gp',
    'audio/3gpp2': '3g2',
    'audio/aac': 'aac',
    'audio/midi': 'midi',
    'audio/mpeg': 'mp3',
    'audio/ogg': 'ogg',
    'audio/opus': 'opus',
    'audio/wav': 'wav',
    'audio/webm': 'webm',
    'audio/x-midi': 'midi',
    'font/otf': 'otf',
    'font/ttf': 'ttf',
    'font/woff': 'woff',
    'font/woff2': 'woff2',
    'image/bmp': 'bmp',
    'image/gif': 'gif',
    'image/jpeg': 'jpg',
    'image/png': 'png',
    'image/svg+xml': 'svg',
    'image/tiff': 'tiff',
    'image/vnd.microsoft.icon': 'ico',
    'image/webp': 'webp',
    'text/calendar': 'ics',
    'text/css': 'css',
    'text/csv': 'csv',
    'text/html': 'html',
    'text/javascript': 'js',
    'text/plain': 'txt',
    'text/xml': 'xml',
    'video/3gpp': '3gp',
    'video/3gpp2': '3g2',
    'video/mp4': 'mp4',
    'video/mpeg': 'mpeg',
    'video/ogg': 'ogv',
    'video/webm': 'webm',
    'video/x-msvideo': 'avi'
}


