# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

from pydantic import BaseModel
from ..base import (
    TrpPrimaryKey_ID_UUID
)
from ..transcription import (
    NewTranscriptionStructurePartialModel,
    UpdateTranscriptionStructureModel
)





class MediaModel(BaseModel):
    id: int
    uuid: str
    document: TrpPrimaryKey_ID_UUID
    uri: str | None = None
    path: str | None = None
    filename: str
    title: str
    cid: str
    mime: str
    extension: str
    size: int
    duration: float
    purpose: str
    status: int = 0
    reason: str | None = None


class NewMediaPartialModel(BaseModel):
    uri: str | None = None
    path: str | None = None
    #cid: str
    title: str | None = None
    #mime: str
    purpose: str | None = None

class NewMediaModel(NewMediaPartialModel):
    document: TrpPrimaryKey_ID_UUID

class NewMediaMinimalPartialModel(BaseModel):
    uri: str | None = None
    path: str | None = None
    title: str | None = None
    purpose: str | None = None

class NewMediaMinimalModel(NewMediaMinimalPartialModel):
    document: TrpPrimaryKey_ID_UUID

class NewMediaStructurePartialModel(NewMediaPartialModel):
    transcriptions: list[NewTranscriptionStructurePartialModel] | NewTranscriptionStructurePartialModel | None = None

class NewMediaStructureModel(NewMediaModel):
    transcriptions: list[NewTranscriptionStructurePartialModel] | NewTranscriptionStructurePartialModel | None = None


class UpdateMediaModel(BaseModel):
    #uri: str | None = None
    #path: str | None = None
    #cid: str | None = None
    title: str | None = None
    #mime: str | None = None
    purpose: str | None = None
    status: int | None = None
    reason: str | None = None

class UpdateMediaStructureModel(UpdateMediaModel):
    transcriptions: list[UpdateTranscriptionStructureModel] | UpdateTranscriptionStructureModel | None = None

class UpdateMediaMinimalModel(BaseModel):
    #uri: str | None = None
    #path: str | None = None
    title: str | None = None
    purpose: str | None = None
    status: int | None = None
    reason: str | None = None

class UpdateMediaStructureMinimalModel(UpdateMediaMinimalModel):
    transcriptions: list[UpdateTranscriptionStructureModel] | UpdateTranscriptionStructureModel | None = None

