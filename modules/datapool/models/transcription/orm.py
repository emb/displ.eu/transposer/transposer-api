# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import typing
import datetime

import sqlalchemy
from sqlalchemy.orm import (
    Mapped,
    mapped_column,
    relationship,
    validates
)

from regex_patterns import (
    RE_INT_VALUE,
    RE_LANG_CODE,
    RE_UUID4
)
from data_utils import (
    is_empty,
    split_terms,
    split_datetime,
    sanitize_text
)
from ..base import BaseOrm





class TranscriptionOrm(BaseOrm):
    __tablename__ = 'transcriptions'
    id: Mapped[int] = mapped_column(
        sqlalchemy.Integer, primary_key=True
    )
    uuid: Mapped[str] = mapped_column(sqlalchemy.String)
    created_at: Mapped[datetime.datetime] = mapped_column(
        sqlalchemy.DateTime, default=sqlalchemy.sql.functions.now()
    )
    media_id: Mapped[int] = mapped_column(sqlalchemy.ForeignKey('medias.id'))
    lang: Mapped[typing.Optional[str]] = mapped_column(sqlalchemy.String, default=None, nullable=True)
    status: Mapped[int] = mapped_column(sqlalchemy.Integer)
    reason: Mapped[typing.Optional[str]] = mapped_column(sqlalchemy.String, default=None, nullable=True)

    media: Mapped['MediaOrm'] = relationship(
        back_populates='transcriptions'
    )
    segments: Mapped[typing.List['TranscriptionSegmentOrm']] = relationship(
        back_populates='transcription',
        primaryjoin="(TranscriptionOrm.id==TranscriptionSegmentOrm.transcription_id)"
    )
    language_probs: Mapped[typing.List['TranscriptionLanguageProbOrm']] = relationship(
        back_populates='transcription',
        primaryjoin="(TranscriptionOrm.id==TranscriptionLanguageProbOrm.transcription_id)"
    )
    speech_activities: Mapped[typing.List['TranscriptionSpeechActivityOrm']] = relationship(
        back_populates='transcription',
        primaryjoin="(TranscriptionOrm.id==TranscriptionSpeechActivityOrm.transcription_id)"
    )
    
    def __repr__(self):
        return (
            f'<Transcription('
            f'id={self.id}, '
            f'uuid={self.uuid}, '
            f'media_id={self.media_id}, '
            f'lang={self.lang}, '
            f'status={self.status}, '
            f'reason={self.reason})>'
        )
    
    def __str__(self):
        return (
            f'Transcription('
            f'id={self.id}, '
            f'uuid={self.uuid}, '
            f'media_id={self.media_id}, '
            f'lang={self.lang}, '
            f'status={self.status}, '
            f'reason={self.reason})'
        )
    
    def __json__(self):
        return {
            'id': self.id,
            'uuid': self.uuid,
            'created_at': self.created_at,
            'media_id': self.media_id,
            'lang': self.lang,
            'status': self.status,
            'reason': self.reason
        }
    
    def _fields(self) -> tuple:
        return (
            ['id', 'uuid', 'created_at', 'media_id', 'lang', 'status', 'reason'],
            [('media', False, None), ('segments', True, None), ('language_probs', True, None), ('speech_activities', True, None)]
        )
    

    @validates('uuid')
    def validate_uuid(self, key, value):
        if is_empty(value):
            raise ValueError("UUID is required")
        if isinstance(value, str):
            result = RE_UUID4.match(value)
            if result is None:
                raise ValueError("Invalid UUID")
            return result.group(1)
        raise ValueError("Invalid UUID")
    
    @validates('media_id')
    def validate_media_id(self, key, value):
        if is_empty(value):
            raise ValueError("Media ID is required")
        if isinstance(value, int):
            return value
        if isinstance(value, str):
            result = RE_INT_VALUE.match(value)
            if result is None:
                raise ValueError("Invalid Media ID")
            return int(result.group(1))
        raise ValueError("Invalid Media ID")

    @validates('lang')
    def validate_lang(self, key, value):
        if is_empty(value):
            return None
        if isinstance(value, str):
            result = RE_LANG_CODE.match(value)
            if result is None:
                raise ValueError("Invalid language code")
            return result.group(1)
        raise ValueError("Invalid language code")

    @validates('status')
    def validate_status(self, key, value):
        if is_empty(value):
            raise ValueError("Status is required")
        if isinstance(value, int):
            return value
        if isinstance(value, str):
            result = RE_INT_VALUE.match(value)
            if result is None:
                raise ValueError("Invalid status")
            return int(result.group(1))
        raise ValueError("Invalid status")

    @validates('reason')
    def validate_reason(self, key, value):
        return sanitize_text(value)




