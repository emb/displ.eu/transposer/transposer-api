# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import re
import typing
from .orm import TranscriptionOrm





TRANSCRIPTION__FIELDS_MAP = {
    'transcription': {
        'id': TranscriptionOrm.id,
        'uuid': TranscriptionOrm.uuid,
        'created_at': TranscriptionOrm.created_at,
        'media': TranscriptionOrm.media_id,
        'media_id': TranscriptionOrm.media_id,
        'lang': TranscriptionOrm.lang,
        'status': TranscriptionOrm.status,
        'reason': TranscriptionOrm.reason
    },
    'transcription_id': TranscriptionOrm.id,
    'transcription_uuid': TranscriptionOrm.uuid,
    'transcription_created_at': TranscriptionOrm.created_at,
    'transcription_media': TranscriptionOrm.media_id,
    'transcription_media_id': TranscriptionOrm.media_id,
    'transcription_lang': TranscriptionOrm.lang,
    'transcription_status': TranscriptionOrm.status,
    'transcription_reason': TranscriptionOrm.reason,

    'transcription.id': TranscriptionOrm.id,
    'transcription.uuid': TranscriptionOrm.uuid,
    'transcription.created_at': TranscriptionOrm.created_at,
    'transcription.media': TranscriptionOrm.media_id,
    'transcription.media_id': TranscriptionOrm.media_id,
    'transcription.lang': TranscriptionOrm.lang,
    'transcription.status': TranscriptionOrm.status,
    'transcription.reason': TranscriptionOrm.reason
}

TRANSCRIPTION__VARIANTS_MAP = {
    'transcription': {
        'id': 'transcription.id',
        'uuid': 'transcription.uuid',
        'created_at': 'transcription.created_at',
        'media': 'transcription.media_id',
        'media_id': 'transcription.media_id',
        'lang': 'transcription.lang',
        'status': 'transcription.status',
        'reason': 'transcription.reason'
    },
    'transcription_id': 'transcription.id',
    'transcription_uuid': 'transcription.uuid',
    'transcription_created_at': 'transcription.created_at',
    'transcription_media': 'transcription.media_id',
    'transcription_media_id': 'transcription.media_id',
    'transcription_lang': 'transcription.lang',
    'transcription_status': 'transcription.status',
    'transcription_reason': 'transcription.reason',

    'transcription.id': 'transcription.id',
    'transcription.uuid': 'transcription.uuid',
    'transcription.created_at': 'transcription.created_at',
    'transcription.media': 'transcription.media_id',
    'transcription.media_id': 'transcription.media_id',
    'transcription.lang': 'transcription.lang',
    'transcription.status': 'transcription.status',
    'transcription.reason': 'transcription.reason'
}



############
### Primary

TRANSCRIPTION__PRIMARY_FIELDS_MAP = {
    'id': TranscriptionOrm.id,
    'uuid': TranscriptionOrm.uuid,
    'created_at': TranscriptionOrm.created_at,
    'media': TranscriptionOrm.media_id,
    'media_id': TranscriptionOrm.media_id,
    'lang': TranscriptionOrm.lang,
    'status': TranscriptionOrm.status,
    'reason': TranscriptionOrm.reason
}

TRANSCRIPTION__PRIMARY_VARIANTS_MAP = {
    'id': 'transcription.id',
    'uuid': 'transcription.uuid',
    'created_at': 'transcription.created_at',
    'media': 'transcription.media_id',
    'media_id': 'transcription.media_id',
    'lang': 'transcription.lang',
    'status': 'transcription.status',
    'reason': 'transcription.reason'
}


