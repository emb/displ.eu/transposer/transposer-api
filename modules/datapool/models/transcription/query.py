# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import typing
import datetime

from regex_patterns import (
    RE_INT_VALUE,
    RE_LANG_CODE,
    RE_UUID4,
    RE_ALPHANUM_TERM
)
from data_utils import (
    split_terms,
    split_datetime,
    split_numeric
)
from ..base import (
    BaseQueryParams
)
from .orm import TranscriptionOrm





class TranscriptionQueryParams(BaseQueryParams):
    __slots__ = [
        '_id',
        '_uuid',
        '_created_at',
        '_media_id',
        '_lang',
        '_status',
        '_reason',

        '_id_stmt',
        '_uuid_stmt',
        '_created_at_stmt',
        '_media_id_stmt',
        '_lang_stmt',
        '_status_stmt',
        '_reason_stmt'
    ]

    def __init__(self, 
        id: int | str | None = None,
        uuid: str | None = None,
        created_at: int | float | str | None = None,
        media_id: str | None = None,
        lang: str | None = None,
        status: str | None = None,
        reason: str | None = None
    ):
        super().__init__()

        # Store raw values
        self._id = id
        self._uuid = uuid
        self._created_at = created_at
        self._media_id = media_id
        self._lang = lang
        self._status = status
        self._reason = reason

        # Init statement values
        self._id_stmt = None
        self._uuid_stmt = None
        self._created_at_stmt = None
        self._media_id_stmt = None
        self._lang_stmt = None
        self._status_stmt = None
        self._reason_stmt = None


    def add_conditions(self, container: list, use_and: bool = False) -> bool:

        # id
        if self._id_stmt is not None:
            container.append(self._id_stmt)
        else:
            val = self._id
            if isinstance(val, int):
                val = str(val)
            val = self.create_filter(
                TranscriptionOrm.id,
                [(x, '==') for x in split_terms(val, RE_INT_VALUE, '', '')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._id_stmt = val
                container.append(val)
        
        # uuid
        if self._uuid_stmt is not None:
            container.append(self._uuid_stmt)
        else:
            val = self._uuid
            val = self.create_filter(
                TranscriptionOrm.uuid,
                [(x, '==') for x in split_terms(val, RE_UUID4, '', '')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._uuid_stmt = val
                container.append(val)

        # created_at
        if self._created_at_stmt is not None:
            container.append(self._created_at_stmt)
        else:
            val = self._created_at
            val = self.create_filter(
                TranscriptionOrm.created_at,
                split_datetime(val),
                use_and
            )
            if val is not None:
                self._has_params = True
                self._created_at_stmt = val
                container.append(val)

        # media_id
        if self._media_id_stmt is not None:
            container.append(self._media_id_stmt)
        else:
            val = self._media_id
            if isinstance(val, int):
                val = str(val)
            val = self.create_filter(
                TranscriptionOrm.media_id,
                [(x, '==') for x in split_terms(val, RE_INT_VALUE, '', '')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._media_id_stmt = val
                container.append(val)

        # lang
        if self._lang_stmt is not None:
            container.append(self._lang_stmt)
        else:
            val = self._lang
            val = self.create_filter(
                TranscriptionOrm.lang,
                [(x, '==') for x in split_terms(val, RE_LANG_CODE, '', '')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._lang_stmt = val
                container.append(val)
        
        # status
        if self._status_stmt is not None:
            container.append(self._status_stmt)
        else:
            val = self._status
            if isinstance(val, int):
                val = str(val)
            val = self.create_filter(
                TranscriptionOrm.status,
                split_numeric(val),
                use_and
            )
            if val is not None:
                self._has_params = True
                self._status_stmt = val
                container.append(val)
        
        # reason
        if self._reason_stmt is not None:
            container.append(self._reason_stmt)
        else:
            val = self._reason
            val = self.create_filter(
                TranscriptionOrm.reason,
                [(x, 'like') for x in split_terms(val, RE_ALPHANUM_TERM, '%', '%')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._reason_stmt = val
                container.append(val)

        # Return True if conditions were added
        return self._has_params
    

    # Getters for raw values

    @property
    def id(self) -> int | str | None:
        return self._id
    
    @property
    def uuid(self) -> str | None:
        return self._uuid
    
    @property
    def created_at(self) -> int | float | str | None:
        return self._created_at
    
    @property
    def media_id(self) -> int | str | None:
        return self._media_id
    
    @property
    def lang(self) -> str | None:
        return self._lang
    
    @property
    def status(self) -> str | None:
        return self._status
    
    @property
    def reason(self) -> str | None:
        return self._reason
    

    # Getters for statements
    
    @property
    def id_stmt(self) -> typing.Any:
        return self._id_stmt
    
    @property
    def uuid_stmt(self) -> typing.Any:
        return self._uuid_stmt
    
    @property
    def created_at_stmt(self) -> typing.Any:
        return self._created_at_stmt
    
    @property
    def media_id_stmt(self) -> typing.Any:
        return self._media_id_stmt
    
    @property
    def lang_stmt(self) -> typing.Any:
        return self._lang_stmt
    
    @property
    def status_stmt(self) -> typing.Any:
        return self._status_stmt
    
    @property
    def reason_stmt(self) -> typing.Any:
        return self._reason_stmt



### Define Interfaces

class TranscriptionQueryParamsPrimary(TranscriptionQueryParams):
    def __init__(self,
        id: int | str | None = None,
        uuid: str | None = None,
        created_at: int | float | str | None = None,
        media: int | str | None = None,
        media_id: int | str | None = None,
        lang: str | None = None,
        status: str | None = None,
        reason: str | None = None
    ):
        super().__init__(
            id,
            uuid,
            created_at,
            media_id if media_id is not None else media,
            lang,
            status,
            reason
        )


class TranscriptionQueryParamsGeneral(TranscriptionQueryParams):
    def __init__(self,
        transcription_id: int | str | None = None,
        transcription_uuid: str | None = None,
        transcription_created_at: int | float | str | None = None,
        transcription_media: int | str | None = None,
        transcription_media_id: int | str | None = None,
        transcription_lang: str | None = None,
        transcription_status: str | None = None,
        transcription_reason: str | None = None
    ):
        super().__init__(
            transcription_id,
            transcription_uuid,
            transcription_created_at,
            transcription_media_id if transcription_media_id is not None else transcription_media,
            transcription_lang,
            transcription_status,
            transcription_reason
        )


