# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

from pydantic import BaseModel
from ..base import (
    TrpPrimaryKey_MediaID
)
from ..transcription_segment import (
    NewTranscriptionSegmentStructurePartialModel,
    UpdateTranscriptionSegmentStructurePartialModel
)
from ..transcription_language_prob import (
    NewTranscriptionLanguageProbPartialModel,
    UpdateTranscriptionLanguageProbModel
)
from ..transcription_speech_activity import (
    NewTranscriptionSpeechActivityPartialModel,
    UpdateTranscriptionSpeechActivityModel
)





class TranscriptionModel(BaseModel):
    id: int
    uuid: str
    media: TrpPrimaryKey_MediaID
    lang: str | None = None
    status: int = 0
    reason: str | None = None


class NewTranscriptionModel(BaseModel):
    media: TrpPrimaryKey_MediaID
    lang: str | None = None
    status: int = 1
    reason: str | None = None

class NewTranscriptionStructurePartialModel(BaseModel):
    lang: str | None = None
    status: int = 1
    reason: str | None = None
    segments: list[NewTranscriptionSegmentStructurePartialModel] | None = None
    language_probs: list[NewTranscriptionLanguageProbPartialModel] | None = None
    speech_activities: list[NewTranscriptionSpeechActivityPartialModel] | None = None

class NewTranscriptionStructureModel(NewTranscriptionStructurePartialModel):
    media: TrpPrimaryKey_MediaID


class UpdateTranscriptionModel(BaseModel):
    lang: str | None = None
    status: int | None = None
    reason: str | None = None

class UpdateTranscriptionStructureModel(UpdateTranscriptionModel):
    segments: list[UpdateTranscriptionSegmentStructurePartialModel] | None = None
    language_probs: list[UpdateTranscriptionLanguageProbModel] | None = None
    speech_activities: list[UpdateTranscriptionSpeechActivityModel] | None = None

