# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import typing
import datetime

import sqlalchemy
from sqlalchemy.orm import (
    Mapped,
    mapped_column,
    relationship,
    validates
)

from regex_patterns import (
    RE_INT_VALUE,
    RE_FLOAT_VALUE,
    RE_LANG_CODE,
    RE_UUID4,
    RE_SLUG,
    RE_ALPHANUM_ASCII_WSPACE
)
from data_utils import (
    is_empty,
    split_terms,
    split_datetime,
    sanitize_text
)
from ..base import BaseOrm # PipelineBaseOrm





class PipelineProducerOrm(BaseOrm):
    __tablename__ = 'pipeline_producers'
    id: Mapped[int] = mapped_column(
        sqlalchemy.Integer, primary_key=True
    )
    uuid: Mapped[str] = mapped_column(sqlalchemy.String)
    created_at: Mapped[datetime.datetime] = mapped_column(
        sqlalchemy.DateTime, default=sqlalchemy.sql.functions.now()
    )
    pipeline_id: Mapped[int] = mapped_column(sqlalchemy.ForeignKey('pipelines.id'))
    name: Mapped[typing.Optional[str]] = mapped_column(sqlalchemy.String, default=None, nullable=True)
    client_id: Mapped[str] = mapped_column(sqlalchemy.String)
    transactional_id: Mapped[str] = mapped_column(sqlalchemy.String)

    pipeline: Mapped['PipelineOrm'] = relationship(
        back_populates='producers'
    )
    options: Mapped[typing.List['PipelineProducerOptionOrm']] = relationship(
        back_populates='parent',
        primaryjoin="(PipelineProducerOrm.id==PipelineProducerOptionOrm.parent_id)"
    )
    
    def __repr__(self):
        return (
            f'<PipelineProducer('
            f'id={self.id}, '
            f'uuid={self.uuid}, '
            f'created_at={self.created_at}, '
            f'pipeline_id={self.pipeline_id}, '
            f'name={self.name}, '
            f'client_id={self.client_id}, '
            f'transactional_id={self.transactional_id})>'
        )
    
    def __str__(self):
        return (
            f'PipelineProducer('
            f'id={self.id}, '
            f'uuid={self.uuid}, '
            f'created_at={self.created_at}, '
            f'pipeline_id={self.pipeline_id}, '
            f'name={self.name}, '
            f'client_id={self.client_id}, '
            f'transactional_id={self.transactional_id})'
        )
    
    def __json__(self):
        return {
            'id': self.id,
            'uuid': self.uuid,
            'created_at': self.created_at,
            'pipeline_id': self.pipeline_id,
            'name': self.name,
            'client_id': self.client_id,
            'transactional_id': self.transactional_id
        }
    
    def _fields(self) -> tuple:
        return (
            ['id', 'uuid', 'created_at', 'pipeline_id', 'name', 'client_id', 'transactional_id'],
            [('pipeline', False, None), ('options', True, self._transform_options)]
        )
    

    @validates('uuid')
    def validate_uuid(self, key, value):
        if is_empty(value):
            raise ValueError("UUID is required")
        if isinstance(value, str):
            result = RE_UUID4.match(value)
            if result is None:
                raise ValueError("Invalid UUID")
            return result.group(1)
        raise ValueError("Invalid UUID")
    
    @validates('pipeline_id')
    def validate_pipeline_id(self, key, value):
        if is_empty(value):
            raise ValueError("Pipeline ID is required")
        if isinstance(value, int):
            return value
        if isinstance(value, str):
            result = RE_INT_VALUE.match(value)
            if result is None:
                raise ValueError("Invalid Pipeline ID")
            return int(result.group(1))
        raise ValueError("Invalid Pipeline ID")

    @validates('name')
    def validate_name(self, key, value):
        if is_empty(value):
            return None
        if isinstance(value, str):
            result = RE_ALPHANUM_ASCII_WSPACE.match(value)
            if result is None:
                raise ValueError("Invalid name")
            return result.group(1)
        raise ValueError("Invalid name")

    @validates('client_id')
    def validate_client_id(self, key, value):
        if is_empty(value):
            raise ValueError("client.id is required")
        if isinstance(value, str):
            result = RE_SLUG.match(value)
            if result is None:
                raise ValueError("Invalid client.id")
            return result.group(1)
        raise ValueError("Invalid client.id")

    @validates('transactional_id')
    def validate_transactional_id(self, key, value):
        if is_empty(value):
            raise ValueError("transactional.id is required")
        if isinstance(value, str):
            result = RE_SLUG.match(value)
            if result is None:
                raise ValueError("Invalid transactional.id")
            return result.group(1)
        raise ValueError("Invalid transactional.id")


