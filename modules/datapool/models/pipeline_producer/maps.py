# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

from .orm import PipelineProducerOrm





PIPELINE_PRODUCER__FIELDS_MAP = {
    'pipeline_producer': {
        'id': PipelineProducerOrm.id,
        'uuid': PipelineProducerOrm.uuid,
        'created_at': PipelineProducerOrm.created_at,
        'pipeline': PipelineProducerOrm.pipeline_id,
        'pipeline_id': PipelineProducerOrm.pipeline_id,
        'name': PipelineProducerOrm.name,
        'client': PipelineProducerOrm.client_id,
        'client_id': PipelineProducerOrm.client_id,
        'transactional_id': PipelineProducerOrm.transactional_id
    },
    'pipeline_producer_id': PipelineProducerOrm.id,
    'pipeline_producer_uuid': PipelineProducerOrm.uuid,
    'pipeline_producer_created_at': PipelineProducerOrm.created_at,
    'pipeline_producer_pipeline': PipelineProducerOrm.pipeline_id,
    'pipeline_producer_pipeline_id': PipelineProducerOrm.pipeline_id,
    'pipeline_producer_name': PipelineProducerOrm.name,
    'pipeline_producer_client': PipelineProducerOrm.client_id,
    'pipeline_producer_client_id': PipelineProducerOrm.client_id,
    'pipeline_producer_transactional_id': PipelineProducerOrm.transactional_id,

    'pipeline_producer.id': PipelineProducerOrm.id,
    'pipeline_producer.uuid': PipelineProducerOrm.uuid,
    'pipeline_producer.created_at': PipelineProducerOrm.created_at,
    'pipeline_producer.pipeline': PipelineProducerOrm.pipeline_id,
    'pipeline_producer.pipeline_id': PipelineProducerOrm.pipeline_id,
    'pipeline_producer.name': PipelineProducerOrm.name,
    'pipeline_producer.client': PipelineProducerOrm.client_id,
    'pipeline_producer.client_id': PipelineProducerOrm.client_id,
    'pipeline_producer.transactional_id': PipelineProducerOrm.transactional_id
}

PIPELINE_PRODUCER__VARIANTS_MAP = {
    'pipeline_producer': {
        'id': 'pipeline_producer.id',
        'uuid': 'pipeline_producer.uuid',
        'created_at': 'pipeline_producer.created_at',
        'pipeline': 'pipeline_producer.pipeline_id',
        'pipeline_id': 'pipeline_producer.pipeline_id',
        'name': 'pipeline_producer.name',
        'client': 'pipeline_producer.client_id',
        'client_id': 'pipeline_producer.client_id',
        'transactional_id': 'pipeline_producer.transactional_id'
    },
    'pipeline_producer_id': 'pipeline_producer.id',
    'pipeline_producer_uuid': 'pipeline_producer.uuid',
    'pipeline_producer_created_at': 'pipeline_producer.created_at',
    'pipeline_producer_pipeline': 'pipeline_producer.pipeline_id',
    'pipeline_producer_pipeline_id': 'pipeline_producer.pipeline_id',
    'pipeline_producer_name': 'pipeline_producer.name',
    'pipeline_producer_client': 'pipeline_producer.client_id',
    'pipeline_producer_client_id': 'pipeline_producer.client_id',
    'pipeline_producer_transactional_id': 'pipeline_producer.transactional_id',

    'pipeline_producer.id': 'pipeline_producer.id',
    'pipeline_producer.uuid': 'pipeline_producer.uuid',
    'pipeline_producer.created_at': 'pipeline_producer.created_at',
    'pipeline_producer.pipeline': 'pipeline_producer.pipeline_id',
    'pipeline_producer.pipeline_id': 'pipeline_producer.pipeline_id',
    'pipeline_producer.name': 'pipeline_producer.name',
    'pipeline_producer.client': 'pipeline_producer.client_id',
    'pipeline_producer.client_id': 'pipeline_producer.client_id',
    'pipeline_producer.transactional_id': 'pipeline_producer.transactional_id'
}



############
### Primary

PIPELINE_PRODUCER__PRIMARY_FIELDS_MAP = {
    'id': PipelineProducerOrm.id,
    'uuid': PipelineProducerOrm.uuid,
    'created_at': PipelineProducerOrm.created_at,
    'pipeline': PipelineProducerOrm.pipeline_id,
    'pipeline_id': PipelineProducerOrm.pipeline_id,
    'name': PipelineProducerOrm.name,
    'client': PipelineProducerOrm.client_id,
    'client_id': PipelineProducerOrm.client_id,
    'transactional_id': PipelineProducerOrm.transactional_id
}

PIPELINE_PRODUCER__PRIMARY_VARIANTS_MAP = {
    'id': 'pipeline_producer.id',
    'uuid': 'pipeline_producer.uuid',
    'created_at': 'pipeline_producer.created_at',
    'pipeline': 'pipeline_producer.pipeline_id',
    'pipeline_id': 'pipeline_producer.pipeline_id',
    'name': 'pipeline_producer.name',
    'client': 'pipeline_producer.client_id',
    'client_id': 'pipeline_producer.client_id',
    'transactional_id': 'pipeline_producer.transactional_id'
}


