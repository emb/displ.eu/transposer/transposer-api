# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import typing
import datetime

import sqlalchemy
from sqlalchemy.orm import (
    Mapped,
    mapped_column,
    relationship,
    validates
)

from regex_patterns import (
    RE_INT_VALUE,
    RE_FLOAT_VALUE,
    RE_UUID4,
    RE_ALPHANUM_TERM
)
from data_utils import (
    is_empty, is_true,
    split_terms,
    split_numeric,
    split_datetime,
    sanitize_text
)
from ..base import BaseOrm





class CaptionOrm(BaseOrm):
    __tablename__ = 'captions'
    id: Mapped[int] = mapped_column(
        sqlalchemy.Integer, primary_key=True
    )
    content_id: Mapped[int] = mapped_column(sqlalchemy.ForeignKey('contents.id', ondelete='cascade'))
    start: Mapped[int] = mapped_column(sqlalchemy.Integer, default=-1)
    end: Mapped[int] = mapped_column(sqlalchemy.Integer, default=-1)
    identifier: Mapped[typing.Optional[str]] = mapped_column(sqlalchemy.String, nullable=True, default=None)
    properties: Mapped[typing.Optional[str]] = mapped_column(sqlalchemy.String, nullable=True, default=None)

    content: Mapped['ContentOrm'] = relationship(back_populates='captions')
    
    def __repr__(self):
        return (
            f'<Caption('
            f'id={self.id}, '
            f'content_id={self.content_id}, '
            f'start={self.start}, '
            f'end={self.end}, '
            f'identifier={self.identifier}, '
            f'properties={self.properties})>'
        )
    
    def __str__(self):
        return (
            f'Caption('
            f'id={self.id}, '
            f'content_id={self.content_id}, '
            f'start={self.start}, '
            f'end={self.end}, '
            f'identifier={self.identifier}, '
            f'properties={self.properties})'
        )
    
    def __json__(self):
        return {
            'id': self.id,
            'content_id': self.content_id,
            'start': self.start,
            'end': self.end,
            'identifier': self.identifier,
            'properties': self.properties
        }
    
    def _fields(self) -> tuple:
        return (
            ['id', 'content_id', 'start', 'end', 'identifier', 'properties'],
            [('content', False, None)]
        )
    

    @validates('content_id')
    def validate_content_id(self, key, value):
        if is_empty(value):
            raise ValueError("Content ID is required")
        if isinstance(value, int):
            return value
        if isinstance(value, str):
            result = RE_INT_VALUE.match(value)
            if result is None:
                raise ValueError("Invalid content ID")
            return int(result.group(1))
        raise ValueError("Invalid content ID")

    @validates('start')
    def validate_start(self, key, value):
        if is_empty(value):
            raise ValueError("Start timestamp is required")
        if isinstance(value, (int, float)):
            return value
        if isinstance(value, str):
            result = RE_INT_VALUE.match(value)
            if result is None:
                result = RE_FLOAT_VALUE.match(value)
                if result is None:
                    raise ValueError("Invalid start timestamp")
                return float(result.group(1))
            return int(result.group(1))
        raise ValueError("Invalid start timestamp")

    @validates('end')
    def validate_end(self, key, value):
        if is_empty(value):
            raise ValueError("End timestamp is required")
        if isinstance(value, (int, float)):
            return value
        if isinstance(value, str):
            result = RE_INT_VALUE.match(value)
            if result is None:
                result = RE_FLOAT_VALUE.match(value)
                if result is None:
                    raise ValueError("Invalid end timestamp")
                return float(result.group(1))
            return int(result.group(1))
        raise ValueError("Invalid end timestamp")

    @validates('identifier')
    def validate_identifier(self, key, value):
        if is_empty(value):
            return None
        return sanitize_text(value)

    @validates('properties')
    def validate_properties(self, key, value):
        if is_empty(value):
            return None
        return sanitize_text(value)


