# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import typing

from regex_patterns import (
    RE_INT_VALUE,
    RE_ALPHANUM_TERM
)
from data_utils import (
    split_terms,
    split_numeric
)
from ..base import (
    BaseQueryParams
)
from .orm import CaptionOrm





class CaptionQueryParams(BaseQueryParams):
    __slots__ = [
        '_id',
        '_content_id',
        '_start',
        '_end',
        '_identifier',
        '_properties',

        '_id_stmt',
        '_content_id_stmt',
        '_start_stmt',
        '_end_stmt',
        '_identifier_stmt',
        '_properties_stmt'
    ]

    def __init__(self, 
        id: int | str | None = None,
        content_id: int | str | None = None,
        start: int | str | None = None,
        end: int | str | None = None,
        identifier: str | None = None,
        properties: str | None = None
    ):
        super().__init__()

        # Store raw values
        self._id = id
        self._content_id = content_id
        self._start = start
        self._end = end
        self._identifier = identifier
        self._properties = properties

        # Init statement values
        self._id_stmt = None
        self._content_id_stmt = None
        self._start_stmt = None
        self._end_stmt = None
        self._identifier_stmt = None
        self._properties_stmt = None


    def add_conditions(self, container: list, use_and: bool = False) -> bool:

        # id
        if self._id_stmt is not None:
            container.append(self._id_stmt)
        else:
            val = self._id
            if isinstance(val, int):
                val = str(val)
            val = self.create_filter(
                CaptionOrm.id,
                [(x, '==') for x in split_terms(val, RE_INT_VALUE, '', '')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._id_stmt = val
                container.append(val)

        # content_id
        if self._content_id_stmt is not None:
            container.append(self._content_id_stmt)
        else:
            val = self._content_id
            if isinstance(val, int):
                val = str(val)
            val = self.create_filter(
                CaptionOrm.content_id,
                [(x, '==') for x in split_terms(val, RE_INT_VALUE, '', '')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._content_id_stmt = val
                container.append(val)
        
        # start
        if self._start_stmt is not None:
            container.append(self._start_stmt)
        else:
            val = self._start
            val = self.create_filter(
                CaptionOrm.start,
                split_numeric(val),
                use_and
            )
            if val is not None:
                self._has_params = True
                self._start_stmt = val
                container.append(val)
        
        # end
        if self._end_stmt is not None:
            container.append(self._end_stmt)
        else:
            val = self._end
            if isinstance(val, int):
                val = str(val)
            val = self.create_filter(
                CaptionOrm.end,
                split_numeric(val),
                use_and
            )
            if val is not None:
                self._has_params = True
                self._end_stmt = val
                container.append(val)
        
        # identifier
        if self._identifier_stmt is not None:
            container.append(self._identifier_stmt)
        else:
            val = self._identifier
            val = self.create_filter(
                CaptionOrm.identifier,
                [(x, 'like') for x in split_terms(val, RE_ALPHANUM_TERM, '%', '%')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._identifier_stmt = val
                container.append(val)
        
        # properties
        if self._properties_stmt is not None:
            container.append(self._properties_stmt)
        else:
            val = self._properties
            val = self.create_filter(
                CaptionOrm.properties,
                [(x, 'like') for x in split_terms(val, RE_ALPHANUM_TERM, '%', '%')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._properties_stmt = val
                container.append(val)

        # Return True if conditions were added
        return self._has_params
    

    # Getters for raw values

    @property
    def id(self) -> int | str | None:
        return self._id
    
    @property
    def content_id(self) -> int | str | None:
        return self._content_id
    
    @property
    def start(self) -> int | str | None:
        return self._start
    
    @property
    def end(self) -> int | str | None:
        return self._end
    
    @property
    def identifier(self) -> str | None:
        return self._identifier
    
    @property
    def properties(self) -> str | None:
        return self._properties
    

    # Getters for statements
    
    @property
    def id_stmt(self) -> typing.Any:
        return self._id_stmt
    
    @property
    def content_id_stmt(self) -> typing.Any:
        return self._content_id_stmt
    
    @property
    def start_stmt(self) -> typing.Any:
        return self._start_stmt
    
    @property
    def end_stmt(self) -> typing.Any:
        return self._end_stmt
    
    @property
    def identifier_stmt(self) -> typing.Any:
        return self._identifier_stmt
    
    @property
    def properties_stmt(self) -> typing.Any:
        return self._properties_stmt



### Define Interfaces

class CaptionQueryParamsPrimary(CaptionQueryParams):
    def __init__(self,
        id: int | str | None = None,
        content: int | str | None = None,
        content_id: int | str | None = None,
        start: int | str | None = None,
        end: int | str | None = None,
        identifier: str | None = None,
        properties: str | None = None
    ):
        super().__init__(
            id,
            content_id if content_id is not None else content,
            start,
            end,
            identifier,
            properties
        )


class CaptionQueryParamsGeneral(CaptionQueryParams):
    def __init__(self,
        caption_id: int | str | None = None,
        caption_content: int | str | None = None,
        caption_content_id: int | str | None = None,
        caption_start: int | str | None = None,
        caption_end: int | str | None = None,
        caption_identifier: str | None = None,
        caption_properties: str | None = None
    ):
        super().__init__(
            caption_id,
            caption_content_id if caption_content_id is not None else caption_content,
            caption_start,
            caption_end,
            caption_identifier,
            caption_properties
        )


