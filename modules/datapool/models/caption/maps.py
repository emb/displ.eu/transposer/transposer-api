# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import re
import typing
from .orm import CaptionOrm





CAPTION__FIELDS_MAP = {
    'caption': {
        'id': CaptionOrm.id,
        'content': CaptionOrm.content_id,
        'content_id': CaptionOrm.content_id,
        'start': CaptionOrm.start,
        'end': CaptionOrm.end,
        'identifier': CaptionOrm.identifier,
        'properties': CaptionOrm.properties
    },
    'caption_id': CaptionOrm.id,
    'caption_content': CaptionOrm.content_id,
    'caption_content_id': CaptionOrm.content_id,
    'caption_start': CaptionOrm.start,
    'caption_end': CaptionOrm.end,
    'caption_identifier': CaptionOrm.identifier,
    'caption_properties': CaptionOrm.properties,

    'caption.id': CaptionOrm.id,
    'caption.content': CaptionOrm.content_id,
    'caption.content_id': CaptionOrm.content_id,
    'caption.start': CaptionOrm.start,
    'caption.end': CaptionOrm.end,
    'caption.identifier': CaptionOrm.identifier,
    'caption.properties': CaptionOrm.properties
}

CAPTION__VARIANTS_MAP = {
    'caption': {
        'id': 'caption.id',
        'content': 'caption.content_id',
        'content_id': 'caption.content_id',
        'start': 'caption.start',
        'end': 'caption.end',
        'identifier': 'caption.identifier',
        'properties': 'caption.properties'
    },
    'caption_id': 'caption.id',
    'caption_content': 'caption.content_id',
    'caption_content_id': 'caption.content_id',
    'caption_start': 'caption.start',
    'caption_end': 'caption.end',
    'caption_identifier': 'caption.identifier',
    'caption_properties': 'caption.properties',

    'caption.id': 'caption.id',
    'caption.content': 'caption.content_id',
    'caption.content_id': 'caption.content_id',
    'caption.start': 'caption.start',
    'caption.end': 'caption.end',
    'caption.identifier': 'caption.identifier',
    'caption.properties': 'caption.properties'
}



############
### Primary

CAPTION__PRIMARY_FIELDS_MAP = {
    'id': CaptionOrm.id,
    'content': CaptionOrm.content_id,
    'content_id': CaptionOrm.content_id,
    'start': CaptionOrm.start,
    'end': CaptionOrm.end,
    'identifier': CaptionOrm.identifier,
    'properties': CaptionOrm.properties
}

CAPTION__PRIMARY_VARIANTS_MAP = {
    'id': 'caption.id',
    'content': 'caption.content_id',
    'content_id': 'caption.content_id',
    'start': 'caption.start',
    'end': 'caption.end',
    'identifier': 'caption.identifier',
    'properties': 'caption.properties'
}


