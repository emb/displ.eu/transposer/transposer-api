# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import re
import typing
from .orm import PeerAccessTokenOrm





PEER_ACCESS_TOKEN__FIELDS_MAP = {
    'peer_access_token': {
        'id': PeerAccessTokenOrm.id,
        'created_at': PeerAccessTokenOrm.created_at,
        'peer': PeerAccessTokenOrm.peer_id,
        'peer_id': PeerAccessTokenOrm.peer_id,
        'name': PeerAccessTokenOrm.name,
        'key': PeerAccessTokenOrm.key,
        'token': PeerAccessTokenOrm.token,
        'is_active': PeerAccessTokenOrm.is_active,
        'valid_until': PeerAccessTokenOrm.valid_until
    },
    'peer_access_token_id': PeerAccessTokenOrm.id,
    'peer_access_token_created_at': PeerAccessTokenOrm.created_at,
    'peer_access_token_peer': PeerAccessTokenOrm.peer_id,
    'peer_access_token_peer_id': PeerAccessTokenOrm.peer_id,
    'peer_access_token_name': PeerAccessTokenOrm.name,
    'peer_access_token_key': PeerAccessTokenOrm.key,
    'peer_access_token_token': PeerAccessTokenOrm.token,
    'peer_access_token_is_active': PeerAccessTokenOrm.is_active,
    'peer_access_token_valid_until': PeerAccessTokenOrm.valid_until,

    'peer_access_token.id': PeerAccessTokenOrm.id,
    'peer_access_token.created_at': PeerAccessTokenOrm.created_at,
    'peer_access_token.peer': PeerAccessTokenOrm.peer_id,
    'peer_access_token.peer_id': PeerAccessTokenOrm.peer_id,
    'peer_access_token.name': PeerAccessTokenOrm.name,
    'peer_access_token.key': PeerAccessTokenOrm.key,
    'peer_access_token.token': PeerAccessTokenOrm.token,
    'peer_access_token.is_active': PeerAccessTokenOrm.is_active,
    'peer_access_token.valid_until': PeerAccessTokenOrm.valid_until
}

PEER_ACCESS_TOKEN__VARIANTS_MAP = {
    'peer_access_token': {
        'id': 'peer_access_token.id',
        'created_at': 'peer_access_token.created_at',
        'peer': 'peer_access_token.peer_id',
        'peer_id': 'peer_access_token.peer_id',
        'name': 'peer_access_token.name',
        'key': 'peer_access_token.key',
        'token': 'peer_access_token.token',
        'is_active': 'peer_access_token.is_active',
        'valid_until': 'peer_access_token.valid_until'
    },
    'peer_access_token_id': 'peer_access_token.id',
    'peer_access_token_created_at': 'peer_access_token.created_at',
    'peer_access_token_peer': 'peer_access_token.peer_id',
    'peer_access_token_peer_id': 'peer_access_token.peer_id',
    'peer_access_token_name': 'peer_access_token.name',
    'peer_access_token_key': 'peer_access_token.key',
    'peer_access_token_token': 'peer_access_token.token',
    'peer_access_token_is_active': 'peer_access_token.is_active',
    'peer_access_token_valid_until': 'peer_access_token.valid_until',

    'peer_access_token.id': 'peer_access_token.id',
    'peer_access_token.created_at': 'peer_access_token.created_at',
    'peer_access_token.peer': 'peer_access_token.peer_id',
    'peer_access_token.peer_id': 'peer_access_token.peer_id',
    'peer_access_token.name': 'peer_access_token.name',
    'peer_access_token.key': 'peer_access_token.key',
    'peer_access_token.token': 'peer_access_token.token',
    'peer_access_token.is_active': 'peer_access_token.is_active',
    'peer_access_token.valid_until': 'peer_access_token.valid_until'
}



############
### Primary

PEER_ACCESS_TOKEN__PRIMARY_FIELDS_MAP = {
    'id': PeerAccessTokenOrm.id,
    'created_at': PeerAccessTokenOrm.created_at,
    'peer': PeerAccessTokenOrm.peer_id,
    'peer_id': PeerAccessTokenOrm.peer_id,
    'name': PeerAccessTokenOrm.name,
    'key': PeerAccessTokenOrm.key,
    'token': PeerAccessTokenOrm.token,
    'is_active': PeerAccessTokenOrm.is_active,
    'valid_until': PeerAccessTokenOrm.valid_until
}

PEER_ACCESS_TOKEN__PRIMARY_VARIANTS_MAP = {
    'id': 'peer_access_token.id',
    'created_at': 'peer_access_token.created_at',
    'peer': 'peer_access_token.peer_id',
    'peer_id': 'peer_access_token.peer_id',
    'name': 'peer_access_token.name',
    'key': 'peer_access_token.key',
    'token': 'peer_access_token.token',
    'is_active': 'peer_access_token.is_active',
    'valid_until': 'peer_access_token.valid_until'
}


