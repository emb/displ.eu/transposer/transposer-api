# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

from .orm import WorkflowTemplateOrm





WORKFLOW_TEMPLATE__FIELDS_MAP = {
    'workflow_template': {
        'id': WorkflowTemplateOrm.id,
        'uuid': WorkflowTemplateOrm.uuid,
        'created_at': WorkflowTemplateOrm.created_at,
        'setting': WorkflowTemplateOrm.setting_id,
        'setting_id': WorkflowTemplateOrm.setting_id,
        'template': WorkflowTemplateOrm.template
    },
    'workflow_template_id': WorkflowTemplateOrm.id,
    'workflow_template_uuid': WorkflowTemplateOrm.uuid,
    'workflow_template_created_at': WorkflowTemplateOrm.created_at,
    'workflow_template_setting': WorkflowTemplateOrm.setting_id,
    'workflow_template_setting_id': WorkflowTemplateOrm.setting_id,
    'workflow_template_template': WorkflowTemplateOrm.template,

    'workflow_template.id': WorkflowTemplateOrm.id,
    'workflow_template.uuid': WorkflowTemplateOrm.uuid,
    'workflow_template.created_at': WorkflowTemplateOrm.created_at,
    'workflow_template.setting': WorkflowTemplateOrm.setting_id,
    'workflow_template.setting_id': WorkflowTemplateOrm.setting_id,
    'workflow_template.template': WorkflowTemplateOrm.template
}

WORKFLOW_TEMPLATE__VARIANTS_MAP = {
    'workflow_template': {
        'id': 'workflow_template.id',
        'uuid': 'workflow_template.uuid',
        'created_at': 'workflow_template.created_at',
        'setting': 'workflow_template.setting_id',
        'setting_id': 'workflow_template.setting_id',
        'template': 'workflow_template.template'
    },
    'workflow_template_id': 'workflow_template.id',
    'workflow_template_uuid': 'workflow_template.uuid',
    'workflow_template_created_at': 'workflow_template.created_at',
    'workflow_template_setting': 'workflow_template.setting_id',
    'workflow_template_setting_id': 'workflow_template.setting_id',
    'workflow_template_template': 'workflow_template.template',

    'workflow_template.id': 'workflow_template.id',
    'workflow_template.uuid': 'workflow_template.uuid',
    'workflow_template.created_at': 'workflow_template.created_at',
    'workflow_template.setting': 'workflow_template.setting_id',
    'workflow_template.setting_id': 'workflow_template.setting_id',
    'workflow_template.template': 'workflow_template.template'
}



############
### Primary

WORKFLOW_TEMPLATE__PRIMARY_FIELDS_MAP = {
    'id': WorkflowTemplateOrm.id,
    'uuid': WorkflowTemplateOrm.uuid,
    'created_at': WorkflowTemplateOrm.created_at,
    'setting': WorkflowTemplateOrm.setting_id,
    'setting_id': WorkflowTemplateOrm.setting_id,
    'template': WorkflowTemplateOrm.template
}

WORKFLOW_TEMPLATE__PRIMARY_VARIANTS_MAP = {
    'id': 'workflow_template.id',
    'uuid': 'workflow_template.uuid',
    'created_at': 'workflow_template.created_at',
    'setting': 'workflow_template.setting_id',
    'setting_id': 'workflow_template.setting_id',
    'template': 'workflow_template.template'
}


