# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

from .orm import PipelineOptionOrm





PIPELINE_OPTION__FIELDS_MAP = {
    'pipeline_option': {
        'id': PipelineOptionOrm.id,
        'parent': PipelineOptionOrm.parent_id,
        'parent_id': PipelineOptionOrm.parent_id,
        'key': PipelineOptionOrm.key,
        'value': PipelineOptionOrm.value,
        'type': PipelineOptionOrm.type
    },
    'pipeline_option_id': PipelineOptionOrm.id,
    'pipeline_option_parent': PipelineOptionOrm.parent_id,
    'pipeline_option_parent_id': PipelineOptionOrm.parent_id,
    'pipeline_option_key': PipelineOptionOrm.key,
    'pipeline_option_value': PipelineOptionOrm.value,
    'pipeline_option_type': PipelineOptionOrm.type,

    'pipeline_option.id': PipelineOptionOrm.id,
    'pipeline_option.parent': PipelineOptionOrm.parent_id,
    'pipeline_option.parent_id': PipelineOptionOrm.parent_id,
    'pipeline_option.key': PipelineOptionOrm.key,
    'pipeline_option.value': PipelineOptionOrm.value,
    'pipeline_option.type': PipelineOptionOrm.type
}

PIPELINE_OPTION__VARIANTS_MAP = {
    'pipeline_option': {
        'id': 'pipeline_option.id',
        'parent': 'pipeline_option.parent_id',
        'parent_id': 'pipeline_option.parent_id',
        'key': 'pipeline_option.key',
        'value': 'pipeline_option.value',
        'type': 'pipeline_option.type'
    },
    'pipeline_option_id': 'pipeline_option.id',
    'pipeline_option_parent': 'pipeline_option.parent_id',
    'pipeline_option_parent_id': 'pipeline_option.parent_id',
    'pipeline_option_key': 'pipeline_option.key',
    'pipeline_option_value': 'pipeline_option.value',
    'pipeline_option_type': 'pipeline_option.type',

    'pipeline_option.id': 'pipeline_option.id',
    'pipeline_option.parent': 'pipeline_option.parent_id',
    'pipeline_option.parent_id': 'pipeline_option.parent_id',
    'pipeline_option.key': 'pipeline_option.key',
    'pipeline_option.value': 'pipeline_option.value',
    'pipeline_option.type': 'pipeline_option.type'
}



############
### Primary

PIPELINE_OPTION__PRIMARY_FIELDS_MAP = {
    'id': PipelineOptionOrm.id,
    'parent': PipelineOptionOrm.parent_id,
    'parent_id': PipelineOptionOrm.parent_id,
    'key': PipelineOptionOrm.key,
    'value': PipelineOptionOrm.value,
    'type': PipelineOptionOrm.type
}

PIPELINE_OPTION__PRIMARY_VARIANTS_MAP = {
    'id': 'pipeline_option.id',
    'parent': 'pipeline_option.parent_id',
    'parent_id': 'pipeline_option.parent_id',
    'key': 'pipeline_option.key',
    'value': 'pipeline_option.value',
    'type': 'pipeline_option.type'
}


