# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

from .orm import PipelineProducerOptionOrm





PIPELINE_PRODUCER_OPTION__FIELDS_MAP = {
    'pipeline_producer_option': {
        'id': PipelineProducerOptionOrm.id,
        'parent': PipelineProducerOptionOrm.parent_id,
        'parent_id': PipelineProducerOptionOrm.parent_id,
        'key': PipelineProducerOptionOrm.key,
        'value': PipelineProducerOptionOrm.value,
        'datatype': PipelineProducerOptionOrm.datatype
    },
    'pipeline_producer_option_id': PipelineProducerOptionOrm.id,
    'pipeline_producer_option_parent': PipelineProducerOptionOrm.parent_id,
    'pipeline_producer_option_parent_id': PipelineProducerOptionOrm.parent_id,
    'pipeline_producer_option_key': PipelineProducerOptionOrm.key,
    'pipeline_producer_option_value': PipelineProducerOptionOrm.value,
    'pipeline_producer_option_datatype': PipelineProducerOptionOrm.datatype,

    'pipeline_producer_option.id': PipelineProducerOptionOrm.id,
    'pipeline_producer_option.parent': PipelineProducerOptionOrm.parent_id,
    'pipeline_producer_option.parent_id': PipelineProducerOptionOrm.parent_id,
    'pipeline_producer_option.key': PipelineProducerOptionOrm.key,
    'pipeline_producer_option.value': PipelineProducerOptionOrm.value,
    'pipeline_producer_option.datatype': PipelineProducerOptionOrm.datatype
}

PIPELINE_PRODUCER_OPTION__VARIANTS_MAP = {
    'pipeline_producer_option': {
        'id': 'pipeline_producer_option.id',
        'parent': 'pipeline_producer_option.parent_id',
        'parent_id': 'pipeline_producer_option.parent_id',
        'key': 'pipeline_producer_option.key',
        'value': 'pipeline_producer_option.value',
        'datatype': 'pipeline_producer_option.datatype'
    },
    'pipeline_producer_option_id': 'pipeline_producer_option.id',
    'pipeline_producer_option_parent': 'pipeline_producer_option.parent_id',
    'pipeline_producer_option_parent_id': 'pipeline_producer_option.parent_id',
    'pipeline_producer_option_key': 'pipeline_producer_option.key',
    'pipeline_producer_option_value': 'pipeline_producer_option.value',
    'pipeline_producer_option_datatype': 'pipeline_producer_option.datatype',

    'pipeline_producer_option.id': 'pipeline_producer_option.id',
    'pipeline_producer_option.parent': 'pipeline_producer_option.parent_id',
    'pipeline_producer_option.parent_id': 'pipeline_producer_option.parent_id',
    'pipeline_producer_option.key': 'pipeline_producer_option.key',
    'pipeline_producer_option.value': 'pipeline_producer_option.value',
    'pipeline_producer_option.datatype': 'pipeline_producer_option.datatype'
}



############
### Primary

PIPELINE_PRODUCER_OPTION__PRIMARY_FIELDS_MAP = {
    'id': PipelineProducerOptionOrm.id,
    'parent': PipelineProducerOptionOrm.parent_id,
    'parent_id': PipelineProducerOptionOrm.parent_id,
    'key': PipelineProducerOptionOrm.key,
    'value': PipelineProducerOptionOrm.value,
    'datatype': PipelineProducerOptionOrm.datatype
}

PIPELINE_PRODUCER_OPTION__PRIMARY_VARIANTS_MAP = {
    'id': 'pipeline_producer_option.id',
    'parent': 'pipeline_producer_option.parent_id',
    'parent_id': 'pipeline_producer_option.parent_id',
    'key': 'pipeline_producer_option.key',
    'value': 'pipeline_producer_option.value',
    'datatype': 'pipeline_producer_option.datatype'
}


