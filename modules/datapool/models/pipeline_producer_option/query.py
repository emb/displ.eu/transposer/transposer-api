# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import typing
import datetime

from regex_patterns import (
    RE_INT_VALUE,
    RE_LANG_CODE,
    RE_UUID4,
    RE_ALPHANUM_TERM,
    RE_SLUG,
    RE_ALPHANUM_ASCII_WSPACE
)
from data_utils import (
    split_terms,
    split_datetime,
    split_numeric
)
from ..base import BaseQueryParams
from .orm import PipelineProducerOptionOrm





class PipelineProducerOptionQueryParams(BaseQueryParams):
    __slots__ = [
        '_id',
        '_parent_id',
        '_key',
        '_value',
        '_datatype',

        '_id_stmt',
        '_parent_id_stmt',
        '_key_stmt',
        '_value_stmt',
        '_datatype_stmt'
    ]

    def __init__(self, 
        id: int | str | None = None,
        parent_id: int | str | None = None,
        key: str | None = None,
        value: int | float | bool | str | None = None,
        datatype: str | None = None
    ):
        super().__init__()

        # Store raw values
        self._id = id
        self._parent_id = parent_id
        self._key = key
        self._value = value
        self._datatype = datatype

        # Init statement values
        self._id_stmt = None
        self._parent_id_stmt = None
        self._key_stmt = None
        self._value_stmt = None
        self._datatype_stmt = None


    def add_conditions(self, container: list, use_and: bool = False) -> bool:

        # id
        if self._id_stmt is not None:
            container.append(self._id_stmt)
        else:
            val = self._id
            if isinstance(val, int):
                val = str(val)
            val = self.create_filter(
                PipelineProducerOptionOrm.id,
                [(x, '==') for x in split_terms(val, RE_INT_VALUE, '', '')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._id_stmt = val
                container.append(val)

        # parent_id
        if self._parent_id_stmt is not None:
            container.append(self._parent_id_stmt)
        else:
            val = self._parent_id
            if isinstance(val, int):
                val = str(val)
            val = self.create_filter(
                PipelineProducerOptionOrm.parent_id,
                [(x, '==') for x in split_terms(val, RE_INT_VALUE, '', '')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._parent_id_stmt = val
                container.append(val)

        # key
        if self._key_stmt is not None:
            container.append(self._key_stmt)
        else:
            val = self._key
            val = self.create_filter(
                PipelineProducerOptionOrm.key,
                [(x, '==') for x in split_terms(val, RE_SLUG, '', '')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._key_stmt = val
                container.append(val)
        
        # value
        if self._value_stmt is not None:
            container.append(self._value_stmt)
        else:
            val = self._value
            val = self.create_filter(
                PipelineProducerOptionOrm.value,
                [(x, 'like') for x in split_terms(val, RE_ALPHANUM_TERM, '%', '%')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._value_stmt = val
                container.append(val)

        # datatype
        if self._datatype_stmt is not None:
            container.append(self._datatype_stmt)
        else:
            val = self._datatype
            val = self.create_filter(
                PipelineProducerOptionOrm.datatype,
                [(x, '==') for x in split_terms(val, RE_SLUG, '', '')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._datatype_stmt = val
                container.append(val)

        # Return True if conditions were added
        return self._has_params
    

    # Getters for raw values

    @property
    def id(self) -> int | str | None:
        return self._id
    
    @property
    def parent_id(self) -> int | str | None:
        return self._parent_id
    
    @property
    def key(self) -> str | None:
        return self._key

    @property
    def value(self) -> int | float | bool | str | None:
        return self._value
    
    @property
    def datatype(self) -> str | None:
        return self._datatype
    

    # Getters for statements
    
    @property
    def id_stmt(self) -> typing.Any:
        return self._id_stmt
    
    @property
    def parent_id_stmt(self) -> typing.Any:
        return self._parent_id_stmt
    
    @property
    def key_stmt(self) -> typing.Any:
        return self._key_stmt
    
    @property
    def value_stmt(self) -> typing.Any:
        return self._value_stmt
    
    @property
    def datatype_stmt(self) -> typing.Any:
        return self._datatype_stmt



### Define Interfaces

class PipelineProducerOptionQueryParamsPrimary(PipelineProducerOptionQueryParams):
    def __init__(self,
        id: int | str | None = None,
        parent: int | str | None = None,
        parent_id: int | str | None = None,
        key: str | None = None,
        value: int | float | bool | str | None = None,
        datatype: str | None = None
    ):
        super().__init__(
            id,
            parent_id if parent_id is not None else parent,
            key,
            value,
            datatype
        )


class PipelineProducerOptionQueryParamsGeneral(PipelineProducerOptionQueryParams):
    def __init__(self,
        pipeline_producer_option_id: int | str | None = None,
        pipeline_producer_option_parent: int | str | None = None,
        pipeline_producer_option_parent_id: int | str | None = None,
        pipeline_producer_option_key: str | None = None,
        pipeline_producer_option_value: int | float | bool | str | None = None,
        pipeline_producer_option_datatype: str | None = None
    ):
        super().__init__(
            pipeline_producer_option_id,
            pipeline_producer_option_parent_id if pipeline_producer_option_parent_id is not None else pipeline_producer_option_parent,
            pipeline_producer_option_key,
            pipeline_producer_option_value,
            pipeline_producer_option_datatype
        )


