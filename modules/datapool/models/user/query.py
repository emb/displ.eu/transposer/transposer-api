# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import typing
import datetime

from regex_patterns import (
    RE_INT_VALUE,
    RE_ALPHANUM_TERM,
    RE_USERNAME,
    RE_PASSWORD,
    RE_UUID4
)
from data_utils import (
    split_emails,
    split_terms,
    split_datetime
)
from ..base import (
    BaseQueryParams
)
from .orm import UserOrm





class UserQueryParams(BaseQueryParams):
    __slots__ = [
        '_id',
        '_uuid',
        '_created_at',
        '_modified_at',
        '_username',
        '_email',
        #'_password',
        #'_salt',
        '_display_name',
        '_is_active',

        '_id_stmt',
        '_uuid_stmt',
        '_created_at_stmt',
        '_modified_at_stmt',
        '_username_stmt',
        '_email_stmt',
        #'_password_stmt',
        #'_salt_stmt',
        '_display_name_stmt',
        '_is_active_stmt'
    ]

    def __init__(self, 
        id: int | str | None = None,
        uuid: str | None = None,
        created_at: int | float | str | None = None,
        modified_at: int | float | str | None = None,
        username: str | None = None,
        email: str | None = None,
        #password: str | None = None,
        #salt: str | None = None,
        display_name: str | None = None,
        is_active: bool | None = None
    ):
        super().__init__()

        # Store raw values
        self._id = id
        self._uuid = uuid
        self._created_at = created_at
        self._modified_at = modified_at
        self._username = username
        self._email = email
        #self._password = password
        #self._salt = salt
        self._display_name = display_name
        self._is_active = is_active

        # Init statement values
        self._id_stmt = None
        self._uuid_stmt = None
        self._created_at_stmt = None
        self._modified_at_stmt = None
        self._username_stmt = None
        self._email_stmt = None
        #self._password_stmt = None
        #self._salt_stmt = None
        self._display_name_stmt = None
        self._is_active_stmt = None


    def add_conditions(self, container: list, use_and: bool = False) -> bool:

        # id
        if self._id_stmt is not None:
            container.append(self._id_stmt)
        else:
            val = self._id
            if isinstance(val, int):
                val = str(val)
            val = self.create_filter(
                UserOrm.id,
                [(x, '==') for x in split_terms(val, RE_INT_VALUE, '', '')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._id_stmt = val
                container.append(val)
        
        # uuid
        if self._uuid_stmt is not None:
            container.append(self._uuid_stmt)
        else:
            val = self._uuid
            val = self.create_filter(
                UserOrm.uuid,
                [(x, '==') for x in split_terms(val, RE_UUID4, '', '')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._uuid_stmt = val
                container.append(val)

        # created_at
        if self._created_at_stmt is not None:
            container.append(self._created_at_stmt)
        else:
            val = self._created_at
            val = self.create_filter(
                UserOrm.created_at,
                split_datetime(val),
                use_and
            )
            if val is not None:
                self._has_params = True
                self._created_at_stmt = val
                container.append(val)
        
        # modified_at
        if self._modified_at_stmt is not None:
            container.append(self._modified_at_stmt)
        else:
            val = self._modified_at
            val = self.create_filter(
                UserOrm.modified_at,
                split_datetime(val),
                use_and
            )
            if val is not None:
                self._has_params = True
                self._modified_at_stmt = val
                container.append(val)

        # username
        if self._username_stmt is not None:
            container.append(self._username_stmt)
        else:
            val = self._username
            val = self.create_filter(
                UserOrm.username,
                [(x, 'like') for x in split_terms(val, RE_USERNAME, '%', '%')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._username_stmt = val
                container.append(val)

        # email
        if self._email_stmt is not None:
            container.append(self._email_stmt)
        else:
            val = self._email
            val = self.create_filter(
                UserOrm.email,
                [(x, 'like') for x in split_emails(val, '%', '%')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._email_stmt = val
                container.append(val)

        # password
        # if self._password_stmt is not None:
        #     container.append(self._password_stmt)
        # else:
        #     val = self._password
        #     val = self.create_filter(
        #         UserOrm.password,
        #         [(x, 'like') for x in split_terms(val, RE_PASSWORD, '%', '%')],
        #         use_and
        #     )
        #     if val is not None:
        #         self._has_params = True
        #         self._password_stmt = val
        #         container.append(val)

        # salt
        # if self._salt_stmt is not None:
        #     container.append(self._salt_stmt)
        # else:
        #     val = self._salt
        #     val = self.create_filter(
        #         UserOrm.salt,
        #         [(x, 'like') for x in split_terms(val, RE_PASSWORD, '%', '%')],
        #         use_and
        #     )
        #     if val is not None:
        #         self._has_params = True
        #         self._salt_stmt = val
        #         container.append(val)

        # display_name
        if self._display_name_stmt is not None:
            container.append(self._display_name_stmt)
        else:
            val = self._display_name
            val = self.create_filter(
                UserOrm.display_name,
                [(x, 'like') for x in split_terms(val, RE_ALPHANUM_TERM, '%', '%')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._display_name_stmt = val
                container.append(val)
        
        # is_active
        if self._is_active_stmt is not None:
            container.append(self._is_active_stmt)
        else:
            val = self._is_active
            if val is not None:
                val = self.create_filter(
                    UserOrm.is_active,
                    [(val, '==')],
                    use_and
                )
                if val is not None:
                    self._has_params = True
                    self._is_active_stmt = val
                    container.append(val)

        # Return True if conditions were added
        return self._has_params
    

    # Getters for raw values

    @property
    def id(self) -> int | str | None:
        return self._id
    
    @property
    def uuid(self) -> str | None:
        return self._uuid
    
    @property
    def created_at(self) -> int | float | str | None:
        return self._created_at
    
    @property
    def modified_at(self) -> int | float | str | None:
        return self._modified_at
    
    @property
    def username(self) -> str | None:
        return self._username
    
    @property
    def email(self) -> str | None:
        return self._email
    
    # @property
    # def password(self) -> str | None:
    #     return self._password
    
    # @property
    # def salt(self) -> str | None:
    #     return self._salt
    
    @property
    def display_name(self) -> str | None:
        return self._display_name
    
    @property
    def is_active(self) -> bool | None:
        return self._is_active
    

    # Getters for statements
    
    @property
    def id_stmt(self) -> typing.Any:
        return self._id_stmt
    
    @property
    def uuid_stmt(self) -> typing.Any:
        return self._uuid_stmt
    
    @property
    def created_at_stmt(self) -> typing.Any:
        return self._created_at_stmt
    
    @property
    def modified_at_stmt(self) -> typing.Any:
        return self._modified_at_stmt
    
    @property
    def username_stmt(self) -> typing.Any:
        return self._username_stmt
    
    @property
    def email_stmt(self) -> typing.Any:
        return self._email_stmt
    
    # @property
    # def password_stmt(self) -> typing.Any:
    #     return self._password_stmt
    
    # @property
    # def salt_stmt(self) -> typing.Any:
    #     return self._salt_stmt
    
    @property
    def display_name_stmt(self) -> typing.Any:
        return self._display_name_stmt
    
    @property
    def is_active_stmt(self) -> typing.Any:
        return self._is_active_stmt



### Define Interfaces

class UserQueryParamsPrimary(UserQueryParams):
    def __init__(self,
        id: int | str | None = None,
        uuid: str | None = None,
        created_at: int | float | str | None = None,
        modified_at: int | float | str | None = None,
        username: str | None = None,
        email: str | None = None,
        #password: str | None = None,
        #salt: str | None = None,
        display_name: str | None = None,
        is_active: bool | None = None
    ):
        super().__init__(
            id,
            uuid,
            created_at,
            modified_at,
            username,
            email,
            #password,
            #salt,
            display_name,
            is_active
        )


class UserQueryParamsGeneral(UserQueryParams):
    def __init__(self,
        user_id: int | str | None = None,
        user_uuid: str | None = None,
        user_created_at: int | float | str | None = None,
        user_modified_at: int | float | str | None = None,
        user_username: str | None = None,
        user_email: str | None = None,
        #user_password: str | None = None,
        #user_salt: str | None = None,
        user_display_name: str | None = None,
        user_is_active: bool | None = None
    ):
        super().__init__(
            user_id,
            user_uuid,
            user_created_at,
            user_modified_at,
            user_username,
            user_email,
            #user_password,
            #user_salt,
            user_display_name,
            user_is_active
        )


