# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import re
import typing
from .orm import UserOrm





USER__FIELDS_MAP = {
    'user': {
        'id': UserOrm.id,
        'uuid': UserOrm.uuid,
        'created_at': UserOrm.created_at,
        'modified_at': UserOrm.modified_at,
        'username': UserOrm.username,
        'email': UserOrm.email,
        'password': UserOrm.password,
        'salt': UserOrm.salt,
        'display_name': UserOrm.display_name,
        'is_active': UserOrm.is_active
    },
    'owner': {
        'id': UserOrm.id,
        'uuid': UserOrm.uuid,
        'created_at': UserOrm.created_at,
        'modified_at': UserOrm.modified_at,
        'username': UserOrm.username,
        'email': UserOrm.email,
        'password': UserOrm.password,
        'salt': UserOrm.salt,
        'display_name': UserOrm.display_name,
        'is_active': UserOrm.is_active
    },
    'user_id': UserOrm.id,
    'user_uuid': UserOrm.uuid,
    'user_created_at': UserOrm.created_at,
    'user_modified_at': UserOrm.modified_at,
    'user_username': UserOrm.username,
    'user_email': UserOrm.email,
    'user_password': UserOrm.password,
    'user_salt': UserOrm.salt,
    'user_display_name': UserOrm.display_name,
    'user_is_active': UserOrm.is_active,

    'owner_id': UserOrm.id,
    'owner_uuid': UserOrm.uuid,
    'owner_created_at': UserOrm.created_at,
    'owner_modified_at': UserOrm.modified_at,
    'owner_username': UserOrm.username,
    'owner_email': UserOrm.email,
    'owner_password': UserOrm.password,
    'owner_salt': UserOrm.salt,
    'owner_display_name': UserOrm.display_name,
    'owner_is_active': UserOrm.is_active,

    'user.id': UserOrm.id,
    'user.uuid': UserOrm.uuid,
    'user.created_at': UserOrm.created_at,
    'user.modified_at': UserOrm.modified_at,
    'user.username': UserOrm.username,
    'user.email': UserOrm.email,
    'user.password': UserOrm.password,
    'user.salt': UserOrm.salt,
    'user.display_name': UserOrm.display_name,
    'user.is_active': UserOrm.is_active,

    'owner.id': UserOrm.id,
    'owner.uuid': UserOrm.uuid,
    'owner.created_at': UserOrm.created_at,
    'owner.modified_at': UserOrm.modified_at,
    'owner.username': UserOrm.username,
    'owner.email': UserOrm.email,
    'owner.password': UserOrm.password,
    'owner.salt': UserOrm.salt,
    'owner.display_name': UserOrm.display_name,
    'owner.is_active': UserOrm.is_active
}

USER__VARIANTS_MAP = {
    'user': {
        'id': 'owner.id',
        'uuid': 'owner.uuid',
        'created_at': 'owner.created_at',
        'modified_at': 'owner.modified_at',
        'username': 'owner.username',
        'email': 'owner.email',
        'password': 'owner.password',
        'salt': 'owner.salt',
        'display_name': 'owner.display_name',
        'is_active': 'owner.is_active'
    },
    'owner': {
        'id': 'owner.id',
        'uuid': 'owner.uuid',
        'created_at': 'owner.created_at',
        'modified_at': 'owner.modified_at',
        'username': 'owner.username',
        'email': 'owner.email',
        'password': 'owner.password',
        'salt': 'owner.salt',
        'display_name': 'owner.display_name',
        'is_active': 'owner.is_active'
    },
    'user_id': 'owner.id',
    'user_uuid': 'owner.uuid',
    'user_created_at': 'owner.created_at',
    'user_modified_at': 'owner.modified_at',
    'user_username': 'owner.username',
    'user_email': 'owner.email',
    'user_password': 'owner.password',
    'user_salt': 'owner.salt',
    'user_display_name': 'owner.display_name',
    'user_is_active': 'owner.is_active',

    'owner_id': 'owner.id',
    'owner_uuid': 'owner.uuid',
    'owner_created_at': 'owner.created_at',
    'owner_modified_at': 'owner.modified_at',
    'owner_username': 'owner.username',
    'owner_email': 'owner.email',
    'owner_password': 'owner.password',
    'owner_salt': 'owner.salt',
    'owner_display_name': 'owner.display_name',
    'owner_is_active': 'owner.is_active',

    'user.id': 'owner.id',
    'user.uuid': 'owner.uuid',
    'user.created_at': 'owner.created_at',
    'user.modified_at': 'owner.modified_at',
    'user.username': 'owner.username',
    'user.email': 'owner.email',
    'user.password': 'owner.password',
    'user.salt': 'owner.salt',
    'user.display_name': 'owner.display_name',
    'user.is_active': 'owner.is_active',

    'owner.id': 'owner.id',
    'owner.uuid': 'owner.uuid',
    'owner.created_at': 'owner.created_at',
    'owner.modified_at': 'owner.modified_at',
    'owner.username': 'owner.username',
    'owner.email': 'owner.email',
    'owner.password': 'owner.password',
    'owner.salt': 'owner.salt',
    'owner.display_name': 'owner.display_name',
    'owner.is_active': 'owner.is_active'
}



############
### Primary

USER__PRIMARY_FIELDS_MAP = {
    'id': UserOrm.id,
    'uuid': UserOrm.uuid,
    'created_at': UserOrm.created_at,
    'modified_at': UserOrm.modified_at,
    'username': UserOrm.username,
    'email': UserOrm.email,
    'password': UserOrm.password,
    'salt': UserOrm.salt,
    'display_name': UserOrm.display_name,
    'is_active': UserOrm.is_active
}

USER__PRIMARY_VARIANTS_MAP = {
    'id': 'user.id',
    'uuid': 'user.uuid',
    'created_at': 'user.created_at',
    'modified_at': 'user.modified_at',
    'username': 'user.username',
    'email': 'user.email',
    'password': 'user.password',
    'salt': 'user.salt',
    'display_name': 'user.display_name',
    'is_active': 'user.is_active'
}


