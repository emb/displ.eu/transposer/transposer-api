# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import typing
import datetime
import bcrypt
import base64

import sqlalchemy
from sqlalchemy.orm import (
    Mapped,
    mapped_column,
    relationship,
    validates
)

from regex_patterns import (
    RE_INT_VALUE,
    RE_UUID4,
    RE_ALPHANUM_TERM,
    RE_EMAIL,
    RE_USERNAME,
    RE_SLUG,
    RE_PASSWORD,
    RE_HASH_XX
)
from data_utils import (
    is_empty, is_true,
    validate_email,
    split_emails,
    split_terms,
    split_numeric,
    split_datetime,
    sanitize_text
)
from ..base import BaseOrm
from ..links import (
    user_group_link_table,
    user_role_link_table
)





class UserOrm(BaseOrm):
    __tablename__ = 'users'
    id: Mapped[int] = mapped_column(
        sqlalchemy.Integer, primary_key=True
    )
    uuid: Mapped[str] = mapped_column(sqlalchemy.String)
    created_at: Mapped[datetime.datetime] = mapped_column(
        sqlalchemy.DateTime, default=sqlalchemy.sql.functions.now()
    )
    modified_at: Mapped[datetime.datetime] = mapped_column(
        sqlalchemy.DateTime, default=sqlalchemy.sql.functions.now(), onupdate=sqlalchemy.sql.functions.now()
    )
    username: Mapped[str] = mapped_column(sqlalchemy.String)
    email: Mapped[str] = mapped_column(sqlalchemy.String)
    password: Mapped[str] = mapped_column(sqlalchemy.String)
    salt: Mapped[str] = mapped_column(sqlalchemy.String)
    display_name: Mapped[typing.Optional[str]]
    is_active: Mapped[bool] = mapped_column(sqlalchemy.Boolean, default=False)

    documents: Mapped[typing.List['DocumentOrm']] = relationship(
        back_populates='owner',
        primaryjoin="(UserOrm.id==DocumentOrm.owner_id)"
    )
    requests: Mapped[typing.List['RequestOrm']] = relationship(
        back_populates='owner',
        primaryjoin="(UserOrm.id==RequestOrm.owner_id)"
    )
    jobs: Mapped[typing.List['JobOrm']] = relationship(
        back_populates='owner',
        primaryjoin="(UserOrm.id==JobOrm.owner_id)"
    )
    peers: Mapped[typing.List['PeerOrm']] = relationship(
        back_populates='owner',
        primaryjoin="(UserOrm.id==PeerOrm.owner_id)"
    )

    access_tokens: Mapped[typing.List['AccessTokenOrm']] = relationship(
        back_populates='owner',
        primaryjoin="(UserOrm.id==AccessTokenOrm.owner_id)"
    )
    bearer_tokens: Mapped[typing.List['BearerTokenOrm']] = relationship(
        back_populates='owner',
        primaryjoin="(UserOrm.id==BearerTokenOrm.owner_id)"
    )

    groups: Mapped[typing.List['GroupOrm']] = relationship(
        'GroupOrm',
        secondary=user_group_link_table,
        back_populates='users'
    )
    roles: Mapped[typing.List['RoleOrm']] = relationship(
        'RoleOrm',
        secondary=user_role_link_table,
        back_populates='users'
    )

    workflow_templates: Mapped[typing.List['WorkflowTemplateOrm']] = relationship(
        back_populates='owner',
        primaryjoin="(UserOrm.id==WorkflowTemplateOrm.owner_id)"
    )
    
    def __repr__(self):
        return (
            f'<User('
            f'id={self.id}, '
            f'uuid={self.uuid}, '
            f'username={self.username}, '
            f'email={self.email}, '
            f'display_name={self.display_name}, '
            f'is_active={self.is_active})>'
        )
    
    def __str__(self):
        return (
            f'User('
            f'id={self.id}, '
            f'uuid={self.uuid}, '
            f'username={self.username}, '
            f'email={self.email}, '
            f'display_name={self.display_name}, '
            f'is_active={self.is_active})'
        )
    
    def __json__(self):
        return {
            'id': self.id,
            'uuid': self.uuid,
            'created_at': self.created_at,
            'modified_at': self.modified_at,
            'username': self.username,
            'email': self.email,
            'display_name': self.display_name,
            #'password': '***',
            #'salt': '***',
            'is_active': self.is_active
        }
    
    def _fields(self) -> tuple:
        return (
            ['id', 'uuid', 'created_at', 'modified_at', 'username', 'email', 'display_name', 'is_active'],
            [
                ('documents', True, None),
                ('requests', True, None),
                ('jobs', True, None),
                ('peers', True, None),
                ('access_tokens', True, None),
                ('bearer_tokens', True, None),
                ('groups', True, None),
                ('roles', True, None),
                ('workflow_templates', True, None)
            ]
        )
    

    @staticmethod
    def hash_password(password: str):
        salt = bcrypt.gensalt()
        hash = bcrypt.hashpw(password.encode('utf-8'), salt)

        salt = str(base64.b64encode(salt), encoding='utf-8')
        hash = str(base64.b64encode(hash), encoding='utf-8')

        return {
            'salt': salt,
            'hash': hash
        }
    
    @staticmethod
    def test_password(password: str, hash: str, salt: str):
        hashed_pw = bcrypt.hashpw(
            password.encode('utf-8'),
            base64.b64decode(salt)
        )
        hashed_pw = str(base64.b64encode(hashed_pw), encoding='utf-8')
        return hashed_pw == hash
    

    @validates('uuid')
    def validate_uuid(self, key, value):
        if is_empty(value):
            raise ValueError("UUID is required")
        if RE_UUID4.match(value) is None:
            raise ValueError("Invalid UUID")
        return value

    @validates('username')
    def validate_username(self, key, value):
        if is_empty(value):
            raise ValueError("Username is required")
        if RE_USERNAME.match(value) is None:
            raise ValueError("Invalid username")
        return value

    @validates('email')
    def validate_email(self, key, value):
        if is_empty(value):
            raise ValueError("Email is required")
        if not validate_email(value):
            raise ValueError("Invalid email address")
        return value

    @validates('password')
    def validate_password(self, key, value):
        return value
    @validates('salt')
    def validate_salt(self, key, value):
        return value

    @validates('display_name')
    def validate_display_name(self, key, value):
        if is_empty(value):
            return sqlalchemy.sql.null()
        if not isinstance(value, str) or RE_ALPHANUM_TERM.match(value) is None:
            raise ValueError("Invalid display name")
        return value

    @validates('is_active')
    def validate_is_active(self, key, value):
        return is_true(value)




