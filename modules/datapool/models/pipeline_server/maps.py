# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

from .orm import PipelineServerOrm





PIPELINE_SERVER__FIELDS_MAP = {
    'pipeline_server': {
        'id': PipelineServerOrm.id,
        'uuid': PipelineServerOrm.uuid,
        'created_at': PipelineServerOrm.created_at,
        'pipeline': PipelineServerOrm.pipeline_id,
        'pipeline_id': PipelineServerOrm.pipeline_id,
        'name': PipelineServerOrm.name,
        'host': PipelineServerOrm.host,
        'port': PipelineServerOrm.port
    },
    'pipeline_server_id': PipelineServerOrm.id,
    'pipeline_server_uuid': PipelineServerOrm.uuid,
    'pipeline_server_created_at': PipelineServerOrm.created_at,
    'pipeline_server_pipeline': PipelineServerOrm.pipeline_id,
    'pipeline_server_pipeline_id': PipelineServerOrm.pipeline_id,
    'pipeline_server_name': PipelineServerOrm.name,
    'pipeline_server_host': PipelineServerOrm.host,
    'pipeline_server_port': PipelineServerOrm.port,

    'pipeline_server.id': PipelineServerOrm.id,
    'pipeline_server.uuid': PipelineServerOrm.uuid,
    'pipeline_server.created_at': PipelineServerOrm.created_at,
    'pipeline_server.pipeline': PipelineServerOrm.pipeline_id,
    'pipeline_server.pipeline_id': PipelineServerOrm.pipeline_id,
    'pipeline_server.name': PipelineServerOrm.name,
    'pipeline_server.host': PipelineServerOrm.host,
    'pipeline_server.port': PipelineServerOrm.port
}

PIPELINE_SERVER__VARIANTS_MAP = {
    'pipeline_server': {
        'id': 'pipeline_server.id',
        'uuid': 'pipeline_server.uuid',
        'created_at': 'pipeline_server.created_at',
        'pipeline': 'pipeline_server.pipeline_id',
        'pipeline_id': 'pipeline_server.pipeline_id',
        'name': 'pipeline_server.name',
        'host': 'pipeline_server.host',
        'port': 'pipeline_server.port'
    },
    'pipeline_server_id': 'pipeline_server.id',
    'pipeline_server_uuid': 'pipeline_server.uuid',
    'pipeline_server_created_at': 'pipeline_server.created_at',
    'pipeline_server_pipeline': 'pipeline_server.pipeline_id',
    'pipeline_server_pipeline_id': 'pipeline_server.pipeline_id',
    'pipeline_server_name': 'pipeline_server.name',
    'pipeline_server_host': 'pipeline_server.host',
    'pipeline_server_port': 'pipeline_server.port',

    'pipeline_server.id': 'pipeline_server.id',
    'pipeline_server.uuid': 'pipeline_server.uuid',
    'pipeline_server.created_at': 'pipeline_server.created_at',
    'pipeline_server.pipeline': 'pipeline_server.pipeline_id',
    'pipeline_server.pipeline_id': 'pipeline_server.pipeline_id',
    'pipeline_server.name': 'pipeline_server.name',
    'pipeline_server.host': 'pipeline_server.host',
    'pipeline_server.port': 'pipeline_server.port'
}



############
### Primary

PIPELINE_SERVER__PRIMARY_FIELDS_MAP = {
    'id': PipelineServerOrm.id,
    'uuid': PipelineServerOrm.uuid,
    'created_at': PipelineServerOrm.created_at,
    'pipeline': PipelineServerOrm.pipeline_id,
    'pipeline_id': PipelineServerOrm.pipeline_id,
    'name': PipelineServerOrm.name,
    'host': PipelineServerOrm.host,
    'port': PipelineServerOrm.port
}

PIPELINE_SERVER__PRIMARY_VARIANTS_MAP = {
    'id': 'pipeline_server.id',
    'uuid': 'pipeline_server.uuid',
    'created_at': 'pipeline_server.created_at',
    'pipeline': 'pipeline_server.pipeline_id',
    'pipeline_id': 'pipeline_server.pipeline_id',
    'name': 'pipeline_server.name',
    'host': 'pipeline_server.host',
    'port': 'pipeline_server.port'
}


