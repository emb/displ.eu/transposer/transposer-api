# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import typing
import datetime

import sqlalchemy
from sqlalchemy.orm import (
    Mapped,
    mapped_column,
    relationship,
    validates
)

from regex_patterns import (
    RE_INT_VALUE,
    RE_FLOAT_VALUE,
    RE_LANG_CODE,
    RE_UUID4,
    RE_SLUG,
    RE_ALPHANUM_ASCII_WSPACE,
    RE_DOMAIN
)
from data_utils import (
    is_empty,
    split_terms,
    split_datetime,
    sanitize_text
)
from ..base import BaseOrm





class PipelineServerOrm(BaseOrm):
    __tablename__ = 'pipeline_servers'
    id: Mapped[int] = mapped_column(
        sqlalchemy.Integer, primary_key=True
    )
    uuid: Mapped[str] = mapped_column(sqlalchemy.String)
    created_at: Mapped[datetime.datetime] = mapped_column(
        sqlalchemy.DateTime, default=sqlalchemy.sql.functions.now()
    )
    pipeline_id: Mapped[int] = mapped_column(sqlalchemy.ForeignKey('pipelines.id'))
    name: Mapped[typing.Optional[str]] = mapped_column(sqlalchemy.String, default=None, nullable=True)
    host: Mapped[str] = mapped_column(sqlalchemy.String, default='127.0.0.1')
    port: Mapped[int] = mapped_column(sqlalchemy.Integer, default=9092)

    pipeline: Mapped['PipelineOrm'] = relationship(
        back_populates='servers'
    )
    
    def __repr__(self):
        return (
            f'<PipelineServer('
            f'id={self.id}, '
            f'uuid={self.uuid}, '
            f'created_at={self.created_at}, '
            f'pipeline_id={self.pipeline_id}, '
            f'name={self.name}, '
            f'host={self.host}, '
            f'port={self.port})>'
        )
    
    def __str__(self):
        return (
            f'PipelineServer('
            f'id={self.id}, '
            f'uuid={self.uuid}, '
            f'created_at={self.created_at}, '
            f'pipeline_id={self.pipeline_id}, '
            f'name={self.name}, '
            f'host={self.host}, '
            f'port={self.port})'
        )
    
    def __json__(self):
        return {
            'id': self.id,
            'uuid': self.uuid,
            'created_at': self.created_at,
            'pipeline_id': self.pipeline_id,
            'name': self.name,
            'host': self.host,
            'port': self.port
        }
    
    def _fields(self) -> tuple:
        return (
            ['id', 'uuid', 'created_at', 'pipeline_id', 'name', 'host', 'port'],
            [('pipeline', False, None)]
        )
    

    @validates('uuid')
    def validate_uuid(self, key, value):
        if is_empty(value):
            raise ValueError("UUID is required")
        if isinstance(value, str):
            result = RE_UUID4.match(value)
            if result is None:
                raise ValueError("Invalid UUID")
            return result.group(1)
        raise ValueError("Invalid UUID")
    
    @validates('pipeline_id')
    def validate_pipeline_id(self, key, value):
        if is_empty(value):
            raise ValueError("Pipeline ID is required")
        if isinstance(value, int):
            return value
        if isinstance(value, str):
            result = RE_INT_VALUE.match(value)
            if result is None:
                raise ValueError("Invalid Pipeline ID")
            return int(result.group(1))
        raise ValueError("Invalid Pipeline ID")

    @validates('name')
    def validate_name(self, key, value):
        if is_empty(value):
            return None
        if isinstance(value, str):
            result = RE_ALPHANUM_ASCII_WSPACE.match(value)
            if result is None:
                raise ValueError("Invalid name")
            return result.group(1)
        raise ValueError("Invalid name")

    @validates('host')
    def validate_host(self, key, value):
        if is_empty(value):
            raise ValueError("Host is required")
        if isinstance(value, str):
            result = RE_DOMAIN.match(value)
            if result is None:
                raise ValueError("Invalid host")
            return result.group(1)
        raise ValueError("Invalid host")

    @validates('port')
    def validate_port(self, key, value):
        if is_empty(value):
            raise ValueError("Port is required")
        if isinstance(value, int):
            if value < 1 or value > 65535:
                raise ValueError("Invalid port")
            return value
        if isinstance(value, str):
            result = RE_INT_VALUE.match(value)
            if result is None:
                raise ValueError("Invalid port")
            value = int(result.group(1))
            if value < 1 or value > 65535:
                raise ValueError("Invalid port")
            return value
        raise ValueError("Invalid port")


