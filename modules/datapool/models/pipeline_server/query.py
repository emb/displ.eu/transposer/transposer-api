# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import typing
import datetime

from regex_patterns import (
    RE_INT_VALUE,
    RE_LANG_CODE,
    RE_UUID4,
    RE_ALPHANUM_TERM,
    RE_SLUG,
    RE_ALPHANUM_ASCII_WSPACE,
    RE_DOMAIN
)
from data_utils import (
    split_terms,
    split_datetime,
    split_numeric,
    split_port
)
from ..base import (
    BaseQueryParams
)
from .orm import PipelineServerOrm





class PipelineServerQueryParams(BaseQueryParams):
    __slots__ = [
        '_id',
        '_uuid',
        '_created_at',
        '_pipeline_id',
        '_name',
        '_host',
        '_port',

        '_id_stmt',
        '_uuid_stmt',
        '_created_at_stmt',
        '_pipeline_id_stmt',
        '_name_stmt',
        '_host_stmt',
        '_port_stmt'
    ]

    def __init__(self, 
        id: int | str | None = None,
        uuid: str | None = None,
        created_at: int | float | str | None = None,
        pipeline_id: int | str | None = None,
        name: str | None = None,
        host: str | None = None,
        port: int | str | None = None
    ):
        super().__init__()

        # Store raw values
        self._id = id
        self._uuid = uuid
        self._created_at = created_at
        self._pipeline_id = pipeline_id
        self._name = name
        self._host = host
        self._port = port

        # Init statement values
        self._id_stmt = None
        self._uuid_stmt = None
        self._created_at_stmt = None
        self._pipeline_id_stmt = None
        self._name_stmt = None
        self._host_stmt = None
        self._port_stmt = None


    def add_conditions(self, container: list, use_and: bool = False) -> bool:

        # id
        if self._id_stmt is not None:
            container.append(self._id_stmt)
        else:
            val = self._id
            if isinstance(val, int):
                val = str(val)
            val = self.create_filter(
                PipelineServerOrm.id,
                [(x, '==') for x in split_terms(val, RE_INT_VALUE, '', '')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._id_stmt = val
                container.append(val)

        # uuid
        if self._uuid_stmt is not None:
            container.append(self._uuid_stmt)
        else:
            val = self._uuid
            val = self.create_filter(
                PipelineServerOrm.uuid,
                [(x, '==') for x in split_terms(val, RE_UUID4, '', '')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._uuid_stmt = val
                container.append(val)
        
        # created_at
        if self._created_at_stmt is not None:
            container.append(self._created_at_stmt)
        else:
            val = self._created_at
            val = self.create_filter(
                PipelineServerOrm.created_at,
                split_datetime(val),
                use_and
            )
            if val is not None:
                self._has_params = True
                self._created_at_stmt = val
                container.append(val)

        # pipeline_id
        if self._pipeline_id_stmt is not None:
            container.append(self._pipeline_id_stmt)
        else:
            val = self._pipeline_id
            if isinstance(val, int):
                val = str(val)
            val = self.create_filter(
                PipelineServerOrm.pipeline_id,
                [(x, '==') for x in split_terms(val, RE_INT_VALUE, '', '')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._pipeline_id_stmt = val
                container.append(val)

        # name
        if self._name_stmt is not None:
            container.append(self._name_stmt)
        else:
            val = self._name
            val = self.create_filter(
                PipelineServerOrm.name,
                [(x, 'like') for x in split_terms(val, RE_ALPHANUM_ASCII_WSPACE, '%', '%')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._name_stmt = val
                container.append(val)

        # host
        if self._host_stmt is not None:
            container.append(self._host_stmt)
        else:
            val = self._host
            val = self.create_filter(
                PipelineServerOrm.host,
                [(x, 'like') for x in split_terms(val, RE_DOMAIN, '%', '%')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._host_stmt = val
                container.append(val)
        
        # port
        if self._port_stmt is not None:
            container.append(self._port_stmt)
        else:
            val = self._port
            if isinstance(val, int):
                val = str(val)
            val = self.create_filter(
                PipelineServerOrm.port,
                split_port(val),
                use_and
            )
            if val is not None:
                self._has_params = True
                self._port_stmt = val
                container.append(val)

        # Return True if conditions were added
        return self._has_params
    

    # Getters for raw values

    @property
    def id(self) -> int | str | None:
        return self._id
    
    @property
    def uuid(self) -> str | None:
        return self._uuid
    
    @property
    def created_at(self) -> int | float | str | None:
        return self._created_at

    @property
    def pipeline_id(self) -> int | str | None:
        return self._pipeline_id
    
    @property
    def name(self) -> str | None:
        return self._name
    
    @property
    def host(self) -> str | None:
        return self._host
    
    @property
    def port(self) -> int | str  | None:
        return self._port
    

    # Getters for statements
    
    @property
    def id_stmt(self) -> typing.Any:
        return self._id_stmt
    
    @property
    def uuid_stmt(self) -> typing.Any:
        return self._uuid_stmt
    
    @property
    def created_at_stmt(self) -> typing.Any:
        return self._created_at_stmt
    
    @property
    def pipeline_id_stmt(self) -> typing.Any:
        return self._pipeline_id_stmt
    
    @property
    def name_stmt(self) -> typing.Any:
        return self._name_stmt
    
    @property
    def host_stmt(self) -> typing.Any:
        return self._host_stmt
    
    @property
    def port_stmt(self) -> typing.Any:
        return self._port_stmt



### Define Interfaces

class PipelineServerQueryParamsPrimary(PipelineServerQueryParams):
    def __init__(self,
        id: int | str | None = None,
        uuid: str | None = None,
        created_at: int | float | str | None = None,
        pipeline: int | str | None = None,
        pipeline_id: int | str | None = None,
        name: str | None = None,
        host: str | None = None,
        port: int | str | None = None
    ):
        super().__init__(
            id,
            uuid,
            created_at,
            pipeline_id if pipeline_id is not None else pipeline,
            name,
            host,
            port
        )


class PipelineServerQueryParamsGeneral(PipelineServerQueryParams):
    def __init__(self,
        pipeline_server_id: int | str | None = None,
        pipeline_server_uuid: str | None = None,
        pipeline_server_created_at: int | float | str | None = None,
        pipeline_server_pipeline: int | str | None = None,
        pipeline_server_pipeline_id: int | str | None = None,
        pipeline_server_name: str | None = None,
        pipeline_server_host: str | None = None,
        pipeline_server_port: int | str | None = None
    ):
        super().__init__(
            pipeline_server_id,
            pipeline_server_uuid,
            pipeline_server_created_at,
            pipeline_server_pipeline_id if pipeline_server_pipeline_id is not None else pipeline_server_pipeline,
            pipeline_server_name,
            pipeline_server_host,
            pipeline_server_port
        )


