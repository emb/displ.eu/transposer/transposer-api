# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

from pydantic import BaseModel
from ..base import (
    TrpPrimaryKey_UserID,
    TrpPrimaryKey_ID_UUID
)
from ..content import (
    NewContenStructurePartialModel,
    UpdateContentStructureModel
)
from ..transcription import (
    NewTranscriptionStructurePartialModel,
    UpdateTranscriptionStructureModel
)
from ..request import (
    NewRequestPartialModel,
    UpdateRequestModel
)
from ..media import (
    NewMediaStructurePartialModel,
    UpdateMediaStructureModel
)





class DocumentModel(BaseModel):
    id: TrpPrimaryKey_ID_UUID
    uuid: str
    owner: TrpPrimaryKey_UserID
    type: int = 0
    status: int = 0
    reason: str | None = None


class NewDocumentModel(BaseModel):
    owner: TrpPrimaryKey_UserID
    type: int = 0
    #status: int = 1

class NewDocumentStructureModel(NewDocumentModel):
    content: list[NewContenStructurePartialModel] | NewContenStructurePartialModel | None = None
    transcription: list[NewTranscriptionStructurePartialModel] | NewTranscriptionStructurePartialModel | None = None
    media: NewMediaStructurePartialModel | None = None
    request: NewRequestPartialModel | None = None


class UpdateDocumentModel(BaseModel):
    type: int = 0
    status: int | None = None
    reason: str | None = None

class UpdateDocumentStructureModel(UpdateDocumentModel):
    content: list[UpdateContentStructureModel] | UpdateContentStructureModel | None = None
    transcription: list[UpdateTranscriptionStructureModel] | UpdateTranscriptionStructureModel | None = None
    media: UpdateMediaStructureModel | None = None
    request: UpdateRequestModel | None = None

