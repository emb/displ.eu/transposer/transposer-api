# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import re
import typing
from .orm import DocumentOrm





DOCUMENT__FIELDS_MAP = {
    'document': {
        'id': DocumentOrm.id,
        'uuid': DocumentOrm.uuid,
        'created_at': DocumentOrm.created_at,
        'modified_at': DocumentOrm.modified_at,
        'owner': DocumentOrm.owner_id,
        'owner_id': DocumentOrm.owner_id,
        'type': DocumentOrm.type,
        'status': DocumentOrm.status,
        'reason': DocumentOrm.reason
    },
    'document_id': DocumentOrm.id,
    'document_uuid': DocumentOrm.uuid,
    'document_created_at': DocumentOrm.created_at,
    'document_modified_at': DocumentOrm.modified_at,
    'document_owner': DocumentOrm.owner_id,
    'document_owner_id': DocumentOrm.owner_id,
    'document_type': DocumentOrm.type,
    'document_status': DocumentOrm.status,
    'document_reason': DocumentOrm.reason,

    'document.id': DocumentOrm.id,
    'document.uuid': DocumentOrm.uuid,
    'document.created_at': DocumentOrm.created_at,
    'document.modified_at': DocumentOrm.modified_at,
    'document.owner': DocumentOrm.owner_id,
    'document.owner_id': DocumentOrm.owner_id,
    'document.type': DocumentOrm.type,
    'document.status': DocumentOrm.status,
    'document.reason': DocumentOrm.reason
}

DOCUMENT__VARIANTS_MAP = {
    'document': {
        'id': 'document.id',
        'uuid': 'document.uuid',
        'created_at': 'document.created_at',
        'modified_at': 'document.modified_at',
        'owner': 'document.owner_id',
        'owner_id': 'document.owner_id',
        'type': 'document.type',
        'status': 'document.status',
        'reason': 'document.reason'
    },
    'document_id': 'document.id',
    'document_uuid': 'document.uuid',
    'document_created_at': 'document.created_at',
    'document_modified_at': 'document.modified_at',
    'document_owner': 'document.owner_id',
    'document_owner_id': 'document.owner_id',
    'document_type': 'document.type',
    'document_status': 'document.status',
    'document_reason': 'document.reason',

    'document.id': 'document.id',
    'document.uuid': 'document.uuid',
    'document.created_at': 'document.created_at',
    'document.modified_at': 'document.modified_at',
    'document.owner': 'document.owner_id',
    'document.owner_id': 'document.owner_id',
    'document.type': 'document.type',
    'document.status': 'document.status',
    'document.reason': 'document.reason'
}



############
### Primary

DOCUMENT__PRIMARY_FIELDS_MAP = {
    'id': DocumentOrm.id,
    'uuid': DocumentOrm.uuid,
    'created_at': DocumentOrm.created_at,
    'modified_at': DocumentOrm.modified_at,
    'owner': DocumentOrm.owner_id,
    'owner_id': DocumentOrm.owner_id,
    'type': DocumentOrm.type,
    'status': DocumentOrm.status,
    'reason': DocumentOrm.reason
}

DOCUMENT__PRIMARY_VARIANTS_MAP = {
    'id': 'document.id',
    'uuid': 'document.uuid',
    'created_at': 'document.created_at',
    'modified_at': 'document.modified_at',
    'owner': 'document.owner_id',
    'owner_id': 'document.owner_id',
    'type': 'document.type',
    'status': 'document.status',
    'reason': 'document.reason'
}


