# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import typing
import datetime

from regex_patterns import (
    RE_INT_VALUE,
    RE_UUID4,
    RE_ALPHANUM_TERM
)
from data_utils import (
    split_terms,
    split_numeric,
    split_datetime
)
from ..base import (
    BaseQueryParams
)
from .orm import DocumentOrm





class DocumentQueryParams(BaseQueryParams):
    __slots__ = [
        '_id',
        '_uuid',
        '_created_at',
        '_modified_at',
        '_owner_id',
        '_type',
        '_status',
        '_reason',

        '_id_stmt',
        '_uuid_stmt',
        '_created_at_stmt',
        '_modified_at_stmt',
        '_owner_id_stmt',
        '_type_stmt',
        '_status_stmt',
        '_reason_stmt'
    ]

    def __init__(self, 
        id: int | str | None = None,
        uuid: str | None = None,
        created_at: int | float | str | None = None,
        modified_at: int | float | str | None = None,
        owner_id: int | str | None = None,
        type: int | str | None = None,
        status: int | str | None = None,
        reason: str | None = None
    ):
        super().__init__()

        # Store raw values
        self._id = id
        self._uuid = uuid
        self._created_at = created_at
        self._modified_at = modified_at
        self._owner_id = owner_id
        self._type = type
        self._status = status
        self._reason = reason

        # Init statement values
        self._id_stmt = None
        self._uuid_stmt = None
        self._created_at_stmt = None
        self._modified_at_stmt = None
        self._owner_id_stmt = None
        self._type_stmt = None
        self._status_stmt = None
        self._reason_stmt = None


    def add_conditions(self, container: list, use_and: bool = False) -> bool:

        # id
        if self._id_stmt is not None:
            container.append(self._id_stmt)
        else:
            val = self._id
            if isinstance(val, int):
                val = str(val)
            val = self.create_filter(
                DocumentOrm.id,
                [(x, '==') for x in split_terms(val, RE_INT_VALUE, '', '')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._id_stmt = val
                container.append(val)
        
        # uuid
        if self._uuid_stmt is not None:
            container.append(self._uuid_stmt)
        else:
            val = self._uuid
            val = self.create_filter(
                DocumentOrm.uuid,
                [(x, '==') for x in split_terms(val, RE_UUID4, '', '')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._uuid_stmt = val
                container.append(val)
        
        # created_at
        if self._created_at_stmt is not None:
            container.append(self._created_at_stmt)
        else:
            val = self._created_at
            val = self.create_filter(
                DocumentOrm.created_at,
                split_datetime(val),
                use_and
            )
            if val is not None:
                self._has_params = True
                self._created_at_stmt = val
                container.append(val)
        
        # modified_at
        if self._modified_at_stmt is not None:
            container.append(self._modified_at_stmt)
        else:
            val = self._modified_at
            val = self.create_filter(
                DocumentOrm.modified_at,
                split_datetime(val),
                use_and
            )
            if val is not None:
                self._has_params = True
                self._modified_at_stmt = val
                container.append(val)

        # owner_id
        if self._owner_id_stmt is not None:
            container.append(self._owner_id_stmt)
        else:
            val = self._owner_id
            if isinstance(val, int):
                val = str(val)
            val = self.create_filter(
                DocumentOrm.owner_id,
                [(x, '==') for x in split_terms(val, RE_INT_VALUE, '', '')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._owner_id_stmt = val
                container.append(val)
        
        # type
        if self._type_stmt is not None:
            container.append(self._type_stmt)
        else:
            val = self._type
            val = self.create_filter(
                DocumentOrm.type,
                split_numeric(type),
                use_and
            )
            if val is not None:
                self._has_params = True
                self._type_stmt = val
                container.append(val)
        
        # status
        if self._status_stmt is not None:
            container.append(self._status_stmt)
        else:
            val = self._status
            if isinstance(val, int):
                val = str(val)
            val = self.create_filter(
                DocumentOrm.status,
                split_numeric(val),
                use_and
            )
            if val is not None:
                self._has_params = True
                self._status_stmt = val
                container.append(val)
        
        # reason
        if self._reason_stmt is not None:
            container.append(self._reason_stmt)
        else:
            val = self._reason
            val = self.create_filter(
                DocumentOrm.reason,
                [(x, 'like') for x in split_terms(val, RE_ALPHANUM_TERM, '%', '%')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._reason_stmt = val
                container.append(val)

        # Return True if conditions were added
        return self._has_params
    

    # Getters for raw values

    @property
    def id(self) -> int | str | None:
        return self._id
    
    @property
    def uuid(self) -> str | None:
        return self._uuid
    
    @property
    def created_at(self) -> int | float | str | None:
        return self._created_at
    
    @property
    def modified_at(self) -> int | float | str | None:
        return self._modified_at
    
    @property
    def owner_id(self) -> int | str | None:
        return self._owner_id
    
    @property
    def type(self) -> int | str | None:
        return self._type
    
    @property
    def status(self) -> int | str | None:
        return self._status
    
    @property
    def reason(self) -> str | None:
        return self._reason
    

    # Getters for statements
    
    @property
    def id_stmt(self) -> typing.Any:
        return self._id_stmt
    
    @property
    def uuid_stmt(self) -> typing.Any:
        return self._uuid_stmt
    
    @property
    def created_at_stmt(self) -> typing.Any:
        return self._created_at_stmt
    
    @property
    def modified_at_stmt(self) -> typing.Any:
        return self._modified_at_stmt
    
    @property
    def owner_id_stmt(self) -> typing.Any:
        return self._owner_id_stmt
    
    @property
    def type_stmt(self) -> typing.Any:
        return self._type_stmt
    
    @property
    def status_stmt(self) -> typing.Any:
        return self._status_stmt
    
    @property
    def reason_stmt(self) -> typing.Any:
        return self._reason_stmt



### Define Interfaces

class DocumentQueryParamsPrimary(DocumentQueryParams):
    def __init__(self,
        id: int | str | None = None,
        uuid: str | None = None,
        created_at: int | float | str | None = None,
        modified_at: int | float | str | None = None,
        owner: int | str | None = None,
        owner_id: int | str | None = None,
        type: int | str | None = None,
        status: int | str | None = None,
        reason: str | None = None
    ):
        super().__init__(
            id,
            uuid,
            created_at,
            modified_at,
            owner_id if owner_id is not None else owner,
            type,
            status,
            reason
        )


class DocumentQueryParamsGeneral(DocumentQueryParams):
    def __init__(self,
        document_id: int | str | None = None,
        document_uuid: str | None = None,
        document_created_at: int | float | str | None = None,
        document_modified_at: int | float | str | None = None,
        document_owner: int | str | None = None,
        document_owner_id: int | str | None = None,
        document_type: int | str | None = None,
        document_status: int | str | None = None,
        document_reason: str | None = None
    ):
        super().__init__(
            document_id,
            document_uuid,
            document_created_at,
            document_modified_at,
            document_owner_id if document_owner_id is not None else document_owner,
            document_type,
            document_status,
            document_reason
        )


