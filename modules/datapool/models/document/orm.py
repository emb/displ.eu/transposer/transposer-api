# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import typing
import datetime

import sqlalchemy
from sqlalchemy.orm import (
    Mapped,
    mapped_column,
    relationship,
    validates
)

from regex_patterns import (
    RE_INT_VALUE,
    RE_UUID4,
    RE_ALPHANUM_TERM
)
from data_utils import (
    is_empty, is_true,
    split_terms,
    split_numeric,
    split_datetime,
    sanitize_text
)
from ..base import BaseOrm
from ..links import request_document_link_table
from ..links import job_document_link_table





class DocumentOrm(BaseOrm):
    __tablename__ = 'documents'
    id: Mapped[int] = mapped_column(
        sqlalchemy.Integer, primary_key=True
    )
    uuid: Mapped[str] = mapped_column(sqlalchemy.String)
    created_at: Mapped[datetime.datetime] = mapped_column(
        sqlalchemy.DateTime, default=sqlalchemy.sql.functions.now()
    )
    modified_at: Mapped[datetime.datetime] = mapped_column(
        sqlalchemy.DateTime, default=sqlalchemy.sql.functions.now(), onupdate=sqlalchemy.sql.functions.now()
    )
    owner_id: Mapped[int] = mapped_column(sqlalchemy.ForeignKey('users.id'))
    type: Mapped[int] = mapped_column(sqlalchemy.Integer, default=0)
    status: Mapped[int] = mapped_column(sqlalchemy.Integer, default=0)
    reason: Mapped[typing.Optional[str]] = mapped_column(sqlalchemy.String, default=None, nullable=True)

    owner: Mapped['UserOrm'] = relationship(
        back_populates='documents'
    )
    contents: Mapped[typing.List['ContentOrm']] = relationship(
        back_populates='document',
        primaryjoin="(DocumentOrm.id==ContentOrm.document_id)"
    )
    medias: Mapped[typing.List['MediaOrm']] = relationship(
        back_populates='document',
        primaryjoin="(DocumentOrm.id==MediaOrm.document_id)"
    )
    requests: Mapped[typing.List['RequestOrm']] = relationship(
        'RequestOrm',
        secondary=request_document_link_table,
        back_populates='documents'
    )
    jobs: Mapped[typing.List['JobOrm']] = relationship(
        'JobOrm',
        secondary=job_document_link_table,
        back_populates='documents'
    )
    workflows: Mapped[typing.List['WorkflowOrm']] = relationship(
        back_populates='document',
        primaryjoin="(DocumentOrm.id==WorkflowOrm.document_id)"
    )
    
    def __repr__(self):
        return (
            f'<Document('
            f'id={self.id}, '
            f'uuid={self.uuid}, '
            f'owner_id={self.owner_id}, '
            f'type={self.type}, '
            f'status={self.status}, '
            f'reason={self.reason})>'
        )
    
    def __str__(self):
        return (
            f'Document('
            f'id={self.id}, '
            f'uuid={self.uuid}, '
            f'owner_id={self.owner_id}, '
            f'type={self.type}, '
            f'status={self.status}, '
            f'reason={self.reason})'
        )
    
    def __json__(self):
        return {
            'id': self.id,
            'uuid': self.uuid,
            'created_at': self.created_at,
            'modified_at': self.modified_at,
            'owner_id': self.owner_id,
            'type': self.type,
            'status': self.status,
            'reason': self.reason
        }
    
    def _fields(self) -> tuple:
        return (
            ['id', 'uuid', 'created_at', 'modified_at', 'owner_id', 'type', 'status', 'reason'],
            [
                ('owner', False, None),
                ('contents', True, None),
                ('medias', True, None),
                ('requests', True, None),
                ('jobs', True, None),
                ('workflows', True, None)
            ]
        )
    

    @validates('uuid')
    def validate_uuid(self, key, value):
        if is_empty(value):
            raise ValueError("UUID is required")
        if isinstance(value, str):
            result = RE_UUID4.match(value)
            if result is None:
                raise ValueError("Invalid UUID")
            return result.group(1)
        raise ValueError("Invalid UUID")

    @validates('owner_id')
    def validate_owner_id(self, key, value):
        if is_empty(value):
            raise ValueError("Owner ID is required")
        if isinstance(value, int):
            return value
        if isinstance(value, str):
            result = RE_INT_VALUE.match(value)
            if result is None:
                raise ValueError("Invalid owner ID")
            return int(result.group(1))
        raise ValueError("Invalid owner ID")

    @validates('type')
    def validate_type(self, key, value):
        if is_empty(value):
            raise ValueError("Type is required")
        if isinstance(value, int):
            return value
        if isinstance(value, str):
            result = RE_INT_VALUE.match(value)
            if result is None:
                raise ValueError("Invalid type")
            return int(result.group(1))
        raise ValueError("Invalid type")

    @validates('status')
    def validate_status(self, key, value):
        if is_empty(value):
            raise ValueError("Status is required")
        if isinstance(value, int):
            return value
        if isinstance(value, str):
            result = RE_INT_VALUE.match(value)
            if result is None:
                raise ValueError("Invalid status")
            return int(result.group(1))
        raise ValueError("Invalid status")

    @validates('reason')
    def validate_reason(self, key, value):
        return sanitize_text(value)




