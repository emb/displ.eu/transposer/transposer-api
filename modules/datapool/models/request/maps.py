# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import re
import typing
from .orm import RequestOrm





REQUEST__FIELDS_MAP = {
    'request': {
        'id': RequestOrm.id,
        'created_at': RequestOrm.created_at,
        'owner': RequestOrm.owner_id,
        'owner_id': RequestOrm.owner_id,
        'peer_args': RequestOrm.peer_args,
        'is_async': RequestOrm.is_async,
        'status': RequestOrm.status,
        'reason': RequestOrm.reason
    },
    'request_id': RequestOrm.id,
    'request_created_at': RequestOrm.created_at,
    'request_owner': RequestOrm.owner_id,
    'request_owner_id': RequestOrm.owner_id,
    'request_peer_args': RequestOrm.peer_args,
    'request_is_async': RequestOrm.is_async,
    'request_status': RequestOrm.status,
    'request_reason': RequestOrm.reason,

    'request.id': RequestOrm.id,
    'request.created_at': RequestOrm.created_at,
    'request.owner': RequestOrm.owner_id,
    'request.owner_id': RequestOrm.owner_id,
    'request.peer_args': RequestOrm.peer_args,
    'request.is_async': RequestOrm.is_async,
    'request.status': RequestOrm.status,
    'request.reason': RequestOrm.reason
}

REQUEST__VARIANTS_MAP = {
    'request': {
        'id': 'request.id',
        'created_at': 'request.created_at',
        'owner': 'request.owner_id',
        'owner_id': 'request.owner_id',
        'peer_args': 'request.peer_args',
        'is_async': 'request.is_async',
        'status': 'request.status',
        'reason': 'request.reason'
    },
    'request_id': 'request.id',
    'request_created_at': 'request.created_at',
    'request_owner': 'request.owner_id',
    'request_owner_id': 'request.owner_id',
    'request_peer_args': 'request.peer_args',
    'request_is_async': 'request.is_async',
    'request_status': 'request.status',
    'request_reason': 'request.reason',

    'request.id': 'request.id',
    'request.created_at': 'request.created_at',
    'request.owner': 'request.owner_id',
    'request.owner_id': 'request.owner_id',
    'request.peer_args': 'request.peer_args',
    'request.is_async': 'request.is_async',
    'request.status': 'request.status',
    'request.reason': 'request.reason'
}



############
### Primary

REQUEST__PRIMARY_FIELDS_MAP = {
    'id': RequestOrm.id,
    'created_at': RequestOrm.created_at,
    'owner': RequestOrm.owner_id,
    'owner_id': RequestOrm.owner_id,
    'peer_args': RequestOrm.peer_args,
    'is_async': RequestOrm.is_async,
    'status': RequestOrm.status,
    'reason': RequestOrm.reason
}

REQUEST__PRIMARY_VARIANTS_MAP = {
    'id': 'request.id',
    'created_at': 'request.created_at',
    'owner': 'request.owner_id',
    'owner_id': 'request.owner_id',
    'peer_args': 'request.peer_args',
    'is_async': 'request.is_async',
    'status': 'request.status',
    'reason': 'request.reason'
}


