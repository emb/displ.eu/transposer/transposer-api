# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import typing
import datetime

from regex_patterns import (
    RE_INT_VALUE,
    RE_ALPHANUM_TERM
)
from data_utils import (
    split_terms,
    split_numeric,
    split_datetime
)
from ..base import (
    BaseQueryParams
)
from .orm import RequestOrm





class RequestQueryParams(BaseQueryParams):
    __slots__ = [
        '_id',
        '_created_at',
        '_owner_id',
        '_peer_args',
        '_is_async',
        '_status',
        '_reason',

        '_id_stmt',
        '_created_at_stmt',
        '_owner_id_stmt',
        '_peer_args_stmt',
        '_is_async_stmt',
        '_status_stmt',
        '_reason_stmt'
    ]

    def __init__(self, 
        id: int | str | None = None,
        created_at: int | float | str | None = None,
        owner_id: int | str | None = None,
        peer_args: str | dict | None = None,
        is_async: bool | None = None,
        status: int | str | None = None,
        reason: str | None = None
    ):
        super().__init__()

        # Store raw values
        self._id = id
        self._created_at = created_at
        self._owner_id = owner_id
        self._peer_args = peer_args
        self._is_async = is_async
        self._status = status
        self._reason = reason

        # Init statement values
        self._id_stmt = None
        self._created_at_stmt = None
        self._owner_id_stmt = None
        self._peer_args_stmt = None
        self._is_async_stmt = None
        self._status_stmt = None
        self._reason_stmt = None


    def add_conditions(self, container: list, use_and: bool = False) -> bool:

        # id
        if self._id_stmt is not None:
            container.append(self._id_stmt)
        else:
            val = self._id
            if isinstance(val, int):
                val = str(val)
            val = self.create_filter(
                RequestOrm.id,
                [(x, '==') for x in split_terms(val, RE_INT_VALUE, '', '')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._id_stmt = val
                container.append(val)
        
        # created_at
        if self._created_at_stmt is not None:
            container.append(self._created_at_stmt)
        else:
            val = self._created_at
            val = self.create_filter(
                RequestOrm.created_at,
                split_datetime(val),
                use_and
            )
            if val is not None:
                self._has_params = True
                self._created_at_stmt = val
                container.append(val)

        # owner_id
        if self._owner_id_stmt is not None:
            container.append(self._owner_id_stmt)
        else:
            val = self._owner_id
            if isinstance(val, int):
                val = str(val)
            val = self.create_filter(
                RequestOrm.owner_id,
                [(x, '==') for x in split_terms(val, RE_INT_VALUE, '', '')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._owner_id_stmt = val
                container.append(val)

        # peer_args
        if self._peer_args_stmt is not None:
            container.append(self._peer_args_stmt)
        else:
            val = self._peer_args
            val = self.create_filter(
                RequestOrm.peer_args,
                split_numeric(val),
                use_and
            )
            if val is not None:
                self._has_params = True
                self._peer_args_stmt = val
                container.append(val)
        
        # is_async
        if self._is_async_stmt is not None:
            container.append(self._is_async_stmt)
        else:
            val = self._is_async
            if val is not None:
                val = self.create_filter(
                    RequestOrm.is_async,
                    [(val, '==')],
                    use_and
                )
                if val is not None:
                    self._has_params = True
                    self._is_async_stmt = val
                    container.append(val)

        # status
        if self._status_stmt is not None:
            container.append(self._status_stmt)
        else:
            val = self._status
            if isinstance(val, int):
                val = str(val)
            val = self.create_filter(
                RequestOrm.status,
                split_numeric(val),
                use_and
            )
            if val is not None:
                self._has_params = True
                self._status_stmt = val
                container.append(val)
        
        # reason
        if self._reason_stmt is not None:
            container.append(self._reason_stmt)
        else:
            val = self._reason
            val = self.create_filter(
                RequestOrm.reason,
                [(x, 'like') for x in split_terms(val, RE_ALPHANUM_TERM, '%', '%')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._reason_stmt = val
                container.append(val)

        # Return True if conditions were added
        return self._has_params
    

    # Getters for raw values

    @property
    def id(self) -> int | str | None:
        return self._id
    
    @property
    def created_at(self) -> int | float | str | None:
        return self._created_at
    
    @property
    def owner_id(self) -> int | str | None:
        return self._owner_id
    
    @property
    def peer_args(self) -> str | dict | None:
        return self._peer_args
    
    @property
    def is_async(self) -> bool | None:
        return self._is_async
    
    @property
    def status(self) -> int | str | None:
        return self._status
    
    @property
    def reason(self) -> str | None:
        return self._reason
    

    # Getters for statements
    
    @property
    def id_stmt(self) -> typing.Any:
        return self._id_stmt
    
    @property
    def created_at_stmt(self) -> typing.Any:
        return self._created_at_stmt
    
    @property
    def owner_id_stmt(self) -> typing.Any:
        return self._owner_id_stmt
    
    @property
    def peer_args_stmt(self) -> typing.Any:
        return self._peer_args_stmt
    
    @property
    def is_async_stmt(self) -> typing.Any:
        return self._is_async_stmt
    
    @property
    def status_stmt(self) -> typing.Any:
        return self._status_stmt
    
    @property
    def reason_stmt(self) -> typing.Any:
        return self._reason_stmt



### Define Interfaces

class RequestQueryParamsPrimary(RequestQueryParams):
    def __init__(self,
        id: int | str | None = None,
        created_at: int | float | str | None = None,
        owner: int | str | None = None,
        owner_id: int | str | None = None,
        peer_args: str | dict | None = None,
        is_async: str | None = None,
        status: int | str | None = None,
        reason: str | None = None
    ):
        super().__init__(
            id,
            created_at,
            owner_id if owner_id is not None else owner,
            peer_args,
            is_async,
            status,
            reason
        )


class RequestQueryParamsGeneral(RequestQueryParams):
    def __init__(self,
        request_id: int | str | None = None,
        request_created_at: int | float | str | None = None,
        request_owner: int | str | None = None,
        request_owner_id: int | str | None = None,
        request_peer_args: str | dict | None = None,
        request_is_async: str | None = None,
        request_status: int | str | None = None,
        request_reason: str | None = None
    ):
        super().__init__(
            request_id,
            request_created_at,
            request_owner_id if request_owner_id is not None else request_owner,
            request_peer_args,
            request_is_async,
            request_status,
            request_reason
        )


