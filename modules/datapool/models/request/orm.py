# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import typing
import datetime

import sqlalchemy
from sqlalchemy.orm import (
    Mapped,
    mapped_column,
    relationship,
    validates
)
from sqlalchemy.dialects.postgresql import JSONB

from regex_patterns import (
    RE_INT_VALUE,
    RE_UUID4,
    RE_ALPHANUM_TERM,
    RE_HASH_XX
)
from data_utils import (
    is_empty, is_true,
    split_terms,
    split_numeric,
    split_datetime,
    sanitize_text,
    validate_uri
)
from ..base import BaseOrm
from ..links import (
    request_document_link_table,
    request_connection_link_table
)





class RequestOrm(BaseOrm):
    __tablename__ = 'requests'
    id: Mapped[int] = mapped_column(
        sqlalchemy.Integer, primary_key=True
    )
    created_at: Mapped[datetime.datetime] = mapped_column(
        sqlalchemy.DateTime, default=sqlalchemy.sql.functions.now()
    )
    owner_id: Mapped[int] = mapped_column(sqlalchemy.ForeignKey('users.id'))
    peer_args: Mapped[typing.Optional[JSONB]] = mapped_column(type_=JSONB, default='{}')
    is_async: Mapped[bool] = mapped_column(sqlalchemy.Boolean, default=False)
    status: Mapped[int] = mapped_column(sqlalchemy.Integer, default=0)
    reason: Mapped[typing.Optional[str]] = mapped_column(sqlalchemy.String)

    owner: Mapped['UserOrm'] = relationship(back_populates='requests')
    documents: Mapped[typing.List['DocumentOrm']] = relationship(
        'DocumentOrm',
        secondary=request_document_link_table,
        back_populates='requests'
    )
    connections: Mapped[typing.List['PeerConnectionOrm']] = relationship(
        'PeerConnectionOrm',
        secondary=request_connection_link_table,
        back_populates='requests'
    )
    
    def __repr__(self):
        return (
            f'<Request('
            f'id={self.id}, '
            f'owner_id={self.owner_id}, '
            f'peer_args={self.peer_args}, '
            f'is_async={self.is_async}, '
            f'status={self.status}, '
            f'reason={self.reason})>'
        )
    
    def __str__(self):
        return (
            f'Request('
            f'id={self.id}, '
            f'owner_id={self.owner_id}, '
            f'peer_args={self.peer_args}, '
            f'is_async={self.is_async}, '
            f'status={self.status}, '
            f'reason={self.reason})'
        )
    
    def __json__(self):
        return {
            'id': self.id,
            'created_at': self.created_at,
            'owner_id': self.owner_id,
            'peer_args': self.peer_args,
            'is_async': self.is_async,
            'status': self.status,
            'reason': self.reason
        }
    
    def _fields(self) -> tuple:
        return (
            ['id', 'created_at', 'owner_id', 'peer_args', 'is_async', 'status', 'reason'],
            [('owner', False, None), ('documents', True, None), ('connections', True, None)]
        )
    

    @validates('owner_id')
    def validate_owner_id(self, key, value):
        if is_empty(value):
            raise ValueError("Owner ID is required")
        if isinstance(value, int):
            return value
        if isinstance(value, str):
            result = RE_INT_VALUE.match(value)
            if result is None:
                raise ValueError("Invalid owner ID")
            return int(result.group(1))
        raise ValueError("Invalid owner ID")

    @validates('peer_args')
    def validate_peer_args(self, key, value):
        if is_empty(value):
            return None
        if isinstance(value, str):
            result = RE_HASH_XX.match(value)
            if result is None:
                raise ValueError("Invalid peer args")
            return result.group(1)
        raise ValueError("Invalid peer args")

    @validates('is_async')
    def validate_is_async(self, key, value):
        return is_true(value)

    @validates('status')
    def validate_status(self, key, value):
        if is_empty(value):
            raise ValueError("Status is required")
        if isinstance(value, int):
            return value
        if isinstance(value, str):
            result = RE_INT_VALUE.match(value)
            if result is None:
                raise ValueError("Invalid status")
            return int(result.group(1))
        raise ValueError("Invalid status")

    @validates('reason')
    def validate_reason(self, key, value):
        return sanitize_text(value)



