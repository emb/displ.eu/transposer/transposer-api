# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import re
import typing
from .orm import TranscriptionSegmentOrm





TRANSCRIPTION_SEGMENT__FIELDS_MAP = {
    'transcription_segment': {
        'id': TranscriptionSegmentOrm.id,
        'transcription': TranscriptionSegmentOrm.transcription_id,
        'transcription_id': TranscriptionSegmentOrm.transcription_id,
        'pos': TranscriptionSegmentOrm.pos,
        'seek': TranscriptionSegmentOrm.seek,
        'start': TranscriptionSegmentOrm.start,
        'end': TranscriptionSegmentOrm.end,
        'text': TranscriptionSegmentOrm.text,
        'temperature': TranscriptionSegmentOrm.temperature,
        'avg_logprob': TranscriptionSegmentOrm.avg_logprob,
        'compression_ratio': TranscriptionSegmentOrm.compression_ratio,
        'no_speech_prob': TranscriptionSegmentOrm.no_speech_prob,
        'confidence': TranscriptionSegmentOrm.confidence
    },
    'transcription_segment_id': TranscriptionSegmentOrm.id,
    'transcription_segment_transcription': TranscriptionSegmentOrm.transcription_id,
    'transcription_segment_transcription_id': TranscriptionSegmentOrm.transcription_id,
    'transcription_segment_pos': TranscriptionSegmentOrm.pos,
    'transcription_segment_seek': TranscriptionSegmentOrm.seek,
    'transcription_segment_start': TranscriptionSegmentOrm.start,
    'transcription_segment_end': TranscriptionSegmentOrm.end,
    'transcription_segment_text': TranscriptionSegmentOrm.text,
    'transcription_segment_temperature': TranscriptionSegmentOrm.temperature,
    'transcription_segment_avg_logprob': TranscriptionSegmentOrm.avg_logprob,
    'transcription_segment_compression_ratio': TranscriptionSegmentOrm.compression_ratio,
    'transcription_segment_no_speech_prob': TranscriptionSegmentOrm.no_speech_prob,
    'transcription_segment_confidence': TranscriptionSegmentOrm.confidence,

    'transcription_segment.id': TranscriptionSegmentOrm.id,
    'transcription_segment.transcription': TranscriptionSegmentOrm.transcription_id,
    'transcription_segment.transcription_id': TranscriptionSegmentOrm.transcription_id,
    'transcription_segment.pos': TranscriptionSegmentOrm.pos,
    'transcription_segment.seek': TranscriptionSegmentOrm.seek,
    'transcription_segment.start': TranscriptionSegmentOrm.start,
    'transcription_segment.end': TranscriptionSegmentOrm.end,
    'transcription_segment.text': TranscriptionSegmentOrm.text,
    'transcription_segment.temperature': TranscriptionSegmentOrm.temperature,
    'transcription_segment.avg_logprob': TranscriptionSegmentOrm.avg_logprob,
    'transcription_segment.compression_ratio': TranscriptionSegmentOrm.compression_ratio,
    'transcription_segment.no_speech_prob': TranscriptionSegmentOrm.no_speech_prob,
    'transcription_segment.confidence': TranscriptionSegmentOrm.confidence
}

TRANSCRIPTION_SEGMENT__VARIANTS_MAP = {
    'transcription_segment': {
        'id': 'transcription_segment.id',
        'transcription': 'transcription_segment.transcription_id',
        'transcription_id': 'transcription_segment.transcription_id',
        'pos': 'transcription_segment.pos',
        'seek': 'transcription_segment.seek',
        'start': 'transcription_segment.start',
        'end': 'transcription_segment.end',
        'text': 'transcription_segment.text',
        'temperature': 'transcription_segment.temperature',
        'avg_logprob': 'transcription_segment.avg_logprob',
        'compression_ratio': 'transcription_segment.compression_ratio',
        'no_speech_prob': 'transcription_segment.no_speech_prob',
        'confidence': 'transcription_segment.confidence'
    },
    'transcription_segment_id': 'transcription_segment.id',
    'transcription_segment_transcription': 'transcription_segment.transcription_id',
    'transcription_segment_transcription_id': 'transcription_segment.transcription_id',
    'transcription_segment_pos': 'transcription_segment.pos',
    'transcription_segment_seek': 'transcription_segment.seek',
    'transcription_segment_start': 'transcription_segment.start',
    'transcription_segment_end': 'transcription_segment.end',
    'transcription_segment_text': 'transcription_segment.text',
    'transcription_segment_temperature': 'transcription_segment.temperature',
    'transcription_segment_avg_logprob': 'transcription_segment.avg_logprob',
    'transcription_segment_compression_ratio': 'transcription_segment.compression_ratio',
    'transcription_segment_no_speech_prob': 'transcription_segment.no_speech_prob',
    'transcription_segment_confidence': 'transcription_segment.confidence',

    'transcription_segment.id': 'transcription_segment.id',
    'transcription_segment.transcription': 'transcription_segment.transcription_id',
    'transcription_segment.transcription_id': 'transcription_segment.transcription_id',
    'transcription_segment.pos': 'transcription_segment.pos',
    'transcription_segment.seek': 'transcription_segment.seek',
    'transcription_segment.start': 'transcription_segment.start',
    'transcription_segment.end': 'transcription_segment.end',
    'transcription_segment.text': 'transcription_segment.text',
    'transcription_segment.temperature': 'transcription_segment.temperature',
    'transcription_segment.avg_logprob': 'transcription_segment.avg_logprob',
    'transcription_segment.compression_ratio': 'transcription_segment.compression_ratio',
    'transcription_segment.no_speech_prob': 'transcription_segment.no_speech_prob',
    'transcription_segment.confidence': 'transcription_segment.confidence'
}



############
### Primary

TRANSCRIPTION_SEGMENT__PRIMARY_FIELDS_MAP = {
    'id': TranscriptionSegmentOrm.id,
    'transcription': TranscriptionSegmentOrm.transcription_id,
    'transcription_id': TranscriptionSegmentOrm.transcription_id,
    'pos': TranscriptionSegmentOrm.pos,
    'seek': TranscriptionSegmentOrm.seek,
    'start': TranscriptionSegmentOrm.start,
    'end': TranscriptionSegmentOrm.end,
    'text': TranscriptionSegmentOrm.text,
    'temperature': TranscriptionSegmentOrm.temperature,
    'avg_logprob': TranscriptionSegmentOrm.avg_logprob,
    'compression_ratio': TranscriptionSegmentOrm.compression_ratio,
    'no_speech_prob': TranscriptionSegmentOrm.no_speech_prob,
    'confidence': TranscriptionSegmentOrm.confidence
}

TRANSCRIPTION_SEGMENT__PRIMARY_VARIANTS_MAP = {
    'id': 'transcription_segment.id',
    'transcription': 'transcription_segment.transcription_id',
    'transcription_id': 'transcription_segment.transcription_id',
    'pos': 'transcription_segment.pos',
    'seek': 'transcription_segment.seek',
    'start': 'transcription_segment.start',
    'end': 'transcription_segment.end',
    'text': 'transcription_segment.text',
    'temperature': 'transcription_segment.temperature',
    'avg_logprob': 'transcription_segment.avg_logprob',
    'compression_ratio': 'transcription_segment.compression_ratio',
    'no_speech_prob': 'transcription_segment.no_speech_prob',
    'confidence': 'transcription_segment.confidence'
}


