# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import typing

from regex_patterns import (
    RE_INT_VALUE,
    RE_ALPHANUM_TERM
)
from data_utils import (
    split_terms,
    split_numeric
)
from ..base import BaseQueryParams
from .orm import TranscriptionSegmentOrm





class TranscriptionSegmentQueryParams(BaseQueryParams):
    __slots__ = [
        '_id',
        '_transcription_id',
        '_pos',
        '_seek',
        '_start',
        '_end',
        '_text',
        '_temperature',
        '_avg_logprob',
        '_compression_ratio',
        '_no_speech_prob',
        '_confidence',

        '_id_stmt',
        '_transcription_id_stmt',
        '_pos_stmt',
        '_seek_stmt',
        '_start_stmt',
        '_end_stmt',
        '_text_stmt',
        '_temperature_stmt',
        '_avg_logprob_stmt',
        '_compression_ratio_stmt',
        '_no_speech_prob_stmt',
        '_confidence_stmt'
    ]

    def __init__(self, 
        id: int | str | None = None,
        transcription_id: int | str | None = None,
        pos: int | str | None = None,
        seek: int | float | str | None = None,
        start: int | float | str | None = None,
        end: int | float | str | None = None,
        text: str | None = None,
        temperature: int | float | str | None = None,
        avg_logprob: int | float | str | None = None,
        compression_ratio: int | float | str | None = None,
        no_speech_prob: int | float | str | None = None,
        confidence: int | float | str | None = None
    ):
        super().__init__()

        # Store raw values
        self._id = id
        self._transcription_id = transcription_id
        self._pos = pos
        self._seek = seek
        self._start = start
        self._end = end
        self._text = text
        self._temperature = temperature
        self._avg_logprob = avg_logprob
        self._compression_ratio = compression_ratio
        self._no_speech_prob = no_speech_prob
        self._confidence = confidence

        # Init statement values
        self._id_stmt = None
        self._transcription_id_stmt = None
        self._pos_stmt = None
        self._seek_stmt = None
        self._start_stmt = None
        self._end_stmt = None
        self._text_stmt = None
        self._temperature_stmt = None
        self._avg_logprob_stmt = None
        self._compression_ratio_stmt = None
        self._no_speech_prob_stmt = None
        self._confidence_stmt = None


    def add_conditions(self, container: list, use_and: bool = False) -> bool:

        # id
        if self._id_stmt is not None:
            container.append(self._id_stmt)
        else:
            val = self._id
            if isinstance(val, int):
                val = str(val)
            val = self.create_filter(
                TranscriptionSegmentOrm.id,
                [(x, '==') for x in split_terms(val, RE_INT_VALUE, '', '')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._id_stmt = val
                container.append(val)

        # transcription_id
        if self._transcription_id_stmt is not None:
            container.append(self._transcription_id_stmt)
        else:
            val = self._transcription_id
            if isinstance(val, int):
                val = str(val)
            val = self.create_filter(
                TranscriptionSegmentOrm.transcription_id,
                [(x, '==') for x in split_terms(val, RE_INT_VALUE, '', '')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._transcription_id_stmt = val
                container.append(val)
        
        # pos
        if self._pos_stmt is not None:
            container.append(self._pos_stmt)
        else:
            val = self._pos
            if isinstance(val, int):
                val = str(val)
            val = self.create_filter(
                TranscriptionSegmentOrm.pos,
                split_numeric(val),
                use_and
            )
            if val is not None:
                self._has_params = True
                self._pos_stmt = val
                container.append(val)
        
        # seek
        if self._seek_stmt is not None:
            container.append(self._seek_stmt)
        else:
            val = self._seek
            if isinstance(val, (int, float)):
                val = str(val)
            val = self.create_filter(
                TranscriptionSegmentOrm.seek,
                split_numeric(val),
                use_and
            )
            if val is not None:
                self._has_params = True
                self._seek_stmt = val
                container.append(val)
        
        # start
        if self._start_stmt is not None:
            container.append(self._start_stmt)
        else:
            val = self._start
            if isinstance(val, (int, float)):
                val = str(val)
            val = self.create_filter(
                TranscriptionSegmentOrm.start,
                split_numeric(val),
                use_and
            )
            if val is not None:
                self._has_params = True
                self._start_stmt = val
                container.append(val)
        
        # end
        if self._end_stmt is not None:
            container.append(self._end_stmt)
        else:
            val = self._end
            if isinstance(val, (int, float)):
                val = str(val)
            val = self.create_filter(
                TranscriptionSegmentOrm.end,
                split_numeric(val),
                use_and
            )
            if val is not None:
                self._has_params = True
                self._end_stmt = val
                container.append(val)
        
        # text
        if self._text_stmt is not None:
            container.append(self._text_stmt)
        else:
            val = self._text
            val = self.create_filter(
                TranscriptionSegmentOrm.text,
                [(x, 'like') for x in split_terms(val, RE_ALPHANUM_TERM, '%', '%')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._text_stmt = val
                container.append(val)
        
        # temperature
        if self._temperature_stmt is not None:
            container.append(self._temperature_stmt)
        else:
            val = self._temperature
            if isinstance(val, (int, float)):
                val = str(val)
            val = self.create_filter(
                TranscriptionSegmentOrm.temperature,
                split_numeric(val),
                use_and
            )
            if val is not None:
                self._has_params = True
                self._temperature_stmt = val
                container.append(val)
        
        # avg_logprob
        if self._avg_logprob_stmt is not None:
            container.append(self._avg_logprob_stmt)
        else:
            val = self._avg_logprob
            if isinstance(val, (int, float)):
                val = str(val)
            val = self.create_filter(
                TranscriptionSegmentOrm.avg_logprob,
                split_numeric(val),
                use_and
            )
            if val is not None:
                self._has_params = True
                self._avg_logprob_stmt = val
                container.append(val)
        
        # compression_ratio
        if self._compression_ratio_stmt is not None:
            container.append(self._compression_ratio_stmt)
        else:
            val = self._compression_ratio
            if isinstance(val, (int, float)):
                val = str(val)
            val = self.create_filter(
                TranscriptionSegmentOrm.compression_ratio,
                split_numeric(val),
                use_and
            )
            if val is not None:
                self._has_params = True
                self._compression_ratio_stmt = val
                container.append(val)
        
        # no_speech_prob
        if self._no_speech_prob_stmt is not None:
            container.append(self._no_speech_prob_stmt)
        else:
            val = self._no_speech_prob
            if isinstance(val, (int, float)):
                val = str(val)
            val = self.create_filter(
                TranscriptionSegmentOrm.no_speech_prob,
                split_numeric(val),
                use_and
            )
            if val is not None:
                self._has_params = True
                self._no_speech_prob_stmt = val
                container.append(val)
        
        # confidence
        if self._confidence_stmt is not None:
            container.append(self._confidence_stmt)
        else:
            val = self._confidence
            if isinstance(val, (int, float)):
                val = str(val)
            val = self.create_filter(
                TranscriptionSegmentOrm.confidence,
                split_numeric(val),
                use_and
            )
            if val is not None:
                self._has_params = True
                self._confidence_stmt = val
                container.append(val)

        # Return True if conditions were added
        return self._has_params
    

    # Getters for raw values

    @property
    def id(self) -> int | str | None:
        return self._id
    
    @property
    def transcription_id(self) -> int | str | None:
        return self._transcription_id
    
    @property
    def pos(self) -> int | str | None:
        return self._pos
    
    @property
    def seek(self) -> int | float | str | None:
        return self._seek
    
    @property
    def start(self) -> int | float | str | None:
        return self._start
    
    @property
    def end(self) -> int | float | str | None:
        return self._end
    
    @property
    def text(self) -> str | None:
        return self._text
    
    @property
    def temperature(self) -> int | float | str | None:
        return self._temperature
    
    @property
    def avg_logprob(self) -> int | float | str | None:
        return self._avg_logprob
    
    @property
    def compression_ratio(self) -> int | float | str | None:
        return self._compression_ratio
    
    @property
    def no_speech_prob(self) -> int | float | str | None:
        return self._no_speech_prob
    
    @property
    def confidence(self) -> int | float | str | None:
        return self._confidence


    # Getters for statements
    
    @property
    def id_stmt(self) -> typing.Any:
        return self._id_stmt
    
    @property
    def transcription_id_stmt(self) -> typing.Any:
        return self._transcription_id_stmt
    
    @property
    def pos_stmt(self) -> typing.Any:
        return self._pos_stmt
    
    @property
    def seek_stmt(self) -> typing.Any:
        return self._seek_stmt
    
    @property
    def start_stmt(self) -> typing.Any:
        return self._start_stmt
    
    @property
    def end_stmt(self) -> typing.Any:
        return self._end_stmt
    
    @property
    def text_stmt(self) -> typing.Any:
        return self._text_stmt
    
    @property
    def temperature_stmt(self) -> typing.Any:
        return self._temperature_stmt
    
    @property
    def avg_logprob_stmt(self) -> typing.Any:
        return self._avg_logprob_stmt
    
    @property
    def compression_ratio_stmt(self) -> typing.Any:
        return self._compression_ratio_stmt
    
    @property
    def no_speech_prob_stmt(self) -> typing.Any:
        return self._no_speech_prob_stmt
    
    @property
    def confidence_stmt(self) -> typing.Any:
        return self._confidence_stmt



### Define Interfaces

class TranscriptionSegmentQueryParamsPrimary(TranscriptionSegmentQueryParams):
    def __init__(self,
        id: int | str | None = None,
        transcription: int | str | None = None,
        transcription_id: int | str | None = None,
        pos: int | str | None = None,
        seek: int | float | str | None = None,
        start: int | float | str | None = None,
        end: int | float | str | None = None,
        text: str | None = None,
        temperature: int | float | str | None = None,
        avg_logprob: int | float | str | None = None,
        compression_ratio: int | float | str | None = None,
        no_speech_prob: int | float | str | None = None,
        confidence: int | float | str | None = None
    ):
        super().__init__(
            id,
            transcription_id if transcription_id is not None else transcription,
            pos,
            seek,
            start,
            end,
            text,
            temperature,
            avg_logprob,
            compression_ratio,
            no_speech_prob,
            confidence
        )


class TranscriptionSegmentQueryParamsGeneral(TranscriptionSegmentQueryParams):
    def __init__(self,
        transcription_segment_id: int | str | None = None,
        transcription_segment_transcription: int | str | None = None,
        transcription_segment_transcription_id: int | str | None = None,
        transcription_segment_pos: int | str | None = None,
        transcription_segment_seek: int | float | str | None = None,
        transcription_segment_start: int | float | str | None = None,
        transcription_segment_end: int | float | str | None = None,
        transcription_segment_text: str | None = None,
        transcription_segment_temperature: int | float | str | None = None,
        transcription_segment_avg_logprob: int | float | str | None = None,
        transcription_segment_compression_ratio: int | float | str | None = None,
        transcription_segment_no_speech_prob: int | float | str | None = None,
        transcription_segment_confidence: int | float | str | None = None
    ):
        super().__init__(
            transcription_segment_id,
            transcription_segment_transcription_id if transcription_segment_transcription_id is not None else transcription_segment_transcription,
            transcription_segment_pos,
            transcription_segment_seek,
            transcription_segment_start,
            transcription_segment_end,
            transcription_segment_text,
            transcription_segment_temperature,
            transcription_segment_avg_logprob,
            transcription_segment_compression_ratio,
            transcription_segment_no_speech_prob,
            transcription_segment_confidence
        )


