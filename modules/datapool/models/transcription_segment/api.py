# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

from pydantic import BaseModel
from ..base import (
    TrpPrimaryKey_ID_UUID
)
from ..transcription_word import (
    NewTranscriptionWordPartialModel,
    UpdateTranscriptionWordModel
)





class TranscriptionSegmentModel(BaseModel):
    id: int
    transcription: TrpPrimaryKey_ID_UUID
    pos: int = 0
    seek: float = -1.0
    start: float = -1.0
    end: float = -1.0
    text: str
    temperature: float = -1.0
    avg_logprob: float = -1.0
    compression_ratio: float = -1.0
    no_speech_prob: float = -1.0
    confidence: float = -1.0


class NewTranscriptionSegmentPartialModel(BaseModel):
    pos: int = 0
    seek: float = -1.0
    start: float = -1.0
    end: float = -1.0
    text: str
    temperature: float = -1.0
    avg_logprob: float = -1.0
    compression_ratio: float = -1.0
    no_speech_prob: float = -1.0
    confidence: float = -1.0

class NewTranscriptionSegmentModel(NewTranscriptionSegmentPartialModel):
    transcription: TrpPrimaryKey_ID_UUID

class NewTranscriptionSegmentStructurePartialModel(BaseModel):
    pos: int = 0
    seek: float = -1.0
    start: float = -1.0
    end: float = -1.0
    text: str
    temperature: float = -1.0
    avg_logprob: float = -1.0
    compression_ratio: float = -1.0
    no_speech_prob: float = -1.0
    confidence: float = -1.0
    words: list[NewTranscriptionWordPartialModel] | None = None

class NewTranscriptionSegmentStructureModel(NewTranscriptionSegmentStructurePartialModel):
    transcription: TrpPrimaryKey_ID_UUID


class UpdateTranscriptionSegmentModel(BaseModel):
    pos: int | None = None
    seek: float | None = None
    start: float | None = None
    end: float | None = None
    text: str | None = None
    temperature: float | None = None
    avg_logprob: float | None = None
    compression_ratio: float | None = None
    no_speech_prob: float | None = None
    confidence: float | None = None

class UpdateTranscriptionSegmentStructureModel(UpdateTranscriptionSegmentModel):
    words: list[UpdateTranscriptionWordModel] | None = None

class UpdateTranscriptionSegmentPartialModel(UpdateTranscriptionSegmentModel):
    id: int | None = None # id is optional if pos is given, otherwise required for update

class UpdateTranscriptionSegmentStructurePartialModel(UpdateTranscriptionSegmentStructureModel):
    id: int | None = None # id is optional if pos is given, otherwise required for update

