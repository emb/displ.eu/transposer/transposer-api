# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import typing
import datetime

import sqlalchemy
from sqlalchemy.orm import (
    Mapped,
    mapped_column,
    relationship,
    validates
)

from regex_patterns import (
    RE_INT_VALUE,
    RE_FLOAT_VALUE,
    RE_UUID4,
    RE_ALPHANUM_TERM,
    RE_LANG_CODE,
    RE_FIELD_NAME
)
from data_utils import (
    is_empty, is_true,
    split_terms,
    split_numeric,
    split_datetime,
    sanitize_text
)
from ..base import BaseOrm





class TranscriptionSegmentOrm(BaseOrm):
    __tablename__ = 'transcription_segments'
    id: Mapped[int] = mapped_column(
        sqlalchemy.Integer, primary_key=True
    )
    transcription_id: Mapped[int] = mapped_column(sqlalchemy.ForeignKey('transcriptions.id'))
    pos: Mapped[int] = mapped_column(sqlalchemy.Integer, default=0)
    seek: Mapped[float] = mapped_column(sqlalchemy.Float, default=-1.0)
    start: Mapped[float] = mapped_column(sqlalchemy.Float, default=-1.0)
    end: Mapped[float] = mapped_column(sqlalchemy.Float, default=-1.0)
    text: Mapped[str] = mapped_column(sqlalchemy.String)
    temperature: Mapped[float] = mapped_column(sqlalchemy.Float, default=-1.0)
    avg_logprob: Mapped[float] = mapped_column(sqlalchemy.Float, default=-1.0)
    compression_ratio: Mapped[float] = mapped_column(sqlalchemy.Float, default=-1.0)
    no_speech_prob: Mapped[float] = mapped_column(sqlalchemy.Float, default=-1.0)
    confidence: Mapped[float] = mapped_column(sqlalchemy.Float, default=-1.0)

    transcription: Mapped['TranscriptionOrm'] = relationship(back_populates='segments')
    words: Mapped[typing.List['TranscriptionWordOrm']] = relationship(
        back_populates='segment',
        primaryjoin="(TranscriptionSegmentOrm.id==TranscriptionWordOrm.segment_id)"
    )
    
    def __repr__(self):
        return (
            f'<TranscriptionSegment('
            f'id={self.id}, '
            f'transcription_id={self.transcription_id}, '
            f'pos={self.pos}, '
            f'seek={self.seek}, '
            f'start={self.start}, '
            f'end={self.end}, '
            f'text={self.text}, '
            f'temperature={self.temperature}, '
            f'avg_logprob={self.avg_logprob}, '
            f'compression_ratio={self.compression_ratio}, '
            f'no_speech_prob={self.no_speech_prob}, '
            f'confidence={self.confidence})>'
        )
    
    def __str__(self):
        return (
            f'TranscriptionSegment('
            f'id={self.id}, '
            f'transcription_id={self.transcription_id}, '
            f'pos={self.pos}, '
            f'seek={self.seek}, '
            f'start={self.start}, '
            f'end={self.end}, '
            f'text={self.text}, '
            f'temperature={self.temperature}, '
            f'avg_logprob={self.avg_logprob}, '
            f'compression_ratio={self.compression_ratio}, '
            f'no_speech_prob={self.no_speech_prob}, '
            f'confidence={self.confidence})'
        )
    
    def __json__(self):
        return {
            'id': self.id,
            'transcription_id': self.transcription_id,
            'pos': self.pos,
            'seek': self.seek,
            'start': self.start,
            'end': self.end,
            'text': self.text,
            'temperature': self.temperature,
            'avg_logprob': self.avg_logprob,
            'compression_ratio': self.compression_ratio,
            'no_speech_prob': self.no_speech_prob,
            'confidence': self.confidence
        }
    
    def _fields(self) -> tuple:
        return (
            ['id', 'transcription_id', 'pos', 'seek', 'start', 'end', 'text', 'temperature', 'avg_logprob', 'compression_ratio', 'no_speech_prob', 'confidence'],
            [('transcription', False, None), ('words', True, None)]
        )
    

    @validates('transcription_id')
    def validate_transcription_id(self, key, value):
        if is_empty(value):
            raise ValueError("Transcription ID is required")
        if isinstance(value, int):
            return value
        if isinstance(value, str):
            result = RE_INT_VALUE.match(value)
            if result is None:
                raise ValueError("Invalid transcription ID")
            return int(result.group(1))
        raise ValueError("Invalid transcription ID")

    @validates('pos')
    def validate_pos(self, key, value):
        if is_empty(value):
            raise ValueError("Position is required")
        if isinstance(value, int):
            return value
        if isinstance(value, str):
            result = RE_INT_VALUE.match(value)
            if result is None:
                raise ValueError("Invalid position")
            return int(result.group(1))
        raise ValueError("Invalid position")

    @validates('seek')
    def validate_seek(self, key, value):
        if is_empty(value):
            raise ValueError("Seek is required")
        if isinstance(value, (int, float)):
            return float(value)
        if isinstance(value, str):
            result = RE_FLOAT_VALUE.match(value)
            if result is None:
                raise ValueError("Invalid seek")
            return float(result.group(1))
        raise ValueError("Invalid seek")

    @validates('start')
    def validate_start(self, key, value):
        if is_empty(value):
            raise ValueError("Start is required")
        if isinstance(value, (int, float)):
            return float(value)
        if isinstance(value, str):
            result = RE_FLOAT_VALUE.match(value)
            if result is None:
                raise ValueError("Invalid start")
            return float(result.group(1))
        raise ValueError("Invalid start")

    @validates('end')
    def validate_end(self, key, value):
        if is_empty(value):
            raise ValueError("End is required")
        if isinstance(value, (int, float)):
            return float(value)
        if isinstance(value, str):
            result = RE_FLOAT_VALUE.match(value)
            if result is None:
                raise ValueError("Invalid end")
            return float(result.group(1))
        raise ValueError("Invalid end")

    @validates('text')
    def validate_text(self, key, value):
        return sanitize_text(value)

    @validates('temperature')
    def validate_temperature(self, key, value):
        if is_empty(value):
            raise ValueError("Temperature is required")
        if isinstance(value, (int, float)):
            return float(value)
        if isinstance(value, str):
            result = RE_FLOAT_VALUE.match(value)
            if result is None:
                raise ValueError("Invalid temperature")
            return float(result.group(1))
        raise ValueError("Invalid temperature")

    @validates('avg_logprob')
    def validate_avg_logprob(self, key, value):
        if is_empty(value):
            raise ValueError("Average-log-probability is required")
        if isinstance(value, (int, float)):
            return float(value)
        if isinstance(value, str):
            result = RE_FLOAT_VALUE.match(value)
            if result is None:
                raise ValueError("Invalid average-log-probability")
            return float(result.group(1))
        raise ValueError("Invalid average-log-probability")

    @validates('compression_ratio')
    def validate_compression_ratio(self, key, value):
        if is_empty(value):
            raise ValueError("Compression ratio is required")
        if isinstance(value, (int, float)):
            return float(value)
        if isinstance(value, str):
            result = RE_FLOAT_VALUE.match(value)
            if result is None:
                raise ValueError("Invalid compression ratio")
            return float(result.group(1))
        raise ValueError("Invalid compression ratio")

    @validates('no_speech_prob')
    def validate_no_speech_prob(self, key, value):
        if is_empty(value):
            raise ValueError("No-speech-probability is required")
        if isinstance(value, (int, float)):
            return float(value)
        if isinstance(value, str):
            result = RE_FLOAT_VALUE.match(value)
            if result is None:
                raise ValueError("Invalid no-speech-probability")
            return float(result.group(1))
        raise ValueError("Invalid no-speech-probability")

    @validates('confidence')
    def validate_confidence(self, key, value):
        if is_empty(value):
            raise ValueError("Confidence is required")
        if isinstance(value, (int, float)):
            return float(value)
        if isinstance(value, str):
            result = RE_FLOAT_VALUE.match(value)
            if result is None:
                raise ValueError("Invalid confidence")
            return float(result.group(1))
        raise ValueError("Invalid confidence")



