# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import typing
import datetime

import sqlalchemy
from sqlalchemy.orm import (
    Mapped,
    mapped_column,
    relationship,
    validates
)

from regex_patterns import (
    RE_INT_VALUE,
    RE_UUID4,
    RE_ALPHANUM_TERM,
    RE_TOKEN,
    RE_HASH_XX
)
from data_utils import (
    is_empty, is_true,
    split_terms,
    split_numeric,
    split_datetime,
    sanitize_text,
    get_datetime_obj
)
from ..base import BaseOrm





class PeerBearerTokenOrm(BaseOrm):
    __tablename__ = 'peer_bearer_tokens'
    id: Mapped[int] = mapped_column(
        sqlalchemy.Integer, primary_key=True
    )
    created_at: Mapped[datetime.datetime] = mapped_column(
        sqlalchemy.DateTime, default=sqlalchemy.sql.functions.now()
    )
    peer_id: Mapped[int] = mapped_column(sqlalchemy.ForeignKey('peers.id'))
    token: Mapped[str] = mapped_column(sqlalchemy.String)
    is_active: Mapped[bool] = mapped_column(sqlalchemy.Boolean, default=True)
    valid_until: Mapped[typing.Optional[datetime.datetime]] = mapped_column(
        sqlalchemy.DateTime, default=None, nullable=True
    )

    peer: Mapped['PeerOrm'] = relationship(back_populates='bearer_tokens')
    
    def __repr__(self):
        return (
            f'<PeerBearerToken('
            f'id={self.id}, '
            f'peer_id={self.peer_id}, '
            f'token={self.token}, '
            f'is_active={self.is_active}, '
            f'valid_until={self.valid_until})>'
        )
    
    def __str__(self):
        return (
            f'PeerBearerToken('
            f'id={self.id}, '
            f'peer_id={self.peer_id}, '
            f'token={self.token}, '
            f'is_active={self.is_active}, '
            f'valid_until={self.valid_until})'
        )
    
    def __json__(self):
        return {
            'id': self.id,
            'created_at': self.created_at,
            'peer_id': self.peer_id,
            'token': self.token,
            'is_active': self.is_active,
            'valid_until': self.valid_until
        }
    
    def _fields(self) -> tuple:
        return (
            ['id', 'created_at', 'peer_id', 'token', 'is_active', 'valid_until'],
            [('peer', False, None)]
        )
    

    @validates('peer_id')
    def validate_peer_id(self, key, value):
        if is_empty(value):
            raise ValueError("Peer ID is required")
        if isinstance(value, int):
            return value
        if isinstance(value, str):
            result = RE_INT_VALUE.match(value)
            if result is None:
                raise ValueError("Invalid peer ID")
            return int(result.group(1))
        raise ValueError("Invalid peer ID")

    @validates('token')
    def validate_token(self, key, value):
        if is_empty(value):
            raise ValueError("Token is required")
        if isinstance(value, str):
            result = RE_HASH_XX.match(value)
            if result is None:
                raise ValueError("Invalid token")
            return result.group(1)
        raise ValueError("Invalid token")
    
    @validates('is_active')
    def validate_is_active(self, key, value):
        return is_true(value)

    @validates('valid_until')
    def validate_valid_until(self, key, value):
        if is_empty(value):
            raise ValueError("Valid-Until timestamp is required")
        if isinstance(value, str):
            result = RE_INT_VALUE.match(value)
            if result is not None:
                return datetime.datetime.fromtimestamp(int(result.group(1)) / 1000.0)
            result = get_datetime_obj(value)
            if result is not None:
                return result
            raise ValueError("Invalid valid-until timestamp")
        if isinstance(value, int):
            return datetime.datetime.fromtimestamp(value / 1000.0)
        if isinstance(value, datetime.datetime):
            return value
        raise ValueError("Invalid valid-until timestamp")



