# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import re
import typing
from .orm import PeerBearerTokenOrm





PEER_BEARER_TOKEN__FIELDS_MAP = {
    'peer_bearer_token': {
        'id': PeerBearerTokenOrm.id,
        'created_at': PeerBearerTokenOrm.created_at,
        'peer': PeerBearerTokenOrm.peer_id,
        'peer_id': PeerBearerTokenOrm.peer_id,
        'token': PeerBearerTokenOrm.token,
        'is_active': PeerBearerTokenOrm.is_active,
        'valid_until': PeerBearerTokenOrm.valid_until
    },
    'peer_bearer_token_id': PeerBearerTokenOrm.id,
    'peer_bearer_token_created_at': PeerBearerTokenOrm.created_at,
    'peer_bearer_token_peer': PeerBearerTokenOrm.peer_id,
    'peer_bearer_token_peer_id': PeerBearerTokenOrm.peer_id,
    'peer_bearer_token_token': PeerBearerTokenOrm.token,
    'peer_bearer_token_is_active': PeerBearerTokenOrm.is_active,
    'peer_bearer_token_valid_until': PeerBearerTokenOrm.valid_until,

    'peer_bearer_token.id': PeerBearerTokenOrm.id,
    'peer_bearer_token.created_at': PeerBearerTokenOrm.created_at,
    'peer_bearer_token.peer': PeerBearerTokenOrm.peer_id,
    'peer_bearer_token.peer_id': PeerBearerTokenOrm.peer_id,
    'peer_bearer_token.token': PeerBearerTokenOrm.token,
    'peer_bearer_token.is_active': PeerBearerTokenOrm.is_active,
    'peer_bearer_token.valid_until': PeerBearerTokenOrm.valid_until
}

PEER_BEARER_TOKEN__VARIANTS_MAP = {
    'peer_bearer_token': {
        'id': 'peer_bearer_token.id',
        'created_at': 'peer_bearer_token.created_at',
        'peer': 'peer_bearer_token.peer_id',
        'peer_id': 'peer_bearer_token.peer_id',
        'token': 'peer_bearer_token.token',
        'is_active': 'peer_bearer_token.is_active',
        'valid_until': 'peer_bearer_token.valid_until'
    },
    'peer_bearer_token_id': 'peer_bearer_token.id',
    'peer_bearer_token_created_at': 'peer_bearer_token.created_at',
    'peer_bearer_token_peer': 'peer_bearer_token.peer_id',
    'peer_bearer_token_peer_id': 'peer_bearer_token.peer_id',
    'peer_bearer_token_token': 'peer_bearer_token.token',
    'peer_bearer_token_is_active': 'peer_bearer_token.is_active',
    'peer_bearer_token_valid_until': 'peer_bearer_token.valid_until',

    'peer_bearer_token.id': 'peer_bearer_token.id',
    'peer_bearer_token.created_at': 'peer_bearer_token.created_at',
    'peer_bearer_token.peer': 'peer_bearer_token.peer_id',
    'peer_bearer_token.peer_id': 'peer_bearer_token.peer_id',
    'peer_bearer_token.token': 'peer_bearer_token.token',
    'peer_bearer_token.is_active': 'peer_bearer_token.is_active',
    'peer_bearer_token.valid_until': 'peer_bearer_token.valid_until'
}



############
### Primary

PEER_BEARER_TOKEN__PRIMARY_FIELDS_MAP = {
    'id': PeerBearerTokenOrm.id,
    'created_at': PeerBearerTokenOrm.created_at,
    'peer': PeerBearerTokenOrm.peer_id,
    'peer_id': PeerBearerTokenOrm.peer_id,
    'token': PeerBearerTokenOrm.token,
    'is_active': PeerBearerTokenOrm.is_active,
    'valid_until': PeerBearerTokenOrm.valid_until
}

PEER_BEARER_TOKEN__PRIMARY_VARIANTS_MAP = {
    'id': 'peer_bearer_token.id',
    'created_at': 'peer_bearer_token.created_at',
    'peer': 'peer_bearer_token.peer_id',
    'peer_id': 'peer_bearer_token.peer_id',
    'token': 'peer_bearer_token.token',
    'is_active': 'peer_bearer_token.is_active',
    'valid_until': 'peer_bearer_token.valid_until'
}


