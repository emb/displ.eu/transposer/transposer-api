# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

from .orm import PipelineConnectorOrm





PIPELINE_CONNECTOR__FIELDS_MAP = {
    'pipeline_connector': {
        'id': PipelineConnectorOrm.id,
        'uuid': PipelineConnectorOrm.uuid,
        'created_at': PipelineConnectorOrm.created_at,
        'pipeline': PipelineConnectorOrm.pipeline_id,
        'pipeline_id': PipelineConnectorOrm.pipeline_id,
        'name': PipelineConnectorOrm.name
    },
    'pipeline_connector_id': PipelineConnectorOrm.id,
    'pipeline_connector_uuid': PipelineConnectorOrm.uuid,
    'pipeline_connector_created_at': PipelineConnectorOrm.created_at,
    'pipeline_connector_pipeline': PipelineConnectorOrm.pipeline_id,
    'pipeline_connector_pipeline_id': PipelineConnectorOrm.pipeline_id,
    'pipeline_connector_name': PipelineConnectorOrm.name,

    'pipeline_connector.id': PipelineConnectorOrm.id,
    'pipeline_connector.uuid': PipelineConnectorOrm.uuid,
    'pipeline_connector.created_at': PipelineConnectorOrm.created_at,
    'pipeline_connector.pipeline': PipelineConnectorOrm.pipeline_id,
    'pipeline_connector.pipeline_id': PipelineConnectorOrm.pipeline_id,
    'pipeline_connector.name': PipelineConnectorOrm.name
}

PIPELINE_CONNECTOR__VARIANTS_MAP = {
    'pipeline_connector': {
        'id': 'pipeline_connector.id',
        'uuid': 'pipeline_connector.uuid',
        'created_at': 'pipeline_connector.created_at',
        'pipeline': 'pipeline_connector.pipeline_id',
        'pipeline_id': 'pipeline_connector.pipeline_id',
        'name': 'pipeline_connector.name'
    },
    'pipeline_connector_id': 'pipeline_connector.id',
    'pipeline_connector_uuid': 'pipeline_connector.uuid',
    'pipeline_connector_created_at': 'pipeline_connector.created_at',
    'pipeline_connector_pipeline': 'pipeline_connector.pipeline_id',
    'pipeline_connector_pipeline_id': 'pipeline_connector.pipeline_id',
    'pipeline_connector_name': 'pipeline_connector.name',

    'pipeline_connector.id': 'pipeline_connector.id',
    'pipeline_connector.uuid': 'pipeline_connector.uuid',
    'pipeline_connector.created_at': 'pipeline_connector.created_at',
    'pipeline_connector.pipeline': 'pipeline_connector.pipeline_id',
    'pipeline_connector.pipeline_id': 'pipeline_connector.pipeline_id',
    'pipeline_connector.name': 'pipeline_connector.name'
}



############
### Primary

PIPELINE_CONNECTOR__PRIMARY_FIELDS_MAP = {
    'id': PipelineConnectorOrm.id,
    'uuid': PipelineConnectorOrm.uuid,
    'created_at': PipelineConnectorOrm.created_at,
    'pipeline': PipelineConnectorOrm.pipeline_id,
    'pipeline_id': PipelineConnectorOrm.pipeline_id,
    'name': PipelineConnectorOrm.name
}

PIPELINE_CONNECTOR__PRIMARY_VARIANTS_MAP = {
    'id': 'pipeline_connector.id',
    'uuid': 'pipeline_connector.uuid',
    'created_at': 'pipeline_connector.created_at',
    'pipeline': 'pipeline_connector.pipeline_id',
    'pipeline_id': 'pipeline_connector.pipeline_id',
    'name': 'pipeline_connector.name'
}


