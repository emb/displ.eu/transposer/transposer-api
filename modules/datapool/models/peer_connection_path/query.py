# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import typing

from regex_patterns import (
    RE_INT_VALUE,
    RE_HOST_PATH
)
from data_utils import (
    split_terms
)
from ..base import BaseQueryParams
from .orm import PeerConnectionPathOrm





class PeerConnectionPathQueryParams(BaseQueryParams):
    __slots__ = [
        '_id',
        '_connection_id',
        '_method',
        '_path',
        '_type',
        '_encoding',
        '_param_schema',

        '_id_stmt',
        '_connection_id_stmt',
        '_method_stmt',
        '_path_stmt',
        '_type_stmt',
        '_encoding_stmt',
        '_param_schema_stmt'
    ]

    def __init__(self, 
        id: int | str | None = None,
        connection_id: int | str | None = None,
        method: int | str | None = None,
        path: str | None = None,
        type: int | str | None = None,
        encoding: int | str | None = None,
        param_schema: str | None = None
    ):
        super().__init__()

        # Store raw values
        self._id = id
        self._connection_id = connection_id
        self._method = method
        self._path = path
        self._type = type
        self._encoding = encoding
        self._param_schema = param_schema

        # Init statement values
        self._id_stmt = None
        self._connection_id_stmt = None
        self._method_stmt = None
        self._path_stmt = None
        self._type_stmt = None
        self._encoding_stmt = None
        self._param_schema_stmt = None


    def add_conditions(self, container: list, use_and: bool = False) -> bool:

        # id
        if self._id_stmt is not None:
            container.append(self._id_stmt)
        else:
            val = self._id
            if isinstance(val, int):
                val = str(val)
            val = self.create_filter(
                PeerConnectionPathOrm.id,
                [(x, '==') for x in split_terms(val, RE_INT_VALUE, '', '')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._id_stmt = val
                container.append(val)

        # connection_id
        if self._connection_id_stmt is not None:
            container.append(self._connection_id_stmt)
        else:
            val = self._connection_id
            if isinstance(val, int):
                val = str(val)
            val = self.create_filter(
                PeerConnectionPathOrm.connection_id,
                [(x, '==') for x in split_terms(val, RE_INT_VALUE, '', '')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._connection_id_stmt = val
                container.append(val)

        # method
        if self._method_stmt is not None:
            container.append(self._method_stmt)
        else:
            val = self._method
            val = self.create_filter(
                PeerConnectionPathOrm.method,
                [(x, '==') for x in split_terms(val, RE_INT_VALUE, '', '')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._method_stmt = val
                container.append(val)

        # path
        if self._path_stmt is not None:
            container.append(self._path_stmt)
        else:
            val = self._path
            val = self.create_filter(
                PeerConnectionPathOrm.path,
                [(x, 'like') for x in split_terms(val, RE_HOST_PATH, '%', '%')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._path_stmt = val
                container.append(val)

        # type
        if self._type_stmt is not None:
            container.append(self._type_stmt)
        else:
            val = self._type
            val = self.create_filter(
                PeerConnectionPathOrm.type,
                [(x, '==') for x in split_terms(val, RE_INT_VALUE, '', '')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._type_stmt = val
                container.append(val)

        # encoding
        if self._encoding_stmt is not None:
            container.append(self._encoding_stmt)
        else:
            val = self._encoding
            val = self.create_filter(
                PeerConnectionPathOrm.encoding,
                [(x, '==') for x in split_terms(val, RE_INT_VALUE, '', '')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._encoding_stmt = val
                container.append(val)

        # param_schema
        # We ignore schema for the time being, it only exists fro completemess
        # TODO: Implement schema filtering

        # Return True if conditions were added
        return self._has_params
    

    # Getters for raw values

    @property
    def id(self) -> int | str | None:
        return self._id
    
    @property
    def connection_id(self) -> int | str | None:
        return self._connection_id
    
    @property
    def method(self) -> int | str | None:
        return self._method
    
    @property
    def path(self) -> str | None:
        return self._path
    
    @property
    def type(self) -> int | str | None:
        return self._type
    
    @property
    def encoding(self) -> int | str | None:
        return self._encoding
    
    @property
    def param_schema(self) -> str | dict | None:
        return None
    

    # Getters for statements
    
    @property
    def id_stmt(self) -> typing.Any:
        return self._id_stmt
    
    @property
    def connection_id_stmt(self) -> typing.Any:
        return self._connection_id_stmt
    
    @property
    def method_stmt(self) -> typing.Any:
        return self._method_stmt
    
    @property
    def path_stmt(self) -> typing.Any:
        return self._path_stmt
    
    @property
    def type_stmt(self) -> typing.Any:
        return self._type_stmt
    
    @property
    def encoding_stmt(self) -> typing.Any:
        return self._encoding_stmt
    
    @property
    def param_schema_stmt(self) -> typing.Any:
        return None



### Define Interfaces

class PeerConnectionPathQueryParamsPrimary(PeerConnectionPathQueryParams):
    def __init__(self,
        id: int | str | None = None,
        connection: int | str | None = None,
        connection_id: int | str | None = None,
        method: int | str | None = None,
        path: str | None = None,
        type: int | str | None = None,
        encoding: int | str | None = None,
        param_schema: str | dict | None = None
    ):
        super().__init__(
            id,
            connection_id if connection_id is not None else connection,
            method,
            path,
            type,
            encoding,
            param_schema
        )


class PeerConnectionPathQueryParamsGeneral(PeerConnectionPathQueryParams):
    def __init__(self,
        peer_connection_path_id: int | str | None = None,
        peer_connection_path_connection: int | str | None = None,
        peer_connection_path_connection_id: int | str | None = None,
        peer_connection_path_method: int | str | None = None,
        peer_connection_path_path: str | None = None,
        peer_connection_path_type: int | str | None = None,
        peer_connection_path_encoding: int | str | None = None,
        peer_connection_path_param_schema: str | dict | None = None
    ):
        super().__init__(
            peer_connection_path_id,
            peer_connection_path_connection_id if peer_connection_path_connection_id is not None else peer_connection_path_connection,
            peer_connection_path_method,
            peer_connection_path_path,
            peer_connection_path_type,
            peer_connection_path_encoding,
            peer_connection_path_param_schema
        )


