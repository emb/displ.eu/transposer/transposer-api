# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

from pydantic import BaseModel





class PeerConnectionPathModel(BaseModel):
    id: int
    connection: int
    method: int | str
    path: str
    type: int | str
    encoding: int | str
    param_schema: str | None = None
    token: int | str | None = None


class NewPeerConnectionPathPartialModel(BaseModel):
    method: int | str
    path: str
    type: int | str
    encoding: int | str
    param_schema: str | None = None
    token: int | str | None = None

class NewPeerConnectionPathModel(NewPeerConnectionPathPartialModel):
    connection: int


class UpdatePeerConnectionPathModel(BaseModel):
    method: int | str | None = None
    path: str | None = None
    type: int | str | None = None
    encoding: int | str | None = None
    param_schema: str | None = None
    token: int | str | None = None

class UpdatePeerConnectionPathPartialModel(UpdatePeerConnectionPathModel):
    id: int | None = None # Required for update, missing id performs add

