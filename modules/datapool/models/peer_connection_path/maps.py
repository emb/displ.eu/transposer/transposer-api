# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import re
import typing
from .orm import PeerConnectionPathOrm





PEER_CONNECTION_PATH__FIELDS_MAP = {
    'peer_connection_path': {
        'id': PeerConnectionPathOrm.id,
        'connection': PeerConnectionPathOrm.connection,
        'connection_id': PeerConnectionPathOrm.connection_id,
        'method': PeerConnectionPathOrm.method,
        'path': PeerConnectionPathOrm.path,
        'type': PeerConnectionPathOrm.type,
        'encoding': PeerConnectionPathOrm.encoding,
        'param_schema': PeerConnectionPathOrm.param_schema
    },
    'peer_connection_path_id': PeerConnectionPathOrm.id,
    'peer_connection_path_connection': PeerConnectionPathOrm.connection_id,
    'peer_connection_path_connection_id': PeerConnectionPathOrm.connection_id,
    'peer_connection_path_method': PeerConnectionPathOrm.method,
    'peer_connection_path_path': PeerConnectionPathOrm.path,
    'peer_connection_path_type': PeerConnectionPathOrm.type,
    'peer_connection_path_encoding': PeerConnectionPathOrm.encoding,
    'peer_connection_path_param_schema': PeerConnectionPathOrm.param_schema,

    'peer_connection_path.id': PeerConnectionPathOrm.id,
    'peer_connection_path.connection': PeerConnectionPathOrm.connection_id,
    'peer_connection_path.connection_id': PeerConnectionPathOrm.connection_id,
    'peer_connection_path.method': PeerConnectionPathOrm.method,
    'peer_connection_path.path': PeerConnectionPathOrm.path,
    'peer_connection_path.type': PeerConnectionPathOrm.type,
    'peer_connection_path.encoding': PeerConnectionPathOrm.encoding,
    'peer_connection_path.param_schema': PeerConnectionPathOrm.param_schema
}

PEER_CONNECTION_PATH__VARIANTS_MAP = {
    'peer_connection_path': {
        'id': 'peer_connection_path.id',
        'connection': 'peer_connection_path.connection_id',
        'connection_id': 'peer_connection_path.connection_id',
        'method': 'peer_connection_path.method',
        'path': 'peer_connection_path.path',
        'type': 'peer_connection_path.type',
        'encoding': 'peer_connection_path.encoding',
        'param_schema': 'peer_connection_path.param_schema'
    },
    'peer_connection_path_id': 'peer_connection_path.id',
    'peer_connection_path_connection': 'peer_connection_path.connection_id',
    'peer_connection_path_connection_id': 'peer_connection_path.connection_id',
    'peer_connection_path_method': 'peer_connection_path.method',
    'peer_connection_path_path': 'peer_connection_path.path',
    'peer_connection_path_type': 'peer_connection_path.type',
    'peer_connection_path_encoding': 'peer_connection_path.encoding',
    'peer_connection_path_param_schema': 'peer_connection_path.param_schema',

    'peer_connection_path.id': 'peer_connection_path.id',
    'peer_connection_path.connection': 'peer_connection_path.connection_id',
    'peer_connection_path.connection_id': 'peer_connection_path.connection_id',
    'peer_connection_path.method': 'peer_connection_path.method',
    'peer_connection_path.path': 'peer_connection_path.path',
    'peer_connection_path.type': 'peer_connection_path.type',
    'peer_connection_path.encoding': 'peer_connection_path.encoding',
    'peer_connection_path.param_schema': 'peer_connection_path.param_schema'
}



############
### Primary

PEER_CONNECTION_PATH__PRIMARY_FIELDS_MAP = {
    'id': PeerConnectionPathOrm.id,
    'connection': PeerConnectionPathOrm.connection_id,
    'connection_id': PeerConnectionPathOrm.connection_id,
    'method': PeerConnectionPathOrm.method,
    'path': PeerConnectionPathOrm.path,
    'type': PeerConnectionPathOrm.type,
    'encoding': PeerConnectionPathOrm.encoding,
    'param_schema': PeerConnectionPathOrm.param_schema
}

PEER_CONNECTION_PATH__PRIMARY_VARIANTS_MAP = {
    'id': 'peer_connection_path.id',
    'connection': 'peer_connection_path.connection_id',
    'connection_id': 'peer_connection_path.connection_id',
    'method': 'peer_connection_path.method',
    'path': 'peer_connection_path.path',
    'type': 'peer_connection_path.type',
    'encoding': 'peer_connection_path.encoding',
    'param_schema': 'peer_connection_path.param_schema'
}


