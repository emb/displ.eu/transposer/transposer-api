# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import typing
import json

import sqlalchemy
from sqlalchemy.orm import (
    Mapped,
    mapped_column,
    relationship,
    validates
)
from sqlalchemy.dialects.postgresql import JSONB

from regex_patterns import (
    RE_INT_VALUE,
    RE_UUID4,
    RE_HOST_PATH
)
from data_utils import (
    is_empty,
    split_terms,
    split_numeric
)
from ..base import BaseOrm





class PeerConnectionPathOrm(BaseOrm):
    __tablename__ = 'peer_connection_paths'
    id: Mapped[int] = mapped_column(
        sqlalchemy.Integer, primary_key=True
    )
    connection_id: Mapped[int] = mapped_column(sqlalchemy.ForeignKey('peer_connections.id'))
    method: Mapped[int] = mapped_column(sqlalchemy.Integer, default=0)
    path: Mapped[str] = mapped_column(sqlalchemy.String)
    type: Mapped[int] = mapped_column(sqlalchemy.Integer, default=0)
    encoding: Mapped[int] = mapped_column(sqlalchemy.Integer, default=0)
    param_schema: Mapped[typing.Optional[JSONB]] = mapped_column(type_=JSONB, default='{}')
    token_id: Mapped[typing.Optional[int]] = mapped_column(sqlalchemy.ForeignKey('peer_access_tokens.id'), nullable=True, default=None)

    connection: Mapped['PeerConnectionOrm'] = relationship(
        back_populates='paths'
    )
    token: Mapped['PeerAccessTokenOrm'] = relationship(
        back_populates='paths'
    )
    
    def __repr__(self):
        return (
            f'<PeerConnectionPath('
            f'id={self.id}, '
            f'connection_id={self.connection_id}, '
            f'method={self.method}, '
            f'path={self.path}, '
            f'type={self.type}, '
            f'encoding={self.encoding}, '
            #f'param_schema={self.param_schema}, '
            f'token_id={self.token_id})>'
        )
    
    def __str__(self):
        return (
            f'PeerConnectionPath('
            f'id={self.id}, '
            f'connection_id={self.connection_id}, '
            f'method={self.method}, '
            f'path={self.path}, '
            f'type={self.type}, '
            f'encoding={self.encoding}, '
            #f'param_schema={self.param_schema}, '
            f'token_id={self.token_id})'
        )
    
    def __json__(self):
        return {
            'id': self.id,
            'connection_id': self.connection_id,
            'method': self.method,
            'path': self.path,
            'type': self.type,
            'encoding': self.encoding,
            'param_schema': self.param_schema,
            'token_id': self.token_id
        }
    
    def _fields(self) -> tuple:
        return (
            ['id', 'connection_id', 'method', 'path', 'type', 'encoding', 'param_schema', 'token_id'],
            [('connection', False, None), ('token', False, None)]
        )
    

    @validates('connection_id')
    def validate_connection_id(self, key, value):
        if is_empty(value):
            raise ValueError("Connection ID is required")
        if isinstance(value, int):
            return value
        if isinstance(value, str):
            result = RE_INT_VALUE.match(value)
            if result is None:
                raise ValueError("Invalid connection ID")
            return int(result.group(1))
        raise ValueError("Invalid connection ID")
    
    @validates('method')
    def validate_method(self, key, value):
        if is_empty(value):
            raise ValueError("Method is required")
        if isinstance(value, int):
            return value
        if isinstance(value, str):
            result = RE_INT_VALUE.match(value)
            if result is None:
                raise ValueError("Invalid method")
            return int(result.group(1))
        raise ValueError("Invalid method")

    @validates('path')
    def validate_path(self, key, value):
        if is_empty(value):
            raise ValueError("Path is required")
        if isinstance(value, str):
            result = RE_HOST_PATH.match(value)
            if result is None:
                raise ValueError("Invalid path")
            return result.group(1)
        raise ValueError("Invalid path")
    
    @validates('type')
    def validate_type(self, key, value):
        if is_empty(value):
            raise ValueError("Type is required")
        if isinstance(value, int):
            return value
        if isinstance(value, str):
            result = RE_INT_VALUE.match(value)
            if result is None:
                raise ValueError("Invalid type")
            return int(result.group(1))
        raise ValueError("Invalid type")
    
    @validates('encoding')
    def validate_encoding(self, key, value):
        if is_empty(value):
            raise ValueError("Encoding is required")
        if isinstance(value, int):
            return value
        if isinstance(value, str):
            result = RE_INT_VALUE.match(value)
            if result is None:
                raise ValueError("Invalid encoding")
            return int(result.group(1))
        raise ValueError("Invalid encoding")

    @validates('param_schema')
    def validate_param_schema(self, key, value):
        if is_empty(value):
            return {}
        if isinstance(value, dict):
            return value
        if not isinstance(value, str):
            raise ValueError("Invalid param-schema")
        try:
            value = json.loads(value)
            return value
        except json.JSONDecodeError:
            raise ValueError("Invalid param-schema")

    @validates('token_id')
    def validate_token_id(self, key, value):
        if is_empty(value):
            return None
        if isinstance(value, int):
            return value
        if isinstance(value, str):
            result = RE_INT_VALUE.match(value)
            if result is None:
                raise ValueError("Invalid token ID")
            return int(result.group(1))
        raise ValueError("Invalid token ID")


