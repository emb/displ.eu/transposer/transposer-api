# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import typing

from regex_patterns import (
    RE_INT_VALUE,
    RE_ALPHANUM_TERM
)
from data_utils import (
    split_terms
)
from ..base import BaseQueryParams
from .orm import RoleOrm





class RoleQueryParams(BaseQueryParams):
    __slots__ = [
        '_id',
        '_name',

        '_id_stmt',
        '_name_stmt'
    ]

    def __init__(self, 
        id: int | str | None = None,
        name: str | None = None
    ):
        super().__init__()

        # Store raw values
        self._id = id
        self._name = name

        # Init statement values
        self._id_stmt = None
        self._name_stmt = None


    def add_conditions(self, container: list, use_and: bool = False) -> bool:

        # id
        if self._id_stmt is not None:
            container.append(self._id_stmt)
        else:
            val = self._id
            if isinstance(val, int):
                val = str(val)
            val = self.create_filter(
                RoleOrm.id,
                [(x, '==') for x in split_terms(val, RE_INT_VALUE, '', '')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._id_stmt = val
                container.append(val)

        # name
        if self._name_stmt is not None:
            container.append(self._name_stmt)
        else:
            val = self._name
            val = self.create_filter(
                RoleOrm.name,
                [(x, 'like') for x in split_terms(val, RE_ALPHANUM_TERM, '%', '%')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._name_stmt = val
                container.append(val)

        # Return True if conditions were added
        return self._has_params
    

    # Getters for raw values

    @property
    def id(self) -> int | str | None:
        return self._id
    
    @property
    def name(self) -> str | None:
        return self._name
    

    # Getters for statements
    
    @property
    def id_stmt(self) -> typing.Any:
        return self._id_stmt
    
    @property
    def name_stmt(self) -> typing.Any:
        return self._name_stmt



### Define Interfaces

class RoleQueryParamsPrimary(RoleQueryParams):
    def __init__(self,
        id: int | str | None = None,
        name: str | None = None
    ):
        super().__init__(
            id,
            name
        )


class RoleQueryParamsGeneral(RoleQueryParams):
    def __init__(self,
        role_id: int | str | None = None,
        role_name: str | None = None
    ):
        super().__init__(
            role_id,
            role_name
        )


