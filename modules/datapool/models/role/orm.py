# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import typing

import sqlalchemy
from sqlalchemy.orm import (
    Mapped,
    mapped_column,
    relationship,
    validates
)

from regex_patterns import (
    RE_INT_VALUE,
    RE_ALPHANUM_TERM
)
from data_utils import (
    is_empty,
    split_terms,
    sanitize_text
)
from ..base import BaseOrm
from ..links import (
    user_role_link_table,
    group_role_link_table
)





class RoleOrm(BaseOrm):
    __tablename__ = 'roles'
    id: Mapped[int] = mapped_column(
        sqlalchemy.Integer, primary_key=True
    )
    name: Mapped[str] = mapped_column(sqlalchemy.String)

    users: Mapped[typing.List['UserOrm']] = relationship(
        'UserOrm',
        secondary=user_role_link_table,
        back_populates='roles'
    )
    groups: Mapped[typing.List['GroupOrm']] = relationship(
        'GroupOrm',
        secondary=group_role_link_table,
        back_populates='roles'
    )
    permissions: Mapped[typing.List['PermissionOrm']] = relationship(
        back_populates='role',
        primaryjoin="(RoleOrm.id==PermissionOrm.role_id)"
    )
    
    def __repr__(self):
        return f'<Role(id={self.id}, name={self.name})>'
    
    def __str__(self):
        return f'Role(id={self.id}, name={self.name})'
    
    def __json__(self):
        return {
            'id': self.id,
            'name': self.name
        }
    
    def _fields(self) -> tuple:
        return (
            ['id', 'name'],
            [('users', True, None), ('groups', True, None), ('permissions', True, None)]
        )
    

    @validates('name')
    def validate_name(self, key, value):
        if is_empty(value):
            raise ValueError("Name is required")
        if isinstance(value, str):
            result = RE_ALPHANUM_TERM.match(value)
            if result is None:
                raise ValueError("Invalid Name")
            return result.group(1)
        raise ValueError("Invalid Name")



