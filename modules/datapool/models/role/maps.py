# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import re
import typing
from .orm import RoleOrm





ROLE__FIELDS_MAP = {
    'role': {
        'id': RoleOrm.id,
        'name': RoleOrm.name
    },
    'role_id': RoleOrm.id,
    'role_name': RoleOrm.name,

    'role.id': RoleOrm.id,
    'role.name': RoleOrm.name
}

ROLE__VARIANTS_MAP = {
    'role': {
        'id': 'role.id',
        'name': 'role.name'
    },
    'role_id': 'role.id',
    'role_name': 'role.name',

    'role.id': 'role.id',
    'role.name': 'role.name'
}



############
### Primary

ROLE__PRIMARY_FIELDS_MAP = {
    'id': RoleOrm.id,
    'name': RoleOrm.name
}

ROLE__PRIMARY_VARIANTS_MAP = {
    'id': 'role.id',
    'name': 'role.name'
}


