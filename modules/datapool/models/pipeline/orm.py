# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import typing
import datetime
import json

import sqlalchemy
from sqlalchemy.orm import (
    Mapped,
    mapped_column,
    relationship,
    validates
)

from regex_patterns import (
    RE_INT_VALUE,
    RE_FLOAT_VALUE,
    RE_TRUE,
    RE_LANG_CODE,
    RE_UUID4,
    RE_SLUG,
    RE_ALPHANUM_ASCII_WSPACE
)
from data_utils import (
    is_empty,
    split_terms,
    split_datetime,
    sanitize_text
)
from ..base import BaseOrm # PipelineBaseOrm





class PipelineOrm(BaseOrm):
    __tablename__ = 'pipelines'
    id: Mapped[int] = mapped_column(
        sqlalchemy.Integer, primary_key=True
    )
    uuid: Mapped[str] = mapped_column(sqlalchemy.String)
    created_at: Mapped[datetime.datetime] = mapped_column(
        sqlalchemy.DateTime, default=sqlalchemy.sql.functions.now()
    )
    setting_id: Mapped[str] = mapped_column(sqlalchemy.ForeignKey('settings.id'))
    name: Mapped[typing.Optional[str]] = mapped_column(sqlalchemy.String, default=None, nullable=True)

    setting: Mapped['SettingOrm'] = relationship(
        back_populates='pipelines'
    )
    
    producers: Mapped[typing.List['PipelineProducerOrm']] = relationship(
        back_populates='pipeline',
        primaryjoin="(PipelineOrm.id==PipelineProducerOrm.pipeline_id)"
    )
    consumers: Mapped[typing.List['PipelineConsumerOrm']] = relationship(
        back_populates='pipeline',
        primaryjoin="(PipelineOrm.id==PipelineConsumerOrm.pipeline_id)"
    )
    servers: Mapped[typing.List['PipelineServerOrm']] = relationship(
        back_populates='pipeline',
        primaryjoin="(PipelineOrm.id==PipelineServerOrm.pipeline_id)"
    )
    connectors: Mapped[typing.List['PipelineConnectorOrm']] = relationship(
        back_populates='pipeline',
        primaryjoin="(PipelineOrm.id==PipelineConnectorOrm.pipeline_id)"
    )
    
    options: Mapped[typing.List['PipelineOptionOrm']] = relationship(
        back_populates='parent',
        primaryjoin="(PipelineOrm.id==PipelineOptionOrm.parent_id)"
    )
    

    def __repr__(self):
        return (
            f'<Pipeline('
            f'id={self.id}, '
            f'uuid={self.uuid}, '
            f'created_at={self.created_at}, '
            f'setting_id={self.setting_id}, '
            f'name={self.name})>'
        )
    
    def __str__(self):
        return (
            f'Pipeline('
            f'id={self.id}, '
            f'uuid={self.uuid}, '
            f'created_at={self.created_at}, '
            f'setting_id={self.setting_id}, '
            f'name={self.name})'
        )
    
    def __json__(self):
        return {
            'id': self.id,
            'uuid': self.uuid,
            'created_at': self.created_at,
            'setting_id': self.setting_id,
            'name': self.name
        }
    
    def _fields(self) -> tuple:
        return (
            ['id', 'uuid', 'created_at', 'setting_id', 'name'],
            [
                ('setting', False, None),
                ('producers', True, None),
                ('consumers', True, None),
                ('servers', True, None),
                ('connectors', True, None),
                ('options', True, self._transform_options)
            ]
        )
    

    @validates('uuid')
    def validate_uuid(self, key, value):
        if is_empty(value):
            raise ValueError("UUID is required")
        if isinstance(value, str):
            result = RE_UUID4.match(value)
            if result is None:
                raise ValueError("Invalid UUID")
            return result.group(1)
        raise ValueError("Invalid UUID")
    
    @validates('setting_id')
    def validate_setting_id(self, key, value):
        if is_empty(value):
            raise ValueError("Setting ID is required")
        if isinstance(value, str):
            result = RE_SLUG.match(value)
            if result is None:
                raise ValueError("Invalid setting ID")
            return result.group(1)
        raise ValueError("Invalid setting ID")

    @validates('name')
    def validate_name(self, key, value):
        if is_empty(value):
            return None
        if isinstance(value, str):
            result = RE_ALPHANUM_ASCII_WSPACE.match(value)
            if result is None:
                raise ValueError("Invalid name")
            return result.group(1)
        raise ValueError("Invalid name")


