# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

from .orm import PipelineOrm





PIPELINE__FIELDS_MAP = {
    'pipeline': {
        'id': PipelineOrm.id,
        'uuid': PipelineOrm.uuid,
        'created_at': PipelineOrm.created_at,
        'setting': PipelineOrm.setting_id,
        'setting_id': PipelineOrm.setting_id,
        'name': PipelineOrm.name
    },
    'pipeline_id': PipelineOrm.id,
    'pipeline_uuid': PipelineOrm.uuid,
    'pipeline_created_at': PipelineOrm.created_at,
    'pipeline_setting': PipelineOrm.setting_id,
    'pipeline_setting_id': PipelineOrm.setting_id,
    'pipeline_name': PipelineOrm.name,

    'pipeline.id': PipelineOrm.id,
    'pipeline.uuid': PipelineOrm.uuid,
    'pipeline.created_at': PipelineOrm.created_at,
    'pipeline.setting': PipelineOrm.setting_id,
    'pipeline.setting_id': PipelineOrm.setting_id,
    'pipeline.name': PipelineOrm.name
}

PIPELINE__VARIANTS_MAP = {
    'pipeline': {
        'id': 'pipeline.id',
        'uuid': 'pipeline.uuid',
        'created_at': 'pipeline.created_at',
        'setting': 'pipeline.setting_id',
        'setting_id': 'pipeline.setting_id',
        'name': 'pipeline.name'
    },
    'pipeline_id': 'pipeline.id',
    'pipeline_uuid': 'pipeline.uuid',
    'pipeline_created_at': 'pipeline.created_at',
    'pipeline_setting': 'pipeline.setting_id',
    'pipeline_setting_id': 'pipeline.setting_id',
    'pipeline_name': 'pipeline.name',

    'pipeline.id': 'pipeline.id',
    'pipeline.uuid': 'pipeline.uuid',
    'pipeline.created_at': 'pipeline.created_at',
    'pipeline.setting': 'pipeline.setting_id',
    'pipeline.setting_id': 'pipeline.setting_id',
    'pipeline.name': 'pipeline.name'
}



############
### Primary

PIPELINE__PRIMARY_FIELDS_MAP = {
    'id': PipelineOrm.id,
    'uuid': PipelineOrm.uuid,
    'created_at': PipelineOrm.created_at,
    'setting': PipelineOrm.setting_id,
    'setting_id': PipelineOrm.setting_id,
    'name': PipelineOrm.name
}

PIPELINE__PRIMARY_VARIANTS_MAP = {
    'id': 'pipeline.id',
    'uuid': 'pipeline.uuid',
    'created_at': 'pipeline.created_at',
    'setting': 'pipeline.setting_id',
    'setting_id': 'pipeline.setting_id',
    'name': 'pipeline.name'
}


