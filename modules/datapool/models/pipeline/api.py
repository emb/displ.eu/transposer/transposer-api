# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

from pydantic import BaseModel

from ..pipeline_connector import (
    NewPipelineConnectorPartialModel,
    UpdatePipelineConnectorModel
)
from ..pipeline_consumer import (
    NewPipelineConsumerPartialModel,
    UpdatePipelineConsumerModel
)
from ..pipeline_producer import (
    NewPipelineProducerPartialModel,
    UpdatePipelineProducerModel
)
from ..pipeline_server import (
    NewPipelineServerPartialModel,
    UpdatePipelineServerModel
)
from ..pipeline_option import (
    NewPipelineOptionPartialModel,
    UpdatePipelineOptionPartialModel
)





class PipelineModel(BaseModel):
    id: int
    uuid: str
    setting: str
    name: str | None = None


class NewPipelinePartialModel(BaseModel):
    name: str | None = None

class NewPipelineModel(NewPipelinePartialModel):
    setting: str


class NewPipelineStructurePartialModel(NewPipelinePartialModel):
    producers: list[NewPipelineProducerPartialModel] | None = None
    consumers: list[NewPipelineConsumerPartialModel] | None = None
    servers: list[NewPipelineServerPartialModel] | None = None
    connectors: list[NewPipelineConnectorPartialModel] | None = None
    options: list[NewPipelineOptionPartialModel] | None = None

class NewPipelineStructureModel(NewPipelineStructurePartialModel):
    setting: str


class UpdatePipelineModel(BaseModel):
    name: str | None = None

class UpdatePipelineStructureModel(UpdatePipelineModel):
    producers: list[UpdatePipelineProducerModel] | None = None
    consumers: list[UpdatePipelineConsumerModel] | None = None
    servers: list[UpdatePipelineServerModel] | None = None
    connectors: list[UpdatePipelineConnectorModel] | None = None
    options: list[UpdatePipelineOptionPartialModel] | None = None


