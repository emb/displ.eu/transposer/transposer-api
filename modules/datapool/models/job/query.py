# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import typing
import datetime

from regex_patterns import (
    RE_INT_VALUE,
    RE_ALPHANUM_TERM,
    RE_UUID4,
    RE_SLUG
)
from data_utils import (
    split_terms,
    split_numeric,
    split_datetime
)
from ..base import (
    BaseQueryParams
)
from .orm import JobOrm





class JobQueryParams(BaseQueryParams):
    __slots__ = [
        '_id',
        '_uuid',
        '_created_at',
        '_finished_at',
        '_owner_id',
        '_action',
        '_async_request',
        '_status',
        '_reason',

        '_id_stmt',
        '_uuid_stmt',
        '_created_at_stmt',
        '_finished_at_stmt',
        '_owner_id_stmt',
        '_action_stmt',
        '_async_request_stmt',
        '_status_stmt',
        '_reason_stmt'
    ]

    def __init__(self, 
        id: int | str | None = None,
        uuid: str | None = None,
        created_at: int | float | str | None = None,
        finished_at: int | float | str | None = None,
        owner_id: int | str | None = None,
        action: str | None = None,
        async_request: bool | None = None,
        status: str | None = None,
        reason: str | None = None
    ):
        super().__init__()

        # Store raw values
        self._id = id
        self._uuid = uuid
        self._created_at = created_at
        self._finished_at = finished_at
        self._owner_id = owner_id
        self._action = action
        self._async_request = async_request
        self._status = status
        self._reason = reason

        # Init statement values
        self._id_stmt = None
        self._uuid_stmt = None
        self._created_at_stmt = None
        self._finished_at_stmt = None
        self._owner_id_stmt = None
        self._action_stmt = None
        self._async_request_stmt = None
        self._status_stmt = None
        self._reason_stmt = None


    def add_conditions(self, container: list, use_and: bool = False) -> bool:

        # id
        if self._id_stmt is not None:
            container.append(self._id_stmt)
        else:
            self._add_stmt_to_container(container, 'id',
                self.create_filter(
                    JobOrm.id,
                    [(x, '==') for x in split_terms(
                        str(self._id), RE_INT_VALUE, '', ''
                    )],
                    use_and
                )
            )
        
        # uuid
        if self._uuid_stmt is not None:
            container.append(self._uuid_stmt)
        else:
            self._add_stmt_to_container(container, 'uuid',
                self.create_filter(
                    JobOrm.uuid,
                    [(x, '==') for x in split_terms(
                        self._uuid, RE_UUID4, '', ''
                    )],
                    use_and
                )
            )
        
        # created_at
        if self._created_at_stmt is not None:
            container.append(self._created_at_stmt)
        else:
            self._add_stmt_to_container(container, 'created_at',
                self.create_filter(
                    JobOrm.created_at,
                    split_datetime(
                        self._created_at
                    ),
                    use_and
                )
            )

        # finished_at
        if self._finished_at_stmt is not None:
            container.append(self._finished_at_stmt)
        else:
            self._add_stmt_to_container(container, 'finished_at',
                self.create_filter(
                    JobOrm.finished_at,
                    split_datetime(
                        self._finished_at
                    ),
                    use_and
                )
            )

        # owner_id
        if self._owner_id_stmt is not None:
            container.append(self._owner_id_stmt)
        else:
            self._add_stmt_to_container(container, 'owner_id',
                self.create_filter(
                    JobOrm.owner_id,
                    [(x, '==') for x in split_terms(
                        str(self._owner_id), RE_INT_VALUE, '', ''
                    )],
                    use_and
                )
            )

        # action
        if self._action_stmt is not None:
            container.append(self._action_stmt)
        else:
            self._add_stmt_to_container(container, 'action',
                self.create_filter(
                    JobOrm.action,
                    [(x, '==') for x in split_terms(
                        self._action, RE_SLUG, '', ''
                    )],
                    use_and
                )
            )
        
        # async_request
        if self._async_request_stmt is not None:
            container.append(self._async_request_stmt)
        else:
            val = self._async_request
            if val is not None:
                self._add_stmt_to_container(container, 'async_request',
                    self.create_filter(
                        JobOrm.async_request,
                        [(val, '==')],
                        use_and
                    )
                )

        # status
        if self._status_stmt is not None:
            container.append(self._status_stmt)
        else:
            self._add_stmt_to_container(container, 'status',
                self.create_filter(
                    JobOrm.status,
                    [(x, '==') for x in split_terms(
                        self._status, RE_SLUG, '', ''
                    )],
                    use_and
                )
            )
        
        # reason
        if self._reason_stmt is not None:
            container.append(self._reason_stmt)
        else:
            self._add_stmt_to_container(container, 'reason',
                self.create_filter(
                    JobOrm.reason,
                    [(x, 'like') for x in split_terms(
                        self._reason, RE_ALPHANUM_TERM, '%', '%'
                    )],
                    use_and
                )
            )

        # Return True if conditions were added
        return self._has_params
    

    # Getters for raw values

    @property
    def id(self) -> int | str | None:
        return self._id
    
    @property
    def uuid(self) -> str | None:
        return self._uuid
    
    @property
    def created_at(self) -> int | float | str | None:
        return self._created_at
    
    @property
    def finished_at(self) -> int | float | str | None:
        return self._finished_at
    
    @property
    def owner_id(self) -> int | str | None:
        return self._owner_id
    
    @property
    def action(self) -> str | None:
        return self._action
    
    @property
    def async_request(self) -> bool | None:
        return self._async_request
    
    @property
    def status(self) -> str | None:
        return self._status
    
    @property
    def reason(self) -> str | None:
        return self._reason
    

    # Getters for statements
    
    @property
    def id_stmt(self) -> typing.Any:
        return self._id_stmt
    
    @property
    def uuid_stmt(self) -> typing.Any:
        return self._uuid_stmt
    
    @property
    def created_at_stmt(self) -> typing.Any:
        return self._created_at_stmt
    
    @property
    def finished_at_stmt(self) -> typing.Any:
        return self._finished_at_stmt
    
    @property
    def owner_id_stmt(self) -> typing.Any:
        return self._owner_id_stmt
    
    @property
    def action_stmt(self) -> typing.Any:
        return self._action_stmt
    
    @property
    def async_request_stmt(self) -> typing.Any:
        return self._async_request_stmt
    
    @property
    def status_stmt(self) -> typing.Any:
        return self._status_stmt
    
    @property
    def reason_stmt(self) -> typing.Any:
        return self._reason_stmt



### Define Interfaces

class JobQueryParamsPrimary(JobQueryParams):
    def __init__(self,
        id: int | str | None = None,
        uuid: str | None = None,
        created_at: int | float | str | None = None,
        finished_at: int | float | str | None = None,
        owner: int | str | None = None,
        owner_id: int | str | None = None,
        action: str | None = None,
        async_request: str | None = None,
        status: str | None = None,
        reason: str | None = None
    ):
        super().__init__(
            id,
            uuid,
            created_at,
            finished_at,
            owner_id if owner_id is not None else owner,
            action,
            async_request,
            status,
            reason
        )


class JobQueryParamsGeneral(JobQueryParams):
    def __init__(self,
        job_id: int | str | None = None,
        job_uuid: str | None = None,
        job_created_at: int | float | str | None = None,
        job_finished_at: int | float | str | None = None,
        job_owner: int | str | None = None,
        job_owner_id: int | str | None = None,
        job_action: str | None = None,
        job_async_request: str | None = None,
        job_status: str | None = None,
        job_reason: str | None = None
    ):
        super().__init__(
            job_id,
            job_uuid,
            job_created_at,
            job_finished_at,
            job_owner_id if job_owner_id is not None else job_owner,
            job_action,
            job_async_request,
            job_status,
            job_reason
        )


