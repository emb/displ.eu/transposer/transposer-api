# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

from .orm import JobOrm





REQUEST__FIELDS_MAP = {
    'job': {
        'id': JobOrm.id,
        'uuid': JobOrm.uuid,
        'created_at': JobOrm.created_at,
        'finished_at': JobOrm.finished_at,
        'owner': JobOrm.owner_id,
        'owner_id': JobOrm.owner_id,
        'action': JobOrm.action,
        'async_request': JobOrm.async_request,
        'status': JobOrm.status,
        'reason': JobOrm.reason
    },
    'job_id': JobOrm.id,
    'job_uuid': JobOrm.uuid,
    'job_created_at': JobOrm.created_at,
    'job_finished_at': JobOrm.finished_at,
    'job_owner': JobOrm.owner_id,
    'job_owner_id': JobOrm.owner_id,
    'job_action': JobOrm.action,
    'job_async_request': JobOrm.async_request,
    'job_status': JobOrm.status,
    'job_reason': JobOrm.reason,

    'job.id': JobOrm.id,
    'job.uuid': JobOrm.uuid,
    'job.created_at': JobOrm.created_at,
    'job.finished_at': JobOrm.finished_at,
    'job.owner': JobOrm.owner_id,
    'job.owner_id': JobOrm.owner_id,
    'job.action': JobOrm.action,
    'job.async_request': JobOrm.async_request,
    'job.status': JobOrm.status,
    'job.reason': JobOrm.reason
}

REQUEST__VARIANTS_MAP = {
    'job': {
        'id': 'job.id',
        'uuid': 'job.uuid',
        'created_at': 'job.created_at',
        'finished_at': 'job.finished_at',
        'owner': 'job.owner_id',
        'owner_id': 'job.owner_id',
        'action': 'job.action',
        'async_request': 'job.async_request',
        'status': 'job.status',
        'reason': 'job.reason'
    },
    'job_id': 'job.id',
    'job_uuid': 'job.uuid',
    'job_created_at': 'job.created_at',
    'job_finished_at': 'job.finished_at',
    'job_owner': 'job.owner_id',
    'job_owner_id': 'job.owner_id',
    'job_action': 'job.action',
    'job_async_request': 'job.async_request',
    'job_status': 'job.status',
    'job_reason': 'job.reason',

    'job.id': 'job.id',
    'job.uuid': 'job.uuid',
    'job.created_at': 'job.created_at',
    'job.finished_at': 'job.finished_at',
    'job.owner': 'job.owner_id',
    'job.owner_id': 'job.owner_id',
    'job.action': 'job.action',
    'job.async_request': 'job.async_request',
    'job.status': 'job.status',
    'job.reason': 'job.reason'
}



############
### Primary

REQUEST__PRIMARY_FIELDS_MAP = {
    'id': JobOrm.id,
    'uuid': JobOrm.uuid,
    'created_at': JobOrm.created_at,
    'finished_at': JobOrm.finished_at,
    'owner': JobOrm.owner_id,
    'owner_id': JobOrm.owner_id,
    'action': JobOrm.action,
    'async_request': JobOrm.async_request,
    'status': JobOrm.status,
    'reason': JobOrm.reason
}

REQUEST__PRIMARY_VARIANTS_MAP = {
    'id': 'job.id',
    'uuid': 'job.uuid',
    'created_at': 'job.created_at',
    'finished_at': 'job.finished_at',
    'owner': 'job.owner_id',
    'owner_id': 'job.owner_id',
    'action': 'job.action',
    'async_request': 'job.async_request',
    'status': 'job.status',
    'reason': 'job.reason'
}


