# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import typing
import datetime

import sqlalchemy
from sqlalchemy.orm import (
    Mapped,
    mapped_column,
    relationship,
    validates
)
from sqlalchemy.dialects.postgresql import JSONB

from regex_patterns import (
    RE_INT_VALUE,
    RE_UUID4,
    RE_ALPHANUM_TERM,
    RE_HASH_XX,
    RE_SLUG
)
from data_utils import (
    is_empty, is_true,
    split_terms,
    split_numeric,
    split_datetime,
    sanitize_text,
    validate_uri
)
from ..base import BaseOrm
from ..links import (
    job_document_link_table,
    job_connection_link_table
)





class JobOrm(BaseOrm):
    __tablename__ = 'jobs'
    id: Mapped[int] = mapped_column(
        sqlalchemy.Integer, primary_key=True
    )
    uuid: Mapped[str] = mapped_column(sqlalchemy.String)
    created_at: Mapped[datetime.datetime] = mapped_column(
        sqlalchemy.DateTime, default=sqlalchemy.sql.functions.now()
    )
    finished_at: Mapped[typing.Optional[datetime.datetime]] = mapped_column(
        sqlalchemy.DateTime, default=None, nullable=True
    )
    owner_id: Mapped[int] = mapped_column(sqlalchemy.ForeignKey('users.id'))
    action: Mapped[str] = mapped_column(sqlalchemy.String)
    async_request: Mapped[bool] = mapped_column(sqlalchemy.Boolean, default=False)
    status: Mapped[str] = mapped_column(sqlalchemy.String, default='undefined')
    reason: Mapped[typing.Optional[str]] = mapped_column(sqlalchemy.String)

    owner: Mapped['UserOrm'] = relationship(back_populates='jobs')
    documents: Mapped[typing.List['DocumentOrm']] = relationship(
        'DocumentOrm',
        secondary=job_document_link_table,
        back_populates='jobs'
    )
    connections: Mapped[typing.List['PeerConnectionOrm']] = relationship(
        'PeerConnectionOrm',
        secondary=job_connection_link_table,
        back_populates='jobs'
    )
    
    def __repr__(self):
        return (
            f'<Job('
            f'id={self.id}, '
            f'uuid={self.uuid}, '
            f'owner_id={self.owner_id}, '
            f'action={self.action}, '
            f'async_request={self.async_request}, '
            f'status={self.status}, '
            f'reason={self.reason})>'
        )
    
    def __str__(self):
        return (
            f'Job('
            f'id={self.id}, '
            f'uuid={self.uuid}, '
            f'owner_id={self.owner_id}, '
            f'action={self.action}, '
            f'async_request={self.async_request}, '
            f'status={self.status}, '
            f'reason={self.reason})'
        )
    
    def __json__(self):
        return {
            'id': self.id,
            'uuid': self.uuid,
            'created_at': self.created_at,
            'finished_at': self.finished_at,
            'owner_id': self.owner_id,
            'action': self.action,
            'async_request': self.async_request,
            'status': self.status,
            'reason': self.reason
        }
    
    def _fields(self) -> tuple:
        return (
            ['id', 'uuid', 'created_at', 'finished_at', 'owner_id', 'action', 'async_request', 'status', 'reason'],
            [
                ('owner', False, None),
                ('documents', True, None),
                ('connections', True, None)
            ]
        )
    

    @validates('uuid')
    def validate_uuid(self, key, value):
        if is_empty(value):
            raise ValueError("UUID is required")
        if isinstance(value, str):
            result = RE_UUID4.match(value)
            if result is None:
                raise ValueError("Invalid UUID")
            return result.group(1)
        raise ValueError("Invalid UUID")
    
    @validates('owner_id')
    def validate_owner_id(self, key, value):
        if is_empty(value):
            raise ValueError("Owner ID is required")
        if isinstance(value, int):
            return value
        if isinstance(value, str):
            result = RE_INT_VALUE.match(value)
            if result is None:
                raise ValueError("Invalid owner ID")
            return int(result.group(1))
        raise ValueError("Invalid owner ID")

    @validates('action')
    def validate_action(self, key, value):
        if is_empty(value):
            raise ValueError("Action is required")
        if isinstance(value, str):
            result = RE_SLUG.match(value)
            if result is None:
                raise ValueError("Invalid action")
            return result.group(1)
        raise ValueError("Invalid action")

    @validates('async_request')
    def validate_async_request(self, key, value):
        return is_true(value)

    @validates('status')
    def validate_status(self, key, value):
        if is_empty(value):
            raise ValueError("Status is required")
        if isinstance(value, str):
            result = RE_SLUG.match(value)
            if result is None:
                raise ValueError("Invalid status")
            return result.group(1)
        raise ValueError("Invalid status")

    @validates('reason')
    def validate_reason(self, key, value):
        return sanitize_text(value)



