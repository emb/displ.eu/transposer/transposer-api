# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import typing
from typing_extensions import Annotated
#import base64

from pydantic import BaseModel, ValidationError
from pydantic.functional_validators import BeforeValidator, AfterValidator

from ..base import (
    TrpTimeMSec,
    TrpTimestamp,
    TrpPrimaryKey_UserID
)

from data_utils import (
    data_2_base64,
    base64_2_data
)
from captions_parser import (
    parse_captions,
    build_captions
)







# Validate a mixed id: id or hash
def validate_captions(value: typing.Any) -> typing.Any:
    """
    Validate and convert WebVTT and SRT captions

    Args:
        value (Any): The value to validate and convert

    Returns:
        Any: The validated and converted value
    """

    #print('validate_captions ... value:', value)

    #value = base64.b64decode(value).decode('utf-8')
    value = base64_2_data(value)

    #print('validate_captions ... value (parsed):', value, type(value))

    if not isinstance(value, str):
        # If value is not a string, it is not convertible
        raise AssertionError('Not a valid value')
    
    #print('validate_captions ... value is string')

    # Parse the captions
    result = None
    try:
        result = parse_captions(value)

        #print('validate_captions ... result:', result)

    except Exception as e:
        raise AssertionError('Not valid captions')

    # Check if we got a result
    if result is None:
        raise AssertionError('Not valid captions')

    # Return the result
    return result


# Create a type for captions string
TrpCaptionsString = Annotated[str | dict, AfterValidator(validate_captions)]









class JobModel(BaseModel):
    id: int
    uuid: str
    finished_at: TrpTimestamp | None = None
    owner: TrpPrimaryKey_UserID
    action: str
    status: str = 'undefined'
    reason: str | None = None


class NewJobPartialModel(BaseModel):
    action: str
    options: str

class NewJobModel(NewJobPartialModel):
    owner: TrpPrimaryKey_UserID


class UpdateJobModel(BaseModel):
    finished_at: TrpTimestamp | None = None
    status: str | None = None
    reason: str | None = None



### Options Models
# Depending on the options' data structure,
# we decide which type of operation to perform

# Child-Models / Types

class JobOptionsModel_Caption(BaseModel):
    content: str
    start: TrpTimeMSec
    end: TrpTimeMSec



# Main Models

class JobOptionsModel_Transcribe_Sync(BaseModel):
    pass

class JobOptionsModel_Transcribe_Async(BaseModel):
    pass

class JobOptionsModel_Transcribe_Transform_Sync(BaseModel):
    pass

class JobOptionsModel_Transcribe_Transform_Async(BaseModel):
    pass

class JobOptionsModel_Transcribe_Transform_Translate_Sync(BaseModel):
    pass

class JobOptionsModel_Transcribe_Transform_Translate_Async(BaseModel):
    pass



class JobOptionsModel_Translate(BaseModel):
    source_lang: str = 'en'
    target_langs: str | list[str]


class JobOptionsModel_Translate_webvtt_Sync(JobOptionsModel_Translate):
    webvtt: TrpCaptionsString

class JobOptionsModel_Translate_webvtt_Async(JobOptionsModel_Translate_webvtt_Sync):
    connection: int | str


class JobOptionsModel_Translate_srt_Sync(JobOptionsModel_Translate):
    srt: TrpCaptionsString

class JobOptionsModel_Translate_srt_Async(JobOptionsModel_Translate_srt_Sync):
    connection: int | str


class JobOptionsModel_Translate_Captions_Data_Async(BaseModel):
    pass

class JobOptionsModel_Translate_Captions_File_Sync(BaseModel):
    pass

class JobOptionsModel_Translate_Captions_File_Async(BaseModel):
    pass

class JobOptionsModel_Translate_Structured_Sync(BaseModel):
    pass

class JobOptionsModel_Translate_Structured_Async(BaseModel):
    pass

class JobOptionsModel_Translate_Structured_File_Sync(BaseModel):
    pass

class JobOptionsModel_Translate_Structured_File_Async(BaseModel):
    pass


# JOB_OPTIONS_MODELS = [
#     (JobOptionsModel_Transcribe_Sync,                       'transcribe_sync'),
#     (JobOptionsModel_Transcribe_Async,                      'transcribe_async'),
#     (JobOptionsModel_Transcribe_Transform_Sync,             'transcribe_transform_sync'),
#     (JobOptionsModel_Transcribe_Transform_Async,            'transcribe_transform_async'),
#     (JobOptionsModel_Transcribe_Transform_Translate_Sync,   'transcribe_transform_translate_sync'),
#     (JobOptionsModel_Transcribe_Transform_Translate_Async,  'transcribe_transform_translate_async'),
#     (JobOptionsModel_Translate_String_Sync,                 'translate_sync'),
#     (JobOptionsModel_Translate_String_Async,                'translate_async'),
#     (JobOptionsModel_Translate_Captions_Data_Sync,          'translate_captions_sync'),
#     (JobOptionsModel_Translate_Captions_Data_Async,         'translate_captions_async'),
#     (JobOptionsModel_Translate_Captions_File_Sync,          'translate_captions_file_sync'),
#     (JobOptionsModel_Translate_Captions_File_Async,         'translate_captions_file_async'),
#     (JobOptionsModel_Translate_Structured_Sync,             'translate_structured_sync'),
#     (JobOptionsModel_Translate_Structured_Async,            'translate_structured_async'),
#     (JobOptionsModel_Translate_Structured_File_Sync,        'translate_structured_file_sync'),
#     (JobOptionsModel_Translate_Structured_File_Async,       'translate_structured_file_async')
# ]
JOB_OPTIONS_MODELS = [
    (JobOptionsModel_Translate_webvtt_Sync,                 'translate_webvtt_sync'),
    (JobOptionsModel_Translate_webvtt_Async,                'translate_webvtt_async'),
    (JobOptionsModel_Translate_srt_Sync,                    'translate_srt_sync'),
    (JobOptionsModel_Translate_srt_Async,                   'translate_srt_async')
]


