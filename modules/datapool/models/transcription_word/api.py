# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

from pydantic import BaseModel
from ..base import (
    TrpTimeSecFloat
)




class TranscriptionWordModel(BaseModel):
    id: int
    segment: int
    pos: int = 0
    start: TrpTimeSecFloat = -1.0
    end: TrpTimeSecFloat = -1.0
    text: str
    confidence: float = -1.0


class NewTranscriptionWordPartialModel(BaseModel):
    pos: int = 0
    start: TrpTimeSecFloat = -1.0
    end: TrpTimeSecFloat = -1.0
    text: str
    confidence: float = -1.0

class NewTranscriptionWordModel(NewTranscriptionWordPartialModel):
    segment: int


class UpdateTranscriptionWordModel(BaseModel):
    pos: int | None = None
    start: TrpTimeSecFloat | None = None
    end: TrpTimeSecFloat | None = None
    text: str | None = None
    confidence: float | None = None

class UpdateTranscriptionWordPartialModel(UpdateTranscriptionWordModel):
    id: int | None = None # id is optional if pos is given, otherwise required for update

