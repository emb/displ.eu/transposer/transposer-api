# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import re
import typing
from .orm import TranscriptionWordOrm





TRANSCRIPTION_WORD__FIELDS_MAP = {
    'transcription_word': {
        'id': TranscriptionWordOrm.id,
        'segment': TranscriptionWordOrm.segment_id,
        'segment_id': TranscriptionWordOrm.segment_id,
        'pos': TranscriptionWordOrm.pos,
        'start': TranscriptionWordOrm.start,
        'end': TranscriptionWordOrm.end,
        'text': TranscriptionWordOrm.text,
        'confidence': TranscriptionWordOrm.confidence
    },
    'transcription_word_id': TranscriptionWordOrm.id,
    'transcription_word_segment': TranscriptionWordOrm.segment_id,
    'transcription_word_segment_id': TranscriptionWordOrm.segment_id,
    'transcription_word_pos': TranscriptionWordOrm.pos,
    'transcription_word_start': TranscriptionWordOrm.start,
    'transcription_word_end': TranscriptionWordOrm.end,
    'transcription_word_text': TranscriptionWordOrm.text,
    'transcription_word_confidence': TranscriptionWordOrm.confidence,

    'transcription_word.id': TranscriptionWordOrm.id,
    'transcription_word.segment': TranscriptionWordOrm.segment_id,
    'transcription_word.segment_id': TranscriptionWordOrm.segment_id,
    'transcription_word.pos': TranscriptionWordOrm.pos,
    'transcription_word.start': TranscriptionWordOrm.start,
    'transcription_word.end': TranscriptionWordOrm.end,
    'transcription_word.text': TranscriptionWordOrm.text,
    'transcription_word.confidence': TranscriptionWordOrm.confidence
}

TRANSCRIPTION_WORD__VARIANTS_MAP = {
    'transcription_word': {
        'id': 'transcription_word.id',
        'segment': 'transcription_word.segment_id',
        'segment_id': 'transcription_word.segment_id',
        'pos': 'transcription_word.pos',
        'start': 'transcription_word.start',
        'end': 'transcription_word.end',
        'text': 'transcription_word.text',
        'confidence': 'transcription_word.confidence'
    },
    'transcription_word_id': 'transcription_word.id',
    'transcription_word_segment': 'transcription_word.segment_id',
    'transcription_word_segment_id': 'transcription_word.segment_id',
    'transcription_word_pos': 'transcription_word.pos',
    'transcription_word_start': 'transcription_word.start',
    'transcription_word_end': 'transcription_word.end',
    'transcription_word_text': 'transcription_word.text',
    'transcription_word_confidence': 'transcription_word.confidence',

    'transcription_word.id': 'transcription_word.id',
    'transcription_word.segment': 'transcription_word.segment_id',
    'transcription_word.segment_id': 'transcription_word.segment_id',
    'transcription_word.pos': 'transcription_word.pos',
    'transcription_word.start': 'transcription_word.start',
    'transcription_word.end': 'transcription_word.end',
    'transcription_word.text': 'transcription_word.text',
    'transcription_word.confidence': 'transcription_word.confidence'
}



############
### Primary

TRANSCRIPTION_WORD__PRIMARY_FIELDS_MAP = {
    'id': TranscriptionWordOrm.id,
    'segment': TranscriptionWordOrm.segment_id,
    'segment_id': TranscriptionWordOrm.segment_id,
    'pos': TranscriptionWordOrm.pos,
    'start': TranscriptionWordOrm.start,
    'end': TranscriptionWordOrm.end,
    'text': TranscriptionWordOrm.text,
    'confidence': TranscriptionWordOrm.confidence
}

TRANSCRIPTION_WORD__PRIMARY_VARIANTS_MAP = {
    'id': 'transcription_word.id',
    'segment': 'transcription_word.segment_id',
    'segment_id': 'transcription_word.segment_id',
    'pos': 'transcription_word.pos',
    'start': 'transcription_word.start',
    'end': 'transcription_word.end',
    'text': 'transcription_word.text',
    'confidence': 'transcription_word.confidence'
}


