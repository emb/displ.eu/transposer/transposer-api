# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import typing

from regex_patterns import (
    RE_INT_VALUE,
    RE_ALPHANUM_TERM
)
from data_utils import (
    split_terms,
    split_numeric
)
from ..base import BaseQueryParams
from .orm import TranscriptionWordOrm





class TranscriptionWordQueryParams(BaseQueryParams):
    __slots__ = [
        '_id',
        '_segment_id',
        '_pos',
        '_start',
        '_end',
        '_text',
        '_confidence',

        '_id_stmt',
        '_segment_id_stmt',
        '_pos_stmt',
        '_start_stmt',
        '_end_stmt',
        '_text_stmt',
        '_confidence_stmt'
    ]

    def __init__(self, 
        id: int | str | None = None,
        segment_id: int | str | None = None,
        pos: int | str | None = None,
        start: int | float | str | None = None,
        end: int | float | str | None = None,
        text: str | None = None,
        confidence: int | float | str | None = None
    ):
        super().__init__()

        # Store raw values
        self._id = id
        self._segment_id = segment_id
        self._pos = pos
        self._start = start
        self._end = end
        self._text = text
        self._confidence = confidence

        # Init statement values
        self._id_stmt = None
        self._segment_id_stmt = None
        self._pos_stmt = None
        self._start_stmt = None
        self._end_stmt = None
        self._text_stmt = None
        self._confidence_stmt = None


    def add_conditions(self, container: list, use_and: bool = False) -> bool:

        # id
        if self._id_stmt is not None:
            container.append(self._id_stmt)
        else:
            val = self._id
            if isinstance(val, int):
                val = str(val)
            val = self.create_filter(
                TranscriptionWordOrm.id,
                [(x, '==') for x in split_terms(val, RE_INT_VALUE, '', '')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._id_stmt = val
                container.append(val)

        # segment_id
        if self._segment_id_stmt is not None:
            container.append(self._segment_id_stmt)
        else:
            val = self._segment_id
            if isinstance(val, int):
                val = str(val)
            val = self.create_filter(
                TranscriptionWordOrm.segment_id,
                [(x, '==') for x in split_terms(val, RE_INT_VALUE, '', '')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._segment_id_stmt = val
                container.append(val)
        
        # pos
        if self._pos_stmt is not None:
            container.append(self._pos_stmt)
        else:
            val = self._pos
            if isinstance(val, int):
                val = str(val)
            val = self.create_filter(
                TranscriptionWordOrm.pos,
                split_numeric(val),
                use_and
            )
            if val is not None:
                self._has_params = True
                self._pos_stmt = val
                container.append(val)
        
        # start
        if self._start_stmt is not None:
            container.append(self._start_stmt)
        else:
            val = self._start
            if isinstance(val, (int, float)):
                val = str(val)
            val = self.create_filter(
                TranscriptionWordOrm.start,
                split_numeric(val),
                use_and
            )
            if val is not None:
                self._has_params = True
                self._start_stmt = val
                container.append(val)
        
        # end
        if self._end_stmt is not None:
            container.append(self._end_stmt)
        else:
            val = self._end
            if isinstance(val, (int, float)):
                val = str(val)
            val = self.create_filter(
                TranscriptionWordOrm.end,
                split_numeric(val),
                use_and
            )
            if val is not None:
                self._has_params = True
                self._end_stmt = val
                container.append(val)
        
        # text
        if self._text_stmt is not None:
            container.append(self._text_stmt)
        else:
            val = self._text
            val = self.create_filter(
                TranscriptionWordOrm.text,
                [(x, 'like') for x in split_terms(val, RE_ALPHANUM_TERM, '%', '%')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._text_stmt = val
                container.append(val)
        
        # confidence
        if self._confidence_stmt is not None:
            container.append(self._confidence_stmt)
        else:
            val = self._confidence
            if isinstance(val, (int, float)):
                val = str(val)
            val = self.create_filter(
                TranscriptionWordOrm.confidence,
                split_numeric(val),
                use_and
            )
            if val is not None:
                self._has_params = True
                self._confidence_stmt = val
                container.append(val)

        # Return True if conditions were added
        return self._has_params
    

    # Getters for raw values

    @property
    def id(self) -> int | str | None:
        return self._id
    
    @property
    def segment_id(self) -> int | str | None:
        return self._segment_id
    
    @property
    def pos(self) -> int | str | None:
        return self._pos
    
    @property
    def start(self) -> int | float | str | None:
        return self._start
    
    @property
    def end(self) -> int | float | str | None:
        return self._end
    
    @property
    def text(self) -> str | None:
        return self._text
    
    @property
    def confidence(self) -> int | float | str | None:
        return self._confidence


    # Getters for statements
    
    @property
    def id_stmt(self) -> typing.Any:
        return self._id_stmt
    
    @property
    def segment_id_stmt(self) -> typing.Any:
        return self._segment_id_stmt
    
    @property
    def pos_stmt(self) -> typing.Any:
        return self._pos_stmt
    
    @property
    def start_stmt(self) -> typing.Any:
        return self._start_stmt
    
    @property
    def end_stmt(self) -> typing.Any:
        return self._end_stmt
    
    @property
    def text_stmt(self) -> typing.Any:
        return self._text_stmt
    
    @property
    def confidence_stmt(self) -> typing.Any:
        return self._confidence_stmt



### Define Interfaces

class TranscriptionWordQueryParamsPrimary(TranscriptionWordQueryParams):
    def __init__(self,
        id: int | str | None = None,
        segment: int | str | None = None,
        segment_id: int | str | None = None,
        pos: int | str | None = None,
        start: int | float | str | None = None,
        end: int | float | str | None = None,
        text: str | None = None,
        confidence: int | float | str | None = None
    ):
        super().__init__(
            id,
            segment_id if segment_id is not None else segment,
            pos,
            start,
            end,
            text,
            confidence
        )


class TranscriptionWordQueryParamsGeneral(TranscriptionWordQueryParams):
    def __init__(self,
        transcription_word_id: int | str | None = None,
        transcription_word_segment: int | str | None = None,
        transcription_word_segment_id: int | str | None = None,
        transcription_word_pos: int | str | None = None,
        transcription_word_start: int | float | str | None = None,
        transcription_word_end: int | float | str | None = None,
        transcription_word_text: str | None = None,
        transcription_word_confidence: int | float | str | None = None
    ):
        super().__init__(
            transcription_word_id,
            transcription_word_segment_id if transcription_word_segment_id is not None else transcription_word_segment,
            transcription_word_pos,
            transcription_word_start,
            transcription_word_end,
            transcription_word_text,
            transcription_word_confidence
        )


