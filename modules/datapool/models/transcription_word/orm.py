# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import typing
import datetime

import sqlalchemy
from sqlalchemy.orm import (
    Mapped,
    mapped_column,
    relationship,
    validates
)

from regex_patterns import (
    RE_INT_VALUE,
    RE_FLOAT_VALUE,
    RE_UUID4,
    RE_ALPHANUM_TERM,
    RE_LANG_CODE,
    RE_FIELD_NAME
)
from data_utils import (
    is_empty, is_true,
    split_terms,
    split_numeric,
    split_datetime,
    sanitize_text
)
from ..base import BaseOrm





class TranscriptionWordOrm(BaseOrm):
    __tablename__ = 'transcription_words'
    id: Mapped[int] = mapped_column(
        sqlalchemy.Integer, primary_key=True
    )
    segment_id: Mapped[int] = mapped_column(sqlalchemy.ForeignKey('transcription_segments.id'))
    pos: Mapped[int] = mapped_column(sqlalchemy.Integer, default=0)
    start: Mapped[float] = mapped_column(sqlalchemy.Float, default=-1.0)
    end: Mapped[float] = mapped_column(sqlalchemy.Float, default=-1.0)
    text: Mapped[str] = mapped_column(sqlalchemy.String)
    confidence: Mapped[float] = mapped_column(sqlalchemy.Float, default=-1.0)

    segment: Mapped['TranscriptionSegmentOrm'] = relationship(back_populates='words')
    
    def __repr__(self):
        return (
            f'<TranscriptionWord('
            f'id={self.id}, '
            f'segment_id={self.segment_id}, '
            f'pos={self.pos}, '
            f'start={self.start}, '
            f'end={self.end}, '
            f'text={self.text}, '
            f'confidence={self.confidence})>'
        )
    
    def __str__(self):
        return (
            f'TranscriptionWord('
            f'id={self.id}, '
            f'segment_id={self.segment_id}, '
            f'pos={self.pos}, '
            f'start={self.start}, '
            f'end={self.end}, '
            f'text={self.text}, '
            f'confidence={self.confidence})'
        )
    
    def __json__(self):
        return {
            'id': self.id,
            'segment_id': self.segment_id,
            'pos': self.pos,
            'start': self.start,
            'end': self.end,
            'text': self.text,
            'confidence': self.confidence
        }
    
    def _fields(self) -> tuple:
        return (
            ['id', 'segment_id', 'pos', 'start', 'end', 'text', 'confidence'],
            [('segment', False, None)]
        )
    

    @validates('segment_id')
    def validate_segment_id(self, key, value):
        if is_empty(value):
            raise ValueError("Segment ID is required")
        if isinstance(value, int):
            return value
        if isinstance(value, str):
            result = RE_INT_VALUE.match(value)
            if result is None:
                raise ValueError("Invalid segment ID")
            return int(result.group(1))
        raise ValueError("Invalid segment ID")

    @validates('pos')
    def validate_pos(self, key, value):
        if is_empty(value):
            raise ValueError("Position is required")
        if isinstance(value, int):
            return value
        if isinstance(value, str):
            result = RE_INT_VALUE.match(value)
            if result is None:
                raise ValueError("Invalid position")
            return int(result.group(1))
        raise ValueError("Invalid position")

    @validates('start')
    def validate_start(self, key, value):
        if is_empty(value):
            raise ValueError("Start is required")
        if isinstance(value, (int, float)):
            return float(value)
        if isinstance(value, str):
            result = RE_FLOAT_VALUE.match(value)
            if result is None:
                raise ValueError("Invalid start")
            return float(result.group(1))
        raise ValueError("Invalid start")

    @validates('end')
    def validate_end(self, key, value):
        if is_empty(value):
            raise ValueError("End is required")
        if isinstance(value, (int, float)):
            return float(value)
        if isinstance(value, str):
            result = RE_FLOAT_VALUE.match(value)
            if result is None:
                raise ValueError("Invalid end")
            return float(result.group(1))
        raise ValueError("Invalid end")

    @validates('text')
    def validate_text(self, key, value):
        return sanitize_text(value)

    @validates('confidence')
    def validate_confidence(self, key, value):
        if is_empty(value):
            raise ValueError("Confidence is required")
        if isinstance(value, (int, float)):
            return float(value)
        if isinstance(value, str):
            result = RE_FLOAT_VALUE.match(value)
            if result is None:
                raise ValueError("Invalid confidence")
            return float(result.group(1))
        raise ValueError("Invalid confidence")




