# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import typing
import datetime

from regex_patterns import (
    RE_INT_VALUE,
    RE_HOST
)
from data_utils import (
    split_terms,
    split_datetime
)
from ..base import (
    BaseQueryParams
)
from .orm import PeerOrm





class PeerQueryParams(BaseQueryParams):
    __slots__ = [
        '_id',
        '_created_at',
        '_owner_id',
        '_host',

        '_id_stmt',
        '_created_at_stmt',
        '_owner_id_stmt',
        '_host_stmt'
    ]

    def __init__(self, 
        id: int | str | None = None,
        created_at: int | float | str | None = None,
        owner_id: str | None = None,
        host: str | None = None
    ):
        super().__init__()

        # Store raw values
        self._id = id
        self._created_at = created_at
        self._owner_id = owner_id
        self._host = host

        # Init statement values
        self._id_stmt = None
        self._created_at_stmt = None
        self._owner_id_stmt = None
        self._host_stmt = None


    def add_conditions(self, container: list, use_and: bool = False) -> bool:

        # id
        if self._id_stmt is not None:
            container.append(self._id_stmt)
        else:
            val = self._id
            if isinstance(val, int):
                val = str(val)
            val = self.create_filter(
                PeerOrm.id,
                [(x, '==') for x in split_terms(val, RE_INT_VALUE, '', '')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._id_stmt = val
                container.append(val)
        
        # created_at
        if self._created_at_stmt is not None:
            container.append(self._created_at_stmt)
        else:
            val = self._created_at
            val = self.create_filter(
                PeerOrm.created_at,
                split_datetime(val),
                use_and
            )
            if val is not None:
                self._has_params = True
                self._created_at_stmt = val
                container.append(val)

        # owner_id
        if self._owner_id_stmt is not None:
            container.append(self._owner_id_stmt)
        else:
            val = self._owner_id
            if isinstance(val, int):
                val = str(val)
            val = self.create_filter(
                PeerOrm.owner_id,
                [(x, '==') for x in split_terms(val, RE_INT_VALUE, '', '')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._owner_id_stmt = val
                container.append(val)

        # host
        if self._host_stmt is not None:
            container.append(self._host_stmt)
        else:
            val = self._host
            val = self.create_filter(
                PeerOrm.host,
                [(x, 'like') for x in split_terms(val, RE_HOST, '%', '%')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._host_stmt = val
                container.append(val)

        # Return True if conditions were added
        return self._has_params
    

    # Getters for raw values

    @property
    def id(self) -> int | str | None:
        return self._id
    
    @property
    def created_at(self) -> int | float | str | None:
        return self._created_at
    
    @property
    def owner_id(self) -> int | str | None:
        return self._owner_id
    
    @property
    def host(self) -> str | None:
        return self._host
    

    # Getters for statements
    
    @property
    def id_stmt(self) -> typing.Any:
        return self._id_stmt
    
    @property
    def created_at_stmt(self) -> typing.Any:
        return self._created_at_stmt
    
    @property
    def owner_id_stmt(self) -> typing.Any:
        return self._owner_id_stmt
    
    @property
    def host_stmt(self) -> typing.Any:
        return self._host_stmt



### Define Interfaces

class PeerQueryParamsPrimary(PeerQueryParams):
    def __init__(self,
        id: int | str | None = None,
        created_at: int | float | str | None = None,
        owner_id: int | str | None = None,
        host: str | None = None
    ):
        super().__init__(
            id,
            created_at,
            owner_id,
            host
        )


class PeerQueryParamsGeneral(PeerQueryParams):
    def __init__(self,
        peer_id: int | str | None = None,
        peer_created_at: int | float | str | None = None,
        peer_owner_id: int | str | None = None,
        peer_host: str | None = None
    ):
        super().__init__(
            peer_id,
            peer_created_at,
            peer_owner_id,
            peer_host
        )


