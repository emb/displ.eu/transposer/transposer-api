# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import re
import typing
from .orm import PeerOrm





PEER__FIELDS_MAP = {
    'peer': {
        'id': PeerOrm.id,
        'created_at': PeerOrm.created_at,
        'owner': PeerOrm.owner_id,
        'owner_id': PeerOrm.owner_id,
        'host': PeerOrm.host
    },
    'peer_id': PeerOrm.id,
    'peer_created_at': PeerOrm.created_at,
    'peer_owner': PeerOrm.owner_id,
    'peer_owner_id': PeerOrm.owner_id,
    'peer_host': PeerOrm.host,

    'peer.id': PeerOrm.id,
    'peer.created_at': PeerOrm.created_at,
    'peer.owner': PeerOrm.owner_id,
    'peer.owner_id': PeerOrm.owner_id,
    'peer.host': PeerOrm.host
}

PEER__VARIANTS_MAP = {
    'peer': {
        'id': 'peer.id',
        'created_at': 'peer.created_at',
        'owner': 'peer.owner_id',
        'owner_id': 'peer.owner_id',
        'host': 'peer.host'
    },
    'peer_id': 'peer.id',
    'peer_created_at': 'peer.created_at',
    'peer_owner': 'peer.owner_id',
    'peer_owner_id': 'peer.owner_id',
    'peer_host': 'peer.host',

    'peer.id': 'peer.id',
    'peer.created_at': 'peer.created_at',
    'peer.owner': 'peer.owner_id',
    'peer.owner_id': 'peer.owner_id',
    'peer.host': 'peer.host'
}



############
### Primary

PEER__PRIMARY_FIELDS_MAP = {
    'id': PeerOrm.id,
    'created_at': PeerOrm.created_at,
    'owner': PeerOrm.owner_id,
    'owner_id': PeerOrm.owner_id,
    'host': PeerOrm.host
}

PEER__PRIMARY_VARIANTS_MAP = {
    'id': 'peer.id',
    'created_at': 'peer.created_at',
    'owner': 'peer.owner_id',
    'owner_id': 'peer.owner_id',
    'host': 'peer.host'
}


