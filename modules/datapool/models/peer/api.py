# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

from pydantic import BaseModel
from ..base import (
    TrpPrimaryKey_UserID
)
from ..peer_connection import (
    NewPeerConnectionStructurePartialModel,
    UpdatePeerConnectionStructureModel,
    UpdatePeerConnectionPartialModel,
    UpdatePeerConnectionStructurePartialModel
)
from ..peer_access_token import (
    NewPeerAccessTokenPartialModel,
    UpdatePeerAccessTokenModel
)
from ..peer_bearer_token import (
    NewPeerBearerTokenPartialModel,
    UpdatePeerBearerTokenModel
)





class PeerModel(BaseModel):
    id: int
    owner: TrpPrimaryKey_UserID
    host: str


class NewPeerModel(BaseModel):
    owner: TrpPrimaryKey_UserID
    host: str

class NewPeerStructureModel(NewPeerModel):
    connections: list[NewPeerConnectionStructurePartialModel] | None = None
    access_tokens: list[NewPeerAccessTokenPartialModel] | None = None
    bearer_tokens: list[NewPeerBearerTokenPartialModel] | None = None


class UpdatePeerModel(BaseModel):
    host: str | None = None

class UpdatePeerStructureModel(UpdatePeerModel):
    connections: list[UpdatePeerConnectionStructurePartialModel] | None = None
    access_tokens: list[UpdatePeerAccessTokenModel] | None = None
    bearer_tokens: list[UpdatePeerBearerTokenModel] | None = None

