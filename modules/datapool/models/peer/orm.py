# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import typing
import datetime
import validators

import sqlalchemy
from sqlalchemy.orm import (
    Mapped,
    mapped_column,
    relationship,
    validates
)

from regex_patterns import (
    RE_INT_VALUE,
    RE_HOST
)
from data_utils import (
    is_empty,
    split_terms,
    split_datetime
)
from ..base import BaseOrm





class PeerOrm(BaseOrm):
    __tablename__ = 'peers'
    id: Mapped[int] = mapped_column(
        sqlalchemy.Integer, primary_key=True
    )
    created_at: Mapped[datetime.datetime] = mapped_column(
        sqlalchemy.DateTime, default=sqlalchemy.sql.functions.now()
    )
    owner_id: Mapped[int] = mapped_column(sqlalchemy.ForeignKey('users.id'))
    host: Mapped[str] = mapped_column(sqlalchemy.String)

    owner: Mapped['UserOrm'] = relationship(
        back_populates='peers'
    )
    connections: Mapped[typing.List['PeerConnectionOrm']] = relationship(
        back_populates='peer',
        primaryjoin="(PeerOrm.id==PeerConnectionOrm.peer_id)"
    )
    access_tokens: Mapped[typing.List['PeerAccessTokenOrm']] = relationship(
        back_populates='peer',
        primaryjoin="(PeerOrm.id==PeerAccessTokenOrm.peer_id)"
    )
    bearer_tokens: Mapped[typing.List['PeerBearerTokenOrm']] = relationship(
        back_populates='peer',
        primaryjoin="(PeerOrm.id==PeerBearerTokenOrm.peer_id)"
    )
    
    def __repr__(self):
        return (
            f'<Peer('
            f'id={self.id}, '
            f'owner_id={self.owner_id}, '
            f'host={self.host})>'
        )
    
    def __str__(self):
        return (
            f'Peer('
            f'id={self.id}, '
            f'owner_id={self.owner_id}, '
            f'host={self.host})'
        )
    
    def __json__(self):
        return {
            'id': self.id,
            'created_at': self.created_at,
            'owner_id': self.owner_id,
            'host': self.host
        }
    
    def _fields(self) -> tuple:
        return (
            ['id', 'created_at', 'owner_id', 'host'],
            [('owner', False, None), ('connections', True, None), ('access_tokens', True, None), ('bearer_tokens', True, None)]
        )
    

    @validates('owner_id')
    def validate_owner_id(self, key, value):
        if is_empty(value):
            raise ValueError("Owner ID is required")
        if isinstance(value, int):
            return value
        if isinstance(value, str):
            result = RE_INT_VALUE.match(value)
            if result is None:
                raise ValueError("Invalid owner ID")
            return int(result.group(1))
        raise ValueError("Invalid owner ID")
    
    @validates('host')
    def validate_host(self, key, value):
        if is_empty(value):
            raise ValueError("Domain is required")
        validation = validators.url(value)
        if validation:
            return value
        raise ValueError("Invalid host")



