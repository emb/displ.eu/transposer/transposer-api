# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import typing

from regex_patterns import (
    RE_INT_VALUE
)
from data_utils import (
    split_terms
)
from ..base import BaseQueryParams
from .orm import PermissionOrm





class PermissionQueryParams(BaseQueryParams):
    __slots__ = [
        '_id',
        '_role_id',
        '_resource_key',
        '_actions',
        '_conditions',

        '_id_stmt',
        '_role_id_stmt',
        '_resource_key_stmt',
        '_actions_stmt',
        '_conditions_stmt'
    ]

    def __init__(self, 
        id: int | str | None = None,
        role_id: int | str | None = None,
        resource_key: str | None = None,
        actions: str | list | None = None,
        conditions: str | dict | None = None
    ):
        super().__init__()

        # Store raw values
        self._id = id
        self._role_id = role_id
        self._resource_key = resource_key
        self._actions = actions
        self._conditions = conditions

        # Init statement values
        self._id_stmt = None
        self._role_id_stmt = None
        self._resource_key_stmt = None
        self._actions_stmt = None
        self._conditions_stmt = None


    def add_conditions(self, container: list, use_and: bool = False) -> bool:

        # id
        if self._id_stmt is not None:
            container.append(self._id_stmt)
        else:
            val = self._id
            if isinstance(val, int):
                val = str(val)
            val = self.create_filter(
                PermissionOrm.id,
                [(x, '==') for x in split_terms(val, RE_INT_VALUE, '', '')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._id_stmt = val
                container.append(val)

        # role_id
        if self._role_id_stmt is not None:
            container.append(self._role_id_stmt)
        else:
            val = self._role_id
            if isinstance(val, int):
                val = str(val)
            val = self.create_filter(
                PermissionOrm.role_id,
                [(x, '==') for x in split_terms(val, RE_INT_VALUE, '', '')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._role_id_stmt = val
                container.append(val)

        # resource_key
        if self._resource_key_stmt is not None:
            container.append(self._resource_key_stmt)
        else:
            val = self._resource_key
            if isinstance(val, int):
                val = str(val)
            val = self.create_filter(
                PermissionOrm.resource_key,
                [(x, '==') for x in split_terms(val, RE_INT_VALUE, '', '')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._resource_key_stmt = val
                container.append(val)

        # Return True if conditions were added
        return self._has_params
    

    # Getters for raw values

    @property
    def id(self) -> int | str | None:
        return self._id
    
    @property
    def role_id(self) -> int | str | None:
        return self._role_id
    
    @property
    def resource_key(self) -> int | str | None:
        return self._resource_key
    
    @property
    def actions(self) -> str | list | None:
        return self._actions
    
    @property
    def conditions(self) -> str | dict | None:
        return self._conditions
    

    # Getters for statements
    
    @property
    def id_stmt(self) -> typing.Any:
        return self._id_stmt
    
    @property
    def role_id_stmt(self) -> typing.Any:
        return self._role_id_stmt
    
    @property
    def resource_key_stmt(self) -> typing.Any:
        return self._resource_key_stmt
    
    @property
    def actions_stmt(self) -> typing.Any:
        return self._actions_stmt
    
    @property
    def conditions_stmt(self) -> typing.Any:
        return self._conditions_stmt



### Define Interfaces

class PermissionQueryParamsPrimary(PermissionQueryParams):
    def __init__(self,
        id: int | str | None = None,
        role: int | str | None = None,
        role_id: int | str | None = None,
        resource_key: int | str | None = None,
        actions: str | list | None = None,
        conditions: str | dict | None = None
    ):
        super().__init__(
            id,
            role_id if role_id is not None else role,
            resource_key,
            actions,
            conditions
        )


class PermissionQueryParamsGeneral(PermissionQueryParams):
    def __init__(self,
        permission_id: int | str | None = None,
        permission_role: int | str | None = None,
        permission_role_id: int | str | None = None,
        permission_resource_key: int | str | None = None,
        permission_actions: str | list | None = None,
        permission_conditions: str | dict | None = None
    ):
        super().__init__(
            permission_id,
            permission_role_id if permission_role_id is not None else permission_role,
            permission_resource_key,
            permission_actions,
            permission_conditions
        )


