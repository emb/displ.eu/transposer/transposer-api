# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

from .orm import PermissionOrm





PERMISSION__FIELDS_MAP = {
    'permission': {
        'id': PermissionOrm.id,
        'role': PermissionOrm.role_id,
        'role_id': PermissionOrm.role_id,
        'resource_key': PermissionOrm.resource_key,
        'actions': PermissionOrm.actions,
        'conditions': PermissionOrm.conditions
    },
    'permission_id': PermissionOrm.id,
    'permission_role': PermissionOrm.role_id,
    'permission_role_id': PermissionOrm.role_id,
    'permission_resource_key': PermissionOrm.resource_key,
    'permission_actions': PermissionOrm.actions,
    'permission_conditions': PermissionOrm.conditions,

    'permission.id': PermissionOrm.id,
    'permission.role': PermissionOrm.role_id,
    'permission.role_id': PermissionOrm.role_id,
    'permission.resource_key': PermissionOrm.resource_key,
    'permission.actions': PermissionOrm.actions,
    'permission.conditions': PermissionOrm.conditions
}

PERMISSION__VARIANTS_MAP = {
    'permission': {
        'id': 'permission.id',
        'role': 'permission.role_id',
        'role_id': 'permission.role_id',
        'resource_key': 'permission.resource_key',
        'actions': 'permission.actions',
        'conditions': 'permission.conditions'
    },
    'permission_id': 'permission.id',
    'permission_role': 'permission.role_id',
    'permission_role_id': 'permission.role_id',
    'permission_resource_key': 'permission.resource_key',
    'permission_actions': 'permission.actions',
    'permission_conditions': 'permission.conditions',

    'permission.id': 'permission.id',
    'permission.role': 'permission.role_id',
    'permission.role_id': 'permission.role_id',
    'permission.resource_key': 'permission.resource_key',
    'permission.actions': 'permission.actions',
    'permission.conditions': 'permission.conditions'
}



############
### Primary

PERMISSION__PRIMARY_FIELDS_MAP = {
    'id': PermissionOrm.id,
    'role': PermissionOrm.role_id,
    'role_id': PermissionOrm.role_id,
    'resource_key': PermissionOrm.resource_key,
    'actions': PermissionOrm.actions,
    'conditions': PermissionOrm.conditions
}

PERMISSION__PRIMARY_VARIANTS_MAP = {
    'id': 'permission.id',
    'role': 'permission.role_id',
    'role_id': 'permission.role_id',
    'resource_key': 'permission.resource_key',
    'actions': 'permission.actions',
    'conditions': 'permission.conditions'
}


