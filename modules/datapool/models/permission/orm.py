# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import typing
import json

import sqlalchemy
from sqlalchemy.orm import (
    Mapped,
    mapped_column,
    relationship,
    validates
)
from sqlalchemy.dialects.postgresql import JSONB

from regex_patterns import (
    RE_INT_VALUE,
    RE_FIELD_NAME
)
from data_utils import (
    is_empty,
    split_terms
)
from ..base import BaseOrm





class PermissionOrm(BaseOrm):
    __tablename__ = 'permissions'
    id: Mapped[int] = mapped_column(
        sqlalchemy.Integer, primary_key=True
    )
    role_id: Mapped[int] = mapped_column(sqlalchemy.ForeignKey('roles.id'))
    resource_key: Mapped[str] = mapped_column(sqlalchemy.String)
    actions: Mapped[typing.Optional[JSONB]] = mapped_column(type_=JSONB, default='[]')
    conditions: Mapped[typing.Optional[JSONB]] = mapped_column(type_=JSONB, default='{}')

    role: Mapped['RoleOrm'] = relationship(
        back_populates='permissions'
    )
    
    def __repr__(self):
        return (
            f'<Permission('
            f'id={self.id}, '
            f'role_id={self.role_id}, '
            f'resource_key={self.resource_key}, '
            f'actions={self.actions}), '
            f'conditions={self.conditions})>'
        )
    
    def __str__(self):
        return (
            f'Permission('
            f'id={self.id}, '
            f'role_id={self.role_id}, '
            f'resource_key={self.resource_key}, '
            f'actions={self.actions}), '
            f'conditions={self.conditions})'
        )
    
    def __json__(self):
        return {
            'id': self.id,
            'role_id': self.role_id,
            'resource_key': self.resource_key,
            'actions': self.actions,
            'conditions': self.conditions
        }
    
    def _fields(self) -> tuple:
        return (
            ['id', 'role_id', 'resource_key', 'actions', 'conditions'],
            [('role', False, None)]
        )
    
    
    @validates('role_id')
    def validate_role_id(self, key, value):
        if is_empty(value):
            raise ValueError("Role ID is required")
        if isinstance(value, int):
            return value
        if isinstance(value, str):
            result = RE_INT_VALUE.match(value)
            if result is None:
                raise ValueError("Invalid role ID")
            return int(result.group(1))
        raise ValueError("Invalid role ID")

    @validates('resource_key')
    def validate_resource_key(self, key, value):
        if is_empty(value):
            raise ValueError("Resource key is required")
        if isinstance(value, str):
            result = RE_FIELD_NAME.match(value)
            if result is None:
                raise ValueError("Invalid resource key")
            return result.group(1)
        raise ValueError("Invalid resource key")

    @validates('actions')
    def validate_actions(self, key, value):
        if is_empty(value):
            return []
        if isinstance(value, list):
            return value
        if not isinstance(value, str):
            raise ValueError("Invalid actions")
        try:
            value = json.loads(value)
            return value
        except json.JSONDecodeError:
            raise ValueError("Invalid actions")
    
    @validates('conditions')
    def validate_conditions(self, key, value):
        if is_empty(value):
            return {}
        if isinstance(value, dict):
            return value
        if not isinstance(value, str):
            raise ValueError("Invalid conditions")
        try:
            value = json.loads(value)
            return value
        except json.JSONDecodeError:
            raise ValueError("Invalid conditions")




