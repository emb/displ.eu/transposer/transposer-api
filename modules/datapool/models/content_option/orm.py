# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import typing
import datetime

import sqlalchemy
from sqlalchemy.orm import (
    Mapped,
    mapped_column,
    relationship,
    validates
)

from regex_patterns import (
    RE_INT_VALUE,
    RE_FLOAT_VALUE,
    RE_UUID4,
    RE_ALPHANUM_TERM,
    RE_SLUG
)
from data_utils import (
    is_empty, is_true,
    split_terms,
    split_numeric,
    split_datetime,
    sanitize_text
)
from ..base import BaseOrm





class ContentOptionOrm(BaseOrm):
    __tablename__ = 'content_options'
    id: Mapped[int] = mapped_column(
        sqlalchemy.Integer, primary_key=True
    )
    parent_id: Mapped[int] = mapped_column(sqlalchemy.ForeignKey('contents.id', ondelete='cascade'))
    key: Mapped[str] = mapped_column(sqlalchemy.String)
    value: Mapped[str] = mapped_column(sqlalchemy.String)
    datatype: Mapped[str] = mapped_column(sqlalchemy.String, default='string')

    content: Mapped['ContentOrm'] = relationship(back_populates='options')
    
    def __repr__(self):
        return (
            f'<ContentOption('
            f'id={self.id}, '
            f'parent_id={self.parent_id}, '
            f'key={self.key}, '
            f'value={self.value}, '
            f'datatype={self.datatype})>'
        )
    
    def __str__(self):
        return (
            f'ContentOption('
            f'id={self.id}, '
            f'parent_id={self.parent_id}, '
            f'key={self.key}, '
            f'value={self.value}, '
            f'datatype={self.datatype})'
        )
    
    def __json__(self):
        return {
            'id': self.id,
            'parent_id': self.parent_id,
            'key': self.key,
            'value': self.value,
            'datatype': self.datatype
        }
    
    def _fields(self) -> tuple:
        return (
            ['id', 'parent_id', 'key', 'value', 'datatype'],
            [('content', False, None)]
        )
    

    @validates('parent_id')
    def validate_parent_id(self, key, value):
        if is_empty(value):
            raise ValueError("Parent ID is required")
        if isinstance(value, int):
            return value
        if isinstance(value, str):
            result = RE_INT_VALUE.match(value)
            if result is None:
                raise ValueError("Invalid parent ID")
            return int(result.group(1))
        raise ValueError("Invalid parent ID")

    @validates('key')
    def validate_key(self, key, value):
        if is_empty(value):
            raise ValueError("Key is required")
        if isinstance(value, str):
            result = RE_SLUG.match(value)
            if result is None:
                raise ValueError("Invalid key")
            return int(result.group(1))
        raise ValueError("Invalid key")

    @validates('value')
    def validate_value(self, key, value):
        if is_empty(value):
            raise ValueError("Value is required")
        return sanitize_text(value)

    @validates('datatype')
    def validate_datatype(self, key, value):
        if is_empty(value):
            raise ValueError("Datatype is required")
        if isinstance(value, str):
            result = RE_SLUG.match(value)
            if result is None:
                raise ValueError("Invalid datatype")
            return int(result.group(1))
        raise ValueError("Invalid datatype")


