# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

from .orm import ContentOptionOrm





CAPTION_OPTION__FIELDS_MAP = {
    'content_option': {
        'id': ContentOptionOrm.id,
        'parent': ContentOptionOrm.parent_id,
        'parent_id': ContentOptionOrm.parent_id,
        'key': ContentOptionOrm.key,
        'value': ContentOptionOrm.value,
        'datatype': ContentOptionOrm.datatype
    },
    'content_option_id': ContentOptionOrm.id,
    'content_option_parent': ContentOptionOrm.parent_id,
    'content_option_parent_id': ContentOptionOrm.parent_id,
    'content_option_key': ContentOptionOrm.key,
    'content_option_value': ContentOptionOrm.value,
    'content_option_datatype': ContentOptionOrm.datatype,

    'content_option.id': ContentOptionOrm.id,
    'content_option.parent': ContentOptionOrm.parent_id,
    'content_option.parent_id': ContentOptionOrm.parent_id,
    'content_option.key': ContentOptionOrm.key,
    'content_option.value': ContentOptionOrm.value,
    'content_option.datatype': ContentOptionOrm.datatype
}

CAPTION_OPTION__VARIANTS_MAP = {
    'content_option': {
        'id': 'content_option.id',
        'parent': 'content_option.parent_id',
        'parent_id': 'content_option.parent_id',
        'key': 'content_option.key',
        'value': 'content_option.value',
        'datatype': 'content_option.datatype'
    },
    'content_option_id': 'content_option.id',
    'content_option_parent': 'content_option.parent_id',
    'content_option_parent_id': 'content_option.parent_id',
    'content_option_key': 'content_option.key',
    'content_option_value': 'content_option.value',
    'content_option_datatype': 'content_option.datatype',

    'content_option.id': 'content_option.id',
    'content_option.parent': 'content_option.parent_id',
    'content_option.parent_id': 'content_option.parent_id',
    'content_option.key': 'content_option.key',
    'content_option.value': 'content_option.value',
    'content_option.datatype': 'content_option.datatype'
}



############
### Primary

CAPTION_OPTION__PRIMARY_FIELDS_MAP = {
    'id': ContentOptionOrm.id,
    'parent': ContentOptionOrm.parent_id,
    'parent_id': ContentOptionOrm.parent_id,
    'key': ContentOptionOrm.key,
    'value': ContentOptionOrm.value,
    'datatype': ContentOptionOrm.datatype
}

CAPTION_OPTION__PRIMARY_VARIANTS_MAP = {
    'id': 'content_option.id',
    'parent': 'content_option.parent_id',
    'parent_id': 'content_option.parent_id',
    'key': 'content_option.key',
    'value': 'content_option.value',
    'datatype': 'content_option.datatype'
}


