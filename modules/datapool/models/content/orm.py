# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import typing
import datetime

import sqlalchemy
from sqlalchemy.orm import (
    Mapped,
    mapped_column,
    relationship,
    validates
)

from regex_patterns import (
    RE_INT_VALUE,
    RE_FLOAT_VALUE,
    RE_UUID4,
    RE_ALPHANUM_TERM,
    RE_LANG_CODE,
    RE_FIELD_NAME,
    RE_SLUG
)
from data_utils import (
    is_empty, is_true,
    split_terms,
    split_numeric,
    split_datetime,
    sanitize_text
)
from ..base import (
    BaseOrm,
    Property_Datatype
)





class ContentOrm(BaseOrm):
    __tablename__ = 'contents'
    id: Mapped[int] = mapped_column(
        sqlalchemy.Integer, primary_key=True
    )
    created_at: Mapped[datetime.datetime] = mapped_column(
        sqlalchemy.DateTime, default=sqlalchemy.sql.functions.now()
    )
    document_id: Mapped[int] = mapped_column(sqlalchemy.ForeignKey('documents.id'))
    pos: Mapped[int] = mapped_column(sqlalchemy.Integer, default=0)
    lang: Mapped[typing.Optional[str]] = mapped_column(sqlalchemy.String, default=None)
    is_original: Mapped[bool] = mapped_column(sqlalchemy.Boolean, default=False)
    confidence: Mapped[float] = mapped_column(sqlalchemy.Float, default=-1.0)
    key: Mapped[typing.Optional[str]] = mapped_column(sqlalchemy.String, default='content')
    value: Mapped[str] = mapped_column(sqlalchemy.String)
    datatype: Mapped[Property_Datatype] = mapped_column(default='STRING')
    status: Mapped[int] = mapped_column(sqlalchemy.Integer, default=0)
    reason: Mapped[typing.Optional[str]] = mapped_column(sqlalchemy.String, default=None)

    document: Mapped['DocumentOrm'] = relationship(back_populates='contents')
    captions: Mapped[typing.List['CaptionOrm']] = relationship(
        back_populates='content',
        primaryjoin="(ContentOrm.id==CaptionOrm.content_id)"
    )
    options: Mapped[typing.List['ContentOptionOrm']] = relationship(
        back_populates='content',
        primaryjoin="(ContentOrm.id==ContentOptionOrm.parent_id)"
    )
    
    def __repr__(self):
        return (
            f'<Content('
            f'id={self.id}, '
            f'document_id={self.document_id}, '
            f'pos={self.pos}, '
            f'lang={self.lang}, '
            f'confidence={self.confidence}, '
            f'key={self.key}, '
            f'value={self.value}, '
            f'datatype={self.datatype}, '
            f'status={self.status}, '
            f'reason={self.reason})>'
        )
    
    def __str__(self):
        return (
            f'Content('
            f'id={self.id}, '
            f'document_id={self.document_id}, '
            f'pos={self.pos}, '
            f'lang={self.lang}, '
            f'confidence={self.confidence}, '
            f'key={self.key}, '
            f'value={self.value}, '
            f'datatype={self.datatype}, '
            f'status={self.status}, '
            f'reason={self.reason})'
        )
    
    def __json__(self):
        return {
            'id': self.id,
            'created_at': self.created_at,
            'document_id': self.document_id,
            'pos': self.pos,
            'lang': self.lang,
            'is_original': self.is_original,
            'confidence': self.confidence,
            'key': self.key,
            'value': self.value,
            'datatype': self.datatype,
            'status': self.status,
            'reason': self.reason
        }
    
    def _fields(self) -> tuple:
        return (
            ['id', 'created_at', 'document_id', 'pos', 'lang', 'is_original', 'confidence', 'key', 'value', 'datatype', 'status', 'reason'],
            [('document', False, None), ('captions', True, None), ('options', True, self._transform_options)]
        )
    

    @validates('document_id')
    def validate_document_id(self, key, value):
        if is_empty(value):
            raise ValueError("Document ID is required")
        if isinstance(value, int):
            return value
        if isinstance(value, str):
            result = RE_INT_VALUE.match(value)
            if result is None:
                raise ValueError("Invalid document ID")
            return int(result.group(1))
        raise ValueError("Invalid document ID")

    @validates('pos')
    def validate_pos(self, key, value):
        if is_empty(value):
            raise ValueError("Position is required")
        if isinstance(value, int):
            return value
        if isinstance(value, str):
            result = RE_INT_VALUE.match(value)
            if result is None:
                raise ValueError("Invalid position")
            return int(result.group(1))
        raise ValueError("Invalid position")

    @validates('lang')
    def validate_lang(self, key, value):
        if is_empty(value):
            return None
        if isinstance(value, str):
            result = RE_LANG_CODE.match(value)
            if result is None:
                raise ValueError("Invalid language code")
            return result.group(1)
        raise ValueError("Invalid language code")

    @validates('is_original')
    def validate_is_original(self, key, value):
        return is_true(value)

    @validates('confidence')
    def validate_confidence(self, key, value):
        if is_empty(value):
            raise ValueError("Confidence is required")
        if isinstance(value, (int, float)):
            return float(value)
        if isinstance(value, str):
            result = RE_INT_VALUE.match(value)
            if result is None:
                result = RE_FLOAT_VALUE.match(value)
                if result is None:
                    raise ValueError("Invalid confidence")
                return float(result.group(1))
            return int(result.group(1))
        raise ValueError("Invalid confidence")

    @validates('key')
    def validate_key(self, key, value):
        if is_empty(value):
            raise ValueError("Key is required")
        if isinstance(value, str):
            result = RE_FIELD_NAME.match(value)
            if result is None:
                raise ValueError("Invalid key")
            return result.group(1)
        raise ValueError("Invalid key")

    @validates('value')
    def validate_value(self, key, value):
        return sanitize_text(value)

    @validates('datatype')
    def validate_datatype(self, key, value):
        if is_empty(value):
            raise ValueError("Datatype is required")
        if isinstance(value, str):
            result = RE_SLUG.match(value)
            if result is None:
                raise ValueError("Invalid datatype")
            return result.group(1)
        raise ValueError("Invalid datatype")

    @validates('status')
    def validate_status(self, key, value):
        if is_empty(value):
            raise ValueError("Status is required")
        if isinstance(value, int):
            return value
        if isinstance(value, str):
            result = RE_INT_VALUE.match(value)
            if result is None:
                raise ValueError("Invalid status")
            return int(result.group(1))
        raise ValueError("Invalid status")

    @validates('reason')
    def validate_reason(self, key, value):
        return sanitize_text(value)


