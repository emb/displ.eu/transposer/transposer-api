# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

from .orm import ContentOrm





CONTENT__FIELDS_MAP = {
    'content': {
        'id': ContentOrm.id,
        'created_at': ContentOrm.created_at,
        'document': ContentOrm.document_id,
        'document_id': ContentOrm.document_id,
        'pos': ContentOrm.pos,
        'lang': ContentOrm.lang,
        'is_original': ContentOrm.is_original,
        'confidence': ContentOrm.confidence,
        'key': ContentOrm.key,
        'value': ContentOrm.value,
        'datatype': ContentOrm.datatype,
        'status': ContentOrm.status,
        'reason': ContentOrm.reason
    },
    'content_id': ContentOrm.id,
    'content_created_at': ContentOrm.created_at,
    'content_document': ContentOrm.document_id,
    'content_document_id': ContentOrm.document_id,
    'content_pos': ContentOrm.pos,
    'content_lang': ContentOrm.lang,
    'content_is_original': ContentOrm.is_original,
    'content_confidence': ContentOrm.confidence,
    'content_key': ContentOrm.key,
    'content_value': ContentOrm.value,
    'content_datatype': ContentOrm.datatype,
    'content_status': ContentOrm.status,
    'content_reason': ContentOrm.reason,

    'content.id': ContentOrm.id,
    'content.created_at': ContentOrm.created_at,
    'content.document': ContentOrm.document_id,
    'content.document_id': ContentOrm.document_id,
    'content.pos': ContentOrm.pos,
    'content.lang': ContentOrm.lang,
    'content.is_original': ContentOrm.is_original,
    'content.confidence': ContentOrm.confidence,
    'content.key': ContentOrm.key,
    'content.value': ContentOrm.value,
    'content.datatype': ContentOrm.datatype,
    'content.status': ContentOrm.status,
    'content.reason': ContentOrm.reason
}

CONTENT__VARIANTS_MAP = {
    'content': {
        'id': 'content.id',
        'created_at': 'content.created_at',
        'document': 'content.document_id',
        'document_id': 'content.document_id',
        'pos': 'content.pos',
        'lang': 'content.lang',
        'is_original': 'content.is_original',
        'confidence': 'content.confidence',
        'key': 'content.key',
        'value': 'content.value',
        'datatype': 'content.datatype',
        'status': 'content.status',
        'reason': 'content.reason'
    },
    'content_id': 'content.id',
    'content_created_at': 'content.created_at',
    'content_document': 'content.document_id',
    'content_document_id': 'content.document_id',
    'content_pos': 'content.pos',
    'content_lang': 'content.lang',
    'content_is_original': 'content.is_original',
    'content_confidence': 'content.confidence',
    'content_key': 'content.key',
    'content_value': 'content.value',
    'content_datatype': 'content.datatype',
    'content_status': 'content.status',
    'content_reason': 'content.reason',

    'content.id': 'content.id',
    'content.created_at': 'content.created_at',
    'content.document': 'content.document_id',
    'content.document_id': 'content.document_id',
    'content.pos': 'content.pos',
    'content.lang': 'content.lang',
    'content.is_original': 'content.is_original',
    'content.confidence': 'content.confidence',
    'content.key': 'content.key',
    'content.value': 'content.value',
    'content.datatype': 'content.datatype',
    'content.status': 'content.status',
    'content.reason': 'content.reason'
}



############
### Primary

CONTENT__PRIMARY_FIELDS_MAP = {
    'id': ContentOrm.id,
    'created_at': ContentOrm.created_at,
    'document': ContentOrm.document_id,
    'document_id': ContentOrm.document_id,
    'pos': ContentOrm.pos,
    'lang': ContentOrm.lang,
    'is_original': ContentOrm.is_original,
    'confidence': ContentOrm.confidence,
    'key': ContentOrm.key,
    'value': ContentOrm.value,
    'datatype': ContentOrm.datatype,
    'status': ContentOrm.status,
    'reason': ContentOrm.reason
}

CONTENT__PRIMARY_VARIANTS_MAP = {
    'id': 'content.id',
    'created_at': 'content.created_at',
    'document': 'content.document_id',
    'document_id': 'content.document_id',
    'pos': 'content.pos',
    'lang': 'content.lang',
    'is_original': 'content.is_original',
    'confidence': 'content.confidence',
    'key': 'content.key',
    'value': 'content.value',
    'datatype': 'content.datatype',
    'status': 'content.status',
    'reason': 'content.reason'
}


