# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

from pydantic import BaseModel
from ..base import (
    TrpTimeMSec,
    TrpPrimaryKey_ID_UUID
)
from ..caption import NewCaptionPartialModel





class ContentModel(BaseModel):
    id: int
    document: TrpPrimaryKey_ID_UUID
    pos: int | None = None
    lang: str | None = None
    is_original: bool = False
    confidence: int | float = -1.0
    key: str = 'content'
    value: str
    datatype: str = 'string'
    status: int = 1
    reason: str | None = None


class NewContenPartialModel(BaseModel):
    pos: int | None = None
    lang: str | None = None
    is_original: bool = False
    confidence: int | float = -1.0
    key: str = 'content'
    value: str
    datatype: str = 'string'
    status: int = 1
    reason: str | None = None
    start: TrpTimeMSec | None = None
    end: TrpTimeMSec | None = None
    identifier: int | None = None
    properties: str | None = None

class NewContentModel(NewContenPartialModel):
    document: TrpPrimaryKey_ID_UUID

class NewContenStructurePartialModel(NewContenPartialModel):
    captions: list[NewCaptionPartialModel] | None = None

class NewContentStructureModel(NewContentModel):
    captions: list[NewCaptionPartialModel] | None = None


class UpdateContentModel(BaseModel):
    pos: int | None = None
    lang: str | None = None
    is_original: bool | None = None
    confidence: int | float | None = None
    key: str | None = None
    value: str | None = None
    datatype: str | None = None
    status: int | None = None
    reason: str | None = None

class UpdateContentStructureModel(UpdateContentModel):
    captions: list[NewCaptionPartialModel] | None = None

