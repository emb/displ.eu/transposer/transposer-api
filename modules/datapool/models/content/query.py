# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import typing
import datetime

from regex_patterns import (
    RE_INT_VALUE,
    RE_ALPHANUM_TERM,
    RE_LANG_CODE,
    RE_FIELD_NAME,
    RE_SLUG
)
from data_utils import (
    split_terms,
    split_numeric,
    split_datetime
)
from ..base import (
    BaseQueryParams
)
from .orm import ContentOrm





class ContentQueryParams(BaseQueryParams):
    __slots__ = [
        '_id',
        '_created_at',
        '_document_id',
        '_pos',
        '_lang',
        '_is_original',
        '_confidence',
        '_key',
        '_value',
        '_datatype',
        '_status',
        '_reason',

        '_id_stmt',
        '_created_at_stmt',
        '_document_id_stmt',
        '_pos_stmt',
        '_lang_stmt',
        '_is_original_stmt',
        '_confidence_stmt',
        '_key_stmt',
        '_value_stmt',
        '_datatype_stmt',
        '_status_stmt',
        '_reason_stmt'
    ]

    def __init__(self, 
        id: int | str | None = None,
        created_at: int | float | str | None = None,
        document_id: int | str | None = None,
        pos: int | str | None = None,
        lang: str | None = None,
        is_original: bool | None = None,
        confidence: int | float | str | None = None,
        key: str | None = None,
        value: str | None = None,
        datatype: str | None = None,
        status: int | str | None = None,
        reason: str | None = None
    ):
        super().__init__()

        # Store raw values
        self._id = id
        self._created_at = created_at
        self._document_id = document_id
        self._pos = pos
        self._lang = lang
        self._is_original = is_original
        self._confidence = confidence
        self._key = key
        self._value = value
        self._datatype = datatype
        self._status = status
        self._reason = reason

        # Init statement values
        self._id_stmt = None
        self._created_at_stmt = None
        self._document_id_stmt = None
        self._pos_stmt = None
        self._lang_stmt = None
        self._is_original_stmt = None
        self._confidence_stmt = None
        self._key_stmt = None
        self._value_stmt = None
        self._datatype_stmt = None
        self._status_stmt = None
        self._reason_stmt = None


    def add_conditions(self, container: list, use_and: bool = False) -> bool:

        # id
        if self._id_stmt is not None:
            container.append(self._id_stmt)
        else:
            val = self._id
            if isinstance(val, int):
                val = str(val)
            val = self.create_filter(
                ContentOrm.id,
                [(x, '==') for x in split_terms(val, RE_INT_VALUE, '', '')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._id_stmt = val
                container.append(val)
        
        # created_at
        if self._created_at_stmt is not None:
            container.append(self._created_at_stmt)
        else:
            val = self._created_at
            val = self.create_filter(
                ContentOrm.created_at,
                split_datetime(val),
                use_and
            )
            if val is not None:
                self._has_params = True
                self._created_at_stmt = val
                container.append(val)

        # document_id
        if self._document_id_stmt is not None:
            container.append(self._document_id_stmt)
        else:
            val = self._document_id
            if isinstance(val, int):
                val = str(val)
            val = self.create_filter(
                ContentOrm.document_id,
                [(x, '==') for x in split_terms(val, RE_INT_VALUE, '', '')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._document_id_stmt = val
                container.append(val)
        
        # pos
        if self._pos_stmt is not None:
            container.append(self._pos_stmt)
        else:
            val = self._pos
            if isinstance(val, int):
                val = str(val)
            val = self.create_filter(
                ContentOrm.pos,
                split_numeric(val),
                use_and
            )
            if val is not None:
                self._has_params = True
                self._pos_stmt = val
                container.append(val)
        
        # lang
        if self._lang_stmt is not None:
            container.append(self._lang_stmt)
        else:
            val = self._lang
            val = self.create_filter(
                ContentOrm.lang,
                [(x, '==') for x in split_terms(val, RE_LANG_CODE, '', '')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._lang_stmt = val
                container.append(val)
        
        # is_original
        if self._is_original_stmt is not None:
            container.append(self._is_original_stmt)
        else:
            val = self._is_original
            if val is not None:
                val = self.create_filter(
                    ContentOrm.is_original,
                    [(val, '==')],
                    use_and
                )
                if val is not None:
                    self._has_params = True
                    self._is_original_stmt = val
                    container.append(val)
        
        # confidence
        if self._confidence_stmt is not None:
            container.append(self._confidence_stmt)
        else:
            val = self._confidence
            if isinstance(val, (int, float)):
                val = str(val)
            val = self.create_filter(
                ContentOrm.confidence,
                split_numeric(val),
                use_and
            )
            if val is not None:
                self._has_params = True
                self._confidence_stmt = val
                container.append(val)
        
        # key
        if self._key_stmt is not None:
            container.append(self._key_stmt)
        else:
            val = self._key
            val = self.create_filter(
                ContentOrm.key,
                [(x, 'like') for x in split_terms(val, RE_FIELD_NAME, '%', '%')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._key_stmt = val
                container.append(val)
        
        # value
        if self._value_stmt is not None:
            container.append(self._value_stmt)
        else:
            val = self._value
            val = self.create_filter(
                ContentOrm.value,
                [(x, 'like') for x in split_terms(val, RE_ALPHANUM_TERM, '%', '%')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._value_stmt = val
                container.append(val)
        
        # datatype
        if self._datatype_stmt is not None:
            container.append(self._datatype_stmt)
        else:
            val = self._datatype
            val = self.create_filter(
                ContentOrm.datatype,
                [(x, '==') for x in split_terms(val, RE_SLUG, '', '')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._datatype_stmt = val
                container.append(val)
        
        # status
        if self._status_stmt is not None:
            container.append(self._status_stmt)
        else:
            val = self._status
            if isinstance(val, int):
                val = str(val)
            val = self.create_filter(
                ContentOrm.status,
                split_numeric(val),
                use_and
            )
            if val is not None:
                self._has_params = True
                self._status_stmt = val
                container.append(val)
        
        # reason
        if self._reason_stmt is not None:
            container.append(self._reason_stmt)
        else:
            val = self._reason
            val = self.create_filter(
                ContentOrm.reason,
                [(x, 'like') for x in split_terms(val, RE_ALPHANUM_TERM, '%', '%')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._reason_stmt = val
                container.append(val)

        # Return True if conditions were added
        return self._has_params
    

    # Getters for raw values

    @property
    def id(self) -> int | str | None:
        return self._id
    
    @property
    def created_at(self) -> int | float | str | None:
        return self._created_at
    
    @property
    def document_id(self) -> int | str | None:
        return self._document_id
    
    @property
    def pos(self) -> int | str | None:
        return self._pos
    
    @property
    def lang(self) -> int | str | None:
        return self._lang
    
    @property
    def is_original(self) -> bool | None:
        return self._is_original
    
    @property
    def confidence(self) -> int | float | str | None:
        return self._confidence
    
    @property
    def key(self) -> str | None:
        return self._key
    
    @property
    def value(self) -> str | None:
        return self._value
    
    @property
    def datatype(self) -> str | None:
        return self._datatype
    
    @property
    def status(self) -> int | str | None:
        return self._status
    
    @property
    def reason(self) -> str | None:
        return self._reason
    

    # Getters for statements
    
    @property
    def id_stmt(self) -> typing.Any:
        return self._id_stmt
    
    @property
    def created_at_stmt(self) -> typing.Any:
        return self._created_at_stmt
    
    @property
    def document_id_stmt(self) -> typing.Any:
        return self._document_id_stmt
    
    @property
    def pos_stmt(self) -> typing.Any:
        return self._pos_stmt
    
    @property
    def lang_stmt(self) -> typing.Any:
        return self._lang_stmt
    
    @property
    def is_original_stmt(self) -> typing.Any:
        return self._is_original_stmt
    
    @property
    def confidence_stmt(self) -> typing.Any:
        return self._confidence_stmt
    
    @property
    def key_stmt(self) -> typing.Any:
        return self._key_stmt
    
    @property
    def value_stmt(self) -> typing.Any:
        return self._value_stmt
    
    @property
    def datatype_stmt(self) -> typing.Any:
        return self._datatype_stmt
    
    @property
    def status_stmt(self) -> typing.Any:
        return self._status_stmt
    
    @property
    def reason_stmt(self) -> typing.Any:
        return self._reason_stmt



### Define Interfaces

class ContentQueryParamsPrimary(ContentQueryParams):
    def __init__(self,
        id: int | str | None = None,
        created_at: int | float | str | None = None,
        document: int | str | None = None,
        document_id: int | str | None = None,
        pos: int | str | None = None,
        lang: str | None = None,
        is_original: bool | None = None,
        confidence: int | float | str | None = None,
        key: str | None = None,
        value: str | None = None,
        datatype: str | None = None,
        status: int | str | None = None,
        reason: str | None = None
    ):
        super().__init__(
            id,
            created_at,
            document_id if document_id is not None else document,
            pos,
            lang,
            is_original,
            confidence,
            key,
            value,
            datatype,
            status,
            reason
        )


class ContentQueryParamsGeneral(ContentQueryParams):
    def __init__(self,
        content_id: int | str | None = None,
        content_created_at: int | float | str | None = None,
        content_document: int | str | None = None,
        content_document_id: int | str | None = None,
        content_pos: int | str | None = None,
        content_lang: str | None = None,
        content_is_original: bool | None = None,
        content_confidence: int | float | str | None = None,
        content_key: str | None = None,
        content_value: str | None = None,
        content_datatype: str | None = None,
        content_status: int | str | None = None,
        content_reason: str | None = None
    ):
        super().__init__(
            content_id,
            content_created_at,
            content_document_id if content_document_id is not None else content_document,
            content_pos,
            content_lang,
            content_is_original,
            content_confidence,
            content_key,
            content_value,
            content_datatype,
            content_status,
            content_reason
        )


