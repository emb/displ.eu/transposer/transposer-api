# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import re
import typing
from .orm import BearerTokenOrm





BEARER_TOKEN__FIELDS_MAP = {
    'bearer_token': {
        'id': BearerTokenOrm.id,
        'created_at': BearerTokenOrm.created_at,
        'owner': BearerTokenOrm.owner_id,
        'owner_id': BearerTokenOrm.owner_id,
        'token': BearerTokenOrm.token,
        'is_active': BearerTokenOrm.is_active,
        'valid_until': BearerTokenOrm.valid_until
    },
    'bearer_token_id': BearerTokenOrm.id,
    'bearer_token_created_at': BearerTokenOrm.created_at,
    'bearer_token_owner': BearerTokenOrm.owner_id,
    'bearer_token_owner_id': BearerTokenOrm.owner_id,
    'bearer_token_token': BearerTokenOrm.token,
    'bearer_token_is_active': BearerTokenOrm.is_active,
    'bearer_token_valid_until': BearerTokenOrm.valid_until,

    'bearer_token.id': BearerTokenOrm.id,
    'bearer_token.created_at': BearerTokenOrm.created_at,
    'bearer_token.owner': BearerTokenOrm.owner_id,
    'bearer_token.owner_id': BearerTokenOrm.owner_id,
    'bearer_token.token': BearerTokenOrm.token,
    'bearer_token.is_active': BearerTokenOrm.is_active,
    'bearer_token.valid_until': BearerTokenOrm.valid_until
}

BEARER_TOKEN__VARIANTS_MAP = {
    'bearer_token': {
        'id': 'bearer_token.id',
        'created_at': 'bearer_token.created_at',
        'owner': 'bearer_token.owner_id',
        'owner_id': 'bearer_token.owner_id',
        'token': 'bearer_token.token',
        'is_active': 'bearer_token.is_active',
        'valid_until': 'bearer_token.valid_until'
    },
    'bearer_token_id': 'bearer_token.id',
    'bearer_token_created_at': 'bearer_token.created_at',
    'bearer_token_owner': 'bearer_token.owner_id',
    'bearer_token_owner_id': 'bearer_token.owner_id',
    'bearer_token_token': 'bearer_token.token',
    'bearer_token_is_active': 'bearer_token.is_active',
    'bearer_token_valid_until': 'bearer_token.valid_until',

    'bearer_token.id': 'bearer_token.id',
    'bearer_token.created_at': 'bearer_token.created_at',
    'bearer_token.owner': 'bearer_token.owner_id',
    'bearer_token.owner_id': 'bearer_token.owner_id',
    'bearer_token.token': 'bearer_token.token',
    'bearer_token.is_active': 'bearer_token.is_active',
    'bearer_token.valid_until': 'bearer_token.valid_until'
}



############
### Typed

BEARER_TOKEN__PRIMARY_FIELDS_MAP = {
    'id': BearerTokenOrm.id,
    'created_at': BearerTokenOrm.created_at,
    'owner': BearerTokenOrm.owner_id,
    'owner_id': BearerTokenOrm.owner_id,
    'token': BearerTokenOrm.token,
    'is_active': BearerTokenOrm.is_active,
    'valid_until': BearerTokenOrm.valid_until
}

BEARER_TOKEN__PRIMARY_VARIANTS_MAP = {
    'id': 'bearer_token.id',
    'created_at': 'bearer_token.created_at',
    'owner': 'bearer_token.owner_id',
    'owner_id': 'bearer_token.owner_id',
    'token': 'bearer_token.token',
    'is_active': 'bearer_token.is_active',
    'valid_until': 'bearer_token.valid_until'
}


