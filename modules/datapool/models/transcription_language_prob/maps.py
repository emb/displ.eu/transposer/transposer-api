# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import re
import typing
from .orm import TranscriptionLanguageProbOrm





TRANSCRIPTION_LANGUAGE_PROB__FIELDS_MAP = {
    'transcription_language_prob': {
        'id': TranscriptionLanguageProbOrm.id,
        'transcription': TranscriptionLanguageProbOrm.transcription_id,
        'transcription_id': TranscriptionLanguageProbOrm.transcription_id,
        'lang': TranscriptionLanguageProbOrm.lang,
        'probability': TranscriptionLanguageProbOrm.probability
    },
    'transcription_language_prob_id': TranscriptionLanguageProbOrm.id,
    'transcription_language_prob_transcription': TranscriptionLanguageProbOrm.transcription_id,
    'transcription_language_prob_transcription_id': TranscriptionLanguageProbOrm.transcription_id,
    'transcription_language_prob_lang': TranscriptionLanguageProbOrm.lang,
    'transcription_language_prob_probability': TranscriptionLanguageProbOrm.probability,

    'transcription_language_prob.id': TranscriptionLanguageProbOrm.id,
    'transcription_language_prob.transcription': TranscriptionLanguageProbOrm.transcription_id,
    'transcription_language_prob.transcription_id': TranscriptionLanguageProbOrm.transcription_id,
    'transcription_language_prob.lang': TranscriptionLanguageProbOrm.lang,
    'transcription_language_prob.probability': TranscriptionLanguageProbOrm.probability
}

TRANSCRIPTION_LANGUAGE_PROB__VARIANTS_MAP = {
    'transcription_language_prob': {
        'id': 'transcription_language_prob.id',
        'transcription': 'transcription_language_prob.transcription_id',
        'transcription_id': 'transcription_language_prob.transcription_id',
        'lang': 'transcription_language_prob.lang',
        'probability': 'transcription_language_prob.probability'
    },
    'transcription_language_prob_id': 'transcription_language_prob.id',
    'transcription_language_prob_transcription': 'transcription_language_prob.transcription_id',
    'transcription_language_prob_transcription_id': 'transcription_language_prob.transcription_id',
    'transcription_language_prob_lang': 'transcription_language_prob.lang',
    'transcription_language_prob_probability': 'transcription_language_prob.probability',

    'transcription_language_prob.id': 'transcription_language_prob.id',
    'transcription_language_prob.transcription': 'transcription_language_prob.transcription_id',
    'transcription_language_prob.transcription_id': 'transcription_language_prob.transcription_id',
    'transcription_language_prob.lang': 'transcription_language_prob.lang',
    'transcription_language_prob.probability': 'transcription_language_prob.probability'
}



############
### Primary

TRANSCRIPTION_LANGUAGE_PROB__PRIMARY_FIELDS_MAP = {
    'id': TranscriptionLanguageProbOrm.id,
    'transcription': TranscriptionLanguageProbOrm.transcription_id,
    'transcription_id': TranscriptionLanguageProbOrm.transcription_id,
    'lang': TranscriptionLanguageProbOrm.lang,
    'probability': TranscriptionLanguageProbOrm.probability
}

TRANSCRIPTION_LANGUAGE_PROB__PRIMARY_VARIANTS_MAP = {
    'id': 'transcription_language_prob.id',
    'transcription': 'transcription_language_prob.transcription_id',
    'transcription_id': 'transcription_language_prob.transcription_id',
    'lang': 'transcription_language_prob.lang',
    'probability': 'transcription_language_prob.probability'
}


