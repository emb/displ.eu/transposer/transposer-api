# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import typing

from regex_patterns import (
    RE_INT_VALUE,
    RE_LANG_CODE
)
from data_utils import (
    split_terms,
    split_numeric
)
from ..base import BaseQueryParams
from .orm import TranscriptionLanguageProbOrm





class TranscriptionLanguageProbQueryParams(BaseQueryParams):
    __slots__ = [
        '_id',
        '_transcription_id',
        '_lang',
        '_probability',

        '_id_stmt',
        '_transcription_id_stmt',
        '_lang_stmt',
        '_probability_stmt'
    ]

    def __init__(self, 
        id: int | str | None = None,
        transcription_id: int | str | None = None,
        lang: int | str | None = None,
        probability: int | float | str | None = None
    ):
        super().__init__()

        # Store raw values
        self._id = id
        self._transcription_id = transcription_id
        self._lang = lang
        self._probability = probability

        # Init statement values
        self._id_stmt = None
        self._transcription_id_stmt = None
        self._lang_stmt = None
        self._probability_stmt = None


    def add_conditions(self, container: list, use_and: bool = False) -> bool:

        # id
        if self._id_stmt is not None:
            container.append(self._id_stmt)
        else:
            val = self._id
            if isinstance(val, int):
                val = str(val)
            val = self.create_filter(
                TranscriptionLanguageProbOrm.id,
                [(x, '==') for x in split_terms(val, RE_INT_VALUE, '', '')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._id_stmt = val
                container.append(val)

        # transcription_id
        if self._transcription_id_stmt is not None:
            container.append(self._transcription_id_stmt)
        else:
            val = self._transcription_id
            if isinstance(val, int):
                val = str(val)
            val = self.create_filter(
                TranscriptionLanguageProbOrm.transcription_id,
                [(x, '==') for x in split_terms(val, RE_INT_VALUE, '', '')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._transcription_id_stmt = val
                container.append(val)
        
        # lang
        if self._lang_stmt is not None:
            container.append(self._lang_stmt)
        else:
            val = self._lang
            val = self.create_filter(
                TranscriptionLanguageProbOrm.lang,
                [(x, '==') for x in split_terms(val, RE_LANG_CODE, '', '')],
                use_and
            )
            if val is not None:
                self._has_params = True
                self._lang_stmt = val
                container.append(val)
        
        # probability
        if self._probability_stmt is not None:
            container.append(self._probability_stmt)
        else:
            val = self._probability
            if isinstance(val, (int, float)):
                val = str(val)
            val = self.create_filter(
                TranscriptionLanguageProbOrm.probability,
                split_numeric(val),
                use_and
            )
            if val is not None:
                self._has_params = True
                self._probability_stmt = val
                container.append(val)

        # Return True if conditions were added
        return self._has_params
    

    # Getters for raw values

    @property
    def id(self) -> int | str | None:
        return self._id
    
    @property
    def transcription_id(self) -> int | str | None:
        return self._transcription_id
    
    @property
    def lang(self) -> str | None:
        return self._lang
    
    @property
    def probability(self) -> int | float | str | None:
        return self._probability


    # Getters for statements
    
    @property
    def id_stmt(self) -> typing.Any:
        return self._id_stmt
    
    @property
    def transcription_id_stmt(self) -> typing.Any:
        return self._transcription_id_stmt
    
    @property
    def lang_stmt(self) -> typing.Any:
        return self._lang_stmt
    
    @property
    def probability_stmt(self) -> typing.Any:
        return self._probability_stmt



### Define Interfaces

class TranscriptionLanguageProbQueryParamsPrimary(TranscriptionLanguageProbQueryParams):
    def __init__(self,
        id: int | str | None = None,
        transcription: int | str | None = None,
        transcription_id: int | str | None = None,
        lang: str | None = None,
        probability: int | float | str | None = None
    ):
        super().__init__(
            id,
            transcription_id if transcription_id is not None else transcription,
            lang,
            probability
        )


class TranscriptionLanguageProbQueryParamsGeneral(TranscriptionLanguageProbQueryParams):
    def __init__(self,
        transcription_language_prob_id: int | str | None = None,
        transcription_language_prob_transcription: int | str | None = None,
        transcription_language_prob_transcription_id: int | str | None = None,
        transcription_language_prob_lang: str | None = None,
        transcription_language_prob_probability: int | float | str | None = None
    ):
        super().__init__(
            transcription_language_prob_id,
            transcription_language_prob_transcription_id if transcription_language_prob_transcription_id is not None else transcription_language_prob_transcription,
            transcription_language_prob_lang,
            transcription_language_prob_probability
        )


