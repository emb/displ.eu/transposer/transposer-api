# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import typing

import sqlalchemy
from sqlalchemy.orm import (
    Mapped,
    mapped_column,
    relationship,
    validates
)

from regex_patterns import (
    RE_INT_VALUE,
    RE_FLOAT_VALUE,
    RE_LANG_CODE
)
from data_utils import (
    is_empty, is_true,
    split_terms,
    split_numeric,
    split_datetime
)
from ..base import BaseOrm





class TranscriptionLanguageProbOrm(BaseOrm):
    __tablename__ = 'transcription_language_probs'
    id: Mapped[int] = mapped_column(
        sqlalchemy.Integer, primary_key=True
    )
    transcription_id: Mapped[int] = mapped_column(sqlalchemy.ForeignKey('transcriptions.id'))
    lang: Mapped[str] = mapped_column(sqlalchemy.String)
    probability: Mapped[float] = mapped_column(sqlalchemy.Float, default=-1.0)

    transcription: Mapped['TranscriptionOrm'] = relationship(back_populates='language_probs')
    
    def __repr__(self):
        return (
            f'<TranscriptionLanguageProb('
            f'id={self.id}, '
            f'transcription_id={self.transcription_id}, '
            f'lang={self.lang}, '
            f'probability={self.probability})>'
        )
    
    def __str__(self):
        return (
            f'TranscriptionLanguageProb('
            f'id={self.id}, '
            f'transcription_id={self.transcription_id}, '
            f'lang={self.lang}, '
            f'probability={self.probability})'
        )
    
    def __json__(self):
        return {
            'id': self.id,
            'transcription_id': self.transcription_id,
            'lang': self.lang,
            'probability': self.probability
        }
    
    def _fields(self) -> tuple:
        return (
            ['id', 'transcription_id', 'lang', 'probability'],
            [('transcription', False, None)]
        )
    

    @validates('transcription_id')
    def validate_transcription_id(self, key, value):
        if is_empty(value):
            raise ValueError("Transcription ID is required")
        if isinstance(value, int):
            return value
        if isinstance(value, str):
            result = RE_INT_VALUE.match(value)
            if result is None:
                raise ValueError("Invalid transcription ID")
            return int(result.group(1))
        raise ValueError("Invalid transcription ID")

    @validates('lang')
    def validate_lang(self, key, value):
        if is_empty(value):
            return None
        if isinstance(value, str):
            result = RE_LANG_CODE.match(value)
            if result is None:
                raise ValueError("Invalid language code")
            return result.group(1)
        raise ValueError("Invalid language code")

    @validates('probability')
    def validate_probability(self, key, value):
        if is_empty(value):
            raise ValueError("Probability is required")
        if isinstance(value, (int, float)):
            return float(value)
        if isinstance(value, str):
            result = RE_FLOAT_VALUE.match(value)
            if result is None:
                raise ValueError("Invalid probability")
            return float(result.group(1))
        raise ValueError("Invalid probability")



