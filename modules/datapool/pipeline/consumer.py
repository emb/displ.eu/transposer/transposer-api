#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================
# Kafka consumer for Transposer API

import typing
import logging
import json
import threading
import concurrent.futures
from deepmerge import always_merger

import sqlalchemy
from sqlalchemy.orm import Session

from confluent_kafka import (
    Consumer,
    KafkaError,
    KafkaException,
    Message
)
from confluent_kafka.admin import (
    AdminClient,
    NewTopic
)

import trp_config

from regex_patterns import (
    RE_INT_VALUE,
    RE_UUID4
)
from ..models.pipeline_consumer import (
    PipelineConsumerOrm,
)



class PipelineConsumer:
    __slots__ = [
        '_db_engine',
        
        '_config',
        '_base_config',
        '_consumer_record',

        '_admin_config',
        '_consumer_config',

        '_admin',
        '_consumer',
        '_consumer_thread',

        '_created_topics',
        '_used_topics',
        '_remove_topics_when_done',
        '_log_tracebacks',

        '_listeners',

        '_running',
        '_stopping'
    ]
        
    def __init__(self,
        config: dict,
        base_config: trp_config.TrpConfig,
        db_engine: typing.Any
    ) -> None:
        
        logging.info('Welcome to Transposer-API Pipeline Consumer')

        # Configuration
        config = config if isinstance(config, dict) else {}
        pipeline_config = base_config.get('pipeline').options if base_config.has('pipeline') else {}
        consumer_config = base_config.get('consumer').options if base_config.has('consumer') else {}
        pipeline_instance_config = config['pipeline'] if 'pipeline' in config and isinstance(config['pipeline'], dict) else {}
        consumer_instance_config = config['consumer'] if 'consumer' in config and isinstance(config['consumer'], dict) else {}

        self._config = config
        self._base_config = base_config

        # SQLAlchemy database engine
        self._db_engine = db_engine

        self._consumer_record = {}
        if (
            isinstance(db_engine, sqlalchemy.engine.base.Engine) and
            'id' in config
        ):
            # Get configuration from database
            consumer_id = config['id']
            consumer_id_type, consumer_id, consumer_id_msg = self._get_id_attr(consumer_id, {
                'int': 'id',
                'str': [
                    ('id', RE_INT_VALUE, lambda x: int(x)),
                    ('uuid', RE_UUID4, None)
                ]
            }, 'Invalid Consumer ID')
            if consumer_id_type is not None:
                with Session(self._db_engine) as db_session:
                    # Create statement
                    stmt = sqlalchemy.select(PipelineConsumerOrm)
                    stmt = stmt.where(getattr(PipelineConsumerOrm, consumer_id_type) == consumer_id)

                    # Get record
                    record, record_msg = self._get_single_record(
                        db_session.execute(stmt),
                        'PipelineConsumerOrm',
                        'PipelineConsumer not found',
                        'Multiple PipelineConsumers found'
                    )
                    if record is not None:
                        # Parse and filter record
                        record = record.to_dict({
                            'current': True,
                            'options': {
                                'current': True
                            }
                        })
                        if 'options' in record:
                            record['options']['group.id'] = record['group_id']
                            record['options']['client.id'] = record['client_id']
                            self._consumer_record = record['options']
                        else:
                            self._consumer_record = {
                                'group.id': record['group_id'],
                                'client.id': record['client_id']
                            }

        # Merge configurations
        self._admin_config = {}
        always_merger.merge(self._admin_config, pipeline_config)
        always_merger.merge(self._admin_config, pipeline_instance_config)

        self._consumer_config = {
            'on_commit': self._on_commit_cb,
            'error_cb': self._error_cb,
            'default.topic.config': {
                'auto.offset.reset': 'latest'
            },
            'enable.auto.commit': True
        }
        always_merger.merge(self._consumer_config, pipeline_config)
        always_merger.merge(self._consumer_config, pipeline_instance_config)
        always_merger.merge(self._consumer_config, consumer_config)
        always_merger.merge(self._consumer_config, consumer_instance_config)
        always_merger.merge(self._consumer_config, self._consumer_record)


        
        print('Transposer-API Pipeline Consumer ... __init__ ... config:', self._config)
        print('Transposer-API Pipeline Consumer ... __init__ ... base_config:', self._base_config)
        print('Transposer-API Pipeline Consumer ... __init__ ... admin_config:', self._admin_config)
        print('Transposer-API Pipeline Consumer ... __init__ ... consumer_config:', self._consumer_config)
        print('Transposer-API Pipeline Consumer ... __init__ ... consumer_record:', self._consumer_record)
        print('Transposer-API Pipeline Consumer ... __init__ ... db_engine:', self._db_engine)



        self._running = False
        self._stopping = False
        
        self._created_topics = []
        self._used_topics = {}
        self._remove_topics_when_done = True if 'remove_topics_when_done' in config and config['remove_topics_when_done'] else False
        self._log_tracebacks = False if 'log_tracebacks' in config and not config['log_tracebacks'] else True

        self._listeners = {}

        # Create AdminClient and Consumer
        self._admin = AdminClient(self._admin_config)
        self._consumer = Consumer(self._consumer_config)



    def __del__(self) -> None:
        """Destructor
        
            Arguments:
                None
            
            Returns:
                None
        """
        self._cleanup()



    ### Public methods

    def run(self) -> None:
        """Run consumer
        
            Arguments:
                None
            
            Returns:
                None
        """
        if self._running:
            return
        if self._stopping:
            return
        logging.info('Start consumer thread ...')
        self._consumer_thread = threading.Thread(target=self._consumer_thread_loop)
        self._consumer_thread.start()
        logging.info('Consumer thread running')



    def stop(self) -> None:
        """Stop consumer
        
            Arguments:
                None
            
            Returns:
                None
        """
        if self._stopping:
            return
        logging.info('Stop consumer thread ...')
        self._stopping = True
        self._consumer_thread.join()
        logging.info('Consumer thread stopped')



    def subscribe(self, topic: str, cb: typing.Callable) -> None:
        """Subscribe to topic
        
            Arguments:
                topic {str} -- Topic
                cb {Callable} -- A callable
            
            Returns:
                None
        """
        # Create topic if it does not exist
        if not self._createTopic(topic):
            return

        # Add listener
        if topic not in self._listeners:
            self._listeners[topic] = []
            # Subscribe to topic
            self._consumer.subscribe(
                [topic],
                self._on_assign_cb,
                self._on_revoke_cb,
                self._on_lost_cb
            )
        self._listeners[topic].append(cb)



    def unsubscribe(self, topic: str | None = None, cb: typing.Callable | None = None) -> None:
        """Unsubscribe from topic

            Remove listener(s) and unsubscribe from consumer if no listeners are left.
        
            Arguments:
                topic {str | None} -- Topic (optional)
                cb {Callable | None} -- A callable (optional)
            
            Returns:
                None
        """
        if topic is None:
            # Unsubscribe from all topics
            self._consumer.unsubscribe()
            return
        
        if topic not in self._listeners:
            # Topic not found
            return
        
        if cb is not None:
            # Remove single callback
            self._listeners[topic].remove(cb)
            # If there are still callbacks left, leave
            if len(self._listeners[topic]) > 0:
                return
        
        # Remove topic, unsubscribe from consumer
        # and re-subscribe to remaining topics
        del self._listeners[topic]
        self._consumer.unsubscribe()
        ts = self._listeners.keys()
        if len(ts) == 0:
            return
        self._consumer.subscribe(
            ts,
            self._on_assign_cb,
            self._on_revoke_cb,
            self._on_lost_cb
        )



    ### Public properties

    @property
    def running(self) -> bool:
        """Check if consumer is running
        
            Arguments:
                None
            
            Returns:
                bool -- True if running, False otherwise
        """
        return self._running



    @property
    def stopping(self) -> bool:
        """Check if consumer is stopping
        
            Arguments:
                None
            
            Returns:
                bool -- True if running, False otherwise
        """
        return self._stopping



    ### Private methods

    # Consumer thread loop
    def _consumer_thread_loop(self) -> None:
        """Consumer thread loop
        
            Arguments:
                None
            
            Returns:
                None
        """
        logging.info('Start consumer')

        msg_count = self._min_commit_count
        self._stopping = False
        self._running = True
        try:
            while self._running and not self._stopping:
                msg = self._consumer.poll(timeout=1.0)
                
                if msg is None: continue

                if msg.error():
                    if msg.error().code() == KafkaError._PARTITION_EOF:
                        # End of partition event
                        logging.error('%% %s [%d] reached end at offset %d' %
                                        (msg.topic(), msg.partition(), msg.offset()))
                        continue

                    logging.critical(f'Kafka Exception: {str(msg.error())}')
                    raise KafkaException(msg.error())

                self._process_msg(msg)
                msg_count -= 1
                if msg_count == 0:
                    msg_count = self._min_commit_count
                    self._consumer.commit(asynchronous=True)
        
        except Exception as e:
            logging.error(f'Consumer thread loop exception: {str(e)}')
            if self._log_tracebacks:
                logging.exception(e)
        
        finally:
            try:
                # Synchronously commit pending offsets
                logging.info('Commit offsets ...')
                self._consumer.commit(asynchronous=False)
            except Exception as e:
                logging.error(f'Consumer thread loop exception: {str(e)}')
                if self._log_tracebacks:
                    logging.exception(e)
            
            try:
                # Close the consumer
                logging.info('Close consumer ...')
                self._consumer.close()
            except Exception as e:
                logging.error(f'Consumer thread loop exception: {str(e)}')
                if self._log_tracebacks:
                    logging.exception(e)
            
            self._created_topics = []
            self._used_topics = {}
            self._listeners = {}

            self._stopping = False
            self._running = False
            logging.info('Consumer stopped')



    ### Message handling
    
    # Process message
    def _process_msg(self, msg: Message) -> None:
        """Process message
        
            Arguments:
                msg {Message} -- Kafka message
            
            Returns:
                None
        """
        # Extract topic
        topic = None
        try:
            topic = msg.topic()
        except:
            # Leave if we get an error here
            logging.error(f'Unable to extract topic from message: {str(e)}')
            return
        
        # No topic, no party
        if topic is None:
            return
        if topic not in self._listeners:
            return

        # Extract payload
        payload = None
        try:
            payload = msg.value()
        except Exception as e:
            # Leave if we get an error here
            logging.error(f'Unable to extract payload from message: {str(e)}')
            return
        
        # No payload, no party
        if payload is None:
            return

        # Decode payload
        try:
            payload = payload.decode('utf-8')
        except Exception as e:
            # Leave if decoding fails
            logging.error(f'Unable to decode payload: {str(e)}')
            return
        
        # Parse payload as JSON
        try:
            payload = json.loads(payload)
        except Exception as e:
            # Leave if JSON decoding fails
            logging.error(f'Unable to parse payload as JSON: {str(e)}')
            return
        
        # Send message to listeners
        for l in self._listeners[topic]:
            try:
                l(payload, msg)
            except Exception as e:
                logging.error(f"Unable to call listener for '{topic}' : {str(e)}")



    ### Consumer callbacks
    
    # On-Assign callback
    def _on_assign_cb(self, cons: typing.Any, partitions: list) -> None:
        """On-Assign callback
        
            Arguments:
                cons {Consumer} -- Consumer
                partitions {list} -- List of partitions
            
            Returns:
                None
        """
        logging.info(f"Assigned partitions: {', '.join([f'{x.topic} [{str(x.partition)}]' for x in partitions])}")
    


    # On-Revoke callback
    def _on_revoke_cb(self, cons: typing.Any, partitions: list) -> None:
        """On-Revoke callback
        
            Arguments:
                cons {Consumer} -- Consumer
                partitions {list} -- List of partitions
            
            Returns:
                None
        """
        logging.info(f"Revoked partitions: {', '.join([f'{x.topic} [{str(x.partition)}]' for x in partitions])}")
    


    # On-Lost callback
    def _on_lost_cb(self, cons: typing.Any, partitions: list) -> None:
        """On-Lost callback
        
            Arguments:
                cons {Consumer} -- Consumer
                partitions {list} -- List of partitions
            
            Returns:
                None
        """
        logging.info(f"Lost partitions: {', '.join([f'{x.topic} [{str(x.partition)}]' for x in partitions])}")
    


    # On-Commit callback
    def _on_commit_cb(self, err: Exception, partitions: list) -> None:
        """On-Commit callback
        
            Arguments:
                err {Exception} -- Exception object
                partitions {list} -- List of partitions
            
            Returns:
                None
        """
        if err:
            logging.error(f"Commit failed for partitions - {', '.join([f'{x.topic} [{str(x.partition)}]' for x in partitions])}: {str(err)}")
            if self._log_tracebacks:
                logging.exception(err)
        else:
            logging.info(f"Commit completed for partitions: {', '.join([f'{x.topic} [{str(x.partition)}]' for x in partitions])}")



    # On-Error callback
    def _error_cb(self, err: Exception) -> None:
        """Kafka Consumer error callback
        
            Arguments:
                err {Exception} -- Exception object
            
            Returns:
                None
        """
        logging.error(f'Kafka Consumer error: {str(err)}')
        if self._log_tracebacks:
            logging.exception(err)



    ### Utility methods

    # Create Kafka topics
    def _createTopic(self, topic: str) -> bool:
        """Create Kafka topic.

        Arguments:
            topic {str} -- Name of topic to create.

        Returns:
            bool -- True if successful, False otherwise
        """
        if topic in self._used_topics:
            return True
        
        # Get the list of existing topics
        existing_topics = self._admin.list_topics().topics.keys()

        # Check if topic already exists
        if topic in existing_topics:
            self._used_topics[topic] = True
            return True
        
        # Create NewTopic instance for the missing topic
        # TODO: Make this configurable -> num_partitions, replication_factor
        new_topic = NewTopic(topic, num_partitions=1, replication_factor=1)
        logging.info(f'Creating topic: {topic}')
        result = self._admin.create_topics([new_topic])
        
        # Wait for operation to finish.
        result = concurrent.futures.wait(list(result.values()), timeout=60)
        
        if len(result.not_done) == 0:
            self._created_topics.append(new_topic)
            self._used_topics[topic] = True
            logging.info('    done')
            return True
        logging.error(f'Unable to create topic: {topic}')
        return False
    
    

    # Remove Kafka topics
    def _removeTopics(self) -> bool:
        """Remove previously created Kafka topics.

        Returns:
            bool -- True if successful, False otherwise
        """
        if len(self._created_topics) == 0:
            return True
        
        # Call the delete_topics() method with the NewTopic instance
        result = self._admin.delete_topics([x.topic for x in self._created_topics])
        
        # Wait for operation to finish.
        result = concurrent.futures.wait(list(result.values()), timeout=60)

        # Return success
        if len(result.not_done) == 0:
            for topic in self._created_topics:
                del self._used_topics[topic.topic]
            self._created_topics = []
            return True
        return False



    # Cleanup resources
    def _cleanup(self) -> None:
        """Cleanup resources
        
            Arguments:
                None
            
            Returns:
                None
        """
        if self._running:
            self.stop()
        if self._remove_topics_when_done:
            self._removeTopics()



###
# Producer und consumer werden in 'rdatapool' generiert und an die entsprechenden router übergeben.
# consumer erzeugt loop in eigenem thread um das umgebende script nicht zu blockieren
# das antwort-topic wird immer vor dem producer topic erzeugt und registriert, um zu garantieren, dass eine antwort auch garantiert ankommen kann
###



###############################################################
# DEBUG / TESTING
#
if __name__ == '__main__':
    pass


