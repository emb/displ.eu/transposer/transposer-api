#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================
# Kafka producer for Transposer API

import typing
import logging
import uuid
import json
import concurrent.futures
from deepmerge import always_merger

import sqlalchemy
from sqlalchemy.orm import Session

from confluent_kafka import (
    Producer
)
from confluent_kafka.admin import (
    AdminClient,
    NewTopic
)

import trp_config

from regex_patterns import (
    RE_INT_VALUE,
    RE_UUID4
)
from ..models.pipeline_producer import (
    PipelineProducerOrm,
)



class PipelineProducer:
    __slots__ = [
        '_db_engine',
        
        '_config',
        '_base_config',
        '_producer_record',

        '_admin_config',
        '_producer_config',

        '_admin',
        '_producer',

        '_created_topics',
        '_used_topics',
        '_remove_topics_when_done',
        '_log_tracebacks'
    ]
        
    def __init__(self,
        config: dict,
        base_config: trp_config.TrpConfig,
        db_engine: typing.Any
    ) -> None:
        
        logging.info('Welcome to Transposer-API Pipeline Producer')

        # Configuration
        config = config if isinstance(config, dict) else {}
        pipeline_config = base_config.get('pipeline').options if base_config.has('pipeline') else {}
        producer_config = base_config.get('producer').options if base_config.has('producer') else {}
        pipeline_instance_config = config['pipeline'] if 'pipeline' in config and isinstance(config['pipeline'], dict) else {}
        producer_instance_config = config['producer'] if 'producer' in config and isinstance(config['producer'], dict) else {}

        self._config = config
        self._base_config = base_config

        # SQLAlchemy database engine
        self._db_engine = db_engine

        self._producer_record = {}
        if (
            isinstance(db_engine, sqlalchemy.engine.base.Engine) and
            'id' in config
        ):
            # Get configuration from database
            producer_id = config['id']
            producer_id_type, producer_id, producer_id_msg = self._get_id_attr(producer_id, {
                'int': 'id',
                'str': [
                    ('id', RE_INT_VALUE, lambda x: int(x)),
                    ('uuid', RE_UUID4, None)
                ]
            }, 'Invalid Producer ID')
            if producer_id_type is not None:
                with Session(self._db_engine) as db_session:
                    # Create statement
                    stmt = sqlalchemy.select(PipelineProducerOrm)
                    stmt = stmt.where(getattr(PipelineProducerOrm, producer_id_type) == producer_id)

                    # Get record
                    record, record_msg = self._get_single_record(
                        db_session.execute(stmt),
                        'PipelineProducerOrm',
                        'PipelineProducer not found',
                        'Multiple PipelineProducers found'
                    )
                    if record is not None:
                        # Parse and filter record
                        record = record.to_dict({
                            'current': True,
                            'options': {
                                'current': True
                            }
                        })
                        if 'options' in record:
                            record['options']['transactional.id'] = record['transactional_id']
                            record['options']['client.id'] = record['client_id']
                            self._producer_record = record['options']
                        else:
                            self._producer_record = {
                                'transactional.id': record['transactional_id'],
                                'client.id': record['client_id']
                            }

        # Merge configurations
        self._admin_config = {}
        always_merger.merge(self._admin_config, pipeline_config)
        always_merger.merge(self._admin_config, pipeline_instance_config)

        self._producer_config = {
            'error_cb': self._error_cb
        }
        always_merger.merge(self._producer_config, pipeline_config)
        always_merger.merge(self._producer_config, pipeline_instance_config)
        always_merger.merge(self._producer_config, producer_config)
        always_merger.merge(self._producer_config, producer_instance_config)
        always_merger.merge(self._producer_config, self._producer_record)
        
        
        
        
        print('Transposer-API Pipeline Producer ... __init__ ... config:', self._config)
        print('Transposer-API Pipeline Producer ... __init__ ... base_config:', self._base_config)
        print('Transposer-API Pipeline Producer ... __init__ ... admin_config:', self._admin_config)
        print('Transposer-API Pipeline Producer ... __init__ ... producer_config:', self._producer_config)
        print('Transposer-API Pipeline Producer ... __init__ ... producer_record:', self._producer_record)
        print('Transposer-API Pipeline Producer ... __init__ ... db_engine:', self._db_engine)


        
        self._created_topics = []
        self._used_topics = {}
        self._remove_topics_when_done = True if 'remove_topics_when_done' in config and config['remove_topics_when_done'] else False
        self._log_tracebacks = False if 'log_tracebacks' in config and not config['log_tracebacks'] else True

        # Create AdminClient and Producer
        self._admin = AdminClient(self._admin_config)
        self._producer = Producer(self._producer_config)



    def __del__(self) -> None:
        """Destructor
        
            Arguments:
                None
            
            Returns:
                None
        """
        self._cleanup()



    def produce(self, msg: dict) -> bool:
        """Produce message
        
            Arguments:
                msg: dict -- Message to produce
            
            Returns:
                bool -- True if successful, False otherwise
        """


        print('Transposer-API Pipeline Producer ... produce ... msg:', msg)


        if 'topic' not in msg or 'payload' not in msg:
            try:
                msg = json.dumps(msg)
            except:
                msg = 'Unable to serialize message'
            logging.error(f'Invalid message: {msg}')
            return False

        if not self._createTopic(msg['topic']):
            return False

        try:
            self._producer.produce(
                msg['topic'],
                key=str(uuid.uuid4()),
                value=json.dumps(msg['payload'])
            )
            return True
        
        except Exception as e:
            logging.error(f'Unable to send message: {str(e)}')
            if self._log_tracebacks:
                logging.exception(e)
            return False



    def _cleanup(self) -> None:
        """Cleanup resources
        
            Arguments:
                None
            
            Returns:
                None
        """
        if self._remove_topics_when_done:
            self._removeTopics()



    def _error_cb(self, err: Exception) -> None:
        """Kafka Producer error callback
        
            Arguments:
                err {Exception} -- Exception object
            
            Returns:
                None
        """
        logging.error(f'Kafka Producer error: {str(err)}')
        if self._log_tracebacks:
            logging.exception(err)



    # Create Kafka topic
    def _createTopic(self, topic: str) -> bool:
        """Create Kafka topic.

        Arguments:
            topic {str} -- Name of topic to create.

        Returns:
            bool -- True if successful, False otherwise
        """
        if topic in self._used_topics:
            return True
        
        # Get the list of existing topics
        existing_topics = self._admin.list_topics().topics.keys()

        # Check if topic already exists
        if topic in existing_topics:
            self._used_topics[topic] = True
            return True
        
        # Create NewTopic instance for the missing topic
        # TODO: Make this configurable -> num_partitions, replication_factor
        new_topic = NewTopic(topic, num_partitions=1, replication_factor=1)
        logging.info(f'Creating topic: {topic}')
        result = self._admin.create_topics([new_topic])
        
        # Wait for operation to finish.
        result = concurrent.futures.wait(list(result.values()), timeout=60)
        
        if len(result.not_done) == 0:
            self._created_topics.append(new_topic)
            self._used_topics[topic] = True
            logging.info('    done')
            return True
        logging.error(f'Unable to create topic: {topic}')
        return False
    
    

    # Remove Kafka topics
    def _removeTopics(self) -> bool:
        """Remove previously created Kafka topics.

        Returns:
            bool -- True if successful, False otherwise
        """
        if len(self._created_topics) == 0:
            return True
        
        # Call the delete_topics() method with the NewTopic instance
        result = self._admin.delete_topics([x.topic for x in self._created_topics])
        
        # Wait for operation to finish.
        result = concurrent.futures.wait(list(result.values()), timeout=60)

        # Return success
        if len(result.not_done) == 0:
            for topic in self._created_topics:
                del self._used_topics[topic.topic]
            self._created_topics = []
            return True
        return False





    ### Database utility functions

    def _get_single_record(self, results: typing.Any, orm: str, msg_none: str, msg_multi: str) -> tuple:
        record = None
        for row in results:
            if record is not None:
                return (None, {
                    'success': False,
                    'message': msg_multi
                })
            record = getattr(row, orm)
    
        if record is None:
            return (None, {
                'success': False,
                'message': msg_none
            })
        
        return (record, None)









###############################################################
# DEBUG / TESTING
#
if __name__ == '__main__':
    pass


