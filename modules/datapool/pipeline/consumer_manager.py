#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================
# Kafka consumer manager for Transposer API

import typing

import trp_config

from .consumer import (
    PipelineConsumer,
)



class PipelineConsumerManager:
    _consumers: dict = {}
    
    _base_config: trp_config.TrpConfig | None = None
    _db_engine: typing.Any = None


    @classmethod
    def create(cls,
            topic: str,
            cb: typing.Callable,
            config: dict | None = None,
            base_config: trp_config.TrpConfig | None = None,
            db_engine: typing.Any | None = None
        ) -> PipelineConsumer:
        """Subscribe to a consumer instance.
            If the consumer instance does not exist, it will be created.
        
            Arguments:
                topic {str} -- Topic
                cb {Callable} -- Callback
                config {dict} -- Configuration
                base_config {TrpConfig} -- Base configuration
                db_engine {Any} -- SQLAlchemy database engine
            
            Returns:
                PipelineConsumer -- Consumer instance
        """

        if topic is None:
            raise ValueError('Topic not set')

        if cb is None or not callable(cb):
            raise ValueError('Callback not set')

        if topic in cls._consumers:
            consumer = cls._consumers[topic]
            consumer.subscribe(topic, cb)
            return consumer

        # Check if base configuration is set
        if base_config is None:
            if cls._base_config is None:
                raise ValueError('Base configuration not set')
            base_config = cls._base_config
        else:
            if cls._base_config is None:
                cls._base_config = base_config
        
        # Check if database engine is set
        if db_engine is None:
            if cls._db_engine is None:
                raise ValueError('Database engine not set')
            db_engine = cls._db_engine
        else:
            if cls._db_engine is None:
                cls._db_engine = db_engine
        
        # Check if configuration is set
        if config is None:
            config = {}
        
        # Create consumer
        consumer = PipelineConsumer(
            config,
            base_config,
            db_engine
        )
        consumer.subscribe(topic, cb)

        cls._consumers[topic] = consumer

        return consumer



    @classmethod
    def unsubscribe(cls,
            topic: str,
            cb: typing.Callable
        ) -> None:
        """Unsubscribe from consumer instance.
        
            Arguments:
                topic {str} -- Topic
                cb {Callable} -- Callback
            
            Returns:
                None
        """

        if topic is None:
            raise ValueError('Topic not set')

        if cb is None or not callable(cb):
            raise ValueError('Callback not set')

        if topic not in cls._consumers:
            raise ValueError('Unknown topic')
        
        consumer = cls._consumers[topic]
        consumer.unsubscribe(topic, cb)



    @classmethod
    def stop(cls,
            topic: str
        ) -> None:
        """Stop consumer instance.
        
            Arguments:
                topic {str} -- Topic
            
            Returns:
                None
        """

        if topic is None:
            raise ValueError('Topic not set')

        if topic not in cls._consumers:
            raise ValueError('Unknown topic')
        
        cls._consumers[topic].stop()
        del cls._consumers[topic]
    






###############################################################
# DEBUG / TESTING
#
if __name__ == '__main__':
    pass


