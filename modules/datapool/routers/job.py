# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import types
import typing
import uuid
import json
import base64

from fastapi import (
    Depends,
    Request
)

import sqlalchemy
from sqlalchemy.orm import Session

from regex_patterns import (
    RE_INT_VALUE,
    RE_UUID4,
    RE_HASH_XX,
    RE_SHA256_HEX,
    RE_SHAKE256_KEY_HEX,
    RE_EMAIL,
    RE_USERNAME,
    RE_HOST
)
from data_utils import (
    is_empty,

    data_2_base64,
    base64_2_data
)
import trp_config
from captions_parser import (
    parse_captions,
    build_captions
)

from ..filter import (
    split_order_fields
)

from ..models.base import (
    ApiResponseModel,
    ApiResponseWithCountModel,

    TrpPrimaryKey_ID_UUID
)
from ..models.job import (
    JobOrm,

    JobModel,
    NewJobModel,
    UpdateJobModel,

    JobQueryParams,
    JobQueryParamsPrimary,
    JobQueryParamsGeneral,

    JOB_OPTIONS_MODELS
)
from ..models.user import (
    UserOrm
)
from ..models.document import (
    DocumentOrm
)
from ..models.content import (
    ContentOrm
)
from ..models.caption import (
    CaptionOrm
)
from ..models.content_option import (
    ContentOptionOrm
)

from ..conditions import (
    add_job_conditions
)

from .base import AuthBaseRouter





class JobRouter(AuthBaseRouter):
    __slots__ = [
        'get_status',
        'cancel_job'
    ]

    def __init__(self,
        db_engine: typing.Any = None,
        app: typing.Any = None,
        router: typing.Any = None,
        config: trp_config.TrpConfig | None = None,
        auth_scheme: typing.Any = None
    ):
        super().__init__(db_engine, app, router, config, auth_scheme)

        print('JobRouter ... db_engine:', db_engine)
        print('JobRouter ... app:', app)
        print('JobRouter ... router:', router)
        print('JobRouter ... config:', config)
        print('JobRouter ... auth_scheme:', auth_scheme)

        # Get records
        async def get_records(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],

            job_primary: typing.Annotated[JobQueryParamsPrimary, Depends(JobQueryParamsPrimary)],
            job: typing.Annotated[JobQueryParamsGeneral, Depends(JobQueryParamsGeneral)],

            limit: int = 0,
            offset: int = 0,
            order: str | None = None,

            use_and: bool = False,

            dataview: str | None = None
        ) -> ApiResponseWithCountModel:
            job = JobQueryParams(
                job_primary.id if job.id is None else job.id,
                job_primary.uuid if job.uuid is None else job.uuid,
                job_primary.created_at if job.created_at is None else job.created_at,
                job_primary.finished_at if job.finished_at is None else job.finished_at,
                job_primary.owner_id if job.owner_id is None else job.owner_id,
                job_primary.action if job.action is None else job.action,
                job_primary.async_request if job.async_request is None else job.async_request,
                job_primary.status if job.status is None else job.status,
                job_primary.reason if job.reason is None else job.reason
            )

            result = None
            with Session(self._db_engine) as db_session:
                # Create count statement
                count_stmt = db_session.query(sqlalchemy.func.count(JobOrm.id))

                # Create query statement
                stmt = sqlalchemy.select(JobOrm)

                ## Add conditions
                where = []

                # Add job conditions
                add_job_conditions(
                    where,
                    job,
                    use_and
                )

                # Add conditions to statements
                if len(where):
                    cond = sqlalchemy.and_(*where)
                    count_stmt = count_stmt.where(cond)
                    stmt = stmt.where(cond)
                
                # Get total number of records
                num_records_total = count_stmt.scalar()
                if num_records_total == 0:
                    return {
                        'success': False,
                        'message': 'No Jobs found'
                    }
                
                # Add limit and offset
                if offset > 0:
                    stmt = stmt.offset(offset)
                if limit > 0:
                    stmt = stmt.limit(limit)

                # Add order
                order = split_order_fields(order, 'job')
                if order is not None:
                    stmt = stmt.order_by(*order)
                
                # Parse dataview
                dataview_struct = self._parse_dataview(dataview)
                
                # Execute statement
                result = []
                for row in db_session.execute(stmt):
                    if row.JobOrm is None:
                        continue
                    result.append(
                        row.JobOrm.to_dict(dataview_struct)
                    )
            
            # If we didn't find any records
            # return an error message
            if result is None:
                return {
                    'success': False,
                    'message': 'No Jobs found'
                }
            num_records = len(result)
            if num_records == 0:
                return {
                    'success': False,
                    'message': 'No Jobs found'
                }
            
            # Return the result
            return {
                'success': True,
                'message': 'Jobs found',
                'total': num_records_total,
                'count': num_records,
                'payload': result
            }

        self.get_records = types.MethodType(get_records, self)



        # Get record
        async def get_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            id: TrpPrimaryKey_ID_UUID,
            dataview: str | None = None
        ) -> ApiResponseModel:
            # Get key and extract value
            if is_empty(id):
                return {
                    'success': False,
                    'message': 'ID missing'
                }
            id_type, id, msg = self._get_id_attr(id, {
                'int': 'id',
                'str': [
                    ('id', RE_INT_VALUE, lambda x: int(x)),
                    ('uuid', RE_UUID4, None)
                ]
            }, 'Invalid ID')
            if id_type is None:
                return msg
            
            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(JobOrm)
                stmt = self._add_id_where_stmt(stmt, JobOrm, id_type, id)

                # Get record
                record, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'JobOrm',
                    'Job not found',
                    'Multiple Jobs found'
                )
                if record is None:
                    return msg
                
                # Parse and filter
                dataview_struct = self._parse_dataview(dataview)
                record_dict = record.to_dict(dataview_struct)
                
                # Return the result
                return {
                    'success': True,
                    'message': 'Job found',
                    'payload': record_dict
                }
        
        self.get_record = types.MethodType(get_record, self)



        # Create record
        async def create_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            data: NewJobModel,
            request: Request
        ) -> ApiResponseModel:
            # Test if request has options
            options = data.options
            if options is None:
                return {
                    'success': False,
                    'message': 'Job options are missing'
                }
            
            #print('JobRouter ... create_record ... data:', data)

            # Get parent key and value
            parent_id = data.owner
            if parent_id is None:
                return {
                    'success': False,
                    'message': 'Owner ID missing'
                }
            parent_id_type, parent_id, msg = self._get_id_attr(parent_id, {
                'int': 'id',
                'str': [
                    ('id', RE_INT_VALUE, lambda x: int(x)),
                    ('uuid', RE_UUID4, None),
                    ('email', RE_EMAIL, None),
                    ('username', RE_USERNAME, None)
                ]
            }, 'Invalid Owner ID')
            if parent_id_type is None:
                return msg

            # Parse options
            try:
                options = json.loads(options)
            except Exception as e:
                return {
                    'success': False,
                    'message': str(e)
                }


            print('JobRouter ... create_record ... options:', options)


            # Evaluate creator method,
            # based on the options' structure
            job_creator_method = self._find_job_creator_method(options)


            print('JobRouter ... create_record ... job_creator_method:', job_creator_method)

            # Leave if we cant't find a creator method,
            # or the method does not exist
            if job_creator_method is None:
                return {
                    'success': False,
                    'message': 'Unable to detect job'
                }
            if not hasattr(self, job_creator_method):
                return {
                    'success': False,
                    'message': 'Unknown job'
                }

            # Get parent
            parent = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(UserOrm)
                stmt = self._add_id_where_stmt(stmt, UserOrm, parent_id_type, parent_id)
                
                # Get parent record
                parent, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'UserOrm',
                    'User not found',
                    'Multiple Users found'
                )
                if parent is None:
                    return msg
            
            # Create job
            result, msg = getattr(self, job_creator_method)(parent, options)
            # It we do not get a result, an error occured
            # and we return the msg containing information
            if result is None:
                return msg

            # Return success with result
            return {
                'success': True,
                'message': 'Job created',
                'payload': result
            }


            # parent = None
            # record = None
            # with Session(self._db_engine) as db_session:
            #     # Create statement
            #     stmt = sqlalchemy.select(UserOrm)
            #     stmt = self._add_id_where_stmt(stmt, UserOrm, parent_id_type, parent_id)
                
            #     # Get parent record
            #     parent, msg = self._get_single_record(
            #         db_session.execute(stmt),
            #         'UserOrm',
            #         'User not found',
            #         'Multiple Users found'
            #     )
            #     if parent is None:
            #         return msg
                
            #     client_host = request.client.host

            #     print('JobRouter ... create_record ... client_host:', client_host)

            #     # Create record
            #     try:
            #         record = JobOrm(
            #             uuid=str(uuid.uuid4()),
            #             owner_id=parent.id,
            #             action=data.action,
            #             async_request=data.async_request,
            #             status='created'
            #         )
            #         db_session.add(record)
            #         db_session.commit()
            #     except Exception as e:
            #         return {
            #             'success': False,
            #             'message': str(e)
            #         }
            
            #     # Return success
            #     return {
            #         'success': True,
            #         'message': 'Job created',
            #         'payload': {
            #             'id': record.id
            #         }
            #     }
        
        self.create_record = types.MethodType(create_record, self)



        # Update record
        async def update_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            id: TrpPrimaryKey_ID_UUID,
            data: UpdateJobModel
        ) -> ApiResponseModel:
            if is_empty(id):
                return {
                    'success': False,
                    'message': 'ID missing'
                }
            id_type, id, msg = self._get_id_attr(id, {
                'int': 'id',
                'str': [
                    ('id', RE_INT_VALUE, lambda x: int(x)),
                    ('uuid', RE_UUID4, None)
                ]
            }, 'Invalid ID')
            if id_type is None:
                return msg

            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(JobOrm)
                stmt = self._add_id_where_stmt(stmt, JobOrm, id_type, id)

                # Get record
                record, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'JobOrm',
                    'Job not found',
                    'Multiple Jobs found'
                )
                if record is None:
                    return msg
                
                # Get data to update
                d = {}
                for f in ['finished_at', 'status', 'reason']:
                    if getattr(data, f) is not None:
                        d[f] = getattr(data, f)
                if len(d) == 0:
                    return {
                        'success': False,
                        'message': 'Nothing to update'
                    }

                # Update record
                try:
                    db_session.query(JobOrm).\
                        filter(JobOrm.id == record.id).\
                        update(d)
                    db_session.commit()
                except Exception as e:
                    return {
                        'success': False,
                        'message': str(e)
                    }
            
                # Return success
                return {
                    'success': True,
                    'message': 'Job updated',
                    'payload': {
                        'id': record.id
                    }
                }
        
        self.update_record = types.MethodType(update_record, self)



        # Delete record
        async def delete_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            id: TrpPrimaryKey_ID_UUID
        ) -> ApiResponseModel:
            if is_empty(id):
                return {
                    'success': False,
                    'message': 'ID missing'
                }
            id_type, id, msg = self._get_id_attr(id, {
                'int': 'id',
                'str': [
                    ('id', RE_INT_VALUE, lambda x: int(x)),
                    ('uuid', RE_UUID4, None)
                ]
            }, 'Invalid ID')
            if id_type is None:
                return msg
            
            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(JobOrm)
                stmt = self._add_id_where_stmt(stmt, JobOrm, id_type, id)

                # Get record
                record, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'JobOrm',
                    'Job not found',
                    'Multiple Jobs found'
                )
                if record is None:
                    return msg

                # Delete record if found
                try:
                    db_session.execute(
                        sqlalchemy.delete(JobOrm).\
                            where(JobOrm.id == record.id)
                    )
                    db_session.commit()
                except Exception as e:
                    return {
                        'success': False,
                        'message': str(e)
                    }
            
                # Return success
                return {
                    'success': True,
                    'message': 'Job deleted',
                    'payload': {
                        'id': record.id
                    }
                }
        
        self.delete_record = types.MethodType(delete_record, self)



        ### Add routes ###
        #router.add_api_route('/job/status/{id}', self.get_status,     methods=['GET'])
        #router.add_api_route('/job/cancel/{id}', self.cancel_job,     methods=['PUT'])

        router.add_api_route('/jobs',          self.get_records,      methods=['GET'])
        router.add_api_route('/job/{id}',      self.get_record,       methods=['GET'])
        router.add_api_route('/job',           self.create_record,    methods=['POST'])
        router.add_api_route('/job/{id}',      self.update_record,    methods=['PUT'])
        router.add_api_route('/job/{id}',      self.delete_record,    methods=['DELETE'])
    


    def _find_job_creator_method(self, opts: typing.Any) -> typing.Any:
        for model, mname in JOB_OPTIONS_MODELS:
            try:
                model(**opts)
                return mname
            except Exception as e:
                pass
        return None
    


    def translate_webvtt_sync(self, parent: typing.Any, opts: typing.Any) -> tuple:
        # translate WebVTT object contains:
        #   webvtt: string
        #   source_lang: string (default: en)
        #   target_langs: string | list[string]


        print('translate_webvtt_sync ... parent:', parent)
        print('translate_webvtt_sync ... opts:', opts)

        data = base64_2_data(opts['webvtt'])

        print('translate_webvtt_sync ... data:', data)

        data = parse_captions(data)

        print('translate_webvtt_sync ... data (parsed):', data)

        if data is None:
            return (
                None,
                {
                    'success': False,
                    'message': 'No valid captions data'
                }
            )

        with Session(self._db_engine) as db_session:

            print('translate_webvtt_sync ... db_session:', db_session)

            # Create record
            try:
                record = JobOrm(
                    uuid=str(uuid.uuid4()),
                    owner_id=parent.id,
                    action='translate',
                    async_request=False,
                    status='created'
                )

                print('translate_webvtt_sync ... create ... record:', record)

                doc = DocumentOrm(
                    owner_id=parent.id,
                    type=1,
                    uuid=str(uuid.uuid4()),
                    status=1
                )
                
                print('translate_webvtt_sync ... create ... doc:', doc)

                record.documents.append(doc)

                if 'head' in data and data['head']:
                    content = ContentOrm(
                        pos=-1,
                        lang=opts['source_lang'],
                        is_original=True,
                        confidence=1.0,
                        key='head',
                        value=data['head'],
                        datatype='STRING',
                        status=1
                    )

                    print('translate_webvtt_sync ... create ... content [head]:', content)

                    doc.contents.append(content)

                for item in data['content']:
                    if item['type'] == 'cue':
                        caption = CaptionOrm(
                            start=item['time']['start'],
                            end=item['time']['end'],
                            identifier=(item['identifier'] if 'identifier' in item and item['identifier'] else None),
                            properties=(item['properties'] if 'properties' in item and item['properties'] else None)
                        )

                        print('translate_webvtt_sync ... create ... caption [cue]:', caption)

                        content = ContentOrm(
                            pos=item['pos'],
                            lang=opts['source_lang'],
                            is_original=True,
                            confidence=1.0,
                            key='cue',
                            value=json.dumps(item['content']),
                            datatype='JSON',
                            status=1
                        )

                        print('translate_webvtt_sync ... create ... content [cue]:', content)

                        content.captions.append(caption)
                        doc.contents.append(content)
                        continue

                    content = ContentOrm(
                        pos=item['pos'],
                        lang=opts['source_lang'],
                        is_original=True,
                        confidence=1.0,
                        key=item['type'],
                        value=item['content'],
                        datatype='STRING',
                        status=1
                    )

                    print(f"translate_webvtt_sync ... create ... content [{item['type']}]:", content)

                    doc.contents.append(content)

                db_session.add(record)
                db_session.commit()

                # Return success
                return (
                    {
                        'id': record.id,
                        'uuid': record.uuid
                    },
                    None
                )
            
            except Exception as e:
                return (
                    None,
                    {
                        'success': False,
                        'message': str(e)
                    }
                )







### Testing ###

if __name__ == '__main__':
    router = JobRouter()
    print('router:', router)


