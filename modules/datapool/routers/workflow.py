# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import types
import typing
import re
import uuid

from fastapi import (
    Depends
)

import sqlalchemy
from sqlalchemy.orm import Session

from regex_patterns import (
    RE_INT_VALUE,
    RE_UUID4,
    RE_HASH_XX,
    RE_XXHASH_CID_HEX
)
from data_utils import (
    is_empty,
    validate_uri as validate_uri_func
)

import trp_config

from ..filter import (
    split_order_fields
)

from ..models.base import (
    ApiResponseModel,
    ApiResponseWithCountModel,

    TrpPrimaryKey_ID_UUID
)
from ..models.workflow import (
    WorkflowOrm,
    
    WorkflowModel,
    NewWorkflowModel,
    UpdateWorkflowModel,
    NewWorkflowStructureModel,
    UpdateWorkflowStructureModel,

    WorkflowQueryParams,
    WorkflowQueryParamsPrimary,
    WorkflowQueryParamsGeneral
)
from ..models.workflow_step import (
    WorkflowStepOrm
)
from ..models.user import (
    UserOrm,
    UserQueryParamsGeneral
)
from ..models.document import (
    DocumentOrm,
    DocumentQueryParamsGeneral
)

from ..conditions import (
    add_workflow_conditions,
    add_document_conditions
)

from .base import AuthBaseRouter





class WorkflowRouter(AuthBaseRouter):
    def __init__(self,
        db_engine: typing.Any = None,
        app: typing.Any = None,
        router: typing.Any = None,
        config: trp_config.TrpConfig | None = None,
        auth_scheme: typing.Any = None
    ):
        super().__init__(db_engine, app, router, config, auth_scheme)

        print('WorkflowRouter ... db_engine:', db_engine)
        print('WorkflowRouter ... app:', app)
        print('WorkflowRouter ... router:', router)
        print('WorkflowRouter ... config:', config)
        print('WorkflowRouter ... auth_scheme:', auth_scheme)

        # Get records
        async def get_records(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],

            workflow_primary: typing.Annotated[WorkflowQueryParamsPrimary, Depends(WorkflowQueryParamsPrimary)],
            workflow: typing.Annotated[WorkflowQueryParamsGeneral, Depends(WorkflowQueryParamsGeneral)],

            document: typing.Annotated[DocumentQueryParamsGeneral, Depends(DocumentQueryParamsGeneral)],
            
            limit: int = 0,
            offset: int = 0,
            order: str | None = None,

            use_and: bool = False,

            dataview: str | None = None
        ) -> ApiResponseWithCountModel:
            workflow = WorkflowQueryParams(
                workflow_primary.id if workflow.id is None else workflow.id,
                workflow_primary.uuid if workflow.uuid is None else workflow.uuid,
                workflow_primary.created_at if workflow.created_at is None else workflow.created_at,
                workflow_primary.document_id if workflow.document_id is None else workflow.document_id,
                workflow_primary.progress if workflow.progress is None else workflow.progress,
                workflow_primary.status if workflow.status is None else workflow.status,
                workflow_primary.reason if workflow.reason is None else workflow.reason
            )

            result = None
            with Session(self._db_engine) as db_session:
                # Create count statement
                count_stmt = db_session.query(sqlalchemy.func.count(WorkflowOrm.id))

                # Create query statement
                stmt = sqlalchemy.select(WorkflowOrm)

                ## Add conditions
                where = []

                # Add workflow conditions
                add_workflow_conditions(
                    where,
                    workflow,
                    use_and
                )

                # Add document conditions
                if add_document_conditions(
                    where,
                    document,
                    use_and
                ):
                    stmt = stmt.join(WorkflowOrm.document, full=True)
                

                # Add conditions to statements
                if len(where):
                    cond = sqlalchemy.and_(*where)
                    count_stmt = count_stmt.where(cond)
                    stmt = stmt.where(cond)
                
                # Get total number of records
                num_records_total = count_stmt.scalar()
                if num_records_total == 0:
                    return {
                        'success': False,
                        'message': 'No Workflows found'
                    }
                
                # Add limit and offset
                if offset > 0:
                    stmt = stmt.offset(offset)
                if limit > 0:
                    stmt = stmt.limit(limit)

                # Add order
                order = split_order_fields(order, 'workflow')
                if order is not None:
                    stmt = stmt.order_by(*order)
                
                # Parse dataview
                dataview_struct = self._parse_dataview(dataview)
                
                # Execute statement
                result = []
                for row in db_session.execute(stmt):
                    if row.WorkflowOrm is None:
                        continue
                    result.append(
                        row.WorkflowOrm.to_dict(dataview_struct)
                    )
            
            # If we didn't find any records
            # return an error message
            if result is None:
                return {
                    'success': False,
                    'message': 'No Workflows found'
                }
            num_records = len(result)
            if num_records == 0:
                return {
                    'success': False,
                    'message': 'No Workflows found'
                }
            
            # Return the result
            return {
                'success': True,
                'message': 'Workflows found',
                'total': num_records_total,
                'count': num_records,
                'payload': result
            }

        self.get_records = types.MethodType(get_records, self)



        # Get record
        async def get_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            id: TrpPrimaryKey_ID_UUID,
            dataview: str | None = None
        ) -> ApiResponseModel:
            # Get key and extract value
            if is_empty(id):
                return {
                    'success': False,
                    'message': 'ID missing'
                }
            id_type, id, msg = self._get_id_attr(id, {
                'int': 'id',
                'str': [
                    ('id', RE_INT_VALUE, lambda x: int(x)),
                    ('uuid', RE_UUID4, None)
                ]
            }, 'Invalid ID')
            if id_type is None:
                return msg
            
            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(WorkflowOrm)
                stmt = self._add_id_where_stmt(stmt, WorkflowOrm, id_type, id)

                # Get record
                record, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'WorkflowOrm',
                    'Workflow not found',
                    'Multiple Workflows found'
                )
                if record is None:
                    return msg
                
                # Parse and filter
                dataview_struct = self._parse_dataview(dataview)
                record_dict = record.to_dict(dataview_struct)
                
                # Return the result
                return {
                    'success': True,
                    'message': 'Workflow found',
                    'payload': record_dict
                }
        
        self.get_record = types.MethodType(get_record, self)



        # Create record
        async def create_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            data: NewWorkflowStructureModel
        ) -> ApiResponseModel:
            # Get parent key and value
            parent_id = data.document
            if parent_id is None:
                return {
                    'success': False,
                    'message': 'Document ID missing'
                }
            parent_id_type, parent_id, msg = self._get_id_attr(parent_id, {
                'int': 'id',
                'str': [
                    ('id', RE_INT_VALUE, lambda x: int(x)),
                    ('uuid', RE_UUID4, None)
                ]
            }, 'Invalid Document ID')
            if parent_id_type is None:
                return msg

            parent = None
            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(DocumentOrm)
                stmt = self._add_id_where_stmt(stmt, DocumentOrm, parent_id_type, parent_id)
                
                # Get parent record
                parent, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'DocumentOrm',
                    'Document not found',
                    'Multiple Documents found'
                )
                if parent is None:
                    return msg

                # Create record
                try:
                    record = WorkflowOrm(
                        uuid=str(uuid.uuid4()),
                        document_id=parent.id,
                        progress=0.0,
                        status=1
                    )

                    # Add workflow steps
                    if isinstance(data.steps, list):
                        step_pos = 0
                        for step in data.steps:
                            stp = WorkflowStepOrm(
                                uuid=str(uuid.uuid4()),
                                pos=step_pos,
                                allow_if_previous_failed=step.allow_if_previous_failed,
                                action=step.action,
                                topic_send=step.topic_send,
                                topic_receive=step.topic_receive,
                                progress=0.0,
                                status=1
                            )
                            record.steps.append(stp)
                            step_pos += 1
                    
                    db_session.add(record)
                    db_session.commit()
                except Exception as e:
                    return {
                        'success': False,
                        'message': str(e)
                    }
            
                # Return success
                return {
                    'success': True,
                    'message': 'Workflow created',
                    'payload': {
                        'id': record.id,
                        'uuid': record.uuid
                    }
                }
        
        self.create_record = types.MethodType(create_record, self)



        # Update record
        async def update_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            id: TrpPrimaryKey_ID_UUID,
            data: UpdateWorkflowStructureModel
        ) -> ApiResponseModel:
            if is_empty(id):
                return {
                    'success': False,
                    'message': 'ID missing'
                }
            id_type, id, msg = self._get_id_attr(id, {
                'int': 'id',
                'str': [
                    ('id', RE_INT_VALUE, lambda x: int(x)),
                    ('uuid', RE_UUID4, None)
                ]
            }, 'Invalid ID')
            if id_type is None:
                return msg
            
            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(WorkflowOrm)
                stmt = self._add_id_where_stmt(stmt, WorkflowOrm, id_type, id)

                # Get record
                record, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'WorkflowOrm',
                    'Workflow not found',
                    'Multiple Workflows found'
                )
                if record is None:
                    return msg
                
                # Get data to update
                d = {}
                for f in ['progress', 'status', 'reason']:
                    if getattr(data, f) is not None:
                        d[f] = getattr(data, f)
                if len(d) == 0:
                    return {
                        'success': False,
                        'message': 'Nothing to update'
                    }

                # Update record
                try:
                    db_session.query(WorkflowOrm).\
                        filter(WorkflowOrm.id == record.id).\
                        update(d)
                    db_session.commit()
                except Exception as e:
                    return {
                        'success': False,
                        'message': str(e)
                    }
            
                # Return success
                return {
                    'success': True,
                    'message': 'Workflow updated',
                    'payload': {
                        'id': record.id
                    }
                }
        
        self.update_record = types.MethodType(update_record, self)



        # Delete record
        async def delete_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            id: TrpPrimaryKey_ID_UUID
        ) -> ApiResponseModel:
            if is_empty(id):
                return {
                    'success': False,
                    'message': 'ID missing'
                }
            id_type, id, msg = self._get_id_attr(id, {
                'int': 'id',
                'str': [
                    ('id', RE_INT_VALUE, lambda x: int(x)),
                    ('uuid', RE_UUID4, None)
                ]
            }, 'Invalid ID')
            if id_type is None:
                return msg

            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(WorkflowOrm)
                stmt = self._add_id_where_stmt(stmt, WorkflowOrm, id_type, id)

                # Get record
                record, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'WorkflowOrm',
                    'Workflow not found',
                    'Multiple Workflows found'
                )
                if record is None:
                    return msg
                
                # Delete record if found
                try:
                    db_session.execute(
                        sqlalchemy.delete(WorkflowOrm).\
                            where(WorkflowOrm.id == record.id)
                    )
                    db_session.commit()
                except Exception as e:
                    return {
                        'success': False,
                        'message': str(e)
                    }
            
                # Return success
                return {
                    'success': True,
                    'message': 'Workflow deleted',
                    'payload': {
                        'id': record.id
                    }
                }
        
        self.delete_record = types.MethodType(delete_record, self)



        ### Add routes ###
        router.add_api_route('/workflows',          self.get_records,      methods=['GET'])
        router.add_api_route('/workflow/{id}',      self.get_record,       methods=['GET'])
        router.add_api_route('/workflow',           self.create_record,    methods=['POST'])
        router.add_api_route('/workflow/{id}',      self.update_record,    methods=['PUT'])
        router.add_api_route('/workflow/{id}',      self.delete_record,    methods=['DELETE'])





### Testing ###

if __name__ == '__main__':
    router = WorkflowRouter()
    print('router:', router)


