# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import types
import typing
import re
import uuid

from fastapi import (
    Depends
)

import sqlalchemy
from sqlalchemy.orm import Session

from regex_patterns import (
    RE_INT_VALUE,
    RE_UUID4,
    RE_HASH_XX,
    RE_XXHASH_CID_HEX
)
from data_utils import (
    is_empty,
    validate_uri as validate_uri_func
)

import trp_config

from ..filter import (
    split_order_fields
)

from ..models.base import (
    ApiResponseModel,
    ApiResponseWithCountModel,

    TrpPrimaryKey_ID_UUID
)
from ..models.pipeline_producer import (
    PipelineProducerOrm,
    
    PipelineProducerModel,
    NewPipelineProducerModel,
    UpdatePipelineProducerModel,

    PipelineProducerQueryParams,
    PipelineProducerQueryParamsPrimary,
    PipelineProducerQueryParamsGeneral
)
from ..models.pipeline_producer_option import (
    PipelineProducerOptionOrm,
    PipelineProducerOptionQueryParamsGeneral
)
from ..models.pipeline import (
    PipelineOrm,
    PipelineQueryParamsGeneral
)

from ..conditions import (
    add_pipeline_producer_conditions,
    add_pipeline_conditions
)

from .base import AuthBaseRouter





class PipelineProducerRouter(AuthBaseRouter):
    def __init__(self,
        db_engine: typing.Any = None,
        app: typing.Any = None,
        router: typing.Any = None,
        config: trp_config.TrpConfig | None = None,
        auth_scheme: typing.Any = None
    ):
        super().__init__(db_engine, app, router, config, auth_scheme)

        print('PipelineProducerRouter ... db_engine:', db_engine)
        print('PipelineProducerRouter ... app:', app)
        print('PipelineProducerRouter ... router:', router)
        print('PipelineProducerRouter ... config:', config)
        print('PipelineProducerRouter ... auth_scheme:', auth_scheme)

        # Get records
        async def get_records(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],

            pipeline_producer_primary: typing.Annotated[PipelineProducerQueryParamsPrimary, Depends(PipelineProducerQueryParamsPrimary)],
            pipeline_producer: typing.Annotated[PipelineProducerQueryParamsGeneral, Depends(PipelineProducerQueryParamsGeneral)],

            pipeline: typing.Annotated[PipelineQueryParamsGeneral, Depends(PipelineQueryParamsGeneral)],
            
            limit: int = 0,
            offset: int = 0,
            order: str | None = None,

            use_and: bool = False,

            dataview: str | None = None
        ) -> ApiResponseWithCountModel:
            pipeline_producer = PipelineProducerQueryParams(
                pipeline_producer_primary.id if pipeline_producer.id is None else pipeline_producer.id,
                pipeline_producer_primary.uuid if pipeline_producer.uuid is None else pipeline_producer.uuid,
                pipeline_producer_primary.created_at if pipeline_producer.created_at is None else pipeline_producer.created_at,
                pipeline_producer_primary.pipeline_id if pipeline_producer.pipeline_id is None else pipeline_producer.pipeline_id,
                pipeline_producer_primary.name if pipeline_producer.name is None else pipeline_producer.name,
                pipeline_producer_primary.client_id if pipeline_producer.client_id is None else pipeline_producer.client_id,
                pipeline_producer_primary.transactional_id if pipeline_producer.transactional_id is None else pipeline_producer.transactional_id
            )

            result = None
            with Session(self._db_engine) as db_session:
                # Create count statement
                count_stmt = db_session.query(sqlalchemy.func.count(PipelineProducerOrm.id))

                # Create query statement
                stmt = sqlalchemy.select(PipelineProducerOrm)

                ## Add conditions
                where = []

                # Add pipeline_producer conditions
                add_pipeline_producer_conditions(
                    where,
                    pipeline_producer,
                    use_and
                )

                # Add pipeline conditions
                if add_pipeline_conditions(
                    where,
                    pipeline,
                    use_and
                ):
                    stmt = stmt.join(PipelineProducerOrm.pipeline, full=True)
                

                # Add conditions to statements
                if len(where):
                    cond = sqlalchemy.and_(*where)
                    count_stmt = count_stmt.where(cond)
                    stmt = stmt.where(cond)
                
                # Get total number of records
                num_records_total = count_stmt.scalar()
                if num_records_total == 0:
                    return {
                        'success': False,
                        'message': 'No PipelineProducers found'
                    }
                
                # Add limit and offset
                if offset > 0:
                    stmt = stmt.offset(offset)
                if limit > 0:
                    stmt = stmt.limit(limit)

                # Add order
                order = split_order_fields(order, 'pipeline_producer')
                if order is not None:
                    stmt = stmt.order_by(*order)
                
                # Parse dataview
                dataview_struct = self._parse_dataview(dataview)
                
                # Execute statement
                result = []
                for row in db_session.execute(stmt):
                    if row.PipelineProducerOrm is None:
                        continue
                    result.append(
                        row.PipelineProducerOrm.to_dict(dataview_struct)
                    )
            
            # If we didn't find any records
            # return an error message
            if result is None:
                return {
                    'success': False,
                    'message': 'No PipelineProducers found'
                }
            num_records = len(result)
            if num_records == 0:
                return {
                    'success': False,
                    'message': 'No PipelineProducers found'
                }
            
            # Return the result
            return {
                'success': True,
                'message': 'PipelineProducers found',
                'total': num_records_total,
                'count': num_records,
                'payload': result
            }

        self.get_records = types.MethodType(get_records, self)



        # Get record
        async def get_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            id: TrpPrimaryKey_ID_UUID,
            dataview: str | None = None
        ) -> ApiResponseModel:
            # Get key and extract value
            if is_empty(id):
                return {
                    'success': False,
                    'message': 'ID missing'
                }
            id_type, id, msg = self._get_id_attr(id, {
                'int': 'id',
                'str': [
                    ('id', RE_INT_VALUE, lambda x: int(x)),
                    ('uuid', RE_UUID4, None)
                ]
            }, 'Invalid ID')
            if id_type is None:
                return msg
            
            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(PipelineProducerOrm)
                stmt = self._add_id_where_stmt(stmt, PipelineProducerOrm, id_type, id)

                # Get record
                record, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'PipelineProducerOrm',
                    'PipelineProducer not found',
                    'Multiple PipelineProducers found'
                )
                if record is None:
                    return msg
                
                # Parse and filter
                dataview_struct = self._parse_dataview(dataview)
                record_dict = record.to_dict(dataview_struct)
                
                # Return the result
                return {
                    'success': True,
                    'message': 'PipelineProducer found',
                    'payload': record_dict
                }
        
        self.get_record = types.MethodType(get_record, self)



        # Create record
        async def create_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            data: NewPipelineProducerModel
        ) -> ApiResponseModel:
            # Get parent key and value
            parent_id = data.setting
            if parent_id is None:
                return {
                    'success': False,
                    'message': 'Pipeline ID missing'
                }
            parent_id_type, parent_id, msg = self._get_id_attr(parent_id, {
                'int': 'id',
                'str': [
                    ('id', RE_INT_VALUE, lambda x: int(x)),
                    ('uuid', RE_UUID4, None)
                ]
            }, 'Invalid Pipeline ID')
            if parent_id_type is None:
                return msg

            parent = None
            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(PipelineOrm)
                stmt = self._add_id_where_stmt(stmt, PipelineOrm, parent_id_type, parent_id)
                
                # Get parent record
                parent, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'PipelineOrm',
                    'Pipeline not found',
                    'Multiple Pipelines found'
                )
                if parent is None:
                    return msg

                # Create record
                try:
                    record = PipelineProducerOrm(
                        uuid=str(uuid.uuid4()),
                        pipeline_id=parent.id,
                        name=data.name,
                        client_id=(data.client_id if data.client_id else str(uuid.uuid4())),
                        transactional_id=(data.transactional_id if data.transactional_id else str(uuid.uuid4()))
                    )

                    # Add pipeline options
                    if isinstance(data.options, list):
                        for item in data.options:
                            option = PipelineProducerOptionOrm(
                                key=item.key,
                                value=item.value,
                                type=(item.type if item.type else 0)
                            )
                            record.options.append(option)
                    
                    db_session.add(record)
                    db_session.commit()
                except Exception as e:
                    return {
                        'success': False,
                        'message': str(e)
                    }
            
                # Return success
                return {
                    'success': True,
                    'message': 'PipelineProducer created',
                    'payload': {
                        'id': record.id,
                        'uuid': record.uuid
                    }
                }
        
        self.create_record = types.MethodType(create_record, self)



        # Update record
        async def update_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            id: TrpPrimaryKey_ID_UUID,
            data: UpdatePipelineProducerModel
        ) -> ApiResponseModel:
            if is_empty(id):
                return {
                    'success': False,
                    'message': 'ID missing'
                }
            id_type, id, msg = self._get_id_attr(id, {
                'int': 'id',
                'str': [
                    ('id', RE_INT_VALUE, lambda x: int(x)),
                    ('uuid', RE_UUID4, None)
                ]
            }, 'Invalid ID')
            if id_type is None:
                return msg
            
            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(PipelineProducerOrm)
                stmt = self._add_id_where_stmt(stmt, PipelineProducerOrm, id_type, id)

                # Get record
                record, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'PipelineProducerOrm',
                    'PipelineProducer not found',
                    'Multiple PipelineProducers found'
                )
                if record is None:
                    return msg
                
                # Get data to update
                d = {}
                for f in ['name', 'client_id', 'transactional_id']:
                    if getattr(data, f) is not None:
                        d[f] = getattr(data, f)
                if len(d) == 0:
                    return {
                        'success': False,
                        'message': 'Nothing to update'
                    }

                # Update record
                try:
                    db_session.query(PipelineProducerOrm).\
                        filter(PipelineProducerOrm.id == record.id).\
                        update(d)
                    db_session.commit()
                except Exception as e:
                    return {
                        'success': False,
                        'message': str(e)
                    }
            
                # Return success
                return {
                    'success': True,
                    'message': 'PipelineProducer updated',
                    'payload': {
                        'id': record.id
                    }
                }
        
        self.update_record = types.MethodType(update_record, self)



        # Delete record
        async def delete_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            id: TrpPrimaryKey_ID_UUID
        ) -> ApiResponseModel:
            if is_empty(id):
                return {
                    'success': False,
                    'message': 'ID missing'
                }
            id_type, id, msg = self._get_id_attr(id, {
                'int': 'id',
                'str': [
                    ('id', RE_INT_VALUE, lambda x: int(x)),
                    ('uuid', RE_UUID4, None)
                ]
            }, 'Invalid ID')
            if id_type is None:
                return msg

            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(PipelineProducerOrm)
                stmt = self._add_id_where_stmt(stmt, PipelineProducerOrm, id_type, id)

                # Get record
                record, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'PipelineProducerOrm',
                    'PipelineProducer not found',
                    'Multiple PipelineProducers found'
                )
                if record is None:
                    return msg
                
                # Delete record if found
                try:
                    db_session.execute(
                        sqlalchemy.delete(PipelineProducerOrm).\
                            where(PipelineProducerOrm.id == record.id)
                    )
                    db_session.commit()
                except Exception as e:
                    return {
                        'success': False,
                        'message': str(e)
                    }
            
                # Return success
                return {
                    'success': True,
                    'message': 'PipelineProducer deleted',
                    'payload': {
                        'id': record.id
                    }
                }
        
        self.delete_record = types.MethodType(delete_record, self)



        ### Add routes ###
        router.add_api_route('/pipeline/producers',          self.get_records,      methods=['GET'])
        router.add_api_route('/pipeline/producer/{id}',      self.get_record,       methods=['GET'])
        router.add_api_route('/pipeline/producer',           self.create_record,    methods=['POST'])
        router.add_api_route('/pipeline/producer/{id}',      self.update_record,    methods=['PUT'])
        router.add_api_route('/pipeline/producer/{id}',      self.delete_record,    methods=['DELETE'])





### Testing ###

if __name__ == '__main__':
    router = PipelineProducerRouter()
    print('router:', router)


