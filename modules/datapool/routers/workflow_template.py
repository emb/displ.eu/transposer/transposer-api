# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import types
import typing
import re
import uuid

from fastapi import (
    Depends
)

import sqlalchemy
from sqlalchemy.orm import Session

from regex_patterns import (
    RE_INT_VALUE,
    RE_UUID4,
    RE_HASH_XX,
    RE_XXHASH_CID_HEX
)
from data_utils import (
    is_empty,
    validate_uri as validate_uri_func
)

import trp_config

from ..filter import (
    split_order_fields
)

from ..models.base import (
    ApiResponseModel,
    ApiResponseWithCountModel,

    TrpPrimaryKey_ID_UUID
)
from ..models.workflow_template import (
    WorkflowTemplateOrm,
    
    WorkflowTemplateModel,
    NewWorkflowTemplateModel,
    UpdateWorkflowTemplateModel,

    WorkflowTemplateQueryParams,
    WorkflowTemplateQueryParamsPrimary,
    WorkflowTemplateQueryParamsGeneral
)
from ..models.setting import (
    SettingOrm,
    SettingQueryParamsGeneral
)

from ..conditions import (
    add_workflow_template_conditions,
    add_setting_conditions
)

from .base import AuthBaseRouter





class WorkflowTemplateRouter(AuthBaseRouter):
    def __init__(self,
        db_engine: typing.Any = None,
        app: typing.Any = None,
        router: typing.Any = None,
        config: trp_config.TrpConfig | None = None,
        auth_scheme: typing.Any = None
    ):
        super().__init__(db_engine, app, router, config, auth_scheme)

        print('WorkflowTemplateRouter ... db_engine:', db_engine)
        print('WorkflowTemplateRouter ... app:', app)
        print('WorkflowTemplateRouter ... router:', router)
        print('WorkflowTemplateRouter ... config:', config)
        print('WorkflowTemplateRouter ... auth_scheme:', auth_scheme)

        # Get records
        async def get_records(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],

            workflow_template_primary: typing.Annotated[WorkflowTemplateQueryParamsPrimary, Depends(WorkflowTemplateQueryParamsPrimary)],
            workflow_template: typing.Annotated[WorkflowTemplateQueryParamsGeneral, Depends(WorkflowTemplateQueryParamsGeneral)],

            setting: typing.Annotated[SettingQueryParamsGeneral, Depends(SettingQueryParamsGeneral)],
            
            limit: int = 0,
            offset: int = 0,
            order: str | None = None,

            use_and: bool = False,

            dataview: str | None = None
        ) -> ApiResponseWithCountModel:
            workflow_template = WorkflowTemplateQueryParams(
                workflow_template_primary.id if workflow_template.id is None else workflow_template.id,
                workflow_template_primary.uuid if workflow_template.uuid is None else workflow_template.uuid,
                workflow_template_primary.created_at if workflow_template.created_at is None else workflow_template.created_at,
                workflow_template_primary.setting_id if workflow_template.setting_id is None else workflow_template.setting_id,
                workflow_template_primary.template if workflow_template.template is None else workflow_template.template
            )

            result = None
            with Session(self._db_engine) as db_session:
                # Create count statement
                count_stmt = db_session.query(sqlalchemy.func.count(WorkflowTemplateOrm.id))

                # Create query statement
                stmt = sqlalchemy.select(WorkflowTemplateOrm)

                ## Add conditions
                where = []

                # Add workflow_template conditions
                add_workflow_template_conditions(
                    where,
                    workflow_template,
                    use_and
                )

                # Add setting conditions
                if add_setting_conditions(
                    where,
                    setting,
                    use_and
                ):
                    stmt = stmt.join(WorkflowTemplateOrm.setting, full=True)
                

                # Add conditions to statements
                if len(where):
                    cond = sqlalchemy.and_(*where)
                    count_stmt = count_stmt.where(cond)
                    stmt = stmt.where(cond)
                
                # Get total number of records
                num_records_total = count_stmt.scalar()
                if num_records_total == 0:
                    return {
                        'success': False,
                        'message': 'No WorkflowTemplates found'
                    }
                
                # Add limit and offset
                if offset > 0:
                    stmt = stmt.offset(offset)
                if limit > 0:
                    stmt = stmt.limit(limit)

                # Add order
                order = split_order_fields(order, 'workflow_template')
                if order is not None:
                    stmt = stmt.order_by(*order)
                
                # Parse dataview
                dataview_struct = self._parse_dataview(dataview)
                
                # Execute statement
                result = []
                for row in db_session.execute(stmt):
                    if row.WorkflowTemplateOrm is None:
                        continue
                    result.append(
                        row.WorkflowTemplateOrm.to_dict(dataview_struct)
                    )
            
            # If we didn't find any records
            # return an error message
            if result is None:
                return {
                    'success': False,
                    'message': 'No WorkflowTemplates found'
                }
            num_records = len(result)
            if num_records == 0:
                return {
                    'success': False,
                    'message': 'No WorkflowTemplates found'
                }
            
            # Return the result
            return {
                'success': True,
                'message': 'WorkflowTemplates found',
                'total': num_records_total,
                'count': num_records,
                'payload': result
            }

        self.get_records = types.MethodType(get_records, self)



        # Get record
        async def get_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            id: TrpPrimaryKey_ID_UUID,
            dataview: str | None = None
        ) -> ApiResponseModel:
            # Get key and extract value
            if is_empty(id):
                return {
                    'success': False,
                    'message': 'ID missing'
                }
            id_type, id, msg = self._get_id_attr(id, {
                'int': 'id',
                'str': [
                    ('id', RE_INT_VALUE, lambda x: int(x)),
                    ('uuid', RE_UUID4, None)
                ]
            }, 'Invalid ID')
            if id_type is None:
                return msg
            
            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(WorkflowTemplateOrm)
                stmt = self._add_id_where_stmt(stmt, WorkflowTemplateOrm, id_type, id)

                # Get record
                record, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'WorkflowTemplateOrm',
                    'WorkflowTemplate not found',
                    'Multiple WorkflowTemplates found'
                )
                if record is None:
                    return msg
                
                # Parse and filter
                dataview_struct = self._parse_dataview(dataview)
                record_dict = record.to_dict(dataview_struct)
                
                # Return the result
                return {
                    'success': True,
                    'message': 'WorkflowTemplate found',
                    'payload': record_dict
                }
        
        self.get_record = types.MethodType(get_record, self)



        # Create record
        async def create_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            data: NewWorkflowTemplateModel
        ) -> ApiResponseModel:
            # Get parent key and value
            parent_id = data.setting
            if parent_id is None:
                return {
                    'success': False,
                    'message': 'Setting ID missing'
                }

            parent = None
            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(SettingOrm).\
                    where(SettingOrm.id == parent_id)
                
                # Get parent record
                parent, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'SettingOrm',
                    'Setting not found',
                    'Multiple Settings found'
                )
                if parent is None:
                    return msg

                # Create record
                try:
                    record = WorkflowTemplateOrm(
                        uuid=str(uuid.uuid4()),
                        setting_id=parent.id,
                        template=data.template
                    )
                    db_session.add(record)
                    db_session.commit()
                except Exception as e:
                    return {
                        'success': False,
                        'message': str(e)
                    }
            
                # Return success
                return {
                    'success': True,
                    'message': 'WorkflowTemplate created',
                    'payload': {
                        'id': record.id,
                        'uuid': record.uuid
                    }
                }
        
        self.create_record = types.MethodType(create_record, self)



        # Update record
        async def update_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            id: TrpPrimaryKey_ID_UUID,
            data: UpdateWorkflowTemplateModel
        ) -> ApiResponseModel:
            if is_empty(id):
                return {
                    'success': False,
                    'message': 'ID missing'
                }
            id_type, id, msg = self._get_id_attr(id, {
                'int': 'id',
                'str': [
                    ('id', RE_INT_VALUE, lambda x: int(x)),
                    ('uuid', RE_UUID4, None)
                ]
            }, 'Invalid ID')
            if id_type is None:
                return msg
            
            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(WorkflowTemplateOrm)
                stmt = self._add_id_where_stmt(stmt, WorkflowTemplateOrm, id_type, id)

                # Get record
                record, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'WorkflowTemplateOrm',
                    'WorkflowTemplate not found',
                    'Multiple WorkflowTemplates found'
                )
                if record is None:
                    return msg
                
                # Get data to update
                d = {}
                for f in ['template']:
                    if getattr(data, f) is not None:
                        d[f] = getattr(data, f)
                if len(d) == 0:
                    return {
                        'success': False,
                        'message': 'Nothing to update'
                    }

                # Update record
                try:
                    db_session.query(WorkflowTemplateOrm).\
                        filter(WorkflowTemplateOrm.id == record.id).\
                        update(d)
                    db_session.commit()
                except Exception as e:
                    return {
                        'success': False,
                        'message': str(e)
                    }
            
                # Return success
                return {
                    'success': True,
                    'message': 'WorkflowTemplate updated',
                    'payload': {
                        'id': record.id
                    }
                }
        
        self.update_record = types.MethodType(update_record, self)



        # Delete record
        async def delete_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            id: TrpPrimaryKey_ID_UUID
        ) -> ApiResponseModel:
            if is_empty(id):
                return {
                    'success': False,
                    'message': 'ID missing'
                }
            id_type, id, msg = self._get_id_attr(id, {
                'int': 'id',
                'str': [
                    ('id', RE_INT_VALUE, lambda x: int(x)),
                    ('uuid', RE_UUID4, None)
                ]
            }, 'Invalid ID')
            if id_type is None:
                return msg

            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(WorkflowTemplateOrm)
                stmt = self._add_id_where_stmt(stmt, WorkflowTemplateOrm, id_type, id)

                # Get record
                record, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'WorkflowTemplateOrm',
                    'WorkflowTemplate not found',
                    'Multiple WorkflowTemplates found'
                )
                if record is None:
                    return msg
                
                # Delete record if found
                try:
                    db_session.execute(
                        sqlalchemy.delete(WorkflowTemplateOrm).\
                            where(WorkflowTemplateOrm.id == record.id)
                    )
                    db_session.commit()
                except Exception as e:
                    return {
                        'success': False,
                        'message': str(e)
                    }
            
                # Return success
                return {
                    'success': True,
                    'message': 'WorkflowTemplate deleted',
                    'payload': {
                        'id': record.id
                    }
                }
        
        self.delete_record = types.MethodType(delete_record, self)



        ### Add routes ###
        router.add_api_route('/workflow/templates',          self.get_records,      methods=['GET'])
        router.add_api_route('/workflow/template/{id}',      self.get_record,       methods=['GET'])
        router.add_api_route('/workflow/template',           self.create_record,    methods=['POST'])
        router.add_api_route('/workflow/template/{id}',      self.update_record,    methods=['PUT'])
        router.add_api_route('/workflow/template/{id}',      self.delete_record,    methods=['DELETE'])





### Testing ###

if __name__ == '__main__':
    router = WorkflowTemplateRouter()
    print('router:', router)


