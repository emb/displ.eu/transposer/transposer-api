# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import types
import typing

from fastapi import (
    Depends
)

import sqlalchemy
from sqlalchemy.orm import Session

from regex_patterns import (
    RE_INT_VALUE,
    RE_UUID4,
    RE_HASH_XX,
    RE_SHA256_HEX,
    RE_SHAKE256_KEY_HEX,
    RE_EMAIL,
    RE_USERNAME
)
from data_utils import (
    is_empty
)
import trp_config

from ..filter import (
    split_order_fields
)

from ..models.base import (
    ApiResponseModel,
    ApiResponseWithCountModel
)
from ..models.transcription_word import (
    TranscriptionWordOrm,

    NewTranscriptionWordModel,
    UpdateTranscriptionWordModel,

    TranscriptionWordQueryParams,
    TranscriptionWordQueryParamsPrimary,
    TranscriptionWordQueryParamsGeneral
)
from ..models.transcription_segment import (
    TranscriptionSegmentOrm
)

from ..conditions import (
    add_transcription_word_conditions
)

from .base import AuthBaseRouter





class TranscriptionWordRouter(AuthBaseRouter):
    def __init__(self,
        db_engine: typing.Any = None,
        app: typing.Any = None,
        router: typing.Any = None,
        config: trp_config.TrpConfig | None = None,
        auth_scheme: typing.Any = None
    ):
        super().__init__(db_engine, app, router, config, auth_scheme)

        print('TranscriptionWordRouter ... db_engine:', db_engine)
        print('TranscriptionWordRouter ... app:', app)
        print('TranscriptionWordRouter ... router:', router)
        print('TranscriptionWordRouter ... config:', config)
        print('TranscriptionWordRouter ... auth_scheme:', auth_scheme)

        # Get records
        async def get_records(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],

            transcription_word_primary: typing.Annotated[TranscriptionWordQueryParamsPrimary, Depends(TranscriptionWordQueryParamsPrimary)],
            transcription_word: typing.Annotated[TranscriptionWordQueryParamsGeneral, Depends(TranscriptionWordQueryParamsGeneral)],

            limit: int = 0,
            offset: int = 0,
            order: str | None = None,

            use_and: bool = False,

            dataview: str | None = None
        ) -> ApiResponseWithCountModel:
            transcription_word = TranscriptionWordQueryParams(
                transcription_word_primary.id if transcription_word.id is None else transcription_word.id,
                transcription_word_primary.segment_id if transcription_word.segment_id is None else transcription_word.segment_id,
                transcription_word_primary.pos if transcription_word.pos is None else transcription_word.pos,
                transcription_word_primary.start if transcription_word.start is None else transcription_word.start,
                transcription_word_primary.end if transcription_word.end is None else transcription_word.end,
                transcription_word_primary.text if transcription_word.text is None else transcription_word.text,
                transcription_word_primary.confidence if transcription_word.confidence is None else transcription_word.confidence
            )

            result = None
            with Session(self._db_engine) as db_session:
                # Create count statement
                count_stmt = db_session.query(sqlalchemy.func.count(TranscriptionWordOrm.id))

                # Create query statement
                stmt = sqlalchemy.select(TranscriptionWordOrm)

                ## Add conditions
                where = []

                # Add transcription_word conditions
                add_transcription_word_conditions(
                    where,
                    transcription_word,
                    use_and
                )

                # Add conditions to statements
                if len(where):
                    cond = sqlalchemy.and_(*where)
                    count_stmt = count_stmt.where(cond)
                    stmt = stmt.where(cond)
                
                # Get total number of records
                num_records_total = count_stmt.scalar()
                if num_records_total == 0:
                    return {
                        'success': False,
                        'message': 'No TranscriptionWords found'
                    }
                
                # Add limit and offset
                if offset > 0:
                    stmt = stmt.offset(offset)
                if limit > 0:
                    stmt = stmt.limit(limit)

                # Add order
                order = split_order_fields(order, 'transcription_word')
                if order is not None:
                    stmt = stmt.order_by(*order)
                
                # Parse dataview
                dataview_struct = self._parse_dataview(dataview)
                
                # Execute statement
                result = []
                for row in db_session.execute(stmt):
                    if row.TranscriptionWordOrm is None:
                        continue
                    result.append(
                        row.TranscriptionWordOrm.to_dict(dataview_struct)
                    )
            
            # If we didn't find any records
            # return an error message
            if result is None:
                return {
                    'success': False,
                    'message': 'No TranscriptionWords found'
                }
            num_records = len(result)
            if num_records == 0:
                return {
                    'success': False,
                    'message': 'No TranscriptionWords found'
                }
            
            # Return the result
            return {
                'success': True,
                'message': 'TranscriptionWords found',
                'total': num_records_total,
                'count': num_records,
                'payload': result
            }

        self.get_records = types.MethodType(get_records, self)



        # Get record
        async def get_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            id: int,
            dataview: str | None = None
        ) -> ApiResponseModel:
            # Get key and extract value
            if is_empty(id):
                return {
                    'success': False,
                    'message': 'ID missing'
                }
            
            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(TranscriptionWordOrm).\
                    where(TranscriptionWordOrm.id == id)

                # Get record
                record, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'TranscriptionWordOrm',
                    'TranscriptionWord not found',
                    'Multiple TranscriptionWords found'
                )
                if record is None:
                    return msg
                
                # Parse and filter
                dataview_struct = self._parse_dataview(dataview)
                record_dict = record.to_dict(dataview_struct)
                
                # Return the result
                return {
                    'success': True,
                    'message': 'TranscriptionWord found',
                    'payload': record_dict
                }
        
        self.get_record = types.MethodType(get_record, self)



        # Create record
        async def create_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            data: NewTranscriptionWordModel
        ) -> ApiResponseModel:
            # Get parent key and value
            parent_id = data.segment
            if parent_id is None:
                return {
                    'success': False,
                    'message': 'Segment ID missing'
                }

            parent = None
            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(TranscriptionSegmentOrm).\
                    where(TranscriptionSegmentOrm.id == parent_id)
                
                # Get parent record
                parent, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'TranscriptionSegmentOrm',
                    'TranscriptionSegment not found',
                    'Multiple TranscriptionSegments found'
                )
                if parent is None:
                    return msg
                
                # Create record
                try:
                    record = TranscriptionWordOrm(
                        segment_id=parent.id,
                        pos=data.pos,
                        start=data.start,
                        end=data.end,
                        text=data.text,
                        confidence=data.confidence
                    )
                    db_session.add(record)
                    db_session.commit()
                except Exception as e:
                    return {
                        'success': False,
                        'message': str(e)
                    }
            
                # Return success
                return {
                    'success': True,
                    'message': 'TranscriptionWord created',
                    'payload': {
                        'id': record.id
                    }
                }
        
        self.create_record = types.MethodType(create_record, self)



        # Update record
        async def update_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            id: int,
            data: UpdateTranscriptionWordModel
        ) -> ApiResponseModel:
            if is_empty(id):
                return {
                    'success': False,
                    'message': 'ID missing'
                }
            
            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(TranscriptionWordOrm).\
                    where(TranscriptionWordOrm.id == id)

                # Get record
                record, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'TranscriptionWordOrm',
                    'TranscriptionWord not found',
                    'Multiple TranscriptionWords found'
                )
                if record is None:
                    return msg
                
                # Get data to update
                d = {}
                for f in ['pos', 'start', 'end', 'text', 'confidence']:
                    if getattr(data, f) is not None:
                        d[f] = getattr(data, f)
                if len(d) == 0:
                    return {
                        'success': False,
                        'message': 'Nothing to update'
                    }

                # Update record
                try:
                    db_session.query(TranscriptionWordOrm).\
                        filter(TranscriptionWordOrm.id == record.id).\
                        update(d)
                    db_session.commit()
                except Exception as e:
                    return {
                        'success': False,
                        'message': str(e)
                    }
            
                # Return success
                return {
                    'success': True,
                    'message': 'TranscriptionWord updated',
                    'payload': {
                        'id': record.id
                    }
                }
        
        self.update_record = types.MethodType(update_record, self)



        # Delete record
        async def delete_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            id: int
        ) -> ApiResponseModel:
            if is_empty(id):
                return {
                    'success': False,
                    'message': 'ID missing'
                }
            
            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(TranscriptionWordOrm).\
                    where(TranscriptionWordOrm.id == id)

                # Get record
                record, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'TranscriptionWordOrm',
                    'TranscriptionWord not found',
                    'Multiple TranscriptionWords found'
                )
                if record is None:
                    return msg

                # Delete record if found
                try:
                    db_session.execute(
                        sqlalchemy.delete(TranscriptionWordOrm).\
                            where(TranscriptionWordOrm.id == record.id)
                    )
                    db_session.commit()
                except Exception as e:
                    return {
                        'success': False,
                        'message': str(e)
                    }
            
                # Return success
                return {
                    'success': True,
                    'message': 'TranscriptionWord deleted',
                    'payload': {
                        'id': record.id
                    }
                }
        
        self.delete_record = types.MethodType(delete_record, self)



        ### Add routes ###
        router.add_api_route('/transcription/words',          self.get_records,      methods=['GET'])
        router.add_api_route('/transcription/word/{id}',      self.get_record,       methods=['GET'])
        router.add_api_route('/transcription/word',           self.create_record,    methods=['POST'])
        router.add_api_route('/transcription/word/{id}',      self.update_record,    methods=['PUT'])
        router.add_api_route('/transcription/word/{id}',      self.delete_record,    methods=['DELETE'])





### Testing ###

if __name__ == '__main__':
    router = TranscriptionWordRouter()
    print('router:', router)


