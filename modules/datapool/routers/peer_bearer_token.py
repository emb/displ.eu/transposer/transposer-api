# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import types
import typing

from fastapi import (
    Depends
)

import sqlalchemy
from sqlalchemy.orm import Session

from regex_patterns import (
    RE_INT_VALUE,
    RE_UUID4,
    RE_HASH_XX,
    RE_SHA256_HEX,
    RE_SHAKE256_KEY_HEX,
    RE_EMAIL,
    RE_USERNAME,
    RE_HOST
)
from data_utils import (
    is_empty
)
import trp_config

from ..filter import (
    split_order_fields
)

from ..models.base import (
    ApiResponseModel,
    ApiResponseWithCountModel,

    TrpPrimaryKey_ID_HASH
)
from ..models.peer_bearer_token import (
    PeerBearerTokenOrm,

    PeerBearerTokenModel,
    NewPeerBearerTokenModel,
    UpdatePeerBearerTokenModel,

    PeerBearerTokenQueryParams,
    PeerBearerTokenQueryParamsPrimary,
    PeerBearerTokenQueryParamsGeneral
)
from ..models.peer import (
    PeerOrm
)

from ..conditions import (
    add_peer_bearer_token_conditions
)

from .base import AuthBaseRouter





class PeerBearerTokenRouter(AuthBaseRouter):
    def __init__(self,
        db_engine: typing.Any = None,
        app: typing.Any = None,
        router: typing.Any = None,
        config: trp_config.TrpConfig | None = None,
        auth_scheme: typing.Any = None
    ):
        super().__init__(db_engine, app, router, config, auth_scheme)

        print('PeerBearerTokenRouter ... db_engine:', db_engine)
        print('PeerBearerTokenRouter ... app:', app)
        print('PeerBearerTokenRouter ... router:', router)
        print('PeerBearerTokenRouter ... config:', config)
        print('PeerBearerTokenRouter ... auth_scheme:', auth_scheme)

        # Get records
        async def get_records(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],

            peer_bearer_token_primary: typing.Annotated[PeerBearerTokenQueryParamsPrimary, Depends(PeerBearerTokenQueryParamsPrimary)],
            peer_bearer_token: typing.Annotated[PeerBearerTokenQueryParamsGeneral, Depends(PeerBearerTokenQueryParamsGeneral)],

            limit: int = 0,
            offset: int = 0,
            order: str | None = None,

            use_and: bool = False,

            dataview: str | None = None
        ) -> ApiResponseWithCountModel:
            peer_bearer_token = PeerBearerTokenQueryParams(
                peer_bearer_token_primary.id if peer_bearer_token.id is None else peer_bearer_token.id,
                peer_bearer_token_primary.created_at if peer_bearer_token.created_at is None else peer_bearer_token.created_at,
                peer_bearer_token_primary.peer_id if peer_bearer_token.peer_id is None else peer_bearer_token.peer_id,
                peer_bearer_token_primary.token if peer_bearer_token.token is None else peer_bearer_token.token,
                peer_bearer_token_primary.is_active if peer_bearer_token.is_active is None else peer_bearer_token.is_active,
                peer_bearer_token_primary.valid_until if peer_bearer_token.valid_until is None else peer_bearer_token.valid_until
            )

            result = None
            with Session(self._db_engine) as db_session:
                # Create count statement
                count_stmt = db_session.query(sqlalchemy.func.count(PeerBearerTokenOrm.id))

                # Create query statement
                stmt = sqlalchemy.select(PeerBearerTokenOrm)

                ## Add conditions
                where = []

                # Add peer_bearer_token conditions
                add_peer_bearer_token_conditions(
                    where,
                    peer_bearer_token,
                    use_and
                )

                # Add conditions to statements
                if len(where):
                    cond = sqlalchemy.and_(*where)
                    count_stmt = count_stmt.where(cond)
                    stmt = stmt.where(cond)
                
                # Get total number of records
                num_records_total = count_stmt.scalar()
                if num_records_total == 0:
                    return {
                        'success': False,
                        'message': 'No PeerBearerTokens found'
                    }
                
                # Add limit and offset
                if offset > 0:
                    stmt = stmt.offset(offset)
                if limit > 0:
                    stmt = stmt.limit(limit)

                # Add order
                order = split_order_fields(order, 'peer_bearer_token')
                if order is not None:
                    stmt = stmt.order_by(*order)
                
                # Parse dataview
                dataview_struct = self._parse_dataview(dataview)
                
                # Execute statement
                result = []
                for row in db_session.execute(stmt):
                    if row.PeerBearerTokenOrm is None:
                        continue
                    result.append(
                        row.PeerBearerTokenOrm.to_dict(dataview_struct)
                    )
            
            # If we didn't find any records
            # return an error message
            if result is None:
                return {
                    'success': False,
                    'message': 'No PeerBearerTokens found'
                }
            num_records = len(result)
            if num_records == 0:
                return {
                    'success': False,
                    'message': 'No PeerBearerTokens found'
                }
            
            # Return the result
            return {
                'success': True,
                'message': 'PeerBearerTokens found',
                'total': num_records_total,
                'count': num_records,
                'payload': result
            }

        self.get_records = types.MethodType(get_records, self)



        # Get record
        async def get_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            id: TrpPrimaryKey_ID_HASH,
            dataview: str | None = None
        ) -> ApiResponseModel:
            # Get key and extract value
            if is_empty(id):
                return {
                    'success': False,
                    'message': 'ID missing'
                }
            id_type, id, msg = self._get_id_attr(id, {
                'int': 'id',
                'str': [
                    ('id', RE_INT_VALUE, lambda x: int(x)),
                    ('token', RE_HASH_XX, None)
                ]
            }, 'Invalid ID')
            if id_type is None:
                return msg
            
            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(PeerBearerTokenOrm)
                stmt = self._add_id_where_stmt(stmt, PeerBearerTokenOrm, id_type, id)

                # Get record
                record, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'PeerBearerTokenOrm',
                    'PeerBearerToken not found',
                    'Multiple PeerBearerTokens found'
                )
                if record is None:
                    return msg
                
                # Parse and filter
                dataview_struct = self._parse_dataview(dataview)
                record_dict = record.to_dict(dataview_struct)
                
                # Return the result
                return {
                    'success': True,
                    'message': 'PeerBearerToken found',
                    'payload': record_dict
                }
        
        self.get_record = types.MethodType(get_record, self)



        # Create record
        async def create_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            data: NewPeerBearerTokenModel
        ) -> ApiResponseModel:
            # Get parent key and value
            parent_id = data.peer
            if parent_id is None:
                return {
                    'success': False,
                    'message': 'Peer ID missing'
                }
            parent_id_type, parent_id, msg = self._get_id_attr(parent_id, {
                'int': 'id',
                'str': [
                    ('id', RE_INT_VALUE, lambda x: int(x)),
                    ('host', RE_HOST, None)
                ]
            }, 'Invalid Peer ID')
            if parent_id_type is None:
                return msg

            parent = None
            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(PeerOrm)
                stmt = self._add_id_where_stmt(stmt, PeerOrm, parent_id_type, parent_id)
                
                # Get parent record
                parent, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'PeerOrm',
                    'Peer not found',
                    'Multiple Peers found'
                )
                if parent is None:
                    return msg
                
                # Create record
                try:
                    record = PeerBearerTokenOrm(
                        peer_id=parent.id,
                        token=data.token,
                        is_active=data.is_active,
                        valid_until=data.valid_until
                    )
                    db_session.add(record)
                    db_session.commit()
                except Exception as e:
                    return {
                        'success': False,
                        'message': str(e)
                    }
            
                # Return success
                return {
                    'success': True,
                    'message': 'PeerBearerToken created',
                    'payload': {
                        'id': record.id
                    }
                }
        
        self.create_record = types.MethodType(create_record, self)



        # Update record
        async def update_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            id: TrpPrimaryKey_ID_HASH,
            data: UpdatePeerBearerTokenModel
        ) -> ApiResponseModel:
            if is_empty(id):
                return {
                    'success': False,
                    'message': 'ID missing'
                }
            id_type, id, msg = self._get_id_attr(id, {
                'int': 'id',
                'str': [
                    ('id', RE_INT_VALUE, lambda x: int(x)),
                    ('token', RE_HASH_XX, None)
                ]
            }, 'Invalid ID')
            if id_type is None:
                return msg

            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(PeerBearerTokenOrm)
                stmt = self._add_id_where_stmt(stmt, PeerBearerTokenOrm, id_type, id)

                # Get record
                record, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'PeerBearerTokenOrm',
                    'PeerBearerToken not found',
                    'Multiple PeerBearerTokens found'
                )
                if record is None:
                    return msg
                
                # Get data to update
                d = {}
                for f in ['is_active', 'valid_until']:
                    if getattr(data, f) is not None:
                        d[f] = getattr(data, f)
                if len(d) == 0:
                    return {
                        'success': False,
                        'message': 'Nothing to update'
                    }

                # Update record
                try:
                    db_session.query(PeerBearerTokenOrm).\
                        filter(PeerBearerTokenOrm.id == record.id).\
                        update(d)
                    db_session.commit()
                except Exception as e:
                    return {
                        'success': False,
                        'message': str(e)
                    }
            
                # Return success
                return {
                    'success': True,
                    'message': 'PeerBearerToken updated',
                    'payload': {
                        'id': record.id
                    }
                }
        
        self.update_record = types.MethodType(update_record, self)



        # Delete record
        async def delete_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            id: TrpPrimaryKey_ID_HASH
        ) -> ApiResponseModel:
            if is_empty(id):
                return {
                    'success': False,
                    'message': 'ID missing'
                }
            id_type, id, msg = self._get_id_attr(id, {
                'int': 'id',
                'str': [
                    ('id', RE_INT_VALUE, lambda x: int(x)),
                    ('token', RE_HASH_XX, None)
                ]
            }, 'Invalid ID')
            if id_type is None:
                return msg
            
            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(PeerBearerTokenOrm)
                stmt = self._add_id_where_stmt(stmt, PeerBearerTokenOrm, id_type, id)

                # Get record
                record, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'PeerBearerTokenOrm',
                    'PeerBearerToken not found',
                    'Multiple PeerBearerTokens found'
                )
                if record is None:
                    return msg

                # Delete record if found
                try:
                    db_session.execute(
                        sqlalchemy.delete(PeerBearerTokenOrm).\
                            where(PeerBearerTokenOrm.id == record.id)
                    )
                    db_session.commit()
                except Exception as e:
                    return {
                        'success': False,
                        'message': str(e)
                    }
            
                # Return success
                return {
                    'success': True,
                    'message': 'PeerBearerToken deleted',
                    'payload': {
                        'id': record.id
                    }
                }
        
        self.delete_record = types.MethodType(delete_record, self)



        ### Add routes ###
        router.add_api_route('/peer/bearer-tokens',          self.get_records,      methods=['GET'])
        router.add_api_route('/peer/bearer-token/{id}',      self.get_record,       methods=['GET'])
        router.add_api_route('/peer/bearer-token',           self.create_record,    methods=['POST'])
        router.add_api_route('/peer/bearer-token/{id}',      self.update_record,    methods=['PUT'])
        router.add_api_route('/peer/bearer-token/{id}',      self.delete_record,    methods=['DELETE'])





### Testing ###

if __name__ == '__main__':
    router = PeerBearerTokenRouter()
    print('router:', router)


