# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import types
import typing
import re
import uuid

from fastapi import (
    Depends
)

import sqlalchemy
from sqlalchemy.orm import Session

from regex_patterns import (
    RE_INT_VALUE,
    RE_UUID4,
    RE_HASH_XX,
    RE_XXHASH_CID_HEX
)
from data_utils import (
    is_empty,
    validate_uri as validate_uri_func
)

import trp_config

from ..filter import (
    split_order_fields
)

from ..models.base import (
    ApiResponseModel,
    ApiResponseWithCountModel,

    TrpPrimaryKey_ID_UUID
)
from ..models.workflow_step import (
    WorkflowStepOrm,
    
    WorkflowStepModel,
    NewWorkflowStepModel,
    UpdateWorkflowStepModel,

    WorkflowStepQueryParams,
    WorkflowStepQueryParamsPrimary,
    WorkflowStepQueryParamsGeneral
)
from ..models.workflow import (
    WorkflowOrm,
    WorkflowQueryParamsGeneral
)
from ..models.document import (
    DocumentOrm,
    DocumentQueryParamsGeneral
)
from ..models.user import (
    UserOrm,
    UserQueryParamsGeneral
)

from ..conditions import (
    add_workflow_step_conditions,
    add_user_conditions,
    add_document_conditions,
    add_workflow_conditions
)

from .base import AuthBaseRouter





class WorkflowStepRouter(AuthBaseRouter):
    def __init__(self,
        db_engine: typing.Any = None,
        app: typing.Any = None,
        router: typing.Any = None,
        config: trp_config.TrpConfig | None = None,
        auth_scheme: typing.Any = None
    ):
        super().__init__(db_engine, app, router, config, auth_scheme)

        print('WorkflowStepRouter ... db_engine:', db_engine)
        print('WorkflowStepRouter ... app:', app)
        print('WorkflowStepRouter ... router:', router)
        print('WorkflowStepRouter ... config:', config)
        print('WorkflowStepRouter ... auth_scheme:', auth_scheme)

        # Get records
        async def get_records(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],

            workflow_step_primary: typing.Annotated[WorkflowStepQueryParamsPrimary, Depends(WorkflowStepQueryParamsPrimary)],
            workflow_step: typing.Annotated[WorkflowStepQueryParamsGeneral, Depends(WorkflowStepQueryParamsGeneral)],

            workflow: typing.Annotated[WorkflowQueryParamsGeneral, Depends(WorkflowQueryParamsGeneral)],
            document: typing.Annotated[DocumentQueryParamsGeneral, Depends(DocumentQueryParamsGeneral)],
            owner: typing.Annotated[UserQueryParamsGeneral, Depends(UserQueryParamsGeneral)],

            limit: int = 0,
            offset: int = 0,
            order: str | None = None,

            use_and: bool = False,

            dataview: str | None = None
        ) -> ApiResponseWithCountModel:
            workflow_step = WorkflowStepQueryParams(
                workflow_step_primary.id if workflow_step.id is None else workflow_step.id,
                workflow_step_primary.uuid if workflow_step.uuid is None else workflow_step.uuid,
                workflow_step_primary.workflow_id if workflow_step.workflow_id is None else workflow_step.workflow_id,
                workflow_step_primary.pos if workflow_step.pos is None else workflow_step.pos,
                workflow_step_primary.allow_if_previous_failed if workflow_step.allow_if_previous_failed is None else workflow_step.allow_if_previous_failed,
                workflow_step_primary.action if workflow_step.action is None else workflow_step.action,
                workflow_step_primary.topic_send if workflow_step.topic_send is None else workflow_step.topic_send,
                workflow_step_primary.topic_receive if workflow_step.topic_receive is None else workflow_step.topic_receive,
                workflow_step_primary.progress if workflow_step.progress is None else workflow_step.progress,
                workflow_step_primary.status if workflow_step.status is None else workflow_step.status,
                workflow_step_primary.reason if workflow_step.reason is None else workflow_step.reason
            )

            result = None
            with Session(self._db_engine) as db_session:
                # Create count statement
                count_stmt = db_session.query(sqlalchemy.func.count(WorkflowStepOrm.id))

                # Create query statement
                stmt = sqlalchemy.select(WorkflowStepOrm)

                ## Add conditions
                where = []

                has_join__workflow = False

                # Add workflow_step conditions
                add_workflow_step_conditions(
                    where,
                    workflow_step,
                    use_and
                )

                # Add workflow conditions
                if add_workflow_conditions(
                    where,
                    workflow,
                    use_and
                ):
                    stmt = stmt.join(WorkflowStepOrm.workflow, full=True)
                    has_join__workflow = True
                
                # Add document conditions
                if add_document_conditions(
                    where,
                    document,
                    use_and
                ):
                    if not has_join__workflow:
                        stmt = stmt.join(WorkflowStepOrm.workflow, full=True)
                        has_join__workflow = True
                    stmt = stmt.join(WorkflowOrm.document, full=True)
                    has_join__document = True
                
                # Add user conditions
                if add_user_conditions(
                    where,
                    owner,
                    use_and
                ):
                    if not has_join__workflow:
                        stmt = stmt.join(WorkflowStepOrm.workflow, full=True)
                        has_join__workflow = True
                    if not has_join__document:
                        stmt = stmt.join(WorkflowOrm.document, full=True)
                        has_join__document = True
                    stmt = stmt.join(DocumentOrm.owner, full=True)
                    has_join__user = True
                

                # Add conditions to statements
                if len(where):
                    cond = sqlalchemy.and_(*where)
                    count_stmt = count_stmt.where(cond)
                    stmt = stmt.where(cond)
                
                # Get total number of records
                num_records_total = count_stmt.scalar()
                if num_records_total == 0:
                    return {
                        'success': False,
                        'message': 'No WorkflowSteps found'
                    }
                
                # Add limit and offset
                if offset > 0:
                    stmt = stmt.offset(offset)
                if limit > 0:
                    stmt = stmt.limit(limit)

                # Add order
                order = split_order_fields(order, 'workflow_step')
                if order is not None:
                    stmt = stmt.order_by(*order)
                
                # Parse dataview
                dataview_struct = self._parse_dataview(dataview)
                
                # Execute statement
                result = []
                for row in db_session.execute(stmt):
                    if row.WorkflowStepOrm is None:
                        continue
                    result.append(
                        row.WorkflowStepOrm.to_dict(dataview_struct)
                    )
            
            # If we didn't find any records
            # return an error message
            if result is None:
                return {
                    'success': False,
                    'message': 'No WorkflowSteps found'
                }
            num_records = len(result)
            if num_records == 0:
                return {
                    'success': False,
                    'message': 'No WorkflowSteps found'
                }
            
            # Return the result
            return {
                'success': True,
                'message': 'WorkflowSteps found',
                'total': num_records_total,
                'count': num_records,
                'payload': result
            }

        self.get_records = types.MethodType(get_records, self)



        # Get record
        async def get_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            id: TrpPrimaryKey_ID_UUID,
            dataview: str | None = None
        ) -> ApiResponseModel:
            # Get key and extract value
            if is_empty(id):
                return {
                    'success': False,
                    'message': 'ID missing'
                }
            id_type, id, msg = self._get_id_attr(id, {
                'int': 'id',
                'str': [
                    ('id', RE_INT_VALUE, lambda x: int(x)),
                    ('uuid', RE_UUID4, None)
                ]
            }, 'Invalid ID')
            if id_type is None:
                return msg
            
            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(WorkflowStepOrm)
                stmt = self._add_id_where_stmt(stmt, WorkflowStepOrm, id_type, id)

                # Get record
                record, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'WorkflowStepOrm',
                    'WorkflowStep not found',
                    'Multiple WorkflowSteps found'
                )
                if record is None:
                    return msg
                
                # Parse and filter
                dataview_struct = self._parse_dataview(dataview)
                record_dict = record.to_dict(dataview_struct)
                
                # Return the result
                return {
                    'success': True,
                    'message': 'WorkflowStep found',
                    'payload': record_dict
                }
        
        self.get_record = types.MethodType(get_record, self)



        # Create record
        async def create_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            data: NewWorkflowStepModel
        ) -> ApiResponseModel:
            # Get parent key and value
            parent_id = data.media
            if parent_id is None:
                return {
                    'success': False,
                    'message': 'Workflow ID missing'
                }
            parent_id_type, parent_id, msg = self._get_id_attr(parent_id, {
                'int': 'id',
                'str': [
                    ('id', RE_INT_VALUE, lambda x: int(x)),
                    ('uuid', RE_UUID4, None)
                ]
            }, 'Invalid Workflow ID')
            if parent_id_type is None:
                return msg

            parent = None
            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(WorkflowOrm)
                stmt = self._add_id_where_stmt(stmt, WorkflowOrm, parent_id_type, parent_id)
                
                # Get parent record
                parent, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'WorkflowOrm',
                    'Workflow not found',
                    'Multiple Workflows found'
                )
                if parent is None:
                    return msg

                # Create record
                try:
                    record = WorkflowStepOrm(
                        uuid=str(uuid.uuid4()),
                        workflow_id=parent.id,
                        pos=data.pos,
                        allow_if_previous_failed=data.allow_if_previous_failed,
                        action=data.action,
                        topic_send=data.topic_send,
                        topic_receive=data.topic_receive,
                        progress=0.0,
                        status=1
                    )
                    db_session.add(record)
                    db_session.commit()
                except Exception as e:
                    return {
                        'success': False,
                        'message': str(e)
                    }
            
                # Return success
                return {
                    'success': True,
                    'message': 'WorkflowStep created',
                    'payload': {
                        'id': record.id,
                        'uuid': record.uuid
                    }
                }
        
        self.create_record = types.MethodType(create_record, self)



        # Update record
        async def update_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            id: TrpPrimaryKey_ID_UUID,
            data: UpdateWorkflowStepModel
        ) -> ApiResponseModel:
            if is_empty(id):
                return {
                    'success': False,
                    'message': 'ID missing'
                }
            id_type, id, msg = self._get_id_attr(id, {
                'int': 'id',
                'str': [
                    ('id', RE_INT_VALUE, lambda x: int(x)),
                    ('uuid', RE_UUID4, None)
                ]
            }, 'Invalid ID')
            if id_type is None:
                return msg
            
            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(WorkflowStepOrm)
                stmt = self._add_id_where_stmt(stmt, WorkflowStepOrm, id_type, id)

                # Get record
                record, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'WorkflowStepOrm',
                    'WorkflowStep not found',
                    'Multiple WorkflowSteps found'
                )
                if record is None:
                    return msg
                
                # Get data to update
                d = {}
                for f in ['pos', 'allow_if_previous_failed', 'action', 'topic_send', 'topic_receive', 'progress', 'status', 'reason']:
                    if getattr(data, f) is not None:
                        d[f] = getattr(data, f)
                if len(d) == 0:
                    return {
                        'success': False,
                        'message': 'Nothing to update'
                    }

                # Update record
                try:
                    db_session.query(WorkflowStepOrm).\
                        filter(WorkflowStepOrm.id == record.id).\
                        update(d)
                    db_session.commit()
                except Exception as e:
                    return {
                        'success': False,
                        'message': str(e)
                    }
            
                # Return success
                return {
                    'success': True,
                    'message': 'WorkflowStep updated',
                    'payload': {
                        'id': record.id
                    }
                }
        
        self.update_record = types.MethodType(update_record, self)



        # Delete record
        async def delete_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            id: TrpPrimaryKey_ID_UUID
        ) -> ApiResponseModel:
            if is_empty(id):
                return {
                    'success': False,
                    'message': 'ID missing'
                }
            id_type, id, msg = self._get_id_attr(id, {
                'int': 'id',
                'str': [
                    ('id', RE_INT_VALUE, lambda x: int(x)),
                    ('uuid', RE_UUID4, None)
                ]
            }, 'Invalid ID')
            if id_type is None:
                return msg

            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(WorkflowStepOrm)
                stmt = self._add_id_where_stmt(stmt, WorkflowStepOrm, id_type, id)

                # Get record
                record, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'WorkflowStepOrm',
                    'WorkflowStep not found',
                    'Multiple WorkflowSteps found'
                )
                if record is None:
                    return msg
                
                # Delete record if found
                try:
                    db_session.execute(
                        sqlalchemy.delete(WorkflowStepOrm).\
                            where(WorkflowStepOrm.id == record.id)
                    )
                    db_session.commit()
                except Exception as e:
                    return {
                        'success': False,
                        'message': str(e)
                    }
            
                # Return success
                return {
                    'success': True,
                    'message': 'WorkflowStep deleted',
                    'payload': {
                        'id': record.id
                    }
                }
        
        self.delete_record = types.MethodType(delete_record, self)



        ### Add routes ###
        router.add_api_route('/workflow/steps',          self.get_records,      methods=['GET'])
        router.add_api_route('/workflow/step/{id}',      self.get_record,       methods=['GET'])
        router.add_api_route('/workflow/step',           self.create_record,    methods=['POST'])
        router.add_api_route('/workflow/step/{id}',      self.update_record,    methods=['PUT'])
        router.add_api_route('/workflow/step/{id}',      self.delete_record,    methods=['DELETE'])





### Testing ###

if __name__ == '__main__':
    router = WorkflowStepRouter()
    print('router:', router)


