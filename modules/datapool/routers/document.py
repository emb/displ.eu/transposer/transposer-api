# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import types
import typing
import re
import uuid

from fastapi import (
    Depends
)

import sqlalchemy
from sqlalchemy.orm import Session

from regex_patterns import (
    RE_INT_VALUE,
    RE_UUID4,
    RE_EMAIL,
    RE_USERNAME
)
from data_utils import (
    is_empty
)
import trp_config

from ..filter import (
    split_order_fields
)

from ..models.base import (
    ApiResponseModel,
    ApiResponseWithCountModel,

    TrpPrimaryKey_ID_UUID
)
from ..models.document import (
    DocumentOrm,
    
    DocumentModel,
    NewDocumentModel,
    UpdateDocumentModel,
    NewDocumentStructureModel,
    UpdateDocumentStructureModel,

    DocumentQueryParams,
    DocumentQueryParamsPrimary,
    DocumentQueryParamsGeneral
)
from ..models.user import (
    UserOrm,
    UserQueryParamsGeneral
)
from ..models.caption import (
    CaptionOrm,
    CaptionQueryParamsGeneral
)
from ..models.content import (
    ContentOrm,
    ContentQueryParamsGeneral
)
from ..models.media import (
    MediaOrm,
    MediaQueryParamsGeneral
)
from ..models.request import (
    RequestOrm,
    RequestQueryParamsGeneral
)
from ..models.access_token import (
    AccessTokenOrm,
    AccessTokenQueryParamsGeneral
)
from ..models.bearer_token import (
    BearerTokenOrm,
    BearerTokenQueryParamsGeneral
)
from ..models.peer import (
    PeerOrm,
    PeerQueryParamsGeneral
)
from ..models.peer_connection import (
    PeerConnectionOrm,
    PeerConnectionQueryParamsGeneral
)
from ..models.peer_connection_path import (
    PeerConnectionPathOrm,
    PeerConnectionPathQueryParamsGeneral
)
from ..models.peer_access_token import (
    PeerAccessTokenOrm,
    PeerAccessTokenQueryParamsGeneral
)
from ..models.peer_bearer_token import (
    PeerBearerTokenOrm,
    PeerBearerTokenQueryParamsGeneral
)
from ..models.transcription import (
    TranscriptionOrm,
    TranscriptionQueryParamsGeneral
)
from ..models.transcription_segment import (
    TranscriptionSegmentOrm,
    TranscriptionSegmentQueryParamsGeneral
)
from ..models.transcription_word import (
    TranscriptionWordOrm,
    TranscriptionWordQueryParamsGeneral
)
from ..models.transcription_language_prob import (
    TranscriptionLanguageProbOrm,
    TranscriptionLanguageProbQueryParamsGeneral
)
from ..models.transcription_speech_activity import (
    TranscriptionSpeechActivityOrm,
    TranscriptionSpeechActivityQueryParamsGeneral
)

from ..conditions import (
    add_document_conditions,
    add_user_conditions,
    add_content_conditions,
    add_caption_conditions,
    add_media_conditions,
    add_request_conditions,
    add_access_token_conditions,
    add_bearer_token_conditions,
    add_peer_conditions,
    add_peer_connection_conditions,
    add_peer_access_token_conditions,
    add_peer_bearer_token_conditions,
    add_peer_connection_path_conditions,
    add_transcription_conditions,
    add_transcription_segment_conditions,
    add_transcription_language_prob_conditions,
    add_transcription_speech_activity_conditions,
    add_transcription_word_conditions
)

from .base import AuthBaseRouter





class DocumentRouter(AuthBaseRouter):
    def __init__(self,
        db_engine: typing.Any = None,
        app: typing.Any = None,
        router: typing.Any = None,
        config: trp_config.TrpConfig | None = None,
        auth_scheme: typing.Any = None
    ):
        super().__init__(db_engine, app, router, config, auth_scheme)

        print('DocumentRouter ... db_engine:', db_engine)
        print('DocumentRouter ... app:', app)
        print('DocumentRouter ... router:', router)
        print('DocumentRouter ... config:', config)
        print('DocumentRouter ... auth_scheme:', auth_scheme)

        # Get records
        async def get_records(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],

            document_primary: typing.Annotated[DocumentQueryParamsPrimary, Depends(DocumentQueryParamsPrimary)],
            document: typing.Annotated[DocumentQueryParamsGeneral, Depends(DocumentQueryParamsGeneral)],

            owner: typing.Annotated[UserQueryParamsGeneral, Depends(UserQueryParamsGeneral)],
            content: typing.Annotated[ContentQueryParamsGeneral, Depends(ContentQueryParamsGeneral)],
            caption: typing.Annotated[CaptionQueryParamsGeneral, Depends(CaptionQueryParamsGeneral)],
            media: typing.Annotated[MediaQueryParamsGeneral, Depends(MediaQueryParamsGeneral)],
            request: typing.Annotated[RequestQueryParamsGeneral, Depends(RequestQueryParamsGeneral)],
            
            access_token: typing.Annotated[AccessTokenQueryParamsGeneral, Depends(AccessTokenQueryParamsGeneral)],
            bearer_token: typing.Annotated[BearerTokenQueryParamsGeneral, Depends(BearerTokenQueryParamsGeneral)],
            
            peer: typing.Annotated[PeerQueryParamsGeneral, Depends(PeerQueryParamsGeneral)],
            peer_connection: typing.Annotated[PeerConnectionQueryParamsGeneral, Depends(PeerConnectionQueryParamsGeneral)],
            peer_access_token: typing.Annotated[PeerAccessTokenQueryParamsGeneral, Depends(PeerAccessTokenQueryParamsGeneral)],
            peer_bearer_token: typing.Annotated[PeerBearerTokenQueryParamsGeneral, Depends(PeerBearerTokenQueryParamsGeneral)],
            peer_connection_path: typing.Annotated[PeerConnectionPathQueryParamsGeneral, Depends(PeerConnectionPathQueryParamsGeneral)],

            transcription: typing.Annotated[TranscriptionQueryParamsGeneral, Depends(TranscriptionQueryParamsGeneral)],
            transcription_segment: typing.Annotated[TranscriptionSegmentQueryParamsGeneral, Depends(TranscriptionSegmentQueryParamsGeneral)],
            transcription_language_prob: typing.Annotated[TranscriptionLanguageProbQueryParamsGeneral, Depends(TranscriptionLanguageProbQueryParamsGeneral)],
            transcription_speech_activity: typing.Annotated[TranscriptionSpeechActivityQueryParamsGeneral, Depends(TranscriptionSpeechActivityQueryParamsGeneral)],
            transcription_word: typing.Annotated[TranscriptionWordQueryParamsGeneral, Depends(TranscriptionWordQueryParamsGeneral)],

            limit: int = 0,
            offset: int = 0,
            order: str | None = None,

            use_and: bool = False,

            dataview: str | None = None
        ) -> ApiResponseWithCountModel:
            document = DocumentQueryParams(
                document_primary.id if document.id is None else document.id,
                document_primary.uuid if document.uuid is None else document.uuid,
                document_primary.created_at if document.created_at is None else document.created_at,
                document_primary.modified_at if document.modified_at is None else document.modified_at,
                document_primary.owner_id if document.owner_id is None else document.owner_id,
                document_primary.type if document.type is None else document.type,
                document_primary.status if document.status is None else document.status,
                document_primary.reason if document.reason is None else document.reason
            )

            result = None
            with Session(self._db_engine) as db_session:
                # Create count statement
                count_stmt = db_session.query(sqlalchemy.func.count(DocumentOrm.id))

                # Create query statement
                stmt = sqlalchemy.select(DocumentOrm)

                ## Add conditions
                where = []

                has_join__user = False
                has_join__content = False
                has_join__media = False
                has_join__request = False
                has_join__peer = False
                has_join__peer_connection = False
                has_join__peer_connection_path = False
                has_join__transcription = False
                has_join__transcription_segment = False
                has_join__transcription_word = False
                has_join__transcription_language_prob = False
                has_join__transcription_speech_activity = False

                # Add document conditions
                add_document_conditions(
                    where,
                    document,
                    use_and
                )
                
                # Add user conditions
                if add_user_conditions(
                    where,
                    owner,
                    use_and
                ):
                    stmt = stmt.join(DocumentOrm.owner, full=True)
                    has_join__user = True
                
                # Add content conditions
                if add_content_conditions(
                    where,
                    content,
                    use_and
                ):
                    stmt = stmt.join(DocumentOrm.contents, full=True)
                    has_join__content = True
                
                # Add caption conditions
                if add_caption_conditions(
                    where,
                    caption,
                    use_and
                ):
                    if not has_join__content:
                        stmt = stmt.join(DocumentOrm.contents, full=True)
                        has_join__content = True
                    stmt = stmt.join(ContentOrm.captions, full=True)
                
                # Add media conditions
                if add_media_conditions(
                    where,
                    media,
                    use_and
                ):
                    stmt = stmt.join(DocumentOrm.medias, full=True)
                    has_join__media = True
                
                # Add request conditions
                if add_request_conditions(
                    where,
                    request,
                    use_and
                ):
                    stmt = stmt.join(DocumentOrm.requests, full=True)
                    has_join__request = True
                
                # Add access_token conditions
                if add_access_token_conditions(
                    where,
                    access_token,
                    use_and
                ):
                    if not has_join__user:
                        stmt = stmt.join(DocumentOrm.owner, full=True)
                        has_join__user = True
                    stmt = stmt.join(UserOrm.access_tokens, full=True)
                
                # Add bearer_token conditions
                if add_bearer_token_conditions(
                    where,
                    bearer_token,
                    use_and
                ):
                    if not has_join__user:
                        stmt = stmt.join(DocumentOrm.owner, full=True)
                        has_join__user = True
                    stmt = stmt.join(UserOrm.bearer_tokens, full=True)

                # Add peer conditions
                if add_peer_conditions(
                    where,
                    peer,
                    use_and
                ):
                    if not has_join__user:
                        stmt = stmt.join(DocumentOrm.owner, full=True)
                        has_join__user = True
                    stmt = stmt.join(UserOrm.peers, full=True)
                    has_join__peer = True

                # Add peer_connection conditions
                if add_peer_connection_conditions(
                    where,
                    peer_connection,
                    use_and
                ):
                    if not has_join__user:
                        stmt = stmt.join(DocumentOrm.owner, full=True)
                        has_join__user = True
                    if not has_join__peer:
                        stmt = stmt.join(UserOrm.peers, full=True)
                        has_join__peer = True
                    stmt = stmt.join(PeerOrm.connections, full=True)
                    has_join__peer_connection = True

                # Add peer_connection_path conditions
                if add_peer_connection_path_conditions(
                    where,
                    peer_connection_path,
                    use_and
                ):
                    if not has_join__user:
                        stmt = stmt.join(DocumentOrm.owner, full=True)
                        has_join__user = True
                    if not has_join__peer:
                        stmt = stmt.join(UserOrm.peers, full=True)
                        has_join__peer = True
                    if not has_join__peer_connection:
                        stmt = stmt.join(PeerOrm.connections, full=True)
                        has_join__peer_connection = True
                    stmt = stmt.join(PeerConnectionOrm.paths, full=True)
                    has_join__peer_connection_path = True

                # Add peer_access_token conditions
                if add_peer_access_token_conditions(
                    where,
                    peer_access_token,
                    use_and
                ):
                    if not has_join__user:
                        stmt = stmt.join(DocumentOrm.owner, full=True)
                        has_join__user = True
                    if not has_join__peer:
                        stmt = stmt.join(UserOrm.peers, full=True)
                        has_join__peer = True
                    stmt = stmt.join(PeerOrm.access_tokens, full=True)

                # Add peer_bearer_token conditions
                if add_peer_bearer_token_conditions(
                    where,
                    peer_bearer_token,
                    use_and
                ):
                    if not has_join__user:
                        stmt = stmt.join(DocumentOrm.owner, full=True)
                        has_join__user = True
                    if not has_join__peer:
                        stmt = stmt.join(UserOrm.peers, full=True)
                        has_join__peer = True
                    stmt = stmt.join(PeerOrm.bearer_tokens, full=True)

                # Add transcription conditions
                if add_transcription_conditions(
                    where,
                    transcription,
                    use_and
                ):
                    if not has_join__media:
                        stmt = stmt.join(DocumentOrm.medias, full=True)
                        has_join__media = True
                    stmt = stmt.join(MediaOrm.transcriptions, full=True)
                    has_join__transcription = True

                # Add transcription_segment conditions
                if add_transcription_segment_conditions(
                    where,
                    transcription_segment,
                    use_and
                ):
                    if not has_join__media:
                        stmt = stmt.join(DocumentOrm.medias, full=True)
                        has_join__media = True
                    if not has_join__transcription:
                        stmt = stmt.join(MediaOrm.transcriptions, full=True)
                        has_join__transcription = True
                    stmt = stmt.join(TranscriptionOrm.segments, full=True)
                    has_join__transcription_segment = True

                # Add transcription_word conditions
                if add_transcription_word_conditions(
                    where,
                    transcription_word,
                    use_and
                ):
                    if not has_join__media:
                        stmt = stmt.join(DocumentOrm.medias, full=True)
                        has_join__media = True
                    if not has_join__transcription:
                        stmt = stmt.join(MediaOrm.transcriptions, full=True)
                        has_join__transcription = True
                    if not has_join__transcription_segment:
                        stmt = stmt.join(TranscriptionOrm.segments, full=True)
                        has_join__transcription_segment = True
                    stmt = stmt.join(TranscriptionSegmentOrm.words, full=True)
                    has_join__transcription_word = True

                # Add transcription_language_prob conditions
                if add_transcription_language_prob_conditions(
                    where,
                    transcription_language_prob,
                    use_and
                ):
                    if not has_join__media:
                        stmt = stmt.join(DocumentOrm.medias, full=True)
                        has_join__media = True
                    if not has_join__transcription:
                        stmt = stmt.join(MediaOrm.transcriptions, full=True)
                        has_join__transcription = True
                    stmt = stmt.join(TranscriptionOrm.language_probs, full=True)
                    has_join__transcription_language_prob = True

                # Add transcription_speech_activity conditions
                if add_transcription_speech_activity_conditions(
                    where,
                    transcription_speech_activity,
                    use_and
                ):
                    if not has_join__media:
                        stmt = stmt.join(DocumentOrm.medias, full=True)
                        has_join__media = True
                    if not has_join__transcription:
                        stmt = stmt.join(MediaOrm.transcriptions, full=True)
                        has_join__transcription = True
                    stmt = stmt.join(TranscriptionOrm.speech_activities, full=True)
                    has_join__transcription_speech_activity = True
                

                # Add conditions to statements
                if len(where):
                    cond = sqlalchemy.and_(*where)
                    count_stmt = count_stmt.where(cond)
                    stmt = stmt.where(cond)
                
                # Get total number of records
                num_records_total = count_stmt.scalar()
                if num_records_total == 0:
                    return {
                        'success': False,
                        'message': 'No Documents found'
                    }
                
                # Add limit and offset
                if offset > 0:
                    stmt = stmt.offset(offset)
                if limit > 0:
                    stmt = stmt.limit(limit)

                # Add order
                order = split_order_fields(order, 'document')
                if order is not None:
                    stmt = stmt.order_by(*order)
                
                # Parse dataview
                dataview_struct = self._parse_dataview(dataview)
                
                # Execute statement
                result = []
                for row in db_session.execute(stmt):
                    if row.DocumentOrm is None:
                        continue
                    result.append(
                        row.DocumentOrm.to_dict(dataview_struct)
                    )
            
            # If we didn't find any records
            # return an error message
            if result is None:
                return {
                    'success': False,
                    'message': 'No Documents found'
                }
            num_records = len(result)
            if num_records == 0:
                return {
                    'success': False,
                    'message': 'No Documents found'
                }
            
            # Return the result
            return {
                'success': True,
                'message': 'Documents found',
                'total': num_records_total,
                'count': num_records,
                'payload': result
            }

        self.get_records = types.MethodType(get_records, self)



        # Get record
        async def get_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            id: TrpPrimaryKey_ID_UUID,
            dataview: str | None = None
        ) -> ApiResponseModel:
            # Get key and extract value
            if is_empty(id):
                return {
                    'success': False,
                    'message': 'ID missing'
                }
            id_type, id, msg = self._get_id_attr(id, {
                'int': 'id',
                'str': [
                    ('id', RE_INT_VALUE, lambda x: int(x)),
                    ('uuid', RE_UUID4, None)
                ]
            }, 'Invalid ID')
            if id_type is None:
                return msg
            
            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(DocumentOrm)
                stmt = self._add_id_where_stmt(stmt, DocumentOrm, id_type, id)

                # Get record
                record, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'DocumentOrm',
                    'Document not found',
                    'Multiple Documents found'
                )
                if record is None:
                    return msg
                
                # Parse and filter
                dataview_struct = self._parse_dataview(dataview)
                record_dict = record.to_dict(dataview_struct)
                
                # Return the result
                return {
                    'success': True,
                    'message': 'Document found',
                    'payload': record_dict
                }
        
        self.get_record = types.MethodType(get_record, self)



        # Create record
        async def create_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            data: NewDocumentStructureModel
        ) -> ApiResponseModel:
            # Get parent key and value
            parent_id = data.owner
            if parent_id is None:
                return {
                    'success': False,
                    'message': 'User ID missing'
                }
            parent_id_type, parent_id, msg = self._get_id_attr(parent_id, {
                'int': 'id',
                'str': [
                    ('id', RE_INT_VALUE, lambda x: int(x)),
                    ('uuid', RE_UUID4, None),
                    ('email', RE_EMAIL, None),
                    ('username', RE_USERNAME, None)
                ]
            }, 'Invalid User ID')
            if parent_id_type is None:
                return msg

            parent = None
            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(UserOrm)
                stmt = self._add_id_where_stmt(stmt, UserOrm, parent_id_type, parent_id)
                
                # Get parent record
                parent, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'UserOrm',
                    'User not found',
                    'Multiple Users found'
                )
                if parent is None:
                    return msg
                
                # Create record
                try:
                    record = DocumentOrm(
                        owner_id=parent.id,
                        type=data.type,
                        uuid=str(uuid.uuid4()),
                        status=1
                    )

                    # Add request
                    if data.request is not None:
                        request = RequestOrm(
                            owner_id=parent.id,
                            peer_args=data.request.peer_args,
                            is_async=data.request.is_async,
                            status=1
                        )
                        record.requests.append(request)
                    
                    # Add content items
                    content_items = data.content
                    if content_items is not None and not isinstance(content_items, list):
                        content_items = [content_items]
                    if isinstance(content_items, list):
                        i = 0
                        for item in content_items:
                            content = ContentOrm(
                                pos=i,
                                lang=item.lang,
                                is_original=item.is_original,
                                key=item.key,
                                value=item.value,
                                confidence=(1.0 if item.is_original else item.confidence),
                                status=(3 if item.is_original else 1),
                            )
                            record.contents.append(content)
                            if isinstance(item.start, (int, float)) and item.start >= 0:
                                caption = CaptionOrm(
                                    start=item.start,
                                    end=(item.end if isinstance(item.end, (int, float)) and item.end > 0 else -1.0)
                                )
                                content.captions.append(caption)
                            if isinstance(item.captions, list):
                                for caption_item in item.captions:
                                    caption = CaptionOrm(
                                        start=caption_item.start,
                                        end=(caption_item.end if isinstance(caption_item.end, (int, float)) and caption_item.end > 0 else -1.0)
                                    )
                                    content.captions.append(caption)
                            i += 1

                    # Add media
                    if data.media is not None:
                        media = MediaOrm(
                            uri=data.media.uri,
                            cid=data.media.cid,
                            type=data.media.type,
                            status=1
                        )
                        record.medias.append(media)

                        # Add transcription
                        transcription_items = data.media.transcription
                        if transcription_items is not None and not isinstance(transcription_items, list):
                            transcription_items = [transcription_items]
                        if transcription_items is not None:
                            i = 0
                            for transcription_item in transcription_items:
                                transcription = TranscriptionOrm(
                                    lang=transcription_item.lang
                                )
                                media.transcriptions.append(transcription)
                                # Add transcription segments
                                if isinstance(transcription_item.segments, list):
                                    s = 0
                                    for segment in transcription_item.segments:
                                        seg = TranscriptionSegmentOrm(
                                            pos=s,
                                            seek=segment.seek,
                                            start=segment.start,
                                            end=segment.end,
                                            text=segment.text,
                                            temperature=segment.temperature,
                                            avg_logprob=segment.avg_logprob,
                                            compression_ratio=segment.compression_ratio,
                                            no_speech_prob=segment.no_speech_prob,
                                            confidence=segment.confidence
                                        )
                                        transcription.segments.append(seg)
                                        if isinstance(segment.words, list):
                                            w = 0
                                            for word in segment.words:
                                                wrd = TranscriptionWordOrm(
                                                    pos=w,
                                                    start=word.start,
                                                    end=word.end,
                                                    text=word.text,
                                                    confidence=word.confidence
                                                )
                                                seg.words.append(wrd)
                                                w += 1
                                        s += 1
                                # Add language probabilities
                                if isinstance(transcription_item.language_probs, list):
                                    for prob in transcription_item.language_probs:
                                        prb = TranscriptionLanguageProbOrm(
                                            lang=prob.lang,
                                            prob=prob.probability
                                        )
                                        transcription.language_probs.append(prb)
                                # Add speech activities
                                if isinstance(transcription_item.speech_activities, list):
                                    sa = 0
                                    for activity in transcription_item.speech_activities:
                                        act = TranscriptionSpeechActivityOrm(
                                            pos=sa,
                                            start=activity.start,
                                            end=activity.end
                                        )
                                        transcription.speech_activities.append(act)
                                        sa += 1
                    
                    db_session.add(record)
                    db_session.commit()
                except Exception as e:
                    return {
                        'success': False,
                        'message': str(e)
                    }
            
                # Return success
                return {
                    'success': True,
                    'message': 'Document created',
                    'payload': {
                        'id': record.id,
                        'uuid': record.uuid
                    }
                }
        
        self.create_record = types.MethodType(create_record, self)



        # Update record
        async def update_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            id: TrpPrimaryKey_ID_UUID,
            data: UpdateDocumentStructureModel
        ) -> ApiResponseModel:
            if is_empty(id):
                return {
                    'success': False,
                    'message': 'ID missing'
                }
            id_type, id, msg = self._get_id_attr(id, {
                'int': 'id',
                'str': [
                    ('id', RE_INT_VALUE, lambda x: int(x)),
                    ('uuid', RE_UUID4, None)
                ]
            }, 'Invalid ID')
            if id_type is None:
                return msg

            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(DocumentOrm)
                stmt = self._add_id_where_stmt(stmt, DocumentOrm, id_type, id)

                # Get record
                record, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'DocumentOrm',
                    'Document not found',
                    'Multiple Documents found'
                )
                if record is None:
                    return msg
                
                # Get data to update
                d = {}
                for f in ['type', 'status', 'reason']:
                    if getattr(data, f) is not None:
                        d[f] = getattr(data, f)
                if len(d) == 0:
                    return {
                        'success': False,
                        'message': 'Nothing to update'
                    }

                # Update record
                try:
                    db_session.query(DocumentOrm).\
                        filter(DocumentOrm.id == record.id).\
                        update(d)
                    db_session.commit()
                except Exception as e:
                    return {
                        'success': False,
                        'message': str(e)
                    }
            
                # Return success
                return {
                    'success': True,
                    'message': 'Document updated',
                    'payload': {
                        'id': record.id
                    }
                }
        
        self.update_record = types.MethodType(update_record, self)



        # Delete record
        async def delete_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            id: TrpPrimaryKey_ID_UUID
        ) -> ApiResponseModel:
            if is_empty(id):
                return {
                    'success': False,
                    'message': 'ID missing'
                }
            id_type, id, msg = self._get_id_attr(id, {
                'int': 'id',
                'str': [
                    ('id', RE_INT_VALUE, lambda x: int(x)),
                    ('uuid', RE_UUID4, None)
                ]
            }, 'Invalid ID')
            if id_type is None:
                return msg
            
            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(DocumentOrm)
                stmt = self._add_id_where_stmt(stmt, DocumentOrm, id_type, id)

                # Get record
                record, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'DocumentOrm',
                    'Document not found',
                    'Multiple Documents found'
                )
                if record is None:
                    return msg
                
                # Delete record if found
                try:
                    db_session.execute(
                        sqlalchemy.delete(DocumentOrm).\
                            where(DocumentOrm.id == record.id)
                    )
                    db_session.commit()
                except Exception as e:
                    return {
                        'success': False,
                        'message': str(e)
                    }
            
                # Return success
                return {
                    'success': True,
                    'message': 'Document deleted',
                    'payload': {
                        'id': record.id
                    }
                }
        
        self.delete_record = types.MethodType(delete_record, self)



        ### Add routes ###
        router.add_api_route('/documents',          self.get_records,      methods=['GET'])
        router.add_api_route('/document/{id}',      self.get_record,       methods=['GET'])
        router.add_api_route('/document',           self.create_record,    methods=['POST'])
        router.add_api_route('/document/{id}',      self.update_record,    methods=['PUT'])
        router.add_api_route('/document/{id}',      self.delete_record,    methods=['DELETE'])





### Testing ###

if __name__ == '__main__':
    router = DocumentRouter()
    print('router:', router)


