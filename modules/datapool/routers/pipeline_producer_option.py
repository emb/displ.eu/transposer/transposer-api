# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import types
import typing
import re
import uuid

from fastapi import (
    Depends
)

import sqlalchemy
from sqlalchemy.orm import Session

from regex_patterns import (
    RE_INT_VALUE,
    RE_UUID4,
    RE_HASH_XX,
    RE_XXHASH_CID_HEX
)
from data_utils import (
    is_empty,
    validate_uri as validate_uri_func
)

import trp_config

from ..filter import (
    split_order_fields
)

from ..models.base import (
    ApiResponseModel,
    ApiResponseWithCountModel
)
from ..models.pipeline_producer_option import (
    PipelineProducerOptionOrm,
    
    PipelineProducerOptionModel,
    NewPipelineProducerOptionModel,
    UpdatePipelineProducerOptionModel,

    PipelineProducerOptionQueryParams,
    PipelineProducerOptionQueryParamsPrimary,
    PipelineProducerOptionQueryParamsGeneral
)
from ..models.pipeline_producer import (
    PipelineProducerOrm,
    PipelineProducerQueryParamsGeneral
)

from ..conditions import (
    add_pipeline_producer_option_conditions,
    add_pipeline_producer_conditions
)

from .base import AuthBaseRouter





class PipelineProducerOptionRouter(AuthBaseRouter):
    def __init__(self,
        db_engine: typing.Any = None,
        app: typing.Any = None,
        router: typing.Any = None,
        config: trp_config.TrpConfig | None = None,
        auth_scheme: typing.Any = None
    ):
        super().__init__(db_engine, app, router, config, auth_scheme)

        print('PipelineProducerOptionRouter ... db_engine:', db_engine)
        print('PipelineProducerOptionRouter ... app:', app)
        print('PipelineProducerOptionRouter ... router:', router)
        print('PipelineProducerOptionRouter ... config:', config)
        print('PipelineProducerOptionRouter ... auth_scheme:', auth_scheme)

        # Get records
        async def get_records(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],

            pipeline_producer_option_primary: typing.Annotated[PipelineProducerOptionQueryParamsPrimary, Depends(PipelineProducerOptionQueryParamsPrimary)],
            pipeline_producer_option: typing.Annotated[PipelineProducerOptionQueryParamsGeneral, Depends(PipelineProducerOptionQueryParamsGeneral)],

            pipeline_producer: typing.Annotated[PipelineProducerQueryParamsGeneral, Depends(PipelineProducerQueryParamsGeneral)],
            
            limit: int = 0,
            offset: int = 0,
            order: str | None = None,

            use_and: bool = False,

            dataview: str | None = None
        ) -> ApiResponseWithCountModel:
            pipeline_producer_option = PipelineProducerOptionQueryParams(
                pipeline_producer_option_primary.id if pipeline_producer_option.id is None else pipeline_producer_option.id,
                pipeline_producer_option_primary.parent_id if pipeline_producer_option.parent_id is None else pipeline_producer_option.parent_id,
                pipeline_producer_option_primary.key if pipeline_producer_option.key is None else pipeline_producer_option.key,
                pipeline_producer_option_primary.value if pipeline_producer_option.value is None else pipeline_producer_option.value,
                pipeline_producer_option_primary.type if pipeline_producer_option.type is None else pipeline_producer_option.type
            )

            result = None
            with Session(self._db_engine) as db_session:
                # Create count statement
                count_stmt = db_session.query(sqlalchemy.func.count(PipelineProducerOptionOrm.id))

                # Create query statement
                stmt = sqlalchemy.select(PipelineProducerOptionOrm)

                ## Add conditions
                where = []

                # Add pipeline_producer_option conditions
                add_pipeline_producer_option_conditions(
                    where,
                    pipeline_producer_option,
                    use_and
                )

                # Add pipeline_producer conditions
                if add_pipeline_producer_conditions(
                    where,
                    pipeline_producer,
                    use_and
                ):
                    stmt = stmt.join(PipelineProducerOptionOrm.parent, full=True)
                

                # Add conditions to statements
                if len(where):
                    cond = sqlalchemy.and_(*where)
                    count_stmt = count_stmt.where(cond)
                    stmt = stmt.where(cond)
                
                # Get total number of records
                num_records_total = count_stmt.scalar()
                if num_records_total == 0:
                    return {
                        'success': False,
                        'message': 'No PipelineProducerOptions found'
                    }
                
                # Add limit and offset
                if offset > 0:
                    stmt = stmt.offset(offset)
                if limit > 0:
                    stmt = stmt.limit(limit)

                # Add order
                order = split_order_fields(order, 'pipeline_producer_option')
                if order is not None:
                    stmt = stmt.order_by(*order)
                
                # Parse dataview
                dataview_struct = self._parse_dataview(dataview)
                
                # Execute statement
                result = []
                for row in db_session.execute(stmt):
                    if row.PipelineProducerOptionOrm is None:
                        continue
                    result.append(
                        row.PipelineProducerOptionOrm.to_dict(dataview_struct)
                    )
            
            # If we didn't find any records
            # return an error message
            if result is None:
                return {
                    'success': False,
                    'message': 'No PipelineProducerOptions found'
                }
            num_records = len(result)
            if num_records == 0:
                return {
                    'success': False,
                    'message': 'No PipelineProducerOptions found'
                }
            
            # Return the result
            return {
                'success': True,
                'message': 'PipelineProducerOptions found',
                'total': num_records_total,
                'count': num_records,
                'payload': result
            }

        self.get_records = types.MethodType(get_records, self)



        # Get record
        async def get_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            id: int,
            dataview: str | None = None
        ) -> ApiResponseModel:
            # Get key and extract value
            if is_empty(id):
                return {
                    'success': False,
                    'message': 'ID missing'
                }
            
            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(PipelineProducerOptionOrm).\
                    where(PipelineProducerOptionOrm.id == id)

                # Get record
                record, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'PipelineProducerOptionOrm',
                    'PipelineProducerOption not found',
                    'Multiple PipelineProducerOptions found'
                )
                if record is None:
                    return msg
                
                # Parse and filter
                dataview_struct = self._parse_dataview(dataview)
                record_dict = record.to_dict(dataview_struct)
                
                # Return the result
                return {
                    'success': True,
                    'message': 'PipelineProducerOption found',
                    'payload': record_dict
                }
        
        self.get_record = types.MethodType(get_record, self)



        # Create record
        async def create_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            data: NewPipelineProducerOptionModel
        ) -> ApiResponseModel:
            # Get parent key and value
            parent_id = data.parent
            if parent_id is None:
                return {
                    'success': False,
                    'message': 'PipelineProducer ID missing'
                }
            parent_id_type, parent_id, msg = self._get_id_attr(parent_id, {
                'int': 'id',
                'str': [
                    ('id', RE_INT_VALUE, lambda x: int(x)),
                    ('uuid', RE_UUID4, None)
                ]
            }, 'Invalid PipelineProducer ID')
            if parent_id_type is None:
                return msg

            parent = None
            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(PipelineProducerOrm)
                stmt = self._add_id_where_stmt(stmt, PipelineProducerOrm, parent_id_type, parent_id)
                
                # Get parent record
                parent, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'PipelineProducerOrm',
                    'PipelineProducer not found',
                    'Multiple PipelineProducers found'
                )
                if parent is None:
                    return msg

                # Create record
                try:
                    record = PipelineProducerOptionOrm(
                        parent_id=parent.id,
                        key=data.key,
                        value=data.value,
                        type=(data.type if data.type else 0)
                    )
                    db_session.add(record)
                    db_session.commit()
                except Exception as e:
                    return {
                        'success': False,
                        'message': str(e)
                    }
            
                # Return success
                return {
                    'success': True,
                    'message': 'PipelineProducerOption created',
                    'payload': {
                        'id': record.id
                    }
                }
        
        self.create_record = types.MethodType(create_record, self)



        # Update record
        async def update_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            id: int,
            data: UpdatePipelineProducerOptionModel
        ) -> ApiResponseModel:
            if is_empty(id):
                return {
                    'success': False,
                    'message': 'ID missing'
                }
            
            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(PipelineProducerOptionOrm).\
                    where(PipelineProducerOptionOrm.id == id)

                # Get record
                record, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'PipelineProducerOptionOrm',
                    'PipelineProducerOption not found',
                    'Multiple PipelineProducerOptions found'
                )
                if record is None:
                    return msg
                
                # Get data to update
                d = {}
                for f in ['value', 'type']:
                    if getattr(data, f) is not None:
                        d[f] = getattr(data, f)
                if len(d) == 0:
                    return {
                        'success': False,
                        'message': 'Nothing to update'
                    }

                # Update record
                try:
                    db_session.query(PipelineProducerOptionOrm).\
                        filter(PipelineProducerOptionOrm.id == record.id).\
                        update(d)
                    db_session.commit()
                except Exception as e:
                    return {
                        'success': False,
                        'message': str(e)
                    }
            
                # Return success
                return {
                    'success': True,
                    'message': 'PipelineProducerOption updated',
                    'payload': {
                        'id': record.id
                    }
                }
        
        self.update_record = types.MethodType(update_record, self)



        # Delete record
        async def delete_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            id: int
        ) -> ApiResponseModel:
            if is_empty(id):
                return {
                    'success': False,
                    'message': 'ID missing'
                }

            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(PipelineProducerOptionOrm).\
                    where(PipelineProducerOptionOrm.id == id)

                # Get record
                record, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'PipelineProducerOptionOrm',
                    'PipelineProducerOption not found',
                    'Multiple PipelineProducerOptions found'
                )
                if record is None:
                    return msg
                
                # Delete record if found
                try:
                    db_session.execute(
                        sqlalchemy.delete(PipelineProducerOptionOrm).\
                            where(PipelineProducerOptionOrm.id == record.id)
                    )
                    db_session.commit()
                except Exception as e:
                    return {
                        'success': False,
                        'message': str(e)
                    }
            
                # Return success
                return {
                    'success': True,
                    'message': 'PipelineProducerOption deleted',
                    'payload': {
                        'id': record.id
                    }
                }
        
        self.delete_record = types.MethodType(delete_record, self)



        ### Add routes ###
        router.add_api_route('/pipeline/producer/options',          self.get_records,      methods=['GET'])
        router.add_api_route('/pipeline/producer/option/{id}',      self.get_record,       methods=['GET'])
        router.add_api_route('/pipeline/producer/option',           self.create_record,    methods=['POST'])
        router.add_api_route('/pipeline/producer/option/{id}',      self.update_record,    methods=['PUT'])
        router.add_api_route('/pipeline/producer/option/{id}',      self.delete_record,    methods=['DELETE'])





### Testing ###

if __name__ == '__main__':
    router = PipelineProducerOptionRouter()
    print('router:', router)


