# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import types
import typing

from fastapi import (
    Depends
)

import sqlalchemy
from sqlalchemy.orm import Session

from regex_patterns import (
    RE_INT_VALUE,
    RE_UUID4,
    RE_HASH_XX,
    RE_SHA256_HEX,
    RE_SHAKE256_KEY_HEX,
    RE_EMAIL,
    RE_USERNAME
)
from data_utils import (
    is_empty
)
import trp_config

from ..filter import (
    split_order_fields
)

from ..models.base import (
    ApiResponseModel,
    ApiResponseWithCountModel
)
from ..models.content_option import (
    ContentOptionOrm,

    ContentOptionModel,
    NewContentOptionModel,
    UpdateContentOptionModel,

    ContentOptionQueryParams,
    ContentOptionQueryParamsPrimary,
    ContentOptionQueryParamsGeneral
)
from ..models.content import (
    ContentOrm
)

from ..conditions import (
    add_content_option_conditions
)

from .base import AuthBaseRouter





class ContentOptionRouter(AuthBaseRouter):
    def __init__(self,
        db_engine: typing.Any = None,
        app: typing.Any = None,
        router: typing.Any = None,
        config: trp_config.TrpConfig | None = None,
        auth_scheme: typing.Any = None
    ):
        super().__init__(db_engine, app, router, config, auth_scheme)

        print('ContentOptionRouter ... db_engine:', db_engine)
        print('ContentOptionRouter ... app:', app)
        print('ContentOptionRouter ... router:', router)
        print('ContentOptionRouter ... config:', config)
        print('ContentOptionRouter ... auth_scheme:', auth_scheme)

        # Get records
        async def get_records(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],

            content_option_primary: typing.Annotated[ContentOptionQueryParamsPrimary, Depends(ContentOptionQueryParamsPrimary)],
            content_option: typing.Annotated[ContentOptionQueryParamsGeneral, Depends(ContentOptionQueryParamsGeneral)],

            limit: int = 0,
            offset: int = 0,
            order: str | None = None,

            use_and: bool = False,

            dataview: str | None = None
        ) -> ApiResponseWithCountModel:
            content_option = ContentOptionQueryParams(
                content_option_primary.id if content_option.id is None else content_option.id,
                content_option_primary.content_id if content_option.content_id is None else content_option.content_id,
                content_option_primary.start if content_option.start is None else content_option.start,
                content_option_primary.end if content_option.end is None else content_option.end,
                content_option_primary.identifier if content_option.identifier is None else content_option.identifier,
                content_option_primary.properties if content_option.properties is None else content_option.properties
            )

            result = None
            with Session(self._db_engine) as db_session:
                # Create count statement
                count_stmt = db_session.query(sqlalchemy.func.count(ContentOptionOrm.id))

                # Create query statement
                stmt = sqlalchemy.select(ContentOptionOrm)

                ## Add conditions
                where = []

                # Add content_option conditions
                add_content_option_conditions(
                    where,
                    content_option,
                    use_and
                )

                # Add conditions to statements
                if len(where):
                    cond = sqlalchemy.and_(*where)
                    count_stmt = count_stmt.where(cond)
                    stmt = stmt.where(cond)
                
                # Get total number of records
                num_records_total = count_stmt.scalar()
                if num_records_total == 0:
                    return {
                        'success': False,
                        'message': 'No ContentOptions found'
                    }

                # Add limit and offset
                if offset > 0:
                    stmt = stmt.offset(offset)
                if limit > 0:
                    stmt = stmt.limit(limit)

                # Add order
                order = split_order_fields(order, 'content_option')
                if order is not None:
                    stmt = stmt.order_by(*order)
                
                # Parse dataview
                dataview_struct = self._parse_dataview(dataview)
            
                # Execute statement
                result = []
                for row in db_session.execute(stmt):
                    if row.ContentOptionOrm is None:
                        continue
                    result.append(
                        row.ContentOptionOrm.to_dict(dataview_struct)
                    )
            
            # If we didn't find any records
            # return an error message
            if result is None:
                return {
                    'success': False,
                    'message': 'No ContentOptions found'
                }
            num_records = len(result)
            if num_records == 0:
                return {
                    'success': False,
                    'message': 'No ContentOptions found'
                }
            
            # Return the result
            return {
                'success': True,
                'message': 'ContentOptions found',
                'total': num_records_total,
                'count': num_records,
                'payload': result
            }

        self.get_records = types.MethodType(get_records, self)



        # Get record
        async def get_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            id: int,
            dataview: str | None = None
        ) -> ApiResponseModel:
            # Get key and extract value
            if is_empty(id):
                return {
                    'success': False,
                    'message': 'ID missing'
                }
            
            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(ContentOptionOrm).\
                    where(ContentOptionOrm.id == id)

                # Get record
                record, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'ContentOptionOrm',
                    'ContentOption not found',
                    'Multiple ContentOptions found'
                )
                if record is None:
                    return msg
                
                # Parse and filter
                dataview_struct = self._parse_dataview(dataview)
                record_dict = record.to_dict(dataview_struct)
                
                # Return the result
                return {
                    'success': True,
                    'message': 'ContentOption found',
                    'payload': record_dict
                }
        
        self.get_record = types.MethodType(get_record, self)



        # Create record
        async def create_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            data: NewContentOptionModel
        ) -> ApiResponseModel:
            # Get parent key and value
            parent_id = data.content
            if parent_id is None:
                return {
                    'success': False,
                    'message': 'Content ID missing'
                }

            parent = None
            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(ContentOrm).\
                    where(ContentOrm.id == parent_id)
                
                # Get parent record
                parent, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'ContentOrm',
                    'Content not found',
                    'Multiple Contents found'
                )
                if parent is None:
                    return msg
                
                # Create record
                try:
                    record = ContentOptionOrm(
                        parent_id=parent.id,
                        key=data.key,
                        value=data.value,
                        datatype=data.datatype
                    )
                    db_session.add(record)
                    db_session.commit()
                except Exception as e:
                    return {
                        'success': False,
                        'message': str(e)
                    }
            
                # Return success
                return {
                    'success': True,
                    'message': 'ContentOption created',
                    'payload': {
                        'id': record.id
                    }
                }
        
        self.create_record = types.MethodType(create_record, self)



        # Update record
        async def update_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            id: int,
            data: UpdateContentOptionModel
        ) -> ApiResponseModel:
            if is_empty(id):
                return {
                    'success': False,
                    'message': 'ID missing'
                }
            
            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(ContentOptionOrm).\
                    where(ContentOptionOrm.id == id)

                # Get record
                record, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'ContentOptionOrm',
                    'ContentOption not found',
                    'Multiple ContentOptions found'
                )
                if record is None:
                    return msg
                
                # Get data to update
                d = {}
                for f in ['value', 'datatype']:
                    if getattr(data, f) is not None:
                        d[f] = getattr(data, f)
                if len(d) == 0:
                    return {
                        'success': False,
                        'message': 'Nothing to update'
                    }

                # Update record
                try:
                    db_session.query(ContentOptionOrm).\
                        filter(ContentOptionOrm.id == record.id).\
                        update(d)
                    db_session.commit()
                except Exception as e:
                    return {
                        'success': False,
                        'message': str(e)
                    }
            
                # Return success
                return {
                    'success': True,
                    'message': 'ContentOption updated',
                    'payload': {
                        'id': record.id,
                    }
                }
        
        self.update_record = types.MethodType(update_record, self)



        # Delete record
        async def delete_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            id: int
        ) -> ApiResponseModel:
            if is_empty(id):
                return {
                    'success': False,
                    'message': 'ID missing'
                }

            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(ContentOptionOrm).\
                    where(ContentOptionOrm.id == id)

                # Get record
                record, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'ContentOptionOrm',
                    'ContentOption not found',
                    'Multiple ContentOptions found'
                )
                if record is None:
                    return msg

                # Delete record if found
                try:
                    db_session.execute(
                        sqlalchemy.delete(ContentOptionOrm).\
                            where(ContentOptionOrm.id == record.id)
                    )
                    db_session.commit()
                except Exception as e:
                    return {
                        'success': False,
                        'message': str(e)
                    }
            
                # Return success
                return {
                    'success': True,
                    'message': 'ContentOption deleted',
                    'payload': {
                        'id': record.id
                    }
                }
        
        self.delete_record = types.MethodType(delete_record, self)



        ### Add routes ###
        router.add_api_route('/document/content/options',          self.get_records,      methods=['GET'])
        router.add_api_route('/document/content/option/{id}',      self.get_record,       methods=['GET'])
        router.add_api_route('/document/content/option',           self.create_record,    methods=['POST'])
        router.add_api_route('/document/content/option/{id}',      self.update_record,    methods=['PUT'])
        router.add_api_route('/document/content/option/{id}',      self.delete_record,    methods=['DELETE'])





### Testing ###

if __name__ == '__main__':
    router = ContentOptionRouter()
    print('router:', router)


