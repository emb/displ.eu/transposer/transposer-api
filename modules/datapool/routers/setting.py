# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import types
import typing
import re
import uuid

from fastapi import (
    Depends
)

import sqlalchemy
from sqlalchemy.orm import Session

from regex_patterns import (
    RE_INT_VALUE,
    RE_UUID4,
    RE_SLUG,
    RE_HASH_XX,
    RE_XXHASH_CID_HEX
)
from data_utils import (
    is_empty,
    validate_uri as validate_uri_func
)

import trp_config

from ..filter import (
    split_order_fields
)

from ..models.base import (
    ApiResponseModel,
    ApiResponseWithCountModel
)
from ..models.setting import (
    SettingOrm,
    
    SettingModel,
    NewSettingModel,
    UpdateSettingModel,

    SettingQueryParams,
    SettingQueryParamsPrimary,
    SettingQueryParamsGeneral
)

from ..conditions import (
    add_setting_conditions
)

from .base import AuthBaseRouter





class SettingRouter(AuthBaseRouter):
    def __init__(self,
        db_engine: typing.Any = None,
        app: typing.Any = None,
        router: typing.Any = None,
        config: trp_config.TrpConfig | None = None,
        auth_scheme: typing.Any = None
    ):
        super().__init__(db_engine, app, router, config, auth_scheme)

        print('SettingRouter ... db_engine:', db_engine)
        print('SettingRouter ... app:', app)
        print('SettingRouter ... router:', router)
        print('SettingRouter ... config:', config)
        print('SettingRouter ... auth_scheme:', auth_scheme)

        # Get records
        async def get_records(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],

            setting_primary: typing.Annotated[SettingQueryParamsPrimary, Depends(SettingQueryParamsPrimary)],
            setting: typing.Annotated[SettingQueryParamsGeneral, Depends(SettingQueryParamsGeneral)],
            
            limit: int = 0,
            offset: int = 0,
            order: str | None = None,

            use_and: bool = False,

            dataview: str | None = None
        ) -> ApiResponseWithCountModel:
            setting = SettingQueryParams(
                setting_primary.id if setting.id is None else setting.id,
                setting_primary.modified_at if setting.modified_at is None else setting.modified_at,
                setting_primary.created_at if setting.created_at is None else setting.created_at,
                setting_primary.name if setting.name is None else setting.name,
                setting_primary.default_lang if setting.default_lang is None else setting.default_lang,
                setting_primary.default_ttl if setting.default_ttl is None else setting.default_ttl
            )

            result = None
            with Session(self._db_engine) as db_session:
                # Create count statement
                count_stmt = db_session.query(sqlalchemy.func.count(SettingOrm.id))

                # Create query statement
                stmt = sqlalchemy.select(SettingOrm)

                ## Add conditions
                where = []

                # Add setting conditions
                add_setting_conditions(
                    where,
                    setting,
                    use_and
                )
                

                # Add conditions to statements
                if len(where):
                    cond = sqlalchemy.and_(*where)
                    count_stmt = count_stmt.where(cond)
                    stmt = stmt.where(cond)
                
                # Get total number of records
                num_records_total = count_stmt.scalar()
                if num_records_total == 0:
                    return {
                        'success': False,
                        'message': 'No Settings found'
                    }
                
                # Add limit and offset
                if offset > 0:
                    stmt = stmt.offset(offset)
                if limit > 0:
                    stmt = stmt.limit(limit)

                # Add order
                order = split_order_fields(order, 'setting')
                if order is not None:
                    stmt = stmt.order_by(*order)
                
                # Parse dataview
                dataview_struct = self._parse_dataview(dataview)
                
                # Execute statement
                result = []
                for row in db_session.execute(stmt):
                    if row.SettingOrm is None:
                        continue
                    result.append(
                        row.SettingOrm.to_dict(dataview_struct)
                    )
            
            # If we didn't find any records
            # return an error message
            if result is None:
                return {
                    'success': False,
                    'message': 'No Setting found'
                }
            num_records = len(result)
            if num_records == 0:
                return {
                    'success': False,
                    'message': 'No Setting found'
                }
            
            # Return the result
            return {
                'success': True,
                'message': 'Setting found',
                'total': num_records_total,
                'count': num_records,
                'payload': result
            }

        self.get_records = types.MethodType(get_records, self)



        # Get record
        async def get_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            id: str,
            dataview: str | None = None
        ) -> ApiResponseModel:
            # Get key and extract value
            if is_empty(id):
                return {
                    'success': False,
                    'message': 'ID missing'
                }
            result = RE_SLUG.match(id)
            if result is None:
                return {
                    'success': False,
                    'message': 'Invalid ID'
                }
            id = result.group(1)
            
            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(SettingOrm).\
                    where(SettingOrm.id == id)

                # Get record
                record, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'SettingOrm',
                    'Setting not found',
                    'Multiple Settings found'
                )
                if record is None:
                    return msg
                
                # Parse and filter
                dataview_struct = self._parse_dataview(dataview)
                record_dict = record.to_dict(dataview_struct)
                
                # Return the result
                return {
                    'success': True,
                    'message': 'Setting found',
                    'payload': record_dict
                }
        
        self.get_record = types.MethodType(get_record, self)



        # Create record
        async def create_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            data: NewSettingModel
        ) -> ApiResponseModel:
            record = None
            with Session(self._db_engine) as db_session:
                # Create record
                try:
                    record = SettingOrm(
                        id=data.id,
                        name=data.name,
                        default_lang=data.default_lang,
                        default_ttl=data.default_ttl
                    )
                    db_session.add(record)
                    db_session.commit()
                except Exception as e:
                    return {
                        'success': False,
                        'message': str(e)
                    }
            
                # Return success
                return {
                    'success': True,
                    'message': 'Setting created',
                    'payload': {
                        'id': record.id
                    }
                }
        
        self.create_record = types.MethodType(create_record, self)



        # Update record
        async def update_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            id: str,
            data: UpdateSettingModel
        ) -> ApiResponseModel:
            if is_empty(id):
                return {
                    'success': False,
                    'message': 'ID missing'
                }
            result = RE_SLUG.match(id)
            if result:
                return {
                    'success': False,
                    'message': 'Invalid ID'
                }
            id = result.group(1)
            
            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(SettingOrm).\
                    where(SettingOrm.id == id)

                # Get record
                record, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'SettingOrm',
                    'Setting not found',
                    'Multiple Settings found'
                )
                if record is None:
                    return msg
                
                # Get data to update
                d = {}
                for f in ['name', 'default_lang', 'default_ttl']:
                    if getattr(data, f) is not None:
                        d[f] = getattr(data, f)
                if len(d) == 0:
                    return {
                        'success': False,
                        'message': 'Nothing to update'
                    }

                # Update record
                try:
                    db_session.query(SettingOrm).\
                        filter(SettingOrm.id == record.id).\
                        update(d)
                    db_session.commit()
                except Exception as e:
                    return {
                        'success': False,
                        'message': str(e)
                    }
            
                # Return success
                return {
                    'success': True,
                    'message': 'Setting updated',
                    'payload': {
                        'id': record.id
                    }
                }
        
        self.update_record = types.MethodType(update_record, self)



        # Delete record
        async def delete_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            id: str
        ) -> ApiResponseModel:
            if is_empty(id):
                return {
                    'success': False,
                    'message': 'ID missing'
                }
            result = RE_SLUG.match(id)
            if result:
                return {
                    'success': False,
                    'message': 'Invalid ID'
                }
            id = result.group(1)

            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(SettingOrm).\
                    where(SettingOrm.id == id)

                # Get record
                record, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'SettingOrm',
                    'Setting not found',
                    'Multiple Settings found'
                )
                if record is None:
                    return msg
                
                # Delete record if found
                try:
                    db_session.execute(
                        sqlalchemy.delete(SettingOrm).\
                            where(SettingOrm.id == record.id)
                    )
                    db_session.commit()
                except Exception as e:
                    return {
                        'success': False,
                        'message': str(e)
                    }
            
                # Return success
                return {
                    'success': True,
                    'message': 'Setting deleted',
                    'payload': {
                        'id': record.id
                    }
                }
        
        self.delete_record = types.MethodType(delete_record, self)



        ### Add routes ###
        router.add_api_route('/settings',           self.get_records,      methods=['GET'])
        router.add_api_route('/setting/{id}',      self.get_record,       methods=['GET'])
        router.add_api_route('/setting',           self.create_record,    methods=['POST'])
        router.add_api_route('/setting/{id}',      self.update_record,    methods=['PUT'])
        router.add_api_route('/setting/{id}',      self.delete_record,    methods=['DELETE'])





### Testing ###

if __name__ == '__main__':
    router = SettingRouter()
    print('router:', router)


