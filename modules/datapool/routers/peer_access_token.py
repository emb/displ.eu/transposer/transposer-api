# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import types
import typing

from fastapi import (
    Depends
)

import sqlalchemy
from sqlalchemy.orm import Session

from regex_patterns import (
    RE_INT_VALUE,
    RE_UUID4,
    RE_HASH_XX,
    RE_SHA256_HEX,
    RE_SHAKE256_KEY_HEX,
    RE_EMAIL,
    RE_USERNAME,
    RE_HOST
)
from data_utils import (
    is_empty
)
import trp_config

from ..filter import (
    split_order_fields
)

from ..models.base import (
    ApiResponseModel,
    ApiResponseWithCountModel,

    TrpPrimaryKey_ID_HASH
)
from ..models.peer_access_token import (
    PeerAccessTokenOrm,

    PeerAccessTokenModel,
    NewPeerAccessTokenModel,
    UpdatePeerAccessTokenModel,

    PeerAccessTokenQueryParams,
    PeerAccessTokenQueryParamsPrimary,
    PeerAccessTokenQueryParamsGeneral
)
from ..models.peer import (
    PeerOrm
)

from ..conditions import (
    add_peer_access_token_conditions
)

from .base import AuthBaseRouter





class PeerAccessTokenRouter(AuthBaseRouter):
    def __init__(self,
        db_engine: typing.Any = None,
        app: typing.Any = None,
        router: typing.Any = None,
        config: trp_config.TrpConfig | None = None,
        auth_scheme: typing.Any = None
    ):
        super().__init__(db_engine, app, router, config, auth_scheme)

        print('PeerAccessTokenRouter ... db_engine:', db_engine)
        print('PeerAccessTokenRouter ... app:', app)
        print('PeerAccessTokenRouter ... router:', router)
        print('PeerAccessTokenRouter ... config:', config)
        print('PeerAccessTokenRouter ... auth_scheme:', auth_scheme)

        # Get records
        async def get_records(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],

            peer_access_token_primary: typing.Annotated[PeerAccessTokenQueryParamsPrimary, Depends(PeerAccessTokenQueryParamsPrimary)],
            peer_access_token: typing.Annotated[PeerAccessTokenQueryParamsGeneral, Depends(PeerAccessTokenQueryParamsGeneral)],

            limit: int = 0,
            offset: int = 0,
            order: str | None = None,

            use_and: bool = False,

            dataview: str | None = None
        ) -> ApiResponseWithCountModel:
            peer_access_token = PeerAccessTokenQueryParams(
                peer_access_token_primary.id if peer_access_token.id is None else peer_access_token.id,
                peer_access_token_primary.created_at if peer_access_token.created_at is None else peer_access_token.created_at,
                peer_access_token_primary.peer_id if peer_access_token.peer_id is None else peer_access_token.peer_id,
                peer_access_token_primary.name if peer_access_token.name is None else peer_access_token.name,
                peer_access_token_primary.key if peer_access_token.key is None else peer_access_token.key,
                peer_access_token_primary.token if peer_access_token.token is None else peer_access_token.token,
                peer_access_token_primary.is_active if peer_access_token.is_active is None else peer_access_token.is_active,
                peer_access_token_primary.valid_until if peer_access_token.valid_until is None else peer_access_token.valid_until
            )

            result = None
            with Session(self._db_engine) as db_session:
                # Create count statement
                count_stmt = db_session.query(sqlalchemy.func.count(PeerAccessTokenOrm.id))

                # Create query statement
                stmt = sqlalchemy.select(PeerAccessTokenOrm)

                ## Add conditions
                where = []

                # Add peer_access_token conditions
                add_peer_access_token_conditions(
                    where,
                    peer_access_token,
                    use_and
                )

                # Add conditions to statements
                if len(where):
                    cond = sqlalchemy.and_(*where)
                    count_stmt = count_stmt.where(cond)
                    stmt = stmt.where(cond)
                
                # Get total number of records
                num_records_total = count_stmt.scalar()
                if num_records_total == 0:
                    return {
                        'success': False,
                        'message': 'No PeerAccessTokens found'
                    }
                
                # Add limit and offset
                if offset > 0:
                    stmt = stmt.offset(offset)
                if limit > 0:
                    stmt = stmt.limit(limit)

                # Add order
                order = split_order_fields(order, 'peer_access_token')
                if order is not None:
                    stmt = stmt.order_by(*order)
                
                # Parse dataview
                dataview_struct = self._parse_dataview(dataview)
                
                # Execute statement
                result = []
                for row in db_session.execute(stmt):
                    if row.PeerAccessTokenOrm is None:
                        continue
                    result.append(
                        row.PeerAccessTokenOrm.to_dict(dataview_struct)
                    )
            
            # If we didn't find any records
            # return an error message
            if result is None:
                return {
                    'success': False,
                    'message': 'No PeerAccessTokens found'
                }
            num_records = len(result)
            if num_records == 0:
                return {
                    'success': False,
                    'message': 'No PeerAccessTokens found'
                }
            
            # Return the result
            return {
                'success': True,
                'message': 'PeerAccessTokens found',
                'total': num_records_total,
                'count': num_records,
                'payload': result
            }

        self.get_records = types.MethodType(get_records, self)



        # Get record
        async def get_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            id: TrpPrimaryKey_ID_HASH,
            dataview: str | None = None
        ) -> ApiResponseModel:
            # Get key and extract value
            if is_empty(id):
                return {
                    'success': False,
                    'message': 'ID missing'
                }
            id_type, id, msg = self._get_id_attr(id, {
                'int': 'id',
                'str': [
                    ('id', RE_INT_VALUE, lambda x: int(x)),
                    ('key', RE_HASH_XX, None)
                ]
            }, 'Invalid ID')
            if id_type is None:
                return msg
            
            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(PeerAccessTokenOrm)
                stmt = self._add_id_where_stmt(stmt, PeerAccessTokenOrm, id_type, id)

                # Get record
                record, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'PeerAccessTokenOrm',
                    'PeerAccessToken not found',
                    'Multiple PeerAccessTokens found'
                )
                if record is None:
                    return msg
                
                # Parse and filter
                dataview_struct = self._parse_dataview(dataview)
                record_dict = record.to_dict(dataview_struct)
                
                # Return the result
                return {
                    'success': True,
                    'message': 'AccessToken found',
                    'payload': record_dict
                }
        
        self.get_record = types.MethodType(get_record, self)



        # Create record
        async def create_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            data: NewPeerAccessTokenModel
        ) -> ApiResponseModel:
            # Get parent key and value
            parent_id = data.peer
            if parent_id is None:
                return {
                    'success': False,
                    'message': 'Peer ID missing'
                }
            parent_id_type, parent_id, msg = self._get_id_attr(parent_id, {
                'int': 'id',
                'str': [
                    ('id', RE_INT_VALUE, lambda x: int(x)),
                    ('host', RE_HOST, None)
                ]
            }, 'Invalid Peer ID')
            if parent_id_type is None:
                return msg

            parent = None
            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(PeerOrm)
                stmt = self._add_id_where_stmt(stmt, PeerOrm, parent_id_type, parent_id)
                
                # Get parent record
                parent, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'PeerOrm',
                    'Peer not found',
                    'Multiple Peers found'
                )
                if parent is None:
                    return msg
                
                # Create record
                try:
                    record = PeerAccessTokenOrm(
                        peer_id=parent.id,
                        name=data.name,
                        key=data.key,
                        token=data.token,
                        is_active=data.is_active,
                        valid_until=data.valid_until
                    )
                    db_session.add(record)
                    db_session.commit()
                except Exception as e:
                    return {
                        'success': False,
                        'message': str(e)
                    }
            
                # Return success
                return {
                    'success': True,
                    'message': 'PeerAccessToken created',
                    'payload': {
                        'id': record.id
                    }
                }
        
        self.create_record = types.MethodType(create_record, self)



        # Update record
        async def update_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            id: TrpPrimaryKey_ID_HASH,
            data: UpdatePeerAccessTokenModel
        ) -> ApiResponseModel:
            if is_empty(id):
                return {
                    'success': False,
                    'message': 'ID missing'
                }
            id_type, id, msg = self._get_id_attr(id, {
                'int': 'id',
                'str': [
                    ('id', RE_INT_VALUE, lambda x: int(x)),
                    ('key', RE_HASH_XX, None)
                ]
            }, 'Invalid ID')
            if id_type is None:
                return msg
            
            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(PeerAccessTokenOrm)
                stmt = self._add_id_where_stmt(stmt, PeerAccessTokenOrm, id_type, id)

                # Get record
                record, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'PeerAccessTokenOrm',
                    'PeerAccessToken not found',
                    'Multiple PeerAccessTokens found'
                )
                if record is None:
                    return msg
                
                # Get data to update
                d = {}
                for f in ['name', 'is_active', 'valid_until']:
                    if getattr(data, f) is not None:
                        d[f] = getattr(data, f)
                if len(d) == 0:
                    return {
                        'success': False,
                        'message': 'Nothing to update'
                    }

                # Update record
                try:
                    db_session.query(PeerAccessTokenOrm).\
                        filter(PeerAccessTokenOrm.id == record.id).\
                        update(d)
                    db_session.commit()
                except Exception as e:
                    return {
                        'success': False,
                        'message': str(e)
                    }
            
                # Return success
                return {
                    'success': True,
                    'message': 'PeerAccessToken updated',
                    'payload': {
                        'id': record.id
                    }
                }
        
        self.update_record = types.MethodType(update_record, self)



        # Delete record
        async def delete_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            id: TrpPrimaryKey_ID_HASH
        ) -> ApiResponseModel:
            if is_empty(id):
                return {
                    'success': False,
                    'message': 'ID missing'
                }
            id_type, id, msg = self._get_id_attr(id, {
                'int': 'id',
                'str': [
                    ('id', RE_INT_VALUE, lambda x: int(x)),
                    ('key', RE_HASH_XX, None)
                ]
            }, 'Invalid ID')
            if id_type is None:
                return msg
            
            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(PeerAccessTokenOrm)
                stmt = self._add_id_where_stmt(stmt, PeerAccessTokenOrm, id_type, id)

                # Get record
                record, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'PeerAccessTokenOrm',
                    'PeerAccessToken not found',
                    'Multiple PeerAccessTokens found'
                )
                if record is None:
                    return msg

                # Delete record if found
                try:
                    db_session.execute(
                        sqlalchemy.delete(PeerAccessTokenOrm).\
                            where(PeerAccessTokenOrm.id == record.id)
                    )
                    db_session.commit()
                except Exception as e:
                    return {
                        'success': False,
                        'message': str(e)
                    }
            
                # Return success
                return {
                    'success': True,
                    'message': 'PeerAccessToken deleted',
                    'payload': {
                        'id': record.id
                    }
                }
        
        self.delete_record = types.MethodType(delete_record, self)



        ### Add routes ###
        router.add_api_route('/peer/access-tokens',          self.get_records,      methods=['GET'])
        router.add_api_route('/peer/access-token/{id}',      self.get_record,       methods=['GET'])
        router.add_api_route('/peer/access-token',           self.create_record,    methods=['POST'])
        router.add_api_route('/peer/access-token/{id}',      self.update_record,    methods=['PUT'])
        router.add_api_route('/peer/access-token/{id}',      self.delete_record,    methods=['DELETE'])





### Testing ###

if __name__ == '__main__':
    router = PeerAccessTokenRouter()
    print('router:', router)


