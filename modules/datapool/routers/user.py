# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import types
import typing
import re
import uuid

from fastapi import (
    Depends
)

import sqlalchemy
from sqlalchemy.orm import Session

from regex_patterns import (
    RE_INT_VALUE,
    RE_UUID4,
    RE_USERNAME,
    RE_EMAIL,
    RE_PASSWORD
)
from data_utils import (
    is_empty
)
import trp_config

from ..filter import (
    split_order_fields
)

from ..models.base import (
    ApiResponseModel,
    ApiResponseWithCountModel,
    SingleStringModel,

    TrpPrimaryKey_UserID
)
from ..models.user import (
    UserOrm,
    
    UserModel,
    CurrentUserModel,
    NewUserModel,
    UpdateUserModel,
    ChangePasswordModel,

    UserQueryParams,
    UserQueryParamsPrimary,
    UserQueryParamsGeneral
)
from ..models.document import (
    DocumentOrm,
    DocumentQueryParamsGeneral
)
from ..models.caption import (
    CaptionOrm,
    CaptionQueryParamsGeneral
)
from ..models.content import (
    ContentOrm,
    ContentQueryParamsGeneral
)
from ..models.media import (
    MediaOrm,
    MediaQueryParamsGeneral
)
from ..models.request import (
    RequestOrm,
    RequestQueryParamsGeneral
)
from ..models.access_token import (
    AccessTokenOrm,
    AccessTokenQueryParamsGeneral
)
from ..models.bearer_token import (
    BearerTokenOrm,
    BearerTokenQueryParamsGeneral
)
from ..models.peer import (
    PeerOrm,
    PeerQueryParamsGeneral
)
from ..models.peer_connection import (
    PeerConnectionOrm,
    PeerConnectionQueryParamsGeneral
)
from ..models.peer_connection_path import (
    PeerConnectionPathOrm,
    PeerConnectionPathQueryParamsGeneral
)
from ..models.peer_access_token import (
    PeerAccessTokenOrm,
    PeerAccessTokenQueryParamsGeneral
)
from ..models.peer_bearer_token import (
    PeerBearerTokenOrm,
    PeerBearerTokenQueryParamsGeneral
)
from ..models.transcription import (
    TranscriptionOrm,
    TranscriptionQueryParamsGeneral
)
from ..models.transcription_segment import (
    TranscriptionSegmentOrm,
    TranscriptionSegmentQueryParamsGeneral
)
from ..models.transcription_word import (
    TranscriptionWordOrm,
    TranscriptionWordQueryParamsGeneral
)
from ..models.transcription_language_prob import (
    TranscriptionLanguageProbOrm,
    TranscriptionLanguageProbQueryParamsGeneral
)
from ..models.transcription_speech_activity import (
    TranscriptionSpeechActivityOrm,
    TranscriptionSpeechActivityQueryParamsGeneral
)

from ..conditions import (
    add_user_conditions,
    add_document_conditions,
    add_content_conditions,
    add_caption_conditions,
    add_media_conditions,
    add_request_conditions,
    add_access_token_conditions,
    add_bearer_token_conditions,
    add_peer_conditions,
    add_peer_connection_conditions,
    add_peer_access_token_conditions,
    add_peer_bearer_token_conditions,
    add_peer_connection_path_conditions,
    add_transcription_conditions,
    add_transcription_segment_conditions,
    add_transcription_language_prob_conditions,
    add_transcription_speech_activity_conditions,
    add_transcription_word_conditions
)

from .base import AuthBaseRouter





class UserRouter(AuthBaseRouter):
    def __init__(self,
        db_engine: typing.Any = None,
        app: typing.Any = None,
        router: typing.Any = None,
        config: trp_config.TrpConfig | None = None,
        auth_scheme: typing.Any = None
    ):
        super().__init__(db_engine, app, router, config, auth_scheme)

        print('UserRouter ... db_engine:', db_engine)
        print('UserRouter ... app:', app)
        print('UserRouter ... router:', router)
        print('UserRouter ... config:', config)
        print('UserRouter ... auth_scheme:', auth_scheme)

        # Get records
        async def get_records(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],

            user_primary: typing.Annotated[UserQueryParamsPrimary, Depends(UserQueryParamsPrimary)],
            user: typing.Annotated[UserQueryParamsGeneral, Depends(UserQueryParamsGeneral)],
            
            document: typing.Annotated[DocumentQueryParamsGeneral, Depends(DocumentQueryParamsGeneral)],
            content: typing.Annotated[ContentQueryParamsGeneral, Depends(ContentQueryParamsGeneral)],
            caption: typing.Annotated[CaptionQueryParamsGeneral, Depends(CaptionQueryParamsGeneral)],
            media: typing.Annotated[MediaQueryParamsGeneral, Depends(MediaQueryParamsGeneral)],
            request: typing.Annotated[RequestQueryParamsGeneral, Depends(RequestQueryParamsGeneral)],
            
            access_token: typing.Annotated[AccessTokenQueryParamsGeneral, Depends(AccessTokenQueryParamsGeneral)],
            bearer_token: typing.Annotated[BearerTokenQueryParamsGeneral, Depends(BearerTokenQueryParamsGeneral)],
            
            peer: typing.Annotated[PeerQueryParamsGeneral, Depends(PeerQueryParamsGeneral)],
            peer_connection: typing.Annotated[PeerConnectionQueryParamsGeneral, Depends(PeerConnectionQueryParamsGeneral)],
            peer_access_token: typing.Annotated[PeerAccessTokenQueryParamsGeneral, Depends(PeerAccessTokenQueryParamsGeneral)],
            peer_bearer_token: typing.Annotated[PeerBearerTokenQueryParamsGeneral, Depends(PeerBearerTokenQueryParamsGeneral)],
            peer_connection_path: typing.Annotated[PeerConnectionPathQueryParamsGeneral, Depends(PeerConnectionPathQueryParamsGeneral)],

            transcription: typing.Annotated[TranscriptionQueryParamsGeneral, Depends(TranscriptionQueryParamsGeneral)],
            transcription_segment: typing.Annotated[TranscriptionSegmentQueryParamsGeneral, Depends(TranscriptionSegmentQueryParamsGeneral)],
            transcription_language_prob: typing.Annotated[TranscriptionLanguageProbQueryParamsGeneral, Depends(TranscriptionLanguageProbQueryParamsGeneral)],
            transcription_speech_activity: typing.Annotated[TranscriptionSpeechActivityQueryParamsGeneral, Depends(TranscriptionSpeechActivityQueryParamsGeneral)],
            transcription_word: typing.Annotated[TranscriptionWordQueryParamsGeneral, Depends(TranscriptionWordQueryParamsGeneral)],

            limit: int = 0,
            offset: int = 0,
            order: str | None = None,

            use_and: bool = False,

            dataview: str | None = None
        ) -> ApiResponseWithCountModel:
            user = UserQueryParams(
                user_primary.id if user.id is None else user.id,
                user_primary.uuid if user.uuid is None else user.uuid,
                user_primary.created_at if user.created_at is None else user.created_at,
                user_primary.modified_at if user.modified_at is None else user.modified_at,
                user_primary.username if user.username is None else user.username,
                user_primary.email if user.email is None else user.email,
                #user_primary.password if user.password is None else user.password,
                #user_primary.salt if user.salt is None else user.salt,
                user_primary.display_name if user.display_name is None else user.display_name,
                user_primary.is_active if user.is_active is None else user.is_active
            )

            result = None
            with Session(self._db_engine) as db_session:
                # Create count statement
                count_stmt = db_session.query(sqlalchemy.func.count(UserOrm.id))

                # Create query statement
                stmt = sqlalchemy.select(UserOrm)

                ## Add conditions
                where = []

                has_join__user = False
                has_join__content = False
                has_join__media = False
                has_join__request = False
                has_join__peer = False
                has_join__peer_connection = False
                has_join__peer_connection_path = False
                has_join__transcription = False
                has_join__transcription_segment = False
                has_join__transcription_word = False
                has_join__transcription_language_prob = False
                has_join__transcription_speech_activity = False

                # Add user conditions
                add_user_conditions(
                    where,
                    user,
                    use_and
                )
                
                # Add document conditions
                if add_document_conditions(
                    where,
                    document,
                    use_and
                ):
                    stmt = stmt.join(UserOrm.documents, full=True)
                    has_join__document = True
                
                # Add content conditions
                if add_content_conditions(
                    where,
                    content,
                    use_and
                ):
                    if not has_join__document:
                        stmt = stmt.join(UserOrm.documents, full=True)
                        has_join__document = True
                    stmt = stmt.join(DocumentOrm.contents, full=True)
                    has_join__content = True
                
                # Add caption conditions
                if add_caption_conditions(
                    where,
                    caption,
                    use_and
                ):
                    if not has_join__document:
                        stmt = stmt.join(UserOrm.documents, full=True)
                        has_join__document = True
                    if not has_join__content:
                        stmt = stmt.join(DocumentOrm.contents, full=True)
                        has_join__content = True
                    stmt = stmt.join(ContentOrm.captions, full=True)
                
                # Add media conditions
                if add_media_conditions(
                    where,
                    media,
                    use_and
                ):
                    if not has_join__document:
                        stmt = stmt.join(UserOrm.documents, full=True)
                        has_join__document = True
                    stmt = stmt.join(DocumentOrm.medias, full=True)
                    has_join__media = True
                
                # Add request conditions
                if add_request_conditions(
                    where,
                    request,
                    use_and
                ):
                    if not has_join__document:
                        stmt = stmt.join(UserOrm.documents, full=True)
                        has_join__document = True
                    stmt = stmt.join(DocumentOrm.requests, full=True)
                    has_join__request = True
                
                # Add access_token conditions
                if add_access_token_conditions(
                    where,
                    access_token,
                    use_and
                ):
                    stmt = stmt.join(UserOrm.access_tokens, full=True)
                
                # Add bearer_token conditions
                if add_bearer_token_conditions(
                    where,
                    bearer_token,
                    use_and
                ):
                    stmt = stmt.join(UserOrm.bearer_tokens, full=True)

                # Add peer conditions
                if add_peer_conditions(
                    where,
                    peer,
                    use_and
                ):
                    stmt = stmt.join(UserOrm.peers, full=True)
                    has_join__peer = True

                # Add peer_connection conditions
                if add_peer_connection_conditions(
                    where,
                    peer_connection,
                    use_and
                ):
                    if not has_join__peer:
                        stmt = stmt.join(UserOrm.peers, full=True)
                        has_join__peer = True
                    stmt = stmt.join(PeerOrm.connections, full=True)
                    has_join__peer_connection = True

                # Add peer_connection_path conditions
                if add_peer_connection_path_conditions(
                    where,
                    peer_connection_path,
                    use_and
                ):
                    if not has_join__peer:
                        stmt = stmt.join(UserOrm.peers, full=True)
                        has_join__peer = True
                    if not has_join__peer_connection:
                        stmt = stmt.join(PeerOrm.connections, full=True)
                        has_join__peer_connection = True
                    stmt = stmt.join(PeerConnectionOrm.paths, full=True)
                    has_join__peer_connection_path = True

                # Add peer_access_token conditions
                if add_peer_access_token_conditions(
                    where,
                    peer_access_token,
                    use_and
                ):
                    if not has_join__peer:
                        stmt = stmt.join(UserOrm.peers, full=True)
                        has_join__peer = True
                    stmt = stmt.join(PeerOrm.access_tokens, full=True)

                # Add peer_bearer_token conditions
                if add_peer_bearer_token_conditions(
                    where,
                    peer_bearer_token,
                    use_and
                ):
                    if not has_join__peer:
                        stmt = stmt.join(UserOrm.peers, full=True)
                        has_join__peer = True
                    stmt = stmt.join(PeerOrm.bearer_tokens, full=True)

                # Add transcription conditions
                if add_transcription_conditions(
                    where,
                    transcription,
                    use_and
                ):
                    if not has_join__document:
                        stmt = stmt.join(UserOrm.documents, full=True)
                        has_join__document = True
                    if not has_join__media:
                        stmt = stmt.join(DocumentOrm.medias, full=True)
                        has_join__media = True
                    stmt = stmt.join(MediaOrm.transcriptions, full=True)
                    has_join__transcription = True

                # Add transcription_segment conditions
                if add_transcription_segment_conditions(
                    where,
                    transcription_segment,
                    use_and
                ):
                    if not has_join__document:
                        stmt = stmt.join(UserOrm.documents, full=True)
                        has_join__document = True
                    if not has_join__media:
                        stmt = stmt.join(DocumentOrm.medias, full=True)
                        has_join__media = True
                    if not has_join__transcription:
                        stmt = stmt.join(MediaOrm.transcriptions, full=True)
                        has_join__transcription = True
                    stmt = stmt.join(TranscriptionOrm.segments, full=True)
                    has_join__transcription_segment = True

                # Add transcription_word conditions
                if add_transcription_word_conditions(
                    where,
                    transcription_word,
                    use_and
                ):
                    if not has_join__document:
                        stmt = stmt.join(UserOrm.documents, full=True)
                        has_join__document = True
                    if not has_join__media:
                        stmt = stmt.join(DocumentOrm.medias, full=True)
                        has_join__media = True
                    if not has_join__transcription:
                        stmt = stmt.join(MediaOrm.transcriptions, full=True)
                        has_join__transcription = True
                    if not has_join__transcription_segment:
                        stmt = stmt.join(TranscriptionOrm.segments, full=True)
                        has_join__transcription_segment = True
                    stmt = stmt.join(TranscriptionSegmentOrm.words, full=True)
                    has_join__transcription_word = True

                # Add transcription_language_prob conditions
                if add_transcription_language_prob_conditions(
                    where,
                    transcription_language_prob,
                    use_and
                ):
                    if not has_join__document:
                        stmt = stmt.join(UserOrm.documents, full=True)
                        has_join__document = True
                    if not has_join__media:
                        stmt = stmt.join(DocumentOrm.medias, full=True)
                        has_join__media = True
                    if not has_join__transcription:
                        stmt = stmt.join(MediaOrm.transcriptions, full=True)
                        has_join__transcription = True
                    stmt = stmt.join(TranscriptionOrm.language_probs, full=True)
                    has_join__transcription_language_prob = True

                # Add transcription_speech_activity conditions
                if add_transcription_speech_activity_conditions(
                    where,
                    transcription_speech_activity,
                    use_and
                ):
                    if not has_join__document:
                        stmt = stmt.join(UserOrm.documents, full=True)
                        has_join__document = True
                    if not has_join__media:
                        stmt = stmt.join(DocumentOrm.medias, full=True)
                        has_join__media = True
                    if not has_join__transcription:
                        stmt = stmt.join(MediaOrm.transcriptions, full=True)
                        has_join__transcription = True
                    stmt = stmt.join(TranscriptionOrm.speech_activities, full=True)
                    has_join__transcription_speech_activity = True
                

                # Add conditions to statements
                if len(where):
                    cond = sqlalchemy.and_(*where)
                    count_stmt = count_stmt.where(cond)
                    stmt = stmt.where(cond)
                
                # Get total number of records
                num_records_total = count_stmt.scalar()
                if num_records_total == 0:
                    return {
                        'success': False,
                        'message': 'No Users found'
                    }
                
                # Add limit and offset
                if offset > 0:
                    stmt = stmt.offset(offset)
                if limit > 0:
                    stmt = stmt.limit(limit)

                # Add order
                order = split_order_fields(order, 'user')
                if order is not None:
                    stmt = stmt.order_by(*order)
                
                # Parse dataview
                dataview_struct = self._parse_dataview(dataview)
                
                # Execute statement
                result = []
                for row in db_session.execute(stmt):
                    if row.UserOrm is None:
                        continue
                    result.append(
                        row.UserOrm.to_dict(dataview_struct)
                    )
            
            # If we didn't find any records
            # return an error message
            if result is None:
                return {
                    'success': False,
                    'message': 'No Users found'
                }
            num_records = len(result)
            if num_records == 0:
                return {
                    'success': False,
                    'message': 'No Users found'
                }
            
            # Return the result
            return {
                'success': True,
                'message': 'Users found',
                'total': num_records_total,
                'count': num_records,
                'payload': result
            }

        self.get_records = types.MethodType(get_records, self)



        # Get record
        async def get_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            #id: int | str,
            id: TrpPrimaryKey_UserID,
            dataview: str | None = None
        ) -> ApiResponseModel:
            # Get key and extract value
            if is_empty(id):
                return {
                    'success': False,
                    'message': 'ID missing'
                }
            id_type, id, msg = self._get_id_attr(id, {
                'int': 'id',
                'str': [
                    ('id', RE_INT_VALUE, lambda x: int(x)),
                    ('uuid', RE_UUID4, None),
                    ('email', RE_EMAIL, None),
                    ('username', RE_USERNAME, None)
                ]
            }, 'Invalid ID')
            if id_type is None:
                return msg
            
            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(UserOrm)
                stmt = self._add_id_where_stmt(stmt, UserOrm, id_type, id)

                # Get record
                record, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'UserOrm',
                    'User not found',
                    'Multiple Users found'
                )
                if record is None:
                    return msg
                
                # Parse and filter
                dataview_struct = self._parse_dataview(dataview)
                record_dict = record.to_dict(dataview_struct)
                
                # Return the result
                return {
                    'success': True,
                    'message': 'User found',
                    'payload': record_dict
                }
        
        self.get_record = types.MethodType(get_record, self)



        # Create record
        async def create_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            data: NewUserModel
        ) -> ApiResponseModel:
            # Verify password syntax
            pw = data.password
            if is_empty(pw):
                return {
                    'success': False,
                    'message': 'Password missing'
                }
            if RE_PASSWORD.match(pw) is None:
                return {
                    'success': False,
                    'message': 'Invalid password'
                }
            pw = UserOrm.hash_password(pw)

            record = None
            with Session(self._db_engine) as db_session:
                # Create record
                try:
                    record = UserOrm(
                        uuid=str(uuid.uuid4()),
                        username=data.username,
                        email=data.email,
                        password=pw['hash'],
                        salt=pw['salt'],
                        display_name=data.display_name,
                        is_active=(True if data.is_active is None else data.is_active)
                    )
                    db_session.add(record)
                    db_session.commit()
                except Exception as e:
                    return {
                        'success': False,
                        'message': str(e)
                    }
            
                # Return success
                return {
                    'success': True,
                    'message': 'User created',
                    'payload': {
                        'id': record.id
                    }
                }
        
        self.create_record = types.MethodType(create_record, self)



        # Update record
        async def update_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            id: TrpPrimaryKey_UserID,
            data: UpdateUserModel
        ) -> ApiResponseModel:
            if is_empty(id):
                return {
                    'success': False,
                    'message': 'ID missing'
                }
            id_type, id, msg = self._get_id_attr(id, {
                'int': 'id',
                'str': [
                    ('id', RE_INT_VALUE, lambda x: int(x)),
                    ('uuid', RE_UUID4, None),
                    ('email', RE_EMAIL, None),
                    ('username', RE_USERNAME, None)
                ]
            }, 'Invalid ID')
            if id_type is None:
                return msg
            
            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(UserOrm)
                stmt = self._add_id_where_stmt(stmt, UserOrm, id_type, id)

                # Get record
                record, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'UserOrm',
                    'User not found',
                    'Multiple Users found'
                )
                if record is None:
                    return msg
                
                # Get data to update
                d = {}
                for f in ['username', 'email', 'display_name', 'is_active']:
                    if getattr(data, f) is not None:
                        d[f] = getattr(data, f)
                if len(d) == 0:
                    return {
                        'success': False,
                        'message': 'Nothing to update'
                    }

                # Update record
                try:
                    db_session.query(UserOrm).\
                        filter(UserOrm.id == record.id).\
                        update(d)
                    db_session.commit()
                except Exception as e:
                    return {
                        'success': False,
                        'message': str(e)
                    }
            
                # Return success
                return {
                    'success': True,
                    'message': 'User updated',
                    'payload': {
                        'id': record.id
                    }
                }
        
        self.update_record = types.MethodType(update_record, self)



        # Delete record
        async def delete_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            id: TrpPrimaryKey_UserID
        ) -> ApiResponseModel:
            if is_empty(id):
                return {
                    'success': False,
                    'message': 'ID missing'
                }
            id_type, id, msg = self._get_id_attr(id, {
                'int': 'id',
                'str': [
                    ('id', RE_INT_VALUE, lambda x: int(x)),
                    ('uuid', RE_UUID4, None),
                    ('email', RE_EMAIL, None),
                    ('username', RE_USERNAME, None)
                ]
            }, 'Invalid ID')
            if id_type is None:
                return msg
            
            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(UserOrm)
                stmt = self._add_id_where_stmt(stmt, UserOrm, id_type, id)

                # Get record
                record, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'UserOrm',
                    'User not found',
                    'Multiple Users found'
                )
                if record is None:
                    return msg
                
                # Delete record if found
                try:
                    db_session.execute(
                        sqlalchemy.delete(UserOrm).\
                            where(UserOrm.id == record.id)
                    )
                    db_session.commit()
                except Exception as e:
                    return {
                        'success': False,
                        'message': str(e)
                    }
            
                # Return success
                return {
                    'success': True,
                    'message': 'User deleted',
                    'payload': {
                        'id': record.id
                    }
                }
        
        self.delete_record = types.MethodType(delete_record, self)



        # Validate password
        async def validate_password(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            id: TrpPrimaryKey_UserID,
            data: SingleStringModel
        ) -> ApiResponseModel:
            if is_empty(id):
                return {
                    'success': False,
                    'message': 'ID missing'
                }
            id_type, id, msg = self._get_id_attr(id, {
                'int': 'id',
                'str': [
                    ('id', RE_INT_VALUE, lambda x: int(x)),
                    ('uuid', RE_UUID4, None),
                    ('email', RE_EMAIL, None),
                    ('username', RE_USERNAME, None)
                ]
            }, 'Invalid ID')
            if id_type is None:
                return msg
            
            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(UserOrm)
                stmt = self._add_id_where_stmt(stmt, UserOrm, id_type, id)

                # Get record
                record, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'UserOrm',
                    'User not found',
                    'Multiple Users found'
                )
                if record is None:
                    return msg
            
                # Return success
                return {
                    'success': True,
                    'message': 'Password validated',
                    'payload': {
                        'value': UserOrm.test_password(
                            data.value,
                            record.password,
                            record.salt
                        )
                    }
                }
        
        self.validate_password = types.MethodType(validate_password, self)



        # Change password
        async def change_password(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            id: TrpPrimaryKey_UserID,
            data: ChangePasswordModel
        ) -> ApiResponseModel:
            if is_empty(id):
                return {
                    'success': False,
                    'message': 'ID missing'
                }
            id_type, id, msg = self._get_id_attr(id, {
                'int': 'id',
                'str': [
                    ('id', RE_INT_VALUE, lambda x: int(x)),
                    ('uuid', RE_UUID4, None),
                    ('email', RE_EMAIL, None),
                    ('username', RE_USERNAME, None)
                ]
            }, 'Invalid ID')
            if id_type is None:
                return msg
            
            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(UserOrm)
                stmt = self._add_id_where_stmt(stmt, UserOrm, id_type, id)

                # Get record
                record, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'UserOrm',
                    'User not found',
                    'Multiple Users found'
                )
                if record is None:
                    return msg
                
                # Validate data
                pw_old = data.old
                if is_empty(pw_old):
                    return {
                        'success': False,
                        'message': 'Old password missing'
                    }
                if RE_PASSWORD.match(pw_old) is None:
                    return {
                        'success': False,
                        'message': 'Old password invalid'
                    }
                
                pw_new = data.new
                if is_empty(pw_new):
                    return {
                        'success': False,
                        'message': 'New password missing'
                    }
                if RE_PASSWORD.match(pw_new) is None:
                    return {
                        'success': False,
                        'message': 'New password invalid'
                    }
                
                if not UserOrm.test_password(data.old, record.password, record.salt):
                    return {
                        'success': False,
                        'message': 'Old password is incorrect'
                    }

                pw_new = UserOrm.hash_password(pw_new)

                # Update record if found
                try:
                    db_session.query(UserOrm).\
                        filter(UserOrm.id == record.id).\
                        update({
                            'password': pw_new.hash,
                            'salt': pw_new.salt
                        })
                    db_session.commit()
                except Exception as e:
                    return {
                        'success': False,
                        'message': str(e)
                    }
                
                # Return success
                return {
                    'success': True,
                    'message': 'Password changed',
                    'payload': {
                        'id': record.id
                    }
                }
        
        self.change_password = types.MethodType(change_password, self)



        ### Add routes ###
        #router.add_api_route('/user/current',               self.validate_password, methods=['POST'])
        router.add_api_route('/user/{id}/password/validate', self.validate_password, methods=['POST'])
        router.add_api_route('/user/{id}/password/change',   self.change_password,   methods=['POST'])
        router.add_api_route('/users',          self.get_records,      methods=['GET'])
        router.add_api_route('/user/{id}',      self.get_record,       methods=['GET'])
        router.add_api_route('/user',           self.create_record,    methods=['POST'])
        router.add_api_route('/user/{id}',      self.update_record,    methods=['PUT'])
        router.add_api_route('/user/{id}',      self.delete_record,    methods=['DELETE'])





### Testing ###

if __name__ == '__main__':
    router = UserRouter()
    print('router:', router)


