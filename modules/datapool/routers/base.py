# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import typing

import trp_config

from regex_patterns import (
    RE_SPLIT_TO_LIST,
    RE_FIELD_NAME
)



###
# Base Routers
###

class BaseRouter:
    __slots__ = [
        '_db_engine',
        '_app',
        '_router',
        '_config',

        '_server',
        '_templates',

        'get_records',
        'get_record',
        'create_record',
        'update_record',
        'delete_record'
    ]

    def __init__(self,
        db_engine: typing.Any = None,
        app: typing.Any = None,
        router: typing.Any = None,
        config: trp_config.TrpConfig | None = None
    ):
        self._server = None
        self._templates = None

        self._db_engine = db_engine
        self._app = app
        self._router = router
        self._config = config
    

    
    def _get_id_attr(self, val: typing.Any, rules: dict, msg: str = 'Invalid ID') -> tuple:
        if isinstance(val, int):
            if 'int' in rules:
                return rules['int'], val, None
            return 'id', val, None
        
        for attr, regex, func in rules['str']:
            result = regex.match(val)
            if result is None:
                continue
            if func is not None:
                return attr, func(result.group(1)), None
            return attr, result.group(1), None
        
        return None, None, { 'success': False, 'message': msg }

    
    def _add_id_where_stmt(self, stmt: typing.Any, orm: typing.Any, attr: str, val: typing.Any) -> typing.Any:
        return stmt.where(
            getattr(orm, attr) == val
        )

    
    def _get_single_record(self, results: typing.Any, orm: str, msg_none: str, msg_multi: str) -> tuple:
        record = None
        for row in results:
            if record is not None:
                return (None, {
                    'success': False,
                    'message': msg_multi
                })
            record = getattr(row, orm)
    
        if record is None:
            return (None, {
                'success': False,
                'message': msg_none
            })
        
        return (record, None)
    

    def _parse_dataview(self, dataview: str | None) -> dict | None:
        if not isinstance(dataview, str):
            return None
        
        dataview = dataview.strip()
        if len(dataview) == 0:
            return None
        
        fields = RE_SPLIT_TO_LIST.split(dataview)

        container = {}
        for field in fields:
            if not RE_FIELD_NAME.match(field):
                continue
            field_elems = field.split('.')
            
            sub_container = container
            while len(field_elems) > 1:
                f_elem = field_elems.pop(0)
                if f_elem not in sub_container or not isinstance(sub_container[f_elem], dict):
                    sub_container[f_elem] = {}
                sub_container = sub_container[f_elem]
            sub_container[field_elems.pop(0)] = True

        return container



class AuthBaseRouter(BaseRouter):
    __slots__ = [
        '_auth_scheme'
    ]

    def __init__(self,
        db_engine: typing.Any = None,
        app: typing.Any = None,
        router: typing.Any = None,
        config: trp_config.TrpConfig | None = None,
        auth_scheme: typing.Any = None
    ):
        super().__init__(db_engine, app, router, config)
        self._auth_scheme = auth_scheme


