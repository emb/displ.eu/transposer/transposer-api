# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import types
import typing
import re
import uuid

from fastapi import (
    Depends
)

import sqlalchemy
from sqlalchemy.orm import Session

from regex_patterns import (
    RE_INT_VALUE,
    RE_UUID4,
    RE_SLUG,
    RE_HASH_XX,
    RE_XXHASH_CID_HEX
)
from data_utils import (
    is_empty,
    validate_uri as validate_uri_func
)

import trp_config

from ..filter import (
    split_order_fields
)

from ..models.base import (
    ApiResponseModel,
    ApiResponseWithCountModel,

    TrpPrimaryKey_ID_UUID
)
from ..models.pipeline import (
    PipelineOrm,
    
    PipelineModel,
    NewPipelineModel,
    UpdatePipelineModel,
    NewPipelineStructureModel,
    UpdatePipelineStructureModel,

    PipelineQueryParams,
    PipelineQueryParamsPrimary,
    PipelineQueryParamsGeneral
)
from ..models.pipeline_connector import (
    PipelineConnectorOrm,
    PipelineConnectorQueryParamsGeneral
)
from ..models.pipeline_consumer import (
    PipelineConsumerOrm,
    PipelineConsumerQueryParamsGeneral
)
from ..models.pipeline_producer import (
    PipelineProducerOrm,
    PipelineProducerQueryParamsGeneral
)
from ..models.pipeline_server import (
    PipelineServerOrm,
    PipelineServerQueryParamsGeneral
)
from ..models.setting import (
    SettingOrm,
    SettingQueryParamsGeneral
)

from ..conditions import (
    add_pipeline_conditions,
    add_pipeline_connector_conditions,
    add_pipeline_consumer_conditions,
    add_pipeline_producer_conditions,
    add_pipeline_server_conditions,
    add_setting_conditions
)

from .base import AuthBaseRouter





class PipelineRouter(AuthBaseRouter):
    def __init__(self,
        db_engine: typing.Any = None,
        app: typing.Any = None,
        router: typing.Any = None,
        config: trp_config.TrpConfig | None = None,
        auth_scheme: typing.Any = None
    ):
        super().__init__(db_engine, app, router, config, auth_scheme)

        print('PipelineRouter ... db_engine:', db_engine)
        print('PipelineRouter ... app:', app)
        print('PipelineRouter ... router:', router)
        print('PipelineRouter ... config:', config)
        print('PipelineRouter ... auth_scheme:', auth_scheme)

        # Get records
        async def get_records(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],

            pipeline_primary: typing.Annotated[PipelineQueryParamsPrimary, Depends(PipelineQueryParamsPrimary)],
            pipeline: typing.Annotated[PipelineQueryParamsGeneral, Depends(PipelineQueryParamsGeneral)],

            setting: typing.Annotated[SettingQueryParamsGeneral, Depends(SettingQueryParamsGeneral)],

            pipeline_connector: typing.Annotated[PipelineConnectorQueryParamsGeneral, Depends(PipelineConnectorQueryParamsGeneral)],
            pipeline_consumer: typing.Annotated[PipelineConsumerQueryParamsGeneral, Depends(PipelineConsumerQueryParamsGeneral)],
            pipeline_producer: typing.Annotated[PipelineProducerQueryParamsGeneral, Depends(PipelineProducerQueryParamsGeneral)],
            pipeline_server: typing.Annotated[PipelineServerQueryParamsGeneral, Depends(PipelineServerQueryParamsGeneral)],
            
            limit: int = 0,
            offset: int = 0,
            order: str | None = None,

            use_and: bool = False,

            dataview: str | None = None
        ) -> ApiResponseWithCountModel:
            pipeline = PipelineQueryParams(
                pipeline_primary.id if pipeline.id is None else pipeline.id,
                pipeline_primary.uuid if pipeline.uuid is None else pipeline.uuid,
                pipeline_primary.created_at if pipeline.created_at is None else pipeline.created_at,
                pipeline_primary.setting_id if pipeline.setting_id is None else pipeline.setting_id,
                pipeline_primary.name if pipeline.name is None else pipeline.name
            )

            result = None
            with Session(self._db_engine) as db_session:
                # Create count statement
                count_stmt = db_session.query(sqlalchemy.func.count(PipelineOrm.id))

                # Create query statement
                stmt = sqlalchemy.select(PipelineOrm)

                ## Add conditions
                where = []

                # Add pipeline conditions
                add_pipeline_conditions(
                    where,
                    pipeline,
                    use_and
                )

                # Add setting conditions
                if add_setting_conditions(
                    where,
                    setting,
                    use_and
                ):
                    stmt = stmt.join(PipelineOrm.setting_id, full=True)

                # Add pipeline_connector conditions
                if add_pipeline_connector_conditions(
                    where,
                    pipeline_connector,
                    use_and
                ):
                    stmt = stmt.join(PipelineOrm.connectors, full=True)

                # Add pipeline_consumer conditions
                if add_pipeline_consumer_conditions(
                    where,
                    pipeline_consumer,
                    use_and
                ):
                    stmt = stmt.join(PipelineOrm.consumers, full=True)

                # Add pipeline_producer conditions
                if add_pipeline_producer_conditions(
                    where,
                    pipeline_producer,
                    use_and
                ):
                    stmt = stmt.join(PipelineOrm.producers, full=True)

                # Add pipeline_server conditions
                if add_pipeline_server_conditions(
                    where,
                    pipeline_server,
                    use_and
                ):
                    stmt = stmt.join(PipelineOrm.servers, full=True)
                

                # Add conditions to statements
                if len(where):
                    cond = sqlalchemy.and_(*where)
                    count_stmt = count_stmt.where(cond)
                    stmt = stmt.where(cond)
                
                # Get total number of records
                num_records_total = count_stmt.scalar()
                if num_records_total == 0:
                    return {
                        'success': False,
                        'message': 'No Pipelines found'
                    }
                
                # Add limit and offset
                if offset > 0:
                    stmt = stmt.offset(offset)
                if limit > 0:
                    stmt = stmt.limit(limit)

                # Add order
                order = split_order_fields(order, 'pipeline')
                if order is not None:
                    stmt = stmt.order_by(*order)
                
                # Parse dataview
                dataview_struct = self._parse_dataview(dataview)
                
                # Execute statement
                result = []
                for row in db_session.execute(stmt):
                    if row.PipelineOrm is None:
                        continue
                    result.append(
                        row.PipelineOrm.to_dict(dataview_struct)
                    )
            
            # If we didn't find any records
            # return an error message
            if result is None:
                return {
                    'success': False,
                    'message': 'No Pipelines found'
                }
            num_records = len(result)
            if num_records == 0:
                return {
                    'success': False,
                    'message': 'No Pipelines found'
                }
            
            # Return the result
            return {
                'success': True,
                'message': 'Pipelines found',
                'total': num_records_total,
                'count': num_records,
                'payload': result
            }

        self.get_records = types.MethodType(get_records, self)



        # Get record
        async def get_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            id: TrpPrimaryKey_ID_UUID,
            dataview: str | None = None
        ) -> ApiResponseModel:
            # Get key and extract value
            if is_empty(id):
                return {
                    'success': False,
                    'message': 'ID missing'
                }
            id_type, id, msg = self._get_id_attr(id, {
                'int': 'id',
                'str': [
                    ('id', RE_INT_VALUE, lambda x: int(x)),
                    ('uuid', RE_UUID4, None)
                ]
            }, 'Invalid ID')
            if id_type is None:
                return msg
            
            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(PipelineOrm)
                stmt = self._add_id_where_stmt(stmt, PipelineOrm, id_type, id)

                # Get record
                record, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'PipelineOrm',
                    'Pipeline not found',
                    'Multiple Pipelines found'
                )
                if record is None:
                    return msg
                
                # Parse and filter
                dataview_struct = self._parse_dataview(dataview)
                record_dict = record.to_dict(dataview_struct)
                
                # Return the result
                return {
                    'success': True,
                    'message': 'Pipeline found',
                    'payload': record_dict
                }
        
        self.get_record = types.MethodType(get_record, self)



        # Create record
        async def create_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            data: NewPipelineStructureModel
        ) -> ApiResponseModel:
            # Get parent key and value
            parent_id = data.setting
            if parent_id is None:
                return {
                    'success': False,
                    'message': 'Setting Key missing'
                }
            result = RE_SLUG.match(parent_id)
            if result is None:
                return {
                    'success': False,
                    'message': 'Invalid Setting Key'
                }
            parent_id = result.group(1)

            parent = None
            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(SettingOrm).\
                    where(SettingOrm.id == parent_id)
                
                # Get parent record
                parent, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'SettingOrm',
                    'Setting not found',
                    'Multiple Settings found'
                )
                if parent is None:
                    return msg

                # Create record
                try:
                    record = PipelineOrm(
                        uuid=str(uuid.uuid4()),
                        setting_id=parent.id,
                        name=data.name
                    )

                    # Add pipeline_producers steps
                    if isinstance(data.producers, list):
                        for item in data.producers:
                            producer = PipelineProducerOrm(
                                uuid=str(uuid.uuid4()),
                                name=item.name,
                                transaction_id=item.transaction_id
                            )
                            record.producers.append(producer)

                    # Add pipeline_consumers steps
                    if isinstance(data.consumers, list):
                        for item in data.consumers:
                            consumer = PipelineConsumerOrm(
                                uuid=str(uuid.uuid4()),
                                name=item.name
                            )
                            record.consumers.append(consumer)

                    # Add pipeline_servers steps
                    if isinstance(data.servers, list):
                        for item in data.servers:
                            server = PipelineServerOrm(
                                uuid=str(uuid.uuid4()),
                                name=item.name,
                                host=(item.host if item.host else '127.0.0.1'),
                                port=(item.port if item.port else 9092)
                            )
                            record.servers.append(server)

                    # Add pipeline_connectors steps
                    if isinstance(data.connectors, list):
                        for item in data.connectors:
                            connector = PipelineConnectorOrm(
                                uuid=str(uuid.uuid4()),
                                name=item.name
                            )
                            record.connectors.append(connector)
                    
                    db_session.add(record)
                    db_session.commit()
                except Exception as e:
                    return {
                        'success': False,
                        'message': str(e)
                    }
            
                # Return success
                return {
                    'success': True,
                    'message': 'Pipeline created',
                    'payload': {
                        'id': record.id,
                        'uuid': record.uuid
                    }
                }
        
        self.create_record = types.MethodType(create_record, self)



        # Update record
        async def update_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            id: TrpPrimaryKey_ID_UUID,
            data: UpdatePipelineStructureModel
        ) -> ApiResponseModel:
            if is_empty(id):
                return {
                    'success': False,
                    'message': 'ID missing'
                }
            id_type, id, msg = self._get_id_attr(id, {
                'int': 'id',
                'str': [
                    ('id', RE_INT_VALUE, lambda x: int(x)),
                    ('uuid', RE_UUID4, None)
                ]
            }, 'Invalid ID')
            if id_type is None:
                return msg
            
            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(PipelineOrm)
                stmt = self._add_id_where_stmt(stmt, PipelineOrm, id_type, id)

                # Get record
                record, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'PipelineOrm',
                    'Pipeline not found',
                    'Multiple Pipelines found'
                )
                if record is None:
                    return msg
                
                # Get data to update
                d = {}
                for f in ['name']:
                    if getattr(data, f) is not None:
                        d[f] = getattr(data, f)
                if len(d) == 0:
                    return {
                        'success': False,
                        'message': 'Nothing to update'
                    }

                # Update record
                try:
                    db_session.query(PipelineOrm).\
                        filter(PipelineOrm.id == record.id).\
                        update(d)
                    db_session.commit()
                except Exception as e:
                    return {
                        'success': False,
                        'message': str(e)
                    }
            
                # Return success
                return {
                    'success': True,
                    'message': 'Pipeline updated',
                    'payload': {
                        'id': record.id
                    }
                }
        
        self.update_record = types.MethodType(update_record, self)



        # Delete record
        async def delete_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            id: TrpPrimaryKey_ID_UUID
        ) -> ApiResponseModel:
            if is_empty(id):
                return {
                    'success': False,
                    'message': 'ID missing'
                }
            id_type, id, msg = self._get_id_attr(id, {
                'int': 'id',
                'str': [
                    ('id', RE_INT_VALUE, lambda x: int(x)),
                    ('uuid', RE_UUID4, None)
                ]
            }, 'Invalid ID')
            if id_type is None:
                return msg

            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(PipelineOrm)
                stmt = self._add_id_where_stmt(stmt, PipelineOrm, id_type, id)

                # Get record
                record, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'PipelineOrm',
                    'Pipeline not found',
                    'Multiple Pipelines found'
                )
                if record is None:
                    return msg
                
                # Delete record if found
                try:
                    db_session.execute(
                        sqlalchemy.delete(PipelineOrm).\
                            where(PipelineOrm.id == record.id)
                    )
                    db_session.commit()
                except Exception as e:
                    return {
                        'success': False,
                        'message': str(e)
                    }
            
                # Return success
                return {
                    'success': True,
                    'message': 'Pipeline deleted',
                    'payload': {
                        'id': record.id
                    }
                }
        
        self.delete_record = types.MethodType(delete_record, self)



        ### Add routes ###
        router.add_api_route('/pipelines',          self.get_records,      methods=['GET'])
        router.add_api_route('/pipeline/{id}',      self.get_record,       methods=['GET'])
        router.add_api_route('/pipeline',           self.create_record,    methods=['POST'])
        router.add_api_route('/pipeline/{id}',      self.update_record,    methods=['PUT'])
        router.add_api_route('/pipeline/{id}',      self.delete_record,    methods=['DELETE'])





### Testing ###

if __name__ == '__main__':
    router = PipelineRouter()
    print('router:', router)


