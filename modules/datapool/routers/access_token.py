# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import types
import typing

from fastapi import (
    Depends
)

import sqlalchemy
from sqlalchemy.orm import Session

from regex_patterns import (
    RE_INT_VALUE,
    RE_UUID4,
    RE_HASH_XX,
    RE_SHA256_HEX,
    RE_SHAKE256_KEY_HEX,
    RE_EMAIL,
    RE_USERNAME
)
from data_utils import (
    is_empty
)
import trp_config

from ..filter import (
    split_order_fields
)

from ..models.base import (
    ApiResponseModel,
    ApiResponseWithCountModel,

    TrpPrimaryKey_ID_TOKEN_KEY
)
from ..models.access_token import (
    AccessTokenOrm,

    AccessTokenModel,
    NewAccessTokenModel,
    UpdateAccessTokenModel,

    AccessTokenQueryParams,
    AccessTokenQueryParamsPrimary,
    AccessTokenQueryParamsGeneral
)
from ..models.user import (
    UserOrm
)

from ..conditions import (
    add_access_token_conditions
)

from .base import AuthBaseRouter





class AccessTokenRouter(AuthBaseRouter):
    def __init__(self,
        db_engine: typing.Any = None,
        app: typing.Any = None,
        router: typing.Any = None,
        config: trp_config.TrpConfig | None = None,
        auth_scheme: typing.Any = None
    ):
        super().__init__(db_engine, app, router, config, auth_scheme)

        print('AccessTokenRouter ... db_engine:', db_engine)
        print('AccessTokenRouter ... app:', app)
        print('AccessTokenRouter ... router:', router)
        print('AccessTokenRouter ... config:', config)
        print('AccessTokenRouter ... auth_scheme:', auth_scheme)

        # Get records
        async def get_records(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],

            access_token_primary: typing.Annotated[AccessTokenQueryParamsPrimary, Depends(AccessTokenQueryParamsPrimary)],
            access_token: typing.Annotated[AccessTokenQueryParamsGeneral, Depends(AccessTokenQueryParamsGeneral)],

            limit: int = 0,
            offset: int = 0,
            order: str | None = None,

            use_and: bool = False,

            dataview: str | None = None
        ) -> ApiResponseWithCountModel:
            access_token = AccessTokenQueryParams(
                access_token_primary.id if access_token.id is None else access_token.id,
                access_token_primary.created_at if access_token.created_at is None else access_token.created_at,
                access_token_primary.owner_id if access_token.owner_id is None else access_token.owner_id,
                access_token_primary.name if access_token.name is None else access_token.name,
                access_token_primary.key if access_token.key is None else access_token.key,
                access_token_primary.token if access_token.token is None else access_token.token,
                access_token_primary.is_active if access_token.is_active is None else access_token.is_active,
                access_token_primary.valid_until if access_token.valid_until is None else access_token.valid_until
            )

            result = None
            with Session(self._db_engine) as db_session:
                # Create count statement
                count_stmt = db_session.query(sqlalchemy.func.count(AccessTokenOrm.id))

                # Create query statement
                stmt = sqlalchemy.select(AccessTokenOrm)

                ## Add conditions
                where = []

                # Add access_token conditions
                add_access_token_conditions(
                    where,
                    access_token,
                    use_and
                )

                # Add conditions to statements
                if len(where):
                    cond = sqlalchemy.and_(*where)
                    count_stmt = count_stmt.where(cond)
                    stmt = stmt.where(cond)
                
                # Get total number of records
                num_records_total = count_stmt.scalar()
                if num_records_total == 0:
                    return {
                        'success': False,
                        'message': 'No AccessTokens found'
                    }
                
                # Add limit and offset
                if offset > 0:
                    stmt = stmt.offset(offset)
                if limit > 0:
                    stmt = stmt.limit(limit)

                # Add order
                order = split_order_fields(order, 'access_token')
                if order is not None:
                    stmt = stmt.order_by(*order)
                
                # Parse dataview
                dataview_struct = self._parse_dataview(dataview)

                # Execute statement
                result = []
                for row in db_session.execute(stmt):
                    if row.AccessTokenOrm is None:
                        continue
                    result.append(
                        row.AccessTokenOrm.to_dict(dataview_struct)
                    )

            # If we didn't find any records
            # return an error message
            if result is None:
                return {
                    'success': False,
                    'message': 'No AccessTokens found'
                }
            num_records = len(result)
            if num_records == 0:
                return {
                    'success': False,
                    'message': 'No AccessTokens found'
                }
            
            # Return the result
            return {
                'success': True,
                'message': 'AccessTokens found',
                'total': num_records_total,
                'count': num_records,
                'payload': result
            }

        self.get_records = types.MethodType(get_records, self)



        # Get record
        async def get_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            id: TrpPrimaryKey_ID_TOKEN_KEY,
            dataview: str | None = None
        ) -> ApiResponseModel:
            # Get key and extract value
            if is_empty(id):
                return {
                    'success': False,
                    'message': 'ID missing'
                }
            id_type, id, msg = self._get_id_attr(id, {
                'int': 'id',
                'str': [
                    ('id', RE_INT_VALUE, lambda x: int(x)),
                    ('token', RE_SHA256_HEX, None),
                    ('key', RE_SHAKE256_KEY_HEX, None)
                ]
            }, 'Invalid ID')
            if id_type is None:
                return msg
            
            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(AccessTokenOrm)
                stmt = self._add_id_where_stmt(stmt, AccessTokenOrm, id_type, id)

                # Get record
                record, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'AccessTokenOrm',
                    'AccessToken not found',
                    'Multiple AccessTokens found'
                )
                if record is None:
                    return msg
                
                # Parse and filter
                dataview_struct = self._parse_dataview(dataview)
                record_dict = record.to_dict(dataview_struct)

                # Return the result
                return {
                    'success': True,
                    'message': 'AccessToken found',
                    'payload': record_dict
                }
        
        self.get_record = types.MethodType(get_record, self)



        # Create record
        async def create_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            data: NewAccessTokenModel
        ) -> ApiResponseModel:
            # Get parent key and value
            parent_id = data.owner
            if parent_id is None:
                return {
                    'success': False,
                    'message': 'User ID missing'
                }
            parent_id_type, parent_id, msg = self._get_id_attr(parent_id, {
                'int': 'id',
                'str': [
                    ('id', RE_INT_VALUE, lambda x: int(x)),
                    ('uuid', RE_UUID4, None),
                    ('email', RE_EMAIL, None),
                    ('username', RE_USERNAME, None)
                ]
            }, 'Invalid User ID')
            if parent_id_type is None:
                return msg

            parent = None
            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(UserOrm)
                stmt = self._add_id_where_stmt(stmt, UserOrm, parent_id_type, parent_id)

                # Get parent record
                parent, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'UserOrm',
                    'User not found',
                    'Multiple Users found'
                )
                if parent is None:
                    return msg

                # Create record
                token = AccessTokenOrm.get_random_sha256()
                try:
                    record = AccessTokenOrm(
                        owner_id=parent.id,
                        name=data.name,
                        key=AccessTokenOrm.get_random_shake256(10),
                        token=AccessTokenOrm.get_sha256(token),
                        is_active=data.is_active,
                        valid_until=data.valid_until
                    )
                    db_session.add(record)
                    db_session.commit()
                except Exception as e:
                    return {
                        'success': False,
                        'message': str(e)
                    }
            
                # Return success
                return {
                    'success': True,
                    'message': 'AccessToken created',
                    'payload': {
                        'id': record.id,
                        'name': record.name,
                        'key': record.key,
                        'token': token
                    }
                }
        
        self.create_record = types.MethodType(create_record, self)



        # Update record
        async def update_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            id: TrpPrimaryKey_ID_TOKEN_KEY,
            data: UpdateAccessTokenModel
        ) -> ApiResponseModel:
            # Get key and extract value
            if is_empty(id):
                return {
                    'success': False,
                    'message': 'ID missing'
                }
            id_type, id, msg = self._get_id_attr(id, {
                'int': 'id',
                'str': [
                    ('id', RE_INT_VALUE, lambda x: int(x)),
                    ('token', RE_SHA256_HEX, None),
                    ('key', RE_SHAKE256_KEY_HEX, None)
                ]
            }, 'Invalid ID')
            if id_type is None:
                return msg
            if id_type == 'token':
                id = AccessTokenOrm.get_sha256(id)

            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(AccessTokenOrm)
                stmt = self._add_id_where_stmt(stmt, AccessTokenOrm, id_type, id)

                # Get record
                record, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'AccessTokenOrm',
                    'AccessToken not found',
                    'Multiple AccessTokens found'
                )
                if record is None:
                    return msg
                
                # Get data to update
                d = {}
                for f in ['name', 'is_active', 'valid_until']:
                    if getattr(data, f) is not None:
                        d[f] = getattr(data, f)
                if len(d) == 0:
                    return {
                        'success': False,
                        'message': 'Nothing to update'
                    }

                # Update record
                try:
                    db_session.query(AccessTokenOrm).\
                        filter(AccessTokenOrm.id == record.id).\
                        update(d)
                    db_session.commit()
                except Exception as e:
                    return {
                        'success': False,
                        'message': str(e)
                    }
            
                # Return success
                return {
                    'success': True,
                    'message': 'AccessToken updated',
                    'payload': {
                        'id': record.id
                    }
                }
        
        self.update_record = types.MethodType(update_record, self)



        # Delete record
        async def delete_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            id: TrpPrimaryKey_ID_TOKEN_KEY
        ) -> ApiResponseModel:
            # Get key and extract value
            if is_empty(id):
                return {
                    'success': False,
                    'message': 'ID missing'
                }
            id_type, id, msg = self._get_id_attr(id, {
                'int': 'id',
                'str': [
                    ('id', RE_INT_VALUE, lambda x: int(x)),
                    ('token', RE_SHA256_HEX, None),
                    ('key', RE_SHAKE256_KEY_HEX, None)
                ]
            }, 'Invalid ID')
            if id_type is None:
                return msg
            if id_type == 'token':
                id = AccessTokenOrm.get_sha256(id)
            
            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(AccessTokenOrm)
                stmt = self._add_id_where_stmt(stmt, AccessTokenOrm, id_type, id)

                # Get record
                record, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'AccessTokenOrm',
                    'AccessToken not found',
                    'Multiple AccessTokens found'
                )
                if record is None:
                    return msg
                
                # Delete record if found
                try:
                    db_session.execute(
                        sqlalchemy.delete(AccessTokenOrm).\
                            where(AccessTokenOrm.id == record.id)
                    )
                    db_session.commit()
                except Exception as e:
                    return {
                        'success': False,
                        'message': str(e)
                    }
            
                # Return success
                return {
                    'success': True,
                    'message': 'AccessToken deleted',
                    'payload': {
                        'id': record.id
                    }
                }
        
        self.delete_record = types.MethodType(delete_record, self)





        ### Add routes ###
        router.add_api_route('/access-tokens',          self.get_records,      methods=['GET'])
        router.add_api_route('/access-token/{id}',      self.get_record,       methods=['GET'])
        router.add_api_route('/access-token',           self.create_record,    methods=['POST'])
        router.add_api_route('/access-token/{id}',      self.update_record,    methods=['PUT'])
        router.add_api_route('/access-token/{id}',      self.delete_record,    methods=['DELETE'])





### Testing ###

if __name__ == '__main__':
    router = AccessTokenRouter()
    print('router:', router)


