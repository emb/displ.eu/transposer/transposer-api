# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import types
import typing
import re
import uuid

from fastapi import (
    Depends
)

import sqlalchemy
from sqlalchemy.orm import Session

from regex_patterns import (
    RE_INT_VALUE,
    RE_UUID4,
    RE_HASH_XX,
    RE_XXHASH_CID_HEX
)
from data_utils import (
    is_empty,
    validate_uri as validate_uri_func
)

import trp_config

from ..filter import (
    split_order_fields
)

from ..models.base import (
    ApiResponseModel,
    ApiResponseWithCountModel,

    TrpPrimaryKey_ID_UUID
)
from ..models.pipeline_connector import (
    PipelineConnectorOrm,
    
    PipelineConnectorModel,
    NewPipelineConnectorModel,
    UpdatePipelineConnectorModel,

    PipelineConnectorQueryParams,
    PipelineConnectorQueryParamsPrimary,
    PipelineConnectorQueryParamsGeneral
)
from ..models.pipeline import (
    PipelineOrm,
    PipelineQueryParamsGeneral
)

from ..conditions import (
    add_pipeline_connector_conditions,
    add_pipeline_conditions
)

from .base import AuthBaseRouter





class PipelineConnectorRouter(AuthBaseRouter):
    def __init__(self,
        db_engine: typing.Any = None,
        app: typing.Any = None,
        router: typing.Any = None,
        config: trp_config.TrpConfig | None = None,
        auth_scheme: typing.Any = None
    ):
        super().__init__(db_engine, app, router, config, auth_scheme)

        print('PipelineConnectorRouter ... db_engine:', db_engine)
        print('PipelineConnectorRouter ... app:', app)
        print('PipelineConnectorRouter ... router:', router)
        print('PipelineConnectorRouter ... config:', config)
        print('PipelineConnectorRouter ... auth_scheme:', auth_scheme)

        # Get records
        async def get_records(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],

            pipeline_connector_primary: typing.Annotated[PipelineConnectorQueryParamsPrimary, Depends(PipelineConnectorQueryParamsPrimary)],
            pipeline_connector: typing.Annotated[PipelineConnectorQueryParamsGeneral, Depends(PipelineConnectorQueryParamsGeneral)],

            pipeline: typing.Annotated[PipelineQueryParamsGeneral, Depends(PipelineQueryParamsGeneral)],
            
            limit: int = 0,
            offset: int = 0,
            order: str | None = None,

            use_and: bool = False,

            dataview: str | None = None
        ) -> ApiResponseWithCountModel:
            pipeline_connector = PipelineConnectorQueryParams(
                pipeline_connector_primary.id if pipeline_connector.id is None else pipeline_connector.id,
                pipeline_connector_primary.uuid if pipeline_connector.uuid is None else pipeline_connector.uuid,
                pipeline_connector_primary.created_at if pipeline_connector.created_at is None else pipeline_connector.created_at,
                pipeline_connector_primary.pipeline_id if pipeline_connector.pipeline_id is None else pipeline_connector.pipeline_id,
                pipeline_connector_primary.name if pipeline_connector.name is None else pipeline_connector.name
            )

            result = None
            with Session(self._db_engine) as db_session:
                # Create count statement
                count_stmt = db_session.query(sqlalchemy.func.count(PipelineConnectorOrm.id))

                # Create query statement
                stmt = sqlalchemy.select(PipelineConnectorOrm)

                ## Add conditions
                where = []

                # Add pipeline_connector conditions
                add_pipeline_connector_conditions(
                    where,
                    pipeline_connector,
                    use_and
                )

                # Add pipeline conditions
                if add_pipeline_conditions(
                    where,
                    pipeline,
                    use_and
                ):
                    stmt = stmt.join(PipelineConnectorOrm.pipeline, full=True)
                

                # Add conditions to statements
                if len(where):
                    cond = sqlalchemy.and_(*where)
                    count_stmt = count_stmt.where(cond)
                    stmt = stmt.where(cond)
                
                # Get total number of records
                num_records_total = count_stmt.scalar()
                if num_records_total == 0:
                    return {
                        'success': False,
                        'message': 'No PipelineConnectors found'
                    }
                
                # Add limit and offset
                if offset > 0:
                    stmt = stmt.offset(offset)
                if limit > 0:
                    stmt = stmt.limit(limit)

                # Add order
                order = split_order_fields(order, 'pipeline_connector')
                if order is not None:
                    stmt = stmt.order_by(*order)
                
                # Parse dataview
                dataview_struct = self._parse_dataview(dataview)
                
                # Execute statement
                result = []
                for row in db_session.execute(stmt):
                    if row.PipelineConnectorOrm is None:
                        continue
                    result.append(
                        row.PipelineConnectorOrm.to_dict(dataview_struct)
                    )
            
            # If we didn't find any records
            # return an error message
            if result is None:
                return {
                    'success': False,
                    'message': 'No PipelineConnectors found'
                }
            num_records = len(result)
            if num_records == 0:
                return {
                    'success': False,
                    'message': 'No PipelineConnectors found'
                }
            
            # Return the result
            return {
                'success': True,
                'message': 'PipelineConnectors found',
                'total': num_records_total,
                'count': num_records,
                'payload': result
            }

        self.get_records = types.MethodType(get_records, self)



        # Get record
        async def get_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            id: TrpPrimaryKey_ID_UUID,
            dataview: str | None = None
        ) -> ApiResponseModel:
            # Get key and extract value
            if is_empty(id):
                return {
                    'success': False,
                    'message': 'ID missing'
                }
            id_type, id, msg = self._get_id_attr(id, {
                'int': 'id',
                'str': [
                    ('id', RE_INT_VALUE, lambda x: int(x)),
                    ('uuid', RE_UUID4, None)
                ]
            }, 'Invalid ID')
            if id_type is None:
                return msg
            
            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(PipelineConnectorOrm)
                stmt = self._add_id_where_stmt(stmt, PipelineConnectorOrm, id_type, id)

                # Get record
                record, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'PipelineConnectorOrm',
                    'PipelineConnector not found',
                    'Multiple PipelineConnectors found'
                )
                if record is None:
                    return msg
                
                # Parse and filter
                dataview_struct = self._parse_dataview(dataview)
                record_dict = record.to_dict(dataview_struct)
                
                # Return the result
                return {
                    'success': True,
                    'message': 'PipelineConnector found',
                    'payload': record_dict
                }
        
        self.get_record = types.MethodType(get_record, self)



        # Create record
        async def create_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            data: NewPipelineConnectorModel
        ) -> ApiResponseModel:
            # Get parent key and value
            parent_id = data.setting
            if parent_id is None:
                return {
                    'success': False,
                    'message': 'Pipeline ID missing'
                }
            parent_id_type, parent_id, msg = self._get_id_attr(parent_id, {
                'int': 'id',
                'str': [
                    ('id', RE_INT_VALUE, lambda x: int(x)),
                    ('uuid', RE_UUID4, None)
                ]
            }, 'Invalid Pipeline ID')
            if parent_id_type is None:
                return msg

            parent = None
            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(PipelineOrm)
                stmt = self._add_id_where_stmt(stmt, PipelineOrm, parent_id_type, parent_id)
                
                # Get parent record
                parent, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'PipelineOrm',
                    'Pipeline not found',
                    'Multiple Pipelines found'
                )
                if parent is None:
                    return msg

                # Create record
                try:
                    record = PipelineConnectorOrm(
                        uuid=str(uuid.uuid4()),
                        pipeline_id=parent.id,
                        name=data.name
                    )
                    db_session.add(record)
                    db_session.commit()
                except Exception as e:
                    return {
                        'success': False,
                        'message': str(e)
                    }
            
                # Return success
                return {
                    'success': True,
                    'message': 'PipelineConnector created',
                    'payload': {
                        'id': record.id,
                        'uuid': record.uuid
                    }
                }
        
        self.create_record = types.MethodType(create_record, self)



        # Update record
        async def update_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            id: TrpPrimaryKey_ID_UUID,
            data: UpdatePipelineConnectorModel
        ) -> ApiResponseModel:
            if is_empty(id):
                return {
                    'success': False,
                    'message': 'ID missing'
                }
            id_type, id, msg = self._get_id_attr(id, {
                'int': 'id',
                'str': [
                    ('id', RE_INT_VALUE, lambda x: int(x)),
                    ('uuid', RE_UUID4, None)
                ]
            }, 'Invalid ID')
            if id_type is None:
                return msg
            
            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(PipelineConnectorOrm)
                stmt = self._add_id_where_stmt(stmt, PipelineConnectorOrm, id_type, id)

                # Get record
                record, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'PipelineConnectorOrm',
                    'PipelineConnector not found',
                    'Multiple PipelineConnectors found'
                )
                if record is None:
                    return msg
                
                # Get data to update
                d = {}
                for f in ['name']:
                    if getattr(data, f) is not None:
                        d[f] = getattr(data, f)
                if len(d) == 0:
                    return {
                        'success': False,
                        'message': 'Nothing to update'
                    }

                # Update record
                try:
                    db_session.query(PipelineConnectorOrm).\
                        filter(PipelineConnectorOrm.id == record.id).\
                        update(d)
                    db_session.commit()
                except Exception as e:
                    return {
                        'success': False,
                        'message': str(e)
                    }
            
                # Return success
                return {
                    'success': True,
                    'message': 'PipelineConnector updated',
                    'payload': {
                        'id': record.id
                    }
                }
        
        self.update_record = types.MethodType(update_record, self)



        # Delete record
        async def delete_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            id: TrpPrimaryKey_ID_UUID
        ) -> ApiResponseModel:
            if is_empty(id):
                return {
                    'success': False,
                    'message': 'ID missing'
                }
            id_type, id, msg = self._get_id_attr(id, {
                'int': 'id',
                'str': [
                    ('id', RE_INT_VALUE, lambda x: int(x)),
                    ('uuid', RE_UUID4, None)
                ]
            }, 'Invalid ID')
            if id_type is None:
                return msg

            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(PipelineConnectorOrm)
                stmt = self._add_id_where_stmt(stmt, PipelineConnectorOrm, id_type, id)

                # Get record
                record, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'PipelineConnectorOrm',
                    'PipelineConnector not found',
                    'Multiple PipelineConnectors found'
                )
                if record is None:
                    return msg
                
                # Delete record if found
                try:
                    db_session.execute(
                        sqlalchemy.delete(PipelineConnectorOrm).\
                            where(PipelineConnectorOrm.id == record.id)
                    )
                    db_session.commit()
                except Exception as e:
                    return {
                        'success': False,
                        'message': str(e)
                    }
            
                # Return success
                return {
                    'success': True,
                    'message': 'PipelineConnector deleted',
                    'payload': {
                        'id': record.id
                    }
                }
        
        self.delete_record = types.MethodType(delete_record, self)



        ### Add routes ###
        router.add_api_route('/pipeline/connectors',          self.get_records,      methods=['GET'])
        router.add_api_route('/pipeline/connector/{id}',      self.get_record,       methods=['GET'])
        router.add_api_route('/pipeline/connector',           self.create_record,    methods=['POST'])
        router.add_api_route('/pipeline/connector/{id}',      self.update_record,    methods=['PUT'])
        router.add_api_route('/pipeline/connector/{id}',      self.delete_record,    methods=['DELETE'])





### Testing ###

if __name__ == '__main__':
    router = PipelineConnectorRouter()
    print('router:', router)


