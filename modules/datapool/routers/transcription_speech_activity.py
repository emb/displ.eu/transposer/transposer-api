# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import types
import typing

from fastapi import (
    Depends
)

import sqlalchemy
from sqlalchemy.orm import Session

from regex_patterns import (
    RE_INT_VALUE,
    RE_UUID4,
    RE_HASH_XX,
    RE_SHA256_HEX,
    RE_SHAKE256_KEY_HEX,
    RE_EMAIL,
    RE_USERNAME
)
from data_utils import (
    is_empty
)
import trp_config

from ..filter import (
    split_order_fields
)

from ..models.base import (
    ApiResponseModel,
    ApiResponseWithCountModel
)
from ..models.transcription_speech_activity import (
    TranscriptionSpeechActivityOrm,

    NewTranscriptionSpeechActivityModel,
    UpdateTranscriptionSpeechActivityModel,

    TranscriptionSpeechActivityQueryParams,
    TranscriptionSpeechActivityQueryParamsPrimary,
    TranscriptionSpeechActivityQueryParamsGeneral
)
from ..models.transcription import (
    TranscriptionOrm
)

from ..conditions import (
    add_transcription_speech_activity_conditions
)

from .base import AuthBaseRouter





class TranscriptionSpeechActivityRouter(AuthBaseRouter):
    def __init__(self,
        db_engine: typing.Any = None,
        app: typing.Any = None,
        router: typing.Any = None,
        config: trp_config.TrpConfig | None = None,
        auth_scheme: typing.Any = None
    ):
        super().__init__(db_engine, app, router, config, auth_scheme)

        print('TranscriptionSpeechActivityRouter ... db_engine:', db_engine)
        print('TranscriptionSpeechActivityRouter ... app:', app)
        print('TranscriptionSpeechActivityRouter ... router:', router)
        print('TranscriptionSpeechActivityRouter ... config:', config)
        print('TranscriptionSpeechActivityRouter ... auth_scheme:', auth_scheme)

        # Get records
        async def get_records(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],

            transcription_speech_activity_primary: typing.Annotated[TranscriptionSpeechActivityQueryParamsPrimary, Depends(TranscriptionSpeechActivityQueryParamsPrimary)],
            transcription_speech_activity: typing.Annotated[TranscriptionSpeechActivityQueryParamsGeneral, Depends(TranscriptionSpeechActivityQueryParamsGeneral)],

            limit: int = 0,
            offset: int = 0,
            order: str | None = None,

            use_and: bool = False,

            dataview: str | None = None
        ) -> ApiResponseWithCountModel:
            transcription_speech_activity = TranscriptionSpeechActivityQueryParams(
                transcription_speech_activity_primary.id if transcription_speech_activity.id is None else transcription_speech_activity.id,
                transcription_speech_activity_primary.transcription_id if transcription_speech_activity.transcription_id is None else transcription_speech_activity.transcription_id,
                transcription_speech_activity_primary.pos if transcription_speech_activity.pos is None else transcription_speech_activity.pos,
                transcription_speech_activity_primary.start if transcription_speech_activity.start is None else transcription_speech_activity.start,
                transcription_speech_activity_primary.end if transcription_speech_activity.end is None else transcription_speech_activity.end
            )

            result = None
            with Session(self._db_engine) as db_session:
                # Create count statement
                count_stmt = db_session.query(sqlalchemy.func.count(TranscriptionSpeechActivityOrm.id))

                # Create query statement
                stmt = sqlalchemy.select(TranscriptionSpeechActivityOrm)

                ## Add conditions
                where = []

                # Add transcription_speech_activity conditions
                add_transcription_speech_activity_conditions(
                    where,
                    transcription_speech_activity,
                    use_and
                )

                # Add conditions to statements
                if len(where):
                    cond = sqlalchemy.and_(*where)
                    count_stmt = count_stmt.where(cond)
                    stmt = stmt.where(cond)
                
                # Get total number of records
                num_records_total = count_stmt.scalar()
                if num_records_total == 0:
                    return {
                        'success': False,
                        'message': 'No TranscriptionSpeechActivities found'
                    }
                
                # Add limit and offset
                if offset > 0:
                    stmt = stmt.offset(offset)
                if limit > 0:
                    stmt = stmt.limit(limit)

                # Add order
                order = split_order_fields(order, 'transcription_speech_activity')
                if order is not None:
                    stmt = stmt.order_by(*order)
                
                # Parse dataview
                dataview_struct = self._parse_dataview(dataview)
                
                # Execute statement
                result = []
                for row in db_session.execute(stmt):
                    if row.TranscriptionSpeechActivityOrm is None:
                        continue
                    result.append(
                        row.TranscriptionSpeechActivityOrm.to_dict(dataview_struct)
                    )
            
            # If we didn't find any records
            # return an error message
            if result is None:
                return {
                    'success': False,
                    'message': 'No TranscriptionSpeechActivities found'
                }
            num_records = len(result)
            if num_records == 0:
                return {
                    'success': False,
                    'message': 'No TranscriptionSpeechActivities found'
                }
            
            # Return the result
            return {
                'success': True,
                'message': 'TranscriptionSpeechActivities found',
                'total': num_records_total,
                'count': num_records,
                'payload': result
            }

        self.get_records = types.MethodType(get_records, self)



        # Get record
        async def get_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            id: int,
            dataview: str | None = None
        ) -> ApiResponseModel:
            # Get key and extract value
            if is_empty(id):
                return {
                    'success': False,
                    'message': 'ID missing'
                }
            
            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(TranscriptionSpeechActivityOrm).\
                    where(TranscriptionSpeechActivityOrm.id == id)

                # Get record
                record, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'TranscriptionSpeechActivityOrm',
                    'TranscriptionSpeechActivity not found',
                    'Multiple TranscriptionSpeechActivities found'
                )
                if record is None:
                    return msg
                
                # Parse and filter
                dataview_struct = self._parse_dataview(dataview)
                record_dict = record.to_dict(dataview_struct)
                
                # Return the result
                return {
                    'success': True,
                    'message': 'TranscriptionSpeechActivity found',
                    'payload': record_dict
                }
        
        self.get_record = types.MethodType(get_record, self)



        # Create record
        async def create_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            data: NewTranscriptionSpeechActivityModel
        ) -> ApiResponseModel:
            # Get parent key and value
            parent_id = data.transcription
            if parent_id is None:
                return {
                    'success': False,
                    'message': 'Transcription ID missing'
                }

            parent = None
            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(TranscriptionOrm).\
                    where(TranscriptionOrm.id == parent_id)
                
                # Get parent record
                parent, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'TranscriptionOrm',
                    'Transcription not found',
                    'Multiple Transcriptions found'
                )
                if parent is None:
                    return msg
                
                # Create record
                try:
                    record = TranscriptionSpeechActivityOrm(
                        transcription_id=parent.id,
                        pos=data.pos,
                        start=data.start,
                        end=data.end
                    )
                    db_session.add(record)
                    db_session.commit()
                except Exception as e:
                    return {
                        'success': False,
                        'message': str(e)
                    }
            
                # Return success
                return {
                    'success': True,
                    'message': 'TranscriptionSpeechActivity created',
                    'payload': {
                        'id': record.id
                    }
                }
        
        self.create_record = types.MethodType(create_record, self)



        # Update record
        async def update_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            id: int,
            data: UpdateTranscriptionSpeechActivityModel
        ) -> ApiResponseModel:
            if is_empty(id):
                return {
                    'success': False,
                    'message': 'ID missing'
                }
            
            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(TranscriptionSpeechActivityOrm).\
                    where(TranscriptionSpeechActivityOrm.id == id)

                # Get record
                record, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'TranscriptionSpeechActivityOrm',
                    'TranscriptionSpeechActivity not found',
                    'Multiple TranscriptionSpeechActivities found'
                )
                if record is None:
                    return msg
                
                # Get data to update
                d = {}
                for f in ['pos', 'start', 'end']:
                    if getattr(data, f) is not None:
                        d[f] = getattr(data, f)
                if len(d) == 0:
                    return {
                        'success': False,
                        'message': 'Nothing to update'
                    }

                # Update record
                try:
                    db_session.query(TranscriptionSpeechActivityOrm).\
                        filter(TranscriptionSpeechActivityOrm.id == record.id).\
                        update(d)
                    db_session.commit()
                except Exception as e:
                    return {
                        'success': False,
                        'message': str(e)
                    }
            
                # Return success
                return {
                    'success': True,
                    'message': 'TranscriptionSpeechActivity updated',
                    'payload': {
                        'id': record.id
                    }
                }
        
        self.update_record = types.MethodType(update_record, self)



        # Delete record
        async def delete_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            id: int
        ) -> ApiResponseModel:
            if is_empty(id):
                return {
                    'success': False,
                    'message': 'ID missing'
                }
            
            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(TranscriptionSpeechActivityOrm).\
                    where(TranscriptionSpeechActivityOrm.id == id)

                # Get record
                record, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'TranscriptionSpeechActivityOrm',
                    'TranscriptionSpeechActivity not found',
                    'Multiple TranscriptionSpeechActivities found'
                )
                if record is None:
                    return msg

                # Delete record if found
                try:
                    db_session.execute(
                        sqlalchemy.delete(TranscriptionSpeechActivityOrm).\
                            where(TranscriptionSpeechActivityOrm.id == record.id)
                    )
                    db_session.commit()
                except Exception as e:
                    return {
                        'success': False,
                        'message': str(e)
                    }
            
                # Return success
                return {
                    'success': True,
                    'message': 'TranscriptionSpeechActivity deleted',
                    'payload': {
                        'id': record.id
                    }
                }
        
        self.delete_record = types.MethodType(delete_record, self)



        ### Add routes ###
        router.add_api_route('/transcription/speech-activities',         self.get_records,      methods=['GET'])
        router.add_api_route('/transcription/speech-activity/{id}',      self.get_record,       methods=['GET'])
        router.add_api_route('/transcription/speech-activity',           self.create_record,    methods=['POST'])
        router.add_api_route('/transcription/speech-activity/{id}',      self.update_record,    methods=['PUT'])
        router.add_api_route('/transcription/speech-activity/{id}',      self.delete_record,    methods=['DELETE'])





### Testing ###

if __name__ == '__main__':
    router = TranscriptionSpeechActivityRouter()
    print('router:', router)


