# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import os
import types
import typing
import requests
import uuid
import json
from urllib.parse import urlparse, unquote
import magic
import aiofiles

import subprocess

from fastapi import (
    Depends,
    File,
    UploadFile,
    Form
)
from fastapi.staticfiles import StaticFiles
from fastapi.responses import FileResponse

import sqlalchemy
from sqlalchemy.orm import Session

from regex_patterns import (
    RE_INT_VALUE,
    RE_UUID4,
    RE_HASH_XX,
    RE_SHA256_HEX,
    RE_SHAKE256_KEY_HEX,
    RE_XXHASH_CID_HEX,
    RE_EMAIL,
    RE_USERNAME
)
from data_utils import (
    is_empty
)
import trp_config

from xxhash_wrapper import xxhsum

from ..filter import (
    split_order_fields
)

from ..models.media.maps import (
    MIME_TYPES_MAP
)

from ..models.base import (
    ApiResponseModel,
    ApiResponseWithCountModel,

    TrpPrimaryKey_MediaID
)
from ..models.media import (
    MediaOrm,

    MediaModel,
    NewMediaModel,
    UpdateMediaModel,
    NewMediaMinimalModel,
    UpdateMediaMinimalModel,

    MediaQueryParams,
    MediaQueryParamsPrimary,
    MediaQueryParamsGeneral
)
from ..models.document import (
    DocumentOrm
)

from ..conditions import (
    add_media_conditions
)

from .base import AuthBaseRouter





class MediaRouter(AuthBaseRouter):
    __slots__ = [
        'upload_record',
        'download_record',
        '_data_dir'
    ]

    def __init__(self,
        db_engine: typing.Any = None,
        app: typing.Any = None,
        router: typing.Any = None,
        config: trp_config.TrpConfig | None = None,
        auth_scheme: typing.Any = None
    ):
        super().__init__(db_engine, app, router, config, auth_scheme)

        print('MediaRouter ... db_engine:', db_engine)
        print('MediaRouter ... app:', app)
        print('MediaRouter ... router:', router)
        print('MediaRouter ... config:', config)
        print('MediaRouter ... auth_scheme:', auth_scheme)

        # Get data directory
        data_dir = os.getenv('TRP__DATAPOOL__STORAGE_DIR', '~/media')
        if not isinstance(data_dir, str):
            data_dir = '~/media'
        data_dir = data_dir.strip()
        if len(data_dir) == 0:
            data_dir = '~/media'
        
        if data_dir.startswith('~'):
            data_dir = os.path.expanduser(data_dir)
        elif not data_dir.startswith('/'):
            data_dir = os.path.realpath(os.path.join(os.getcwd(), data_dir))
        
        if not os.path.exists(data_dir):
            data_dir = None
        if data_dir.endswith('/'):
            data_dir = data_dir[:-1]
        self._data_dir = data_dir

        # Mount data directory
        if data_dir is not None:
            app.mount('/datapool/files', StaticFiles(directory=data_dir), name='datapool_files')


        # Get records
        async def get_records(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],

            media_primary: typing.Annotated[MediaQueryParamsPrimary, Depends(MediaQueryParamsPrimary)],
            media: typing.Annotated[MediaQueryParamsGeneral, Depends(MediaQueryParamsGeneral)],

            limit: int = 0,
            offset: int = 0,
            order: str | None = None,

            use_and: bool = False,

            dataview: str | None = None
        ) -> ApiResponseWithCountModel:
            media = MediaQueryParams(
                media_primary.id if media.id is None else media.id,
                media_primary.uuid if media.uuid is None else media.uuid,
                media_primary.created_at if media.created_at is None else media.created_at,
                media_primary.document_id if media.document_id is None else media.document_id,
                media_primary.uri if media.uri is None else media.uri,
                media_primary.path if media.path is None else media.path,
                media_primary.filename if media.filename is None else media.filename,
                media_primary.title if media.title is None else media.title,
                media_primary.cid if media.cid is None else media.cid,
                media_primary.mime if media.mime is None else media.mime,
                media_primary.extension if media.extension is None else media.extension,
                media_primary.size if media.size is None else media.size,
                media_primary.duration if media.duration is None else media.duration,
                media_primary.purpose if media.purpose is None else media.purpose,
                media_primary.status if media.status is None else media.status,
                media_primary.reason if media.reason is None else media.reason
            )

            result = None
            with Session(self._db_engine) as db_session:
                # Create count statement
                count_stmt = db_session.query(sqlalchemy.func.count(MediaOrm.id))

                # Create query statement
                stmt = sqlalchemy.select(MediaOrm)

                ## Add conditions
                where = []

                # Add media conditions
                add_media_conditions(
                    where,
                    media,
                    use_and
                )

                # Add conditions to statements
                if len(where):
                    cond = sqlalchemy.and_(*where)
                    count_stmt = count_stmt.where(cond)
                    stmt = stmt.where(cond)
                
                # Get total number of records
                num_records_total = count_stmt.scalar()
                if num_records_total == 0:
                    return {
                        'success': False,
                        'message': 'No Medias found'
                    }
                
                # Add limit and offset
                if offset > 0:
                    stmt = stmt.offset(offset)
                if limit > 0:
                    stmt = stmt.limit(limit)

                # Add order
                order = split_order_fields(order, 'media')
                if order is not None:
                    stmt = stmt.order_by(*order)
                
                # Parse dataview
                dataview_struct = self._parse_dataview(dataview)
                
                # Execute statement
                result = []
                for row in db_session.execute(stmt):
                    if row.MediaOrm is None:
                        continue
                    result.append(
                        row.MediaOrm.to_dict(dataview_struct)
                    )
            
            # If we didn't find any records
            # return an error message
            if result is None:
                return {
                    'success': False,
                    'message': 'No Medias found'
                }
            num_records = len(result)
            if num_records == 0:
                return {
                    'success': False,
                    'message': 'No Medias found'
                }
            
            # Return the result
            return {
                'success': True,
                'message': 'Medias found',
                'total': num_records_total,
                'count': num_records,
                'payload': result
            }

        self.get_records = types.MethodType(get_records, self)



        # Get record
        async def get_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            id: TrpPrimaryKey_MediaID,
            dataview: str | None = None
        ) -> ApiResponseModel:
            # Get key and extract value
            if is_empty(id):
                return {
                    'success': False,
                    'message': 'ID missing'
                }
            id_type, id, msg = self._get_id_attr(id, {
                'int': 'id',
                'str': [
                    ('id', RE_INT_VALUE, lambda x: int(x)),
                    ('uuid', RE_UUID4, None),
                    ('cid', RE_XXHASH_CID_HEX, None)
                ]
            }, 'Invalid ID')
            if id_type is None:
                return msg
            
            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(MediaOrm)
                stmt = self._add_id_where_stmt(stmt, MediaOrm, id_type, id)

                # Get record
                record, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'MediaOrm',
                    'Media not found',
                    'Multiple Medias found'
                )
                if record is None:
                    return msg
                
                # Parse and filter
                dataview_struct = self._parse_dataview(dataview)
                record_dict = record.to_dict(dataview_struct)
                
                # Return the result
                return {
                    'success': True,
                    'message': 'Media found',
                    'payload': record_dict
                }
        
        self.get_record = types.MethodType(get_record, self)



        # Create record
        async def create_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            data: NewMediaMinimalModel
        ) -> ApiResponseModel:
            # Get parent key and value
            parent_id = data.document
            if parent_id is None:
                return {
                    'success': False,
                    'message': 'Document ID missing'
                }
            parent_id_type, parent_id, msg = self._get_id_attr(parent_id, {
                'int': 'id',
                'str': [
                    ('id', RE_INT_VALUE, lambda x: int(x)),
                    ('uuid', RE_UUID4, None)
                ]
            }, 'Invalid Document ID')
            if parent_id_type is None:
                return msg

            parent = None
            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(DocumentOrm)
                stmt = self._add_id_where_stmt(stmt, DocumentOrm, parent_id_type, parent_id)
                
                # Get parent record
                parent, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'DocumentOrm',
                    'Document not found',
                    'Multiple Documents found'
                )
                if parent is None:
                    return msg
                
                uri = data.uri
                path = data.path
                val_state  = 0 if is_empty(uri) else 1
                val_state += 0 if is_empty(path) else 2
                if val_state == 0:
                    return {
                        'success': False,
                        'message': 'URI and path missing'
                    }
                if val_state > 2:
                    return {
                        'success': False,
                        'message': 'URI and path are present. Only provide one, not both.'
                    }

                # We got a URI
                # Download the file, save it and create the record
                if val_state == 1:
                    url_path = urlparse(uri).path
                    url_path = unquote(url_path)

                    file_name = os.path.basename(url_path)
                    file_path = f'{self._data_dir}/{file_name}'

                    if os.path.exists(file_path):
                        # Test if a record for that file exists
                        stmt = sqlalchemy.select(MediaOrm).\
                            where(MediaOrm.path == file_path)
                        record = db_session.execute(stmt).fetchone()
                        if record is not None:
                            record = record.MediaOrm
                            return {
                                'success': True,
                                'message': 'A file with that name already exists',
                                'payload': {
                                    'id': record.id,
                                    'uuid': record.uuid,
                                    'title': record.title,
                                    'cid': record.cid,
                                    'mime': record.mime,
                                    'extension': record.extension,
                                    'size': record.size,
                                    'duration': record.duration,
                                    'purpose': record.purpose
                                }
                            }
                        return {
                            'success': False,
                            'message': 'A file with that name already exists, but record is missing'
                        }

                    response = requests.get(uri)
                    if response.status_code == 200:
                        with open(file_path, 'wb') as fh:
                            fh.write(response.content)

                            # Get file meta
                            file_mime = magic.from_file(file_path, mime=True)
                            file_meta = self._get_metadata(file_path, file_name, file_mime)
                            if not file_meta['is_valid']:
                                os.remove(file_path)
                                return {
                                    'success': False,
                                    'message': 'File type not allowed'
                                }

                            # Hash file
                            file_hash = xxhsum(file_path, 3)

                            # Test if a file with that hash already exists
                            stmt = sqlalchemy.select(MediaOrm).\
                                where(MediaOrm.cid == file_hash)
                            record = db_session.execute(stmt).fetchone()
                            if record is not None:
                                os.remove(file_path)
                                record = record.MediaOrm
                                return {
                                    'success': True,
                                    'message': 'Identical file found',
                                    'payload': {
                                        'id': record.id,
                                        'uuid': record.uuid,
                                        'title': record.title,
                                        'cid': record.cid,
                                        'mime': record.mime,
                                        'extension': record.extension,
                                        'size': record.size,
                                        'duration': record.duration,
                                        'purpose': record.purpose
                                    }
                                }

                            # File record does not exist yet
                            try:
                                record = MediaOrm(
                                    uuid=str(uuid.uuid4()),
                                    document_id=parent.id,
                                    uri=data.uri,
                                    path=file_path,
                                    filename=file_name,
                                    title=(file_name if data.title is None else data.title).strip(),
                                    cid=file_hash,
                                    mime=file_meta['mime'],
                                    extension=file_meta['extension'],
                                    size=file_meta['size'],
                                    duration=file_meta['duration'],
                                    purpose=(data.purpose if data.purpose else 'original'),
                                    status=1
                                )
                                db_session.add(record)
                                db_session.commit()
                            except Exception as e:
                                os.remove(file_path)
                                return {
                                    'success': False,
                                    'message': str(e)
                                }
                
                        # Return success
                        return {
                            'success': True,
                            'message': 'Media created',
                            'payload': {
                                'id': record.id,
                                'uuid': record.uuid,
                                'title': record.title,
                                'cid': record.cid,
                                'mime': record.mime,
                                'extension': record.extension,
                                'size': record.size,
                                'duration': record.duration,
                                'purpose': record.purpose
                            }
                        }
                    return {
                        'success': False,
                        'message': 'Resource does not exist'
                    }
                

                # We got a path
                # Check if the file exists and create the record
                file_path = path.strip()
                if not os.path.exists(file_path):
                    return {
                        'success': False,
                        'message': 'File does not exist'
                    }
                file_name = os.path.basename(file_path)
                
                # Test if a record for that file exists
                stmt = sqlalchemy.select(MediaOrm).\
                    where(MediaOrm.path == file_path)
                record = db_session.execute(stmt).fetchone()
                if record is not None:
                    record = record.MediaOrm
                    return {
                        'success': True,
                        'message': 'File with this name already exists',
                        'payload': {
                            'id': record.id,
                            'uuid': record.uuid,
                            'title': record.title,
                            'cid': record.cid,
                            'mime': record.mime,
                            'extension': record.extension,
                            'size': record.size,
                            'duration': record.duration,
                            'purpose': record.purpose
                        }
                    }
                
                # Get file meta
                file_mime = magic.from_file(file_path, mime=True)
                file_meta = self._get_metadata(file_path, file_name, file_mime)
                if not file_meta['is_valid']:
                    return {
                        'success': False,
                        'message': 'File type not allowed'
                    }

                # Hash file
                file_hash = xxhsum(file_path, 3)

                # Test if a file with that hash already exists
                stmt = sqlalchemy.select(MediaOrm).\
                    where(MediaOrm.cid == file_hash)
                record = db_session.execute(stmt).fetchone()
                if record is not None:
                    record = record.MediaOrm
                    return {
                        'success': True,
                        'message': 'Identical file found',
                        'payload': {
                            'id': record.id,
                            'uuid': record.uuid,
                            'title': record.title,
                            'cid': record.cid,
                            'mime': record.mime,
                            'extension': record.extension,
                            'size': record.size,
                            'duration': record.duration,
                            'purpose': record.purpose
                        }
                    }
                
                # File record does not exist yet
                try:
                    record = MediaOrm(
                        uuid=str(uuid.uuid4()),
                        document_id=parent.id,
                        uri=None,
                        path=file_path,
                        filename=file_name,
                        title=(file_name if data.title is None else data.title).strip(),
                        cid=file_hash,
                        mime=file_meta['mime'],
                        extension=file_meta['extension'],
                        size=file_meta['size'],
                        duration=file_meta['duration'],
                        purpose=(data.purpose if data.purpose else 'original'),
                        status=1
                    )
                    db_session.add(record)
                    db_session.commit()
                except Exception as e:
                    return {
                        'success': False,
                        'message': str(e)
                    }
                
                # Return success
                return {
                    'success': True,
                    'message': 'Media created',
                    'payload': {
                        'id': record.id,
                        'uuid': record.uuid,
                        'title': record.title,
                        'cid': record.cid,
                        'mime': record.mime,
                        'extension': record.extension,
                        'size': record.size,
                        'duration': record.duration,
                        'purpose': record.purpose
                    }
                }
        
        self.create_record = types.MethodType(create_record, self)



        # Upload record
        async def upload_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            file: UploadFile,
            document: typing.Annotated[int | str, Form()],
            purpose: typing.Optional[str] = Form('original')
        ) -> ApiResponseModel:
            # Test if target dir exists
            if self._data_dir is None:
                return {
                    'success': False,
                    'message': 'Data directory missing'
                }
            
            # Get parent key and value
            parent_id = document
            if parent_id is None:
                return {
                    'success': False,
                    'message': 'Document ID missing'
                }
            parent_id_type, parent_id, msg = self._get_id_attr(parent_id, {
                'int': 'id',
                'str': [
                    ('id', RE_INT_VALUE, lambda x: int(x)),
                    ('uuid', RE_UUID4, None)
                ]
            }, 'Invalid Document ID')
            if parent_id_type is None:
                return msg
            
            # Get parent and create record
            parent = None
            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(DocumentOrm)
                stmt = self._add_id_where_stmt(stmt, DocumentOrm, parent_id_type, parent_id)
                
                # Get parent record
                parent, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'DocumentOrm',
                    'Document not found',
                    'Multiple Documents found'
                )
                if parent is None:
                    return msg
                
                parent_id = parent.id

                file_name = file.filename
                file_path = f'{self._data_dir}/{file_name}'
                file_mime = file.content_type.lower()
                if file_mime not in MIME_TYPES_MAP:
                    return {
                        'success': False,
                        'message': 'File type not allowed'
                    }

                # Test if a record for that file exists
                stmt = sqlalchemy.select(MediaOrm).\
                    where(MediaOrm.path == file_path)
                record = db_session.execute(stmt).fetchone()
                if record is not None:
                    record = record.MediaOrm
                    return {
                        'success': True,
                        'message': 'File with this name already exists',
                        'payload': {
                            'id': record.id,
                            'uuid': record.uuid,
                            'title': record.title,
                            'cid': record.cid,
                            'mime': record.mime,
                            'extension': record.extension,
                            'size': record.size,
                            'duration': record.duration,
                            'purpose': record.purpose
                        }
                    }

                # Save file
                async with aiofiles.open(file_path, 'wb') as out_file:
                    content = await file.read()
                    await out_file.write(content)

                # Get file meta
                file_meta = self._get_metadata(file_path, file_name, file_mime)
                if not file_meta['is_valid']:
                    os.remove(file_path)
                    return {
                        'success': False,
                        'message': 'Invalid file'
                    }
                
                # Hash file
                file_hash = xxhsum(file_path, 3)

                # Test if a file with that hash already exists
                stmt = sqlalchemy.select(MediaOrm).\
                    where(MediaOrm.cid == file_hash)
                record = db_session.execute(stmt).fetchone()
                if record is not None:
                    os.remove(file_path)
                    record = record.MediaOrm
                    return {
                        'success': True,
                        'message': 'Identical file found',
                        'payload': {
                            'id': record.id,
                            'uuid': record.uuid,
                            'title': record.title,
                            'cid': record.cid,
                            'mime': record.mime,
                            'extension': record.extension,
                            'size': record.size,
                            'duration': record.duration,
                            'purpose': record.purpose
                        }
                    }

                # File record does not exist yet
                try:
                    record = MediaOrm(
                        uuid=str(uuid.uuid4()),
                        document_id=parent_id,
                        uri=None,
                        path=file_path,
                        filename=file_name,
                        title=file_name,
                        cid=file_hash,
                        mime=file_meta['mime'],
                        extension=file_meta['extension'],
                        size=file_meta['size'],
                        duration=file_meta['duration'],
                        purpose=(purpose if purpose else 'original'),
                        status=1
                    )
                    db_session.add(record)
                    db_session.commit()
                except Exception as e:
                    os.remove(file_path)
                    return {
                        'success': False,
                        'message': str(e)
                    }
                
                # Return success
                return {
                    'success': True,
                    'message': 'Media created',
                    'payload': {
                        'id': record.id,
                        'uuid': record.uuid,
                        'title': record.title,
                        'cid': record.cid,
                        'mime': record.mime,
                        'extension': record.extension,
                        'size': record.size,
                        'duration': record.duration,
                        'purpose': record.purpose
                    }
                }
        
        self.upload_record = types.MethodType(upload_record, self)



        # Download record
        async def download_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            id: TrpPrimaryKey_MediaID
        ) -> ApiResponseModel:
            # Test if target dir exists
            if self._data_dir is None:
                return {
                    'success': False,
                    'message': 'Data directory missing'
                }
            # Test key
            if is_empty(id):
                return {
                    'success': False,
                    'message': 'ID missing'
                }
            id_type, id, msg = self._get_id_attr(id, {
                'int': 'id',
                'str': [
                    ('id', RE_INT_VALUE, lambda x: int(x)),
                    ('uuid', RE_UUID4, None),
                    ('cid', RE_XXHASH_CID_HEX, None)
                ]
            }, 'Invalid ID')
            if id_type is None:
                return msg
            
            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(MediaOrm)
                stmt = self._add_id_where_stmt(stmt, MediaOrm, id_type, id)

                # Get record
                record, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'MediaOrm',
                    'Media not found',
                    'Multiple Medias found'
                )
                if record is None:
                    return msg
                
                if record.path is None:
                    return {
                        'success': False,
                        'message': 'File not found'
                    }
                if os.path.exists(record.path) is False:
                    return {
                        'success': False,
                        'message': 'File not found'
                    }
                
                return FileResponse(
                    path=record.path,
                    filename=record.filename,
                    media_type=record.mime
                )
        
        self.download_record = types.MethodType(download_record, self)



        # Update record
        async def update_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            id: TrpPrimaryKey_MediaID,
            data: UpdateMediaMinimalModel
        ) -> ApiResponseModel:
            if is_empty(id):
                return {
                    'success': False,
                    'message': 'ID missing'
                }
            id_type, id, msg = self._get_id_attr(id, {
                'int': 'id',
                'str': [
                    ('id', RE_INT_VALUE, lambda x: int(x)),
                    ('uuid', RE_UUID4, None),
                    ('cid', RE_XXHASH_CID_HEX, None)
                ]
            }, 'Invalid ID')
            if id_type is None:
                return msg

            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(MediaOrm)
                stmt = self._add_id_where_stmt(stmt, MediaOrm, id_type, id)

                # Get record
                record, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'MediaOrm',
                    'Media not found',
                    'Multiple Medias found'
                )
                if record is None:
                    return msg
                
                # Get data to update
                d = {}
                for f in ['title', 'status', 'reason']:
                    if getattr(data, f) is not None:
                        d[f] = getattr(data, f)
                if len(d) == 0:
                    return {
                        'success': False,
                        'message': 'Nothing to update'
                    }

                # Update record
                try:
                    db_session.query(MediaOrm).\
                        filter(MediaOrm.id == record.id).\
                        update(d)
                    db_session.commit()
                except Exception as e:
                    return {
                        'success': False,
                        'message': str(e)
                    }
            
                # Return success
                return {
                    'success': True,
                    'message': 'Media updated',
                    'payload': {
                        'id': record.id
                    }
                }
        
        self.update_record = types.MethodType(update_record, self)



        # Delete record
        async def delete_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            id: TrpPrimaryKey_MediaID
        ) -> ApiResponseModel:
            if is_empty(id):
                return {
                    'success': False,
                    'message': 'ID missing'
                }
            id_type, id, msg = self._get_id_attr(id, {
                'int': 'id',
                'str': [
                    ('id', RE_INT_VALUE, lambda x: int(x)),
                    ('uuid', RE_UUID4, None),
                    ('cid', RE_XXHASH_CID_HEX, None)
                ]
            }, 'Invalid ID')
            if id_type is None:
                return msg
            
            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(MediaOrm)
                stmt = self._add_id_where_stmt(stmt, MediaOrm, id_type, id)

                # Get record
                record, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'MediaOrm',
                    'Media not found',
                    'Multiple Medias found'
                )
                if record is None:
                    return msg

                file_path = record.path

                # Delete record if found
                try:
                    db_session.execute(
                        sqlalchemy.delete(MediaOrm).\
                            where(MediaOrm.id == record.id)
                    )
                    db_session.commit()
                except Exception as e:
                    return {
                        'success': False,
                        'message': str(e)
                    }
            
                # Remove file if it exists
                if os.path.exists(file_path):
                    os.remove(file_path)

                # Return success
                return {
                    'success': True,
                    'message': 'Media deleted',
                    'payload': {
                        'id': record.id
                    }
                }
        
        self.delete_record = types.MethodType(delete_record, self)



        ### Add routes ###
        router.add_api_route('/media/upload',    self.upload_record,    methods=['POST'])
        router.add_api_route('/media/download/{id}', self.download_record, methods=['GET'])
        router.add_api_route('/medias',          self.get_records,      methods=['GET'])
        router.add_api_route('/media/{id}',      self.get_record,       methods=['GET'])
        router.add_api_route('/media',           self.create_record,    methods=['POST'])
        router.add_api_route('/media/{id}',      self.update_record,    methods=['PUT'])
        router.add_api_route('/media/{id}',      self.delete_record,    methods=['DELETE'])



    # Delete record
    def _get_metadata(self, path: str, filename: str, mime: str) -> dict:
        mime = mime.lower()
        if (
            is_empty(path) or 
            os.path.exists(path) is False or 
            mime not in MIME_TYPES_MAP
        ):
            return {
                'filename': filename,
                'mime': mime,
                'extension': None,
                'size': 0,
                'duration': 0,
                'is_valid': False
            }

        is_timebased = False
        if mime.startswith('audio') or mime.startswith('video'):
            is_timebased = True

        file_stats = os.stat(path)

        if not is_timebased:
            return {
                'filename': filename,
                'mime': mime,
                'extension': MIME_TYPES_MAP[mime],
                'size': file_stats.st_size,
                'duration': 0,
                'is_valid': True
            }
        
        result = None
        try:
            result = subprocess.check_output([
                '/usr/bin/ffprobe',
                '-v',
                'quiet',
                '-print_format',
                'json',
                '-show_format',
                path
            ], text=True)
        except Exception as e:
            return {
                'filename': filename,
                'mime': mime,
                'extension': None,
                'size': file_stats.st_size,
                'duration': 0,
                'is_valid': False
            }

        if not result:
            return {
                'filename': filename,
                'mime': mime,
                'extension': MIME_TYPES_MAP[mime],
                'size': file_stats.st_size,
                'duration': 0,
                'is_valid': False
            }
        
        try:
            result = json.loads(result)
        except Exception as e:
            return {
                'filename': filename,
                'mime': mime,
                'extension': MIME_TYPES_MAP[mime],
                'size': file_stats.st_size,
                'duration': 0,
                'is_valid': False
            }

        return {
            'filename': filename,
            'mime': mime,
            'extension': MIME_TYPES_MAP[mime],
            'size': file_stats.st_size,
            'duration': result['format']['duration'] if 'format' in result and 'duration' in result['format'] else 0,
            'is_valid': True
        }





### Testing ###

if __name__ == '__main__':
    router = MediaRouter()
    print('router:', router)


