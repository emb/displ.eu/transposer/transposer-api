# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import types
import typing
import re
import uuid

from fastapi import (
    Depends
)

import sqlalchemy
from sqlalchemy.orm import Session

from regex_patterns import (
    RE_INT_VALUE,
    RE_UUID4
)
from data_utils import (
    is_empty
)
import trp_config

from ..filter import (
    split_order_fields
)

from ..models.base import (
    ApiResponseModel,
    ApiResponseWithCountModel
)
from ..models.content import (
    ContentOrm,

    ContentModel,
    NewContentModel,
    NewContentStructureModel,
    UpdateContentModel,

    ContentQueryParams,
    ContentQueryParamsPrimary,
    ContentQueryParamsGeneral
)
from ..models.user import (
    UserOrm,
    UserQueryParamsGeneral
)
from ..models.document import (
    DocumentOrm,
    DocumentQueryParamsGeneral
)
from ..models.caption import (
    CaptionOrm,
    CaptionQueryParamsGeneral
)
from ..models.media import (
    MediaOrm,
    MediaQueryParamsGeneral
)
from ..models.request import (
    RequestOrm,
    RequestQueryParamsGeneral
)
from ..models.access_token import (
    AccessTokenOrm,
    AccessTokenQueryParamsGeneral
)
from ..models.bearer_token import (
    BearerTokenOrm,
    BearerTokenQueryParamsGeneral
)
from ..models.peer import (
    PeerOrm,
    PeerQueryParamsGeneral
)
from ..models.peer_connection import (
    PeerConnectionOrm,
    PeerConnectionQueryParamsGeneral
)
from ..models.peer_connection_path import (
    PeerConnectionPathOrm,
    PeerConnectionPathQueryParamsGeneral
)
from ..models.peer_access_token import (
    PeerAccessTokenOrm,
    PeerAccessTokenQueryParamsGeneral
)
from ..models.peer_bearer_token import (
    PeerBearerTokenOrm,
    PeerBearerTokenQueryParamsGeneral
)
from ..models.transcription import (
    TranscriptionOrm,
    TranscriptionQueryParamsGeneral
)
from ..models.transcription_segment import (
    TranscriptionSegmentOrm,
    TranscriptionSegmentQueryParamsGeneral
)
from ..models.transcription_word import (
    TranscriptionWordOrm,
    TranscriptionWordQueryParamsGeneral
)
from ..models.transcription_language_prob import (
    TranscriptionLanguageProbOrm,
    TranscriptionLanguageProbQueryParamsGeneral
)
from ..models.transcription_speech_activity import (
    TranscriptionSpeechActivityOrm,
    TranscriptionSpeechActivityQueryParamsGeneral
)

from ..conditions import (
    add_content_conditions,
    add_user_conditions,
    add_document_conditions,
    add_caption_conditions,
    add_media_conditions,
    add_request_conditions,
    add_access_token_conditions,
    add_bearer_token_conditions,
    add_peer_conditions,
    add_peer_connection_conditions,
    add_peer_access_token_conditions,
    add_peer_bearer_token_conditions,
    add_peer_connection_path_conditions,
    add_transcription_conditions,
    add_transcription_segment_conditions,
    add_transcription_language_prob_conditions,
    add_transcription_speech_activity_conditions,
    add_transcription_word_conditions
)

from .base import AuthBaseRouter





class ContentRouter(AuthBaseRouter):
    def __init__(self,
        db_engine: typing.Any = None,
        app: typing.Any = None,
        router: typing.Any = None,
        config: trp_config.TrpConfig | None = None,
        auth_scheme: typing.Any = None
    ):
        super().__init__(db_engine, app, router, config, auth_scheme)

        print('ContentRouter ... db_engine:', db_engine)
        print('ContentRouter ... app:', app)
        print('ContentRouter ... router:', router)
        print('ContentRouter ... config:', config)
        print('ContentRouter ... auth_scheme:', auth_scheme)

        # Get records
        async def get_records(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            
            content_primary: typing.Annotated[ContentQueryParamsPrimary, Depends(ContentQueryParamsPrimary)],
            content: typing.Annotated[ContentQueryParamsGeneral, Depends(ContentQueryParamsGeneral)],
            
            owner: typing.Annotated[UserQueryParamsGeneral, Depends(UserQueryParamsGeneral)],
            document: typing.Annotated[DocumentQueryParamsGeneral, Depends(DocumentQueryParamsGeneral)],
            caption: typing.Annotated[CaptionQueryParamsGeneral, Depends(CaptionQueryParamsGeneral)],
            media: typing.Annotated[MediaQueryParamsGeneral, Depends(MediaQueryParamsGeneral)],
            request: typing.Annotated[RequestQueryParamsGeneral, Depends(RequestQueryParamsGeneral)],
            
            access_token: typing.Annotated[AccessTokenQueryParamsGeneral, Depends(AccessTokenQueryParamsGeneral)],
            bearer_token: typing.Annotated[BearerTokenQueryParamsGeneral, Depends(BearerTokenQueryParamsGeneral)],
            
            peer: typing.Annotated[PeerQueryParamsGeneral, Depends(PeerQueryParamsGeneral)],
            peer_connection: typing.Annotated[PeerConnectionQueryParamsGeneral, Depends(PeerConnectionQueryParamsGeneral)],
            peer_access_token: typing.Annotated[PeerAccessTokenQueryParamsGeneral, Depends(PeerAccessTokenQueryParamsGeneral)],
            peer_bearer_token: typing.Annotated[PeerBearerTokenQueryParamsGeneral, Depends(PeerBearerTokenQueryParamsGeneral)],
            peer_connection_path: typing.Annotated[PeerConnectionPathQueryParamsGeneral, Depends(PeerConnectionPathQueryParamsGeneral)],

            transcription: typing.Annotated[TranscriptionQueryParamsGeneral, Depends(TranscriptionQueryParamsGeneral)],
            transcription_segment: typing.Annotated[TranscriptionSegmentQueryParamsGeneral, Depends(TranscriptionSegmentQueryParamsGeneral)],
            transcription_language_prob: typing.Annotated[TranscriptionLanguageProbQueryParamsGeneral, Depends(TranscriptionLanguageProbQueryParamsGeneral)],
            transcription_speech_activity: typing.Annotated[TranscriptionSpeechActivityQueryParamsGeneral, Depends(TranscriptionSpeechActivityQueryParamsGeneral)],
            transcription_word: typing.Annotated[TranscriptionWordQueryParamsGeneral, Depends(TranscriptionWordQueryParamsGeneral)],

            limit: int = 0,
            offset: int = 0,
            order: str | None = None,

            use_and: bool = False,

            dataview: str | None = None
        ) -> ApiResponseWithCountModel:
            content = ContentQueryParams(
                content_primary.id if content.id is None else content.id,
                content_primary.created_at if content.created_at is None else content.created_at,
                content_primary.document_id if content.document_id is None else content.document_id,
                content_primary.pos if content.pos is None else content.pos,
                content_primary.lang if content.lang is None else content.lang,
                content_primary.is_original if content.is_original is None else content.is_original,
                content_primary.key if content.key is None else content.key,
                content_primary.value if content.value is None else content.value,
                content_primary.confidence if content.confidence is None else content.confidence,
                content_primary.status if content.status is None else content.status,
                content_primary.reason if content.reason is None else content.reason
            )

            result = None
            with Session(self._db_engine) as db_session:
                # Create count statement
                count_stmt = db_session.query(sqlalchemy.func.count(ContentOrm.id))

                # Create query statement
                stmt = sqlalchemy.select(ContentOrm)

                ## Add conditions
                where = []

                has_join__user = False
                has_join__document = False
                has_join__media = False
                has_join__request = False
                has_join__peer = False
                has_join__peer_connection = False
                has_join__peer_connection_path = False
                has_join__transcription = False
                has_join__transcription_segment = False
                has_join__transcription_word = False
                has_join__transcription_language_prob = False
                has_join__transcription_speech_activity = False

                # Add content conditions
                add_content_conditions(
                    where,
                    content,
                    use_and
                )
                
                # Add caption conditions
                if add_caption_conditions(
                    where,
                    caption,
                    use_and
                ):
                    stmt = stmt.join(ContentOrm.captions, full=True)
                
                # Add document conditions
                if add_document_conditions(
                    where,
                    document,
                    use_and
                ):
                    stmt = stmt.join(ContentOrm.document, full=True)
                    has_join__document = True
                

                # Add user conditions
                if add_user_conditions(
                    where,
                    owner,
                    use_and
                ):
                    if not has_join__document:
                        stmt = stmt.join(ContentOrm.document, full=True)
                        has_join__document = True
                    stmt = stmt.join(DocumentOrm.owner, full=True)
                    has_join__user = True
                
                # Add media conditions
                if add_media_conditions(
                    where,
                    media,
                    use_and
                ):
                    if not has_join__document:
                        stmt = stmt.join(ContentOrm.document, full=True)
                        has_join__document = True
                    stmt = stmt.join(DocumentOrm.medias, full=True)
                    has_join__media = True
                
                # Add request conditions
                if add_request_conditions(
                    where,
                    request,
                    use_and
                ):
                    if not has_join__document:
                        stmt = stmt.join(ContentOrm.document, full=True)
                        has_join__document = True
                    stmt = stmt.join(DocumentOrm.requests, full=True)
                    has_join__request = True
                
                # Add access_token conditions
                if add_access_token_conditions(
                    where,
                    access_token,
                    use_and
                ):
                    if not has_join__document:
                        stmt = stmt.join(ContentOrm.document, full=True)
                        has_join__document = True
                    if not has_join__user:
                        stmt = stmt.join(DocumentOrm.owner, full=True)
                        has_join__user = True
                    stmt = stmt.join(UserOrm.access_tokens, full=True)
                
                # Add bearer_token conditions
                if add_bearer_token_conditions(
                    where,
                    bearer_token,
                    use_and
                ):
                    if not has_join__document:
                        stmt = stmt.join(ContentOrm.document, full=True)
                        has_join__document = True
                    if not has_join__user:
                        stmt = stmt.join(DocumentOrm.owner, full=True)
                        has_join__user = True
                    stmt = stmt.join(UserOrm.bearer_tokens, full=True)


                # Add peer conditions
                if add_peer_conditions(
                    where,
                    peer,
                    use_and
                ):
                    if not has_join__document:
                        stmt = stmt.join(ContentOrm.document, full=True)
                        has_join__document = True
                    if not has_join__user:
                        stmt = stmt.join(DocumentOrm.owner, full=True)
                        has_join__user = True
                    stmt = stmt.join(UserOrm.peers, full=True)
                    has_join__peer = True

                # Add peer_connection conditions
                if add_peer_connection_conditions(
                    where,
                    peer_connection,
                    use_and
                ):
                    if not has_join__document:
                        stmt = stmt.join(ContentOrm.document, full=True)
                        has_join__document = True
                    if not has_join__user:
                        stmt = stmt.join(DocumentOrm.owner, full=True)
                        has_join__user = True
                    if not has_join__peer:
                        stmt = stmt.join(UserOrm.peers, full=True)
                        has_join__peer = True
                    stmt = stmt.join(PeerOrm.connections, full=True)
                    has_join__peer_connection = True

                # Add peer_connection_path conditions
                if add_peer_connection_path_conditions(
                    where,
                    peer_connection_path,
                    use_and
                ):
                    if not has_join__document:
                        stmt = stmt.join(ContentOrm.document, full=True)
                        has_join__document = True
                    if not has_join__user:
                        stmt = stmt.join(DocumentOrm.owner, full=True)
                        has_join__user = True
                    if not has_join__peer:
                        stmt = stmt.join(UserOrm.peers, full=True)
                        has_join__peer = True
                    if not has_join__peer_connection:
                        stmt = stmt.join(PeerOrm.connections, full=True)
                        has_join__peer_connection = True
                    stmt = stmt.join(PeerConnectionOrm.paths, full=True)
                    has_join__peer_connection_path = True

                # Add peer_access_token conditions
                if add_peer_access_token_conditions(
                    where,
                    peer_access_token,
                    use_and
                ):
                    if not has_join__document:
                        stmt = stmt.join(ContentOrm.document, full=True)
                        has_join__document = True
                    if not has_join__user:
                        stmt = stmt.join(DocumentOrm.owner, full=True)
                        has_join__user = True
                    if not has_join__peer:
                        stmt = stmt.join(UserOrm.peers, full=True)
                        has_join__peer = True
                    stmt = stmt.join(PeerOrm.access_tokens, full=True)

                # Add peer_bearer_token conditions
                if add_peer_bearer_token_conditions(
                    where,
                    peer_bearer_token,
                    use_and
                ):
                    if not has_join__document:
                        stmt = stmt.join(ContentOrm.document, full=True)
                        has_join__document = True
                    if not has_join__user:
                        stmt = stmt.join(DocumentOrm.owner, full=True)
                        has_join__user = True
                    if not has_join__peer:
                        stmt = stmt.join(UserOrm.peers, full=True)
                        has_join__peer = True
                    stmt = stmt.join(PeerOrm.bearer_tokens, full=True)


                # Add transcription conditions
                if add_transcription_conditions(
                    where,
                    transcription,
                    use_and
                ):
                    if not has_join__document:
                        stmt = stmt.join(ContentOrm.document, full=True)
                        has_join__document = True
                    if not has_join__media:
                        stmt = stmt.join(DocumentOrm.medias, full=True)
                        has_join__media = True
                    stmt = stmt.join(MediaOrm.transcriptions, full=True)
                    has_join__transcription = True

                # Add transcription_segment conditions
                if add_transcription_segment_conditions(
                    where,
                    transcription_segment,
                    use_and
                ):
                    if not has_join__document:
                        stmt = stmt.join(ContentOrm.document, full=True)
                        has_join__document = True
                    if not has_join__media:
                        stmt = stmt.join(DocumentOrm.medias, full=True)
                        has_join__media = True
                    if not has_join__transcription:
                        stmt = stmt.join(MediaOrm.transcriptions, full=True)
                        has_join__transcription = True
                    stmt = stmt.join(TranscriptionOrm.segments, full=True)
                    has_join__transcription_segment = True

                # Add transcription_word conditions
                if add_transcription_word_conditions(
                    where,
                    transcription_word,
                    use_and
                ):
                    if not has_join__document:
                        stmt = stmt.join(ContentOrm.document, full=True)
                        has_join__document = True
                    if not has_join__media:
                        stmt = stmt.join(DocumentOrm.medias, full=True)
                        has_join__media = True
                    if not has_join__transcription:
                        stmt = stmt.join(MediaOrm.transcriptions, full=True)
                        has_join__transcription = True
                    if not has_join__transcription_segment:
                        stmt = stmt.join(TranscriptionOrm.segments, full=True)
                        has_join__transcription_segment = True
                    stmt = stmt.join(TranscriptionSegmentOrm.words, full=True)
                    has_join__transcription_word = True

                # Add transcription_language_prob conditions
                if add_transcription_language_prob_conditions(
                    where,
                    transcription_language_prob,
                    use_and
                ):
                    if not has_join__document:
                        stmt = stmt.join(ContentOrm.document, full=True)
                        has_join__document = True
                    if not has_join__media:
                        stmt = stmt.join(DocumentOrm.medias, full=True)
                        has_join__media = True
                    if not has_join__transcription:
                        stmt = stmt.join(MediaOrm.transcriptions, full=True)
                        has_join__transcription = True
                    stmt = stmt.join(TranscriptionOrm.language_probs, full=True)
                    has_join__transcription_language_prob = True

                # Add transcription_speech_activity conditions
                if add_transcription_speech_activity_conditions(
                    where,
                    transcription_speech_activity,
                    use_and
                ):
                    if not has_join__document:
                        stmt = stmt.join(ContentOrm.document, full=True)
                        has_join__document = True
                    if not has_join__media:
                        stmt = stmt.join(DocumentOrm.medias, full=True)
                        has_join__media = True
                    if not has_join__transcription:
                        stmt = stmt.join(MediaOrm.transcriptions, full=True)
                        has_join__transcription = True
                    stmt = stmt.join(TranscriptionOrm.speech_activities, full=True)
                    has_join__transcription_speech_activity = True
                

                # Add conditions to statements
                if len(where):
                    cond = sqlalchemy.and_(*where)
                    count_stmt = count_stmt.where(cond)
                    stmt = stmt.where(cond)
                
                # Get total number of records
                num_records_total = count_stmt.scalar()
                if num_records_total == 0:
                    return {
                        'success': False,
                        'message': 'No Contents found'
                    }
                
                # Add limit and offset
                if offset > 0:
                    stmt = stmt.offset(offset)
                if limit > 0:
                    stmt = stmt.limit(limit)

                # Add order
                order = split_order_fields(order, 'content')
                if order is not None:
                    stmt = stmt.order_by(*order)
                
                # Parse dataview
                dataview_struct = self._parse_dataview(dataview)
                
                # Execute statement
                result = []
                for row in db_session.execute(stmt):
                    if row.ContentOrm is None:
                        continue
                    result.append(
                        row.ContentOrm.to_dict(dataview_struct)
                    )
            
            # If we didn't find any records
            # return an error message
            if result is None:
                return {
                    'success': False,
                    'message': 'No Contents found'
                }
            num_records = len(result)
            if num_records == 0:
                return {
                    'success': False,
                    'message': 'No Contents found'
                }
            
            # Return the result
            return {
                'success': True,
                'message': 'Contents found',
                'total': num_records_total,
                'count': num_records,
                'payload': result
            }

        self.get_records = types.MethodType(get_records, self)



        # Get record
        async def get_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            id: int,
            dataview: str | None = None
        ) -> ApiResponseModel:
            # Get key and extract value
            if is_empty(id):
                return {
                    'success': False,
                    'message': 'ID missing'
                }
            
            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(ContentOrm).\
                    where(ContentOrm.id == id)

                # Get record
                record, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'ContentOrm',
                    'Content not found',
                    'Multiple Contents found'
                )
                if record is None:
                    return msg
                
                # Parse and filter
                dataview_struct = self._parse_dataview(dataview)
                record_dict = record.to_dict(dataview_struct)
                
                # Return the result
                return {
                    'success': True,
                    'message': 'Content found',
                    'payload': record_dict
                }
        
        self.get_record = types.MethodType(get_record, self)



        # Create record
        async def create_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            data: NewContentStructureModel
        ) -> ApiResponseModel:
            # Get parent key and value
            parent_id = data.document
            if parent_id is None:
                return {
                    'success': False,
                    'message': 'Document ID missing'
                }
            parent_id_type, parent_id, msg = self._get_id_attr(parent_id, {
                'int': 'id',
                'str': [
                    ('id', RE_INT_VALUE, lambda x: int(x)),
                    ('uuid', RE_UUID4, None)
                ]
            }, 'Invalid Document ID')
            if parent_id_type is None:
                return msg

            parent = None
            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(DocumentOrm)
                stmt = self._add_id_where_stmt(stmt, DocumentOrm, parent_id_type, parent_id)
                
                # Get parent record
                parent, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'DocumentOrm',
                    'Document not found',
                    'Multiple Documents found'
                )
                if parent is None:
                    return msg
                
                # Get next position
                pos = 0
                if data.pos is None:
                    stmt = sqlalchemy.select(ContentOrm).\
                        where(ContentOrm.document_id == parent.id).\
                        order_by(ContentOrm.pos.desc())
                    row = db_session.execute(stmt).fetchone()
                    if row is not None and row.ContentOrm is not None:
                        pos = row.ContentOrm.pos + 1
                else:
                    pos = data.pos

                # Create record
                try:
                    record = ContentOrm(
                        document_id=parent.id,
                        pos=pos,
                        lang=data.lang,
                        is_original=data.is_original,
                        key=data.key,
                        value=data.value,
                        confidence=(1.0 if data.confidence < 0 and data.is_original else data.confidence),
                        status=(3 if data.is_original else 1)
                    )
                    try:
                        if isinstance(data.start, (int, float)) and data.start >= 0:
                            caption = CaptionOrm(
                                start=data.start,
                                end=(data.end if isinstance(data.end, (int, float)) and data.end > 0 else -1.0)
                            )
                            record.captions.append(caption)
                    except:
                        pass
                    try:
                        if isinstance(data.captions, list):
                            for c in data.captions:
                                caption = CaptionOrm(
                                    start=c.start,
                                    end=(c.end if isinstance(c.end, (int, float)) and c.end > 0 else -1.0)
                                )
                                record.captions.append(caption)
                    except:
                        pass
                    
                    db_session.add(record)
                    db_session.commit()
                except Exception as e:
                    return {
                        'success': False,
                        'message': str(e)
                    }
            
                # Return success
                return {
                    'success': True,
                    'message': 'Content created',
                    'payload': {
                        'id': record.id
                    }
                }
        
        self.create_record = types.MethodType(create_record, self)



        # Update record
        async def update_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            id: int,
            data: UpdateContentModel
        ) -> ApiResponseModel:
            if is_empty(id):
                return {
                    'success': False,
                    'message': 'ID missing'
                }
            
            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(ContentOrm).\
                    where(ContentOrm.id == id)

                # Get record
                record, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'ContentOrm',
                    'Content not found',
                    'Multiple Contents found'
                )
                if record is None:
                    return msg
                
                # Get data to update
                d = {}
                for f in ['pos', 'lang', 'is_original', 'key', 'value', 'confidence', 'status', 'reason']:
                    if getattr(data, f) is not None:
                        d[f] = getattr(data, f)
                if len(d) == 0:
                    return {
                        'success': False,
                        'message': 'Nothing to update'
                    }

                # Update record
                try:
                    db_session.query(ContentOrm).\
                        filter(ContentOrm.id == record.id).\
                        update(d)
                    db_session.commit()
                except Exception as e:
                    return {
                        'success': False,
                        'message': str(e)
                    }
            
                # Return success
                return {
                    'success': True,
                    'message': 'Content updated',
                    'payload': {
                        'id': record.id
                    }
                }
        
        self.update_record = types.MethodType(update_record, self)



        # Delete record
        async def delete_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            id: int
        ) -> ApiResponseModel:
            if is_empty(id):
                return {
                    'success': False,
                    'message': 'ID missing'
                }

            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(ContentOrm).\
                    where(ContentOrm.id == id)

                # Get record
                record, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'ContentOrm',
                    'Content not found',
                    'Multiple Contents found'
                )
                if record is None:
                    return msg
                
                # Delete record if found
                try:
                    db_session.execute(
                        sqlalchemy.delete(ContentOrm).\
                            where(ContentOrm.id == record.id)
                    )
                    db_session.commit()
                except Exception as e:
                    return {
                        'success': False,
                        'message': str(e)
                    }
            
                # Return success
                return {
                    'success': True,
                    'message': 'Content deleted',
                    'payload': {
                        'id': record.id
                    }
                }
        
        self.delete_record = types.MethodType(delete_record, self)



        ### Add routes ###
        router.add_api_route('/document/contents',          self.get_records,      methods=['GET'])
        router.add_api_route('/document/content/{id}',      self.get_record,       methods=['GET'])
        router.add_api_route('/document/content',           self.create_record,    methods=['POST'])
        router.add_api_route('/document/content/{id}',      self.update_record,    methods=['PUT'])
        router.add_api_route('/document/content/{id}',      self.delete_record,    methods=['DELETE'])





### Testing ###

if __name__ == '__main__':
    router = ContentRouter()
    print('router:', router)


