# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import types
import typing
import re
import uuid

from fastapi import (
    Depends
)

import sqlalchemy
from sqlalchemy.orm import Session

from regex_patterns import (
    RE_INT_VALUE,
    RE_UUID4,
    RE_HASH_XX,
    RE_XXHASH_CID_HEX
)
from data_utils import (
    is_empty,
    validate_uri as validate_uri_func
)

import trp_config

from ..filter import (
    split_order_fields
)

from ..models.base import (
    ApiResponseModel,
    ApiResponseWithCountModel
)
from ..models.pipeline_consumer_option import (
    PipelineConsumerOptionOrm,
    
    PipelineConsumerOptionModel,
    NewPipelineConsumerOptionModel,
    UpdatePipelineConsumerOptionModel,

    PipelineConsumerOptionQueryParams,
    PipelineConsumerOptionQueryParamsPrimary,
    PipelineConsumerOptionQueryParamsGeneral
)
from ..models.pipeline_consumer import (
    PipelineConsumerOrm,
    PipelineConsumerQueryParamsGeneral
)

from ..conditions import (
    add_pipeline_consumer_option_conditions,
    add_pipeline_consumer_conditions
)

from .base import AuthBaseRouter





class PipelineConsumerOptionRouter(AuthBaseRouter):
    def __init__(self,
        db_engine: typing.Any = None,
        app: typing.Any = None,
        router: typing.Any = None,
        config: trp_config.TrpConfig | None = None,
        auth_scheme: typing.Any = None
    ):
        super().__init__(db_engine, app, router, config, auth_scheme)

        print('PipelineConsumerOptionRouter ... db_engine:', db_engine)
        print('PipelineConsumerOptionRouter ... app:', app)
        print('PipelineConsumerOptionRouter ... router:', router)
        print('PipelineConsumerOptionRouter ... config:', config)
        print('PipelineConsumerOptionRouter ... auth_scheme:', auth_scheme)

        # Get records
        async def get_records(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],

            pipeline_consumer_option_primary: typing.Annotated[PipelineConsumerOptionQueryParamsPrimary, Depends(PipelineConsumerOptionQueryParamsPrimary)],
            pipeline_consumer_option: typing.Annotated[PipelineConsumerOptionQueryParamsGeneral, Depends(PipelineConsumerOptionQueryParamsGeneral)],

            pipeline_consumer: typing.Annotated[PipelineConsumerQueryParamsGeneral, Depends(PipelineConsumerQueryParamsGeneral)],
            
            limit: int = 0,
            offset: int = 0,
            order: str | None = None,

            use_and: bool = False,

            dataview: str | None = None
        ) -> ApiResponseWithCountModel:
            pipeline_consumer_option = PipelineConsumerOptionQueryParams(
                pipeline_consumer_option_primary.id if pipeline_consumer_option.id is None else pipeline_consumer_option.id,
                pipeline_consumer_option_primary.parent_id if pipeline_consumer_option.parent_id is None else pipeline_consumer_option.parent_id,
                pipeline_consumer_option_primary.key if pipeline_consumer_option.key is None else pipeline_consumer_option.key,
                pipeline_consumer_option_primary.value if pipeline_consumer_option.value is None else pipeline_consumer_option.value,
                pipeline_consumer_option_primary.type if pipeline_consumer_option.type is None else pipeline_consumer_option.type
            )

            result = None
            with Session(self._db_engine) as db_session:
                # Create count statement
                count_stmt = db_session.query(sqlalchemy.func.count(PipelineConsumerOptionOrm.id))

                # Create query statement
                stmt = sqlalchemy.select(PipelineConsumerOptionOrm)

                ## Add conditions
                where = []

                # Add pipeline_consumer_option conditions
                add_pipeline_consumer_option_conditions(
                    where,
                    pipeline_consumer_option,
                    use_and
                )

                # Add pipeline_consumer conditions
                if add_pipeline_consumer_conditions(
                    where,
                    pipeline_consumer,
                    use_and
                ):
                    stmt = stmt.join(PipelineConsumerOptionOrm.parent, full=True)
                

                # Add conditions to statements
                if len(where):
                    cond = sqlalchemy.and_(*where)
                    count_stmt = count_stmt.where(cond)
                    stmt = stmt.where(cond)
                
                # Get total number of records
                num_records_total = count_stmt.scalar()
                if num_records_total == 0:
                    return {
                        'success': False,
                        'message': 'No PipelineConsumerOptions found'
                    }
                
                # Add limit and offset
                if offset > 0:
                    stmt = stmt.offset(offset)
                if limit > 0:
                    stmt = stmt.limit(limit)

                # Add order
                order = split_order_fields(order, 'pipeline_consumer_option')
                if order is not None:
                    stmt = stmt.order_by(*order)
                
                # Parse dataview
                dataview_struct = self._parse_dataview(dataview)
                
                # Execute statement
                result = []
                for row in db_session.execute(stmt):
                    if row.PipelineConsumerOptionOrm is None:
                        continue
                    result.append(
                        row.PipelineConsumerOptionOrm.to_dict(dataview_struct)
                    )
            
            # If we didn't find any records
            # return an error message
            if result is None:
                return {
                    'success': False,
                    'message': 'No PipelineConsumerOptions found'
                }
            num_records = len(result)
            if num_records == 0:
                return {
                    'success': False,
                    'message': 'No PipelineConsumerOptions found'
                }
            
            # Return the result
            return {
                'success': True,
                'message': 'PipelineConsumerOptions found',
                'total': num_records_total,
                'count': num_records,
                'payload': result
            }

        self.get_records = types.MethodType(get_records, self)



        # Get record
        async def get_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            id: int,
            dataview: str | None = None
        ) -> ApiResponseModel:
            # Get key and extract value
            if is_empty(id):
                return {
                    'success': False,
                    'message': 'ID missing'
                }
            
            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(PipelineConsumerOptionOrm).\
                    where(PipelineConsumerOptionOrm.id == id)

                # Get record
                record, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'PipelineConsumerOptionOrm',
                    'PipelineConsumerOption not found',
                    'Multiple PipelineConsumerOptions found'
                )
                if record is None:
                    return msg
                
                # Parse and filter
                dataview_struct = self._parse_dataview(dataview)
                record_dict = record.to_dict(dataview_struct)
                
                # Return the result
                return {
                    'success': True,
                    'message': 'PipelineConsumerOption found',
                    'payload': record_dict
                }
        
        self.get_record = types.MethodType(get_record, self)



        # Create record
        async def create_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            data: NewPipelineConsumerOptionModel
        ) -> ApiResponseModel:
            # Get parent key and value
            parent_id = data.parent
            if parent_id is None:
                return {
                    'success': False,
                    'message': 'PipelineConsumer ID missing'
                }
            parent_id_type, parent_id, msg = self._get_id_attr(parent_id, {
                'int': 'id',
                'str': [
                    ('id', RE_INT_VALUE, lambda x: int(x)),
                    ('uuid', RE_UUID4, None)
                ]
            }, 'Invalid PipelineConsumer ID')
            if parent_id_type is None:
                return msg

            parent = None
            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(PipelineConsumerOrm)
                stmt = self._add_id_where_stmt(stmt, PipelineConsumerOrm, parent_id_type, parent_id)
                
                # Get parent record
                parent, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'PipelineConsumerOrm',
                    'PipelineConsumer not found',
                    'Multiple PipelineConsumers found'
                )
                if parent is None:
                    return msg

                # Create record
                try:
                    record = PipelineConsumerOptionOrm(
                        parent_id=parent.id,
                        key=data.key,
                        value=data.value,
                        type=(data.type if data.type else 0)
                    )
                    db_session.add(record)
                    db_session.commit()
                except Exception as e:
                    return {
                        'success': False,
                        'message': str(e)
                    }
            
                # Return success
                return {
                    'success': True,
                    'message': 'PipelineConsumerOption created',
                    'payload': {
                        'id': record.id
                    }
                }
        
        self.create_record = types.MethodType(create_record, self)



        # Update record
        async def update_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            id: int,
            data: UpdatePipelineConsumerOptionModel
        ) -> ApiResponseModel:
            if is_empty(id):
                return {
                    'success': False,
                    'message': 'ID missing'
                }
            
            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(PipelineConsumerOptionOrm).\
                    where(PipelineConsumerOptionOrm.id == id)

                # Get record
                record, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'PipelineConsumerOptionOrm',
                    'PipelineConsumerOption not found',
                    'Multiple PipelineConsumerOptions found'
                )
                if record is None:
                    return msg
                
                # Get data to update
                d = {}
                for f in ['value', 'type']:
                    if getattr(data, f) is not None:
                        d[f] = getattr(data, f)
                if len(d) == 0:
                    return {
                        'success': False,
                        'message': 'Nothing to update'
                    }

                # Update record
                try:
                    db_session.query(PipelineConsumerOptionOrm).\
                        filter(PipelineConsumerOptionOrm.id == record.id).\
                        update(d)
                    db_session.commit()
                except Exception as e:
                    return {
                        'success': False,
                        'message': str(e)
                    }
            
                # Return success
                return {
                    'success': True,
                    'message': 'PipelineConsumerOption updated',
                    'payload': {
                        'id': record.id
                    }
                }
        
        self.update_record = types.MethodType(update_record, self)



        # Delete record
        async def delete_record(
            self,
            #token: typing.Annotated[str, Depends(auth_scheme)],
            id: int
        ) -> ApiResponseModel:
            if is_empty(id):
                return {
                    'success': False,
                    'message': 'ID missing'
                }

            record = None
            with Session(self._db_engine) as db_session:
                # Create statement
                stmt = sqlalchemy.select(PipelineConsumerOptionOrm).\
                    where(PipelineConsumerOptionOrm.id == id)

                # Get record
                record, msg = self._get_single_record(
                    db_session.execute(stmt),
                    'PipelineConsumerOptionOrm',
                    'PipelineConsumerOption not found',
                    'Multiple PipelineConsumerOptions found'
                )
                if record is None:
                    return msg
                
                # Delete record if found
                try:
                    db_session.execute(
                        sqlalchemy.delete(PipelineConsumerOptionOrm).\
                            where(PipelineConsumerOptionOrm.id == record.id)
                    )
                    db_session.commit()
                except Exception as e:
                    return {
                        'success': False,
                        'message': str(e)
                    }
            
                # Return success
                return {
                    'success': True,
                    'message': 'PipelineConsumerOption deleted',
                    'payload': {
                        'id': record.id
                    }
                }
        
        self.delete_record = types.MethodType(delete_record, self)



        ### Add routes ###
        router.add_api_route('/pipeline/consumer/options',          self.get_records,      methods=['GET'])
        router.add_api_route('/pipeline/consumer/option/{id}',      self.get_record,       methods=['GET'])
        router.add_api_route('/pipeline/consumer/option',           self.create_record,    methods=['POST'])
        router.add_api_route('/pipeline/consumer/option/{id}',      self.update_record,    methods=['PUT'])
        router.add_api_route('/pipeline/consumer/option/{id}',      self.delete_record,    methods=['DELETE'])





### Testing ###

if __name__ == '__main__':
    router = PipelineConsumerOptionRouter()
    print('router:', router)


