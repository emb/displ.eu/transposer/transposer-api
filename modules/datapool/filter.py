# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import typing

import sqlalchemy

from regex_patterns import (
    RE_SPLIT_TO_LIST,
    RE_ORDER_FIELD_NAME
)
from .maps import (
    FIELDS_MAPS, VARIANTS_MAPS
)





### Filters

def split_order_fields(
    value: str | None,
    type: str
) -> list:
    if not value:
        return []
    if type not in FIELDS_MAPS:
        return []
    value = value.strip()
    if not value:
        return []
    value = RE_SPLIT_TO_LIST.split(value)
    tmp = []
    for t in value:
        result = RE_ORDER_FIELD_NAME.match(t)
        if not result:
            continue
        key = result.group(1)
        if key not in FIELDS_MAPS[type]:
            continue
        if result.group(2) == '.desc':
            tmp.append(FIELDS_MAPS[type][key].desc())
            continue
        tmp.append(FIELDS_MAPS[type][key])
    
    if len(tmp) == 0:
        return None
    return tmp



# Create filter
def create_filter__render_elem(
    field: typing.Any,
    value: typing.Any,
    comparator: str
) -> typing.Any:
    if comparator == '==':
        return field == value
    if comparator == '!=':
        return field != value
    if comparator == '>':
        return field > value
    if comparator == '<':
        return field < value
    if comparator == '>=':
        return field >= value
    if comparator == '<=':
        return field <= value
    if comparator == 'like':
        return field.ilike(value)
    return None


def create_filter__render_tuple(
    field: typing.Any,
    values: tuple | None
) -> typing.Any:
    if not values:
        return None
    num_grps = int(len(values) * 0.5)
    if num_grps == 0:
        return None
    if num_grps == 1:
        return create_filter__render_elem(
            field,
            values[0],
            values[1]
        )
    container = []
    for i in range(num_grps):
        start_i = i * 2
        elem = create_filter__render_elem(
            field,
            values[start_i],
            values[start_i + 1]
        )
        if elem is None:
            continue
        container.append(elem)
    num_grps = len(container)
    if num_grps == 0:
        return None
    if num_grps == 1:
        return container[0]
    return sqlalchemy.and_(*container)


def create_filter(
    field: typing.Any,
    values: list | tuple | None,
    use_and: bool = False
) -> typing.Any:
    if not values:
        return None
    if isinstance(values, tuple):
        return create_filter__render_tuple(field, values)
    if not isinstance(values, list):
        return None
    num_elems = len(values)
    if num_elems == 0:
        return None
    container = []
    for elem in values:
        elem = create_filter__render_tuple(field, elem)
        if elem is None:
            continue
        container.append(elem)
    num_grps = len(container)
    if num_grps == 0:
        return None
    if num_grps == 1:
        return container[0]
    if use_and:
        return sqlalchemy.and_(*container)
    return sqlalchemy.or_(*container)



