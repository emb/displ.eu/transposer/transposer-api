# ========================================================================================
# 
# Copyright 2023 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import os
import typing

from fastapi.templating import Jinja2Templates



# Setup Jinja templates
def setup_fastapi_jinja_templates(
    container: typing.Any,
    suffix: str = '',
    base: str = 'templates/html'
):
    config = container.config
    project_dir = ''
    if config.has('appdirs') and config.get('appdirs').has('project'):
        project_dir = config.get('appdirs').get('project')
    templates_dir = os.path.join(
        project_dir,
        base
    )
    t_dir = os.path.join(
        templates_dir,
        suffix
    )
    if os.path.isdir(t_dir):
        templates_dir = t_dir
    if config.has('html') and config.get('html').has('templates_dir'):
        t_dir = config.get('html').get('templates_dir')
        if t_dir.startswith('~'):
            t_dir = os.path.expanduser(t_dir)
        if not t_dir.startswith('/'):
            t_dir = os.path.join(
                project_dir,
                t_dir
            )
        if os.path.isdir(t_dir):
            templates_dir = t_dir
    
    container.templates = Jinja2Templates(directory=templates_dir)

