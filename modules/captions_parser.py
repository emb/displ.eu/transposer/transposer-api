# ========================================================================================
# 
# Copyright 2024 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import typing
import datetime
import re



RE_INT_VALUE = re.compile(r'^\s*(\d+)\s*$', re.I)
RE_FLOAT_VALUE = re.compile(r'^\s*(\d+(?:\.\d+)?)\s*$', re.I)

RE_FLEX_TIME_SSXX = re.compile(r'^\s*([0-9]+)[\.,]([0-9]+)\s*$', re.I)
RE_FLEX_TIME_MMSSXX = re.compile(r'^\s*([0-9]+):([0-9]{2})[\.,]([0-9]+)\s*$', re.I)
RE_FLEX_TIME_HHMMSSXX = re.compile(r'^\s*([0-9]+):([0-9]{2}):([0-9]{2})[\.,]([0-9]+)\s*$', re.I)


RE_HEAD_BASE = re.compile(r'^WEBVTT', re.I)
RE_HEAD_W_CONTENT = re.compile(r'^WEBVTT\s+(?:-\s+)?(\S+.*)$', re.I)

RE_NOTE_BASE = re.compile(r'^NOTE', re.I)
RE_NOTE_W_CONTENT = re.compile(r'^NOTE\s+(\S+.*)$', re.I)

RE_CUE_TIME_BASE = re.compile(r'^((?:[0-9]{2}[\.,][0-9]{3})|(?:[1-9][0-9]{2,}[\.,][0-9]{3})|(?:[0-9]{2}:[0-9]{2}[\.,][0-9]{3})|(?:[1-9][0-9]{2,}:[0-9]{2}[\.,][0-9]{3})|(?:[0-9]{2}:[0-9]{2}:[0-9]{2}[\.,][0-9]{3})|(?:[1-9][0-9]{2,}:[0-9]{2}:[0-9]{2}[\.,][0-9]{3}))\s+-->\s+((?:[0-9]{2}[\.,][0-9]{3})|(?:[1-9][0-9]{2,}[\.,][0-9]{3})|(?:[0-9]{2}:[0-9]{2}[\.,][0-9]{3})|(?:[1-9][0-9]{2,}:[0-9]{2}[\.,][0-9]{3})|(?:[0-9]{2}:[0-9]{2}:[0-9]{2}[\.,][0-9]{3})|(?:[1-9][0-9]{2,}:[0-9]{2}:[0-9]{2}[\.,][0-9]{3}))', re.I)
RE_CUE_TIME_W_CONTENT = re.compile(r'^((?:[0-9]{2}[\.,][0-9]{3})|(?:[1-9][0-9]{2,}[\.,][0-9]{3})|(?:[0-9]{2}:[0-9]{2}[\.,][0-9]{3})|(?:[1-9][0-9]{2,}:[0-9]{2}[\.,][0-9]{3})|(?:[0-9]{2}:[0-9]{2}:[0-9]{2}[\.,][0-9]{3})|(?:[1-9][0-9]{2,}:[0-9]{2}:[0-9]{2}[\.,][0-9]{3}))\s+-->\s+((?:[0-9]{2}[\.,][0-9]{3})|(?:[1-9][0-9]{2,}[\.,][0-9]{3})|(?:[0-9]{2}:[0-9]{2}[\.,][0-9]{3})|(?:[1-9][0-9]{2,}:[0-9]{2}[\.,][0-9]{3})|(?:[0-9]{2}:[0-9]{2}:[0-9]{2}[\.,][0-9]{3})|(?:[1-9][0-9]{2,}:[0-9]{2}:[0-9]{2}[\.,][0-9]{3}))\s+(\S+.*)\s*$', re.I)

RE_CONTENT = re.compile(r'^(\S+.*)\s*$', re.I)
RE_LINE = re.compile(r'^-\s+(\S+.*)\s*$', re.I)

RE_STYLE_BASE = re.compile(r'^STYLE', re.I)

RE_NEWLINE = re.compile(r'[\n\r]|\n\r|\r\n', re.I)







# Convert a time value to an integer, representing seconds
def _time_to_int(value: typing.Any, positions: int = 12) -> typing.Any:
    """
    Convert a time value to an integer, representing seconds

    Args:
        value (Any): The value to convert

    Returns:
        Any: The converted value
    """

    if isinstance(value, int):
        # Already an integer
        return value
    
    multiply = int('1'.ljust(positions + 1, '0')) if positions else 1

    if isinstance(value, float):
        # Convert float to int
        # We assume that the float value represents seconds
        return int(value * multiply)

    if isinstance(value, datetime.time):
        # Value is time object
        if multiply > 1:
            return (
                (value.hour * 3600 * multiply) +
                (value.minute * 60 * multiply) +
                (value.second * multiply) +
                int(str(value.microsecond).ljust(positions, '0')[0:positions])
            )
        return (
            (value.hour * 3600) +
            (value.minute * 60) +
            value.second
        )
    
    if not isinstance(value, str):
        # If value is not a string, it is not convertible
        raise Exception('Not convertible value')
    
    # Test the value against several regular expressions

    # INT
    result = RE_INT_VALUE.match(value)
    if result:
        return int(result.group(1))
    
    # FLOAT
    result = RE_FLOAT_VALUE.match(value)
    if result:
        return int(float(result.group(1)) * multiply)

    # HH:MM:SS.xxx
    result = RE_FLEX_TIME_HHMMSSXX.match(value)
    if result:
        if multiply > 1:
            return (
                (int(result.group(1)) * 3600 * multiply) + 
                (int(result.group(2)) * 60 * multiply) + 
                (int(result.group(3)) * multiply) + 
                int(result.group(4).ljust(positions, '0')[0:positions])
            )
        return (
            (int(result.group(1)) * 3600) + 
            (int(result.group(2)) * 60) + 
            int(result.group(3))
        )
    
    # MM:SS.xxx
    result = RE_FLEX_TIME_MMSSXX.match(value)
    if result:
        if multiply > 1:
            return (
                (int(result.group(1)) * 60 * multiply) + 
                (int(result.group(2)) * multiply) + 
                int(result.group(3).ljust(positions, '0')[0:positions])
            )
        return (
            (int(result.group(1)) * 60) + 
            int(result.group(2))
        )
    
    # SS.xxx
    result = RE_FLEX_TIME_SSXX.match(value)
    if result:
        if multiply > 1:
            return (
                (int(result.group(1)) * multiply) + 
                int(result.group(2).ljust(positions, '0')[0:positions])
            )
        return (
            int(result.group(1))
        )
    
    # Invalid value
    raise Exception('Invalid value')


# Convert a time value to an integer, representing milliseconds
def _time_to_millisecs(value: typing.Any) -> typing.Any:
    """
    Convert a time value to an integer, representing milliseconds

    Args:
        value (Any): The value to convert

    Returns:
        Any: The converted value
    """
    return _time_to_int(value, 3)



# Convert a milliseconds integer time value to string
def _msec_int_to_str(value: int, type: str = 'webvtt') -> str | None:
    """
    Convert a milliseconds time value to a string

    Args:
        value (int): The value to convert

    Returns:
        str: The converted value
    """

    if not isinstance(value, int):
        # Already an integer
        return None
    
    msec = value % 1000
    value = int(value / 1000)
    sec = value % 60
    value = int(value / 60)
    min = value % 60
    hour = int(value / 60)

    msec = str(msec).rjust(3, '0')
    sec = str(sec).rjust(2, '0')
    min = str(min).rjust(2, '0')
    hour = str(hour).rjust(2, '0')
    msec_sep = ',' if type == 'srt' else '.'

    return f'{hour}:{min}:{sec}{msec_sep}{msec}'







###### Captions Parser ######

def _parse_cue_section(container: dict, lines: list):
    content = {}
    content_by_type = {
        'content': [],
        'lines': [],
    }

    # Get the first line
    line = lines.pop(0)

    # Check for time
    result = RE_CUE_TIME_BASE.match(line)
    if not result:
        # Pattern didn't match time, so we interpret
        # the line as an identifier
        content['identifier'] = line

        # Get the next line and check for time again
        line = lines.pop(0)
        result = RE_CUE_TIME_BASE.match(line)

    # If we do not find a time, we leave
    if not result:
        return
    # If there are no lines left, we leave
    if len(lines) == 0:
        return

    # Add time to content
    content['time'] = {
        'start':_time_to_millisecs(result.group(1)),
        'end':_time_to_millisecs(result.group(2))
    }
    # Test if time has properties
    result = RE_CUE_TIME_W_CONTENT.match(line)
    if result:
        content['properties'] = result.group(3)

    # Parse the rest of the lines
    for line in lines:
        # Check for line
        result = RE_LINE.match(line)
        if result:
            content_by_type['lines'].append(result.group(1))
            continue

        # Check for content
        result = RE_CONTENT.match(line)
        if result:
            content_by_type['content'].append(result.group(1))
    
    content['content'] = {
        'block': "\n".join(content_by_type['content']),
        'lines': content_by_type['lines']
    }

    # Add the content to the container
    container['has_cues'] = True
    content['type'] = 'cue'
    content['pos'] = len(container['content'])
    container['content'].append(content)



def _parse_note_section(container: dict, lines: list):
    content = []

    # Get the first line, which contains 'NOTE'
    line = lines.pop(0)
    # Test if that line has additional content
    result = RE_NOTE_W_CONTENT.match(line)
    if result:
        content.append(result.group(1))
    
    # If no lines are left,
    # or next line is None,
    # we leave
    if len(lines) == 0 or lines[0] is None:
        return

    # Test for content in next line
    result = RE_CONTENT.match(lines[0])
    # Iterate ove the lines and extract content
    while len(lines) > 0 and result:
        lines.pop(0)
        content.append(result.group(1))
        result = RE_CONTENT.match(lines[0])
    
    # If no content was found, we leave
    if len(content) == 0:
        return
    
    # Add the content to the container
    container['content'].append({
        'type': 'note',
        'pos': len(container['content']),
        'content': "\n".join(content)
    })



def _parse_style_section(container: dict, lines: list):
    content = []

    # Remove the first line, it only contains 'STYLE'
    lines.pop(0)

    # Iterate ove the lines and extract content
    while len(lines) > 0 and lines[0] is not None:
        line = lines.pop(0)
        content.append(line)
    
    # If no content was found, we leave
    if len(content) == 0:
        return
    
    # Add the content to the container
    container['content'].append({
        'type': 'style',
        'pos': len(container['content']),
        'content': "\n".join(content)
    })



def _parse_section(container: dict, lines: list):
    # If no lines are left, we leave
    if len(lines) == 0:
        return
    
    # Clean up start
    while lines[0] is None:
        lines.pop(0)

    # If no lines are left, we leave
    if len(lines) == 0:
        return
    
    # Check for notes
    if RE_NOTE_BASE.match(lines[0]):
        _parse_note_section(container, lines)
        return

    # Check for styles
    if RE_STYLE_BASE.match(lines[0]):
        _parse_style_section(container, lines)
        return

    # Check for cues
    cue_lines = []
    is_cue = False
    for line in lines:
        if line is None:
            break
        cue_lines.append(line)
        if RE_CUE_TIME_BASE.match(line):
            is_cue = True
    if is_cue:
        for i in range(len(cue_lines)):
            lines.pop(0)
        _parse_cue_section(container, cue_lines)
        return

    # Add untyped content
    content = [lines.pop(0)]
    while len(lines) > 0 and lines[0] is not None:
        content.append(lines.pop(0))
    container['content'].append({
        'type': 'untyped',
        'pos': len(container['content']),
        'content': "\n".join(content)
    })



################################################################

def parse_captions(content: str | list) -> dict:
    """Parse captions content into a dictionary.

    Args:
        content (str | list[str]): The captions content
    
    Returns:
        dict: The parsed content.
    """


    print('parse_captions ... content:', content, type(content))


    # Split data into lines
    lines = []
    if not isinstance(content, list):
        content = [content]
    for item in content:
        for line in RE_NEWLINE.split(item):
            line = line.strip()
            if len(line) == 0:
                lines.append(None)
                continue
            lines.append(line)

    print('parse_captions ... lines:', lines, type(lines))

    # Clean up start and end
    while lines[0] is None:
        lines.pop(0)
    while lines[-1] is None:
        lines.pop()

    # Check for empty content
    if len(lines) == 0:
        return None

    # Initialize the container
    container = {
        'type': 'srt',
        'head': None,
        'content': [],
        'has_cues': False
    }

    # Check for head
    if RE_HEAD_BASE.match(lines[0]):
        container['type'] = 'webvtt'
        line = lines.pop(0)
        result = RE_HEAD_W_CONTENT.match(line)
        if result:
            container['head'] = result.group(1)

    # Parse the sections
    while len(lines) > 0:
        _parse_section(container, lines)

    # If we didn't find any cues, return None
    if not container['has_cues']:
        return None
    
    # Return the container
    return {
        'type': container['type'],
        'head': container['head'],
        'content': container['content'],
    }





def build_captions(content: dict, type: str = 'webvtt') -> str:
    """Build WebVTT or SRT string from data dictionary.

    Args:
        content (dict): The data dictionary.
    
    Returns:
        str: The WebVTT or SRT content.
    """

    container = []

    # If type is 'webvtt', we add the header
    if type == 'webvtt':
        cont = 'WEBVTT'
        if 'head' in content and content['head']:
            cont += ' - ' + content['head']
        container.append(cont)
        container.append('')

    # Iterate over the content
    for item in content['content']:
        if item['type'] == 'cue':
            if 'time' not in item:
                continue
            if 'start' not in item['time'] or 'end' not in item['time']:
                continue

            # Convert time to string
            start = _msec_int_to_str(item['time']['start'], type)
            end = _msec_int_to_str(item['time']['end'], type)
            if start is None or end is None:
                continue

            # Add identifier
            if 'identifier' in item and item['identifier']:
                container.append(item['identifier'])
            
            # Add time
            cont = f'{start} --> {end}'
            # Add properties
            if 'properties' in item and item['properties']:
                cont += ' ' + item['properties']
            container.append(cont)

            # Add content
            if 'content' in item:
                # Add content block
                if 'block' in item['content'] and item['content']['block']:
                    container.append(item['content']['block'])
                # Add content lines
                if 'lines' in item['content'] and isinstance(item['content']['lines'], list):
                    for line in item['content']['lines']:
                        container.append(f"- {line}")
            
            # Add empty line
            container.append('')
        
        elif item['type'] == 'note':
            if "\n" in item['content']:
                # Add nultiline note
                container.append('NOTE')
                container.append(item['content'])
            else:
                # Add single line note
                container.append(f"NOTE {item['content']}")
            # Add empty line
            container.append('')
        
        elif item['type'] == 'style':
            # Add style
            container.append('STYLE')
            container.append(item['content'])
            # Add empty line
            container.append('')
        
        elif item['type'] == 'untyped':
            # Add untyped content
            container.append(item['content'])
            # Add empty line
            container.append('')

    if len(container) == 0:
        return None
    if container[-1] == '':
        container.pop()
    return "\n".join(container)







################################################################
# Testing
#
if __name__ == '__main__':
    
    print("------------------------------------------------------------------")

    result = parse_captions("""
WEBVTT

04:02.500 --> 04:05.000
J'ai commencé le basket à l'âge de 13, 14 ans

04:05.001 --> 04:07.800
Sur les <i.foreignphrase><lang en>playground</lang></i>, ici à Montpellier
""")
    print(result)


    print("------------------------------------------------------------------")


    result = parse_captions("""WEBVTT

STYLE
::cue {
  background-image: linear-gradient(to bottom, dimgray, lightgray);
  color: papayawhip;
}
/* Style blocks cannot use blank lines nor "dash dash greater than" */

NOTE comment blocks can be used between style blocks.

STYLE
::cue(b) {
  color: peachpuff;
}

00:00:00.000 --> 00:00:10.000
- Hello <b>world</b>.

NOTE style blocks cannot appear after the first cue.""")
    print(result)


    print("------------------------------------------------------------------")


    result = parse_captions("""

WEBVTT - Some header content

1
00:00.000 --> 00:02.000
That's an, an, that's an L!

crédit de transcription
00:04.000 --> 00:05.000 line:0 position:20% size:60% align:start
Transcrit par Célestes™
                 
""")
    print(result)


    print("------------------------------------------------------------------")


    result = parse_captions("""
1
00:02:16,612 --> 00:02:19,376
Senator, we're making
our final approach into Coruscant.

2
00:02:19,482 --> 00:02:21,609
Very good, Lieutenant.

3
00:03:13,336 --> 00:03:15,167
We made it.

4
00:03:18,608 --> 00:03:20,371
I guess I was wrong.

5
00:03:20,476 --> 00:03:22,671
There was no danger at all.
""")
    print(result)


    print("------------------------------------------------------------------")


    result = build_captions({
        'head': None,
        'content': [
            {
                'time': {
                    'start': 242500,
                    'end': 245000
                },
                'content': {
                    'block': "J'ai commencé le basket à l'âge de 13, 14 ans",
                    'lines': []
                },
                'type': 'cue'#,
                #'pos': 0
            },
            {
                'time': {
                    'start': 245001,
                    'end': 247800
                },
                'content': {
                    'block': 'Sur les <i.foreignphrase><lang en>playground</lang></i>, ici à Montpellier',
                    'lines': []
                },
                'type': 'cue'#,
                #'pos': 1
            }
        ]
    })
    print(result)


    print("------------------------------------------------------------------")


    result = build_captions({
        'head': None,
        'content': [
            {
                'type': 'style',
                #'pos': 0,
                'content': '::cue {\nbackground-image: linear-gradient(to bottom, dimgray, lightgray);\ncolor: papayawhip;\n}\n/* Style blocks cannot use blank lines nor "dash dash greater than" */'
            }, {
                'type': 'style',
                #'pos': 1,
                'content': '::cue(b) {\ncolor: peachpuff;\n}'
            }, {
                'time': {
                    'start': 0,
                    'end': 10000
                },
                'content': {
                    'block': '',
                    'lines': ['Hello <b>world</b>.']
                },
                'type': 'cue'#,
                #'pos': 2
            }
        ]
    }, 'webvtt')
    print(result)


    print("------------------------------------------------------------------")


    result = build_captions({
        'head': 'Some header content',
        'content': [{
            'identifier': '1',
            'time': {
                'start': 0,
                'end': 2000
            },
            'content': {
                'block': "That's an, an, that's an L!",
                'lines': []
            },
            'type': 'cue'#,
            #'pos': 0
        }, {
            'identifier': 'crédit de transcription',
            'time': {
                'start': 4000,
                'end': 5000
            },
            'properties': 'line:0 position:20% size:60% align:start',
            'content': {
                'block': 'Transcrit par Célestes™',
                'lines': []
            },
            'type': 'cue'#,
            #'pos': 1
        }]
    })
    print(result)


    print("------------------------------------------------------------------")


    result = build_captions({
        'head': None,
        'content': [{
            'identifier': '1',
            'time': {
                'start': 136612,
                'end': 139376
            },
            'content': {
                'block': "Senator, we're making\nour final approach into Coruscant.",
                'lines': []
            },
            'type': 'cue'#,
            #'pos': 0
        }, {
            'identifier': '2',
            'time': {
                'start': 139482,
                'end': 141609
            },
            'content': {
                'block': 'Very good, Lieutenant.',
                'lines': []
            },
            'type': 'cue'#,
            #'pos': 1
        }, {
            'identifier': '3',
            'time': {
                'start': 193336,
                'end': 195167
            },
            'content': {
                'block': 'We made it.',
                'lines': []
            },
            'type': 'cue'#,
            #'pos': 2
        }, {
            'identifier': '4',
            'time': {
                'start': 198608,
                'end': 200371
            },
            'content': {
                'block': 'I guess I was wrong.',
                'lines': []
            },
            'type': 'cue'#,
            #'pos': 3
        }, {
            'identifier': '5',
            'time': {
                'start': 200476,
                'end': 202671
            },
            'content': {
                'block': 'There was no danger at all.',
                'lines': []
            },
            'type': 'cue'#,
            #'pos': 4
        }]
    }, 'srt')
    print(result)


