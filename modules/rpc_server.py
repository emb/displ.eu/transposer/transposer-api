# ========================================================================================
# 
# Copyright 2023 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import os
import sys
import types
import typing

import logging

import json
import inspect
import importlib

import asyncio
from functools import wraps, partial
from pydantic import BaseModel



# ========================================================================================

# API classes

class RpcRequestItem(BaseModel):
    method: str
    args: list | None = None
    kwargs: dict | None = None


class RpcDataRequestItem(BaseModel):
    id: str


class RpcResponseItem(BaseModel):
    success: bool = False
    message: str | None = None
    payload: typing.Any = None



# Server class

class RpcServer:
    __slots__ = [
        'paths',
        'modules',
        'methods',
        'classes',
        'constants'
    ]
    
    def __init__(self, paths: list | None = None) -> None:
        """Initialize the RPC server
            
            Args:
                paths (list | None): List of paths to search for modules.
            
            Returns:
                None
        """
        self.paths = paths
        
        self.modules = {}
        self.methods = {
            'echo': self.echo,
            'ping': self.ping
        }
        self.classes = {}
        self.constants = {
            'hello': 'world'
        }
        
        if not isinstance(paths, list):
            logging.error('Paths missing')
            return
        
        for p in paths:
            for root, dirs, files in os.walk(p):
                if '__pycache__' in dirs:
                    dirs.remove('__pycache__') # don't visit __pycache__ directories
                
                for f in files:
                    if f.startswith('__'):
                        continue
                    if f.startswith('.'):
                        continue
                    if not f.endswith('.py'):
                        continue
                    
                    module_name = f.replace('.py', '')
                    module_file = os.path.join(root, f)
                    package_name = root.replace(p, '')
                    if package_name.startswith('/'):
                        package_name = package_name[1:]
                    
                    module_path = os.path.join(package_name, module_name)
                    module_path = module_path.replace('/', '.')
                    
                    module = importlib.import_module(module_path)

                    self.modules[module_name] = {
                        'name': module_name,
                        'full_name': module_name,
                        'path': module_file,
                        'module': module,
                        'package': None
                    }
                    obj = self.modules[module_name]
                    
                    member_methods = {}
                    member_classes = {}
                    member_constants = {}
                    exports = None
                    
                    for member in inspect.getmembers(module):
                        member_name = member[0]
                        member_item = member[1]
                        if member_name.startswith('__package__'):
                            obj['package'] = member_item
                            continue
                        if member_name.startswith('__'):
                            continue
                        if member_name.startswith('_'):
                            continue
                        if inspect.ismodule(member_item):
                            continue
                        
                        if member_name == 'export':
                            exports = member_item
                            continue
                        
                        if not callable(member_item):
                            member_constants[member_name] = {
                                'name': member_name,
                                'path': f'{module_name}.{member_name}',
                                'full_path': f'{module_path}.{member_name}',
                                'item': member_item
                            }
                            continue
                        
                        if inspect.isfunction(member_item):
                            if inspect.iscoroutinefunction(member_item):
                                member_methods[member_name] = {
                                    'name': member_name,
                                    'path': f'{module_name}.{member_name}',
                                    'full_path': f'{module_path}.{member_name}',
                                    'item': member_item
                                }
                                continue
                            
                            member_methods[member_name] = {
                                'name': member_name,
                                'path': f'{module_name}.{member_name}',
                                'full_path': f'{module_path}.{member_name}',
                                'item': self._async_wrap(member_item)
                            }
                            continue
                        
                        if inspect.isclass(member_item):
                            member_classes[member_name] = {
                                'name': member_name,
                                'path': f'{module_name}.{member_name}',
                                'full_path': f'{module_path}.{member_name}',
                                'item': member_item
                            }
                            
                            for cls_member in inspect.getmembers(member_item):
                                cls_member_name = cls_member[0]
                                cls_member_item = cls_member[1]
                                if cls_member_name.startswith('__'):
                                    continue
                                if cls_member_name.startswith('_'):
                                    continue
                                
                                if not callable(cls_member_item):
                                    member_constants[f'{member_name}.{cls_member_name}'] = {
                                        'name': cls_member_name,
                                        'path': f'{module_name}.{member_name}.{cls_member_name}',
                                        'full_path': f'{module_path}.{member_name}.{cls_member_name}',
                                        'item': cls_member_item
                                    }
                                    continue
                                
                                if inspect.isfunction(cls_member_item):
                                    if inspect.iscoroutinefunction(cls_member_item):
                                        member_methods[f'{member_name}.{cls_member_name}'] = {
                                            'name': cls_member_name,
                                            'path': f'{module_name}.{member_name}.{cls_member_name}',
                                            'full_path': f'{module_path}.{member_name}.{cls_member_name}',
                                            'item': cls_member_item
                                        }
                                        continue
                                    
                                    member_methods[f'{member_name}.{cls_member_name}'] = {
                                        'name': cls_member_name,
                                        'path': f'{module_name}.{member_name}.{cls_member_name}',
                                        'full_path': f'{module_path}.{member_name}.{cls_member_name}',
                                        'item': self._async_wrap(cls_member_item)
                                    }
                                    continue
                                
                                if inspect.isclass(cls_member_item):
                                    member_classes[f'{member_name}.{cls_member_name}'] = {
                                        'name': cls_member_name,
                                        'path': f'{module_name}.{member_name}.{cls_member_name}',
                                        'full_path': f'{module_path}.{member_name}.{cls_member_name}',
                                        'item': cls_member_item
                                    }
                    
                    obj['methods'] = member_methods
                    obj['classes'] = member_classes
                    obj['constants'] = member_constants
                    
                    # 'exports' is not defined, so we export everything
                    if exports is None:
                        for item in member_methods.values():
                            self.methods[item['full_path']] = item['item']
                        
                        for item in member_classes.values():
                            self.classes[item['full_path']] = item['item']
                        
                        for item in member_constants.values():
                            self.constants[item['full_path']] = item['item']
                        
                        continue
                    
                    # 'exports' is defined, so we export only the items listed in it.
                    # Items of missing export-keys, or if key is None, are exported.
                    # If export-key is defined, but empty, nothing is exported.
                    # If export-key is defined, and not empty, only the items listed
                    # in it are exported.
                    for t in [
                        ('methods', member_methods, self.methods),
                        ('classes', member_classes, self.classes),
                        ('constants', member_constants, self.constants)
                    ]:
                        t_name = t[0]
                        t_items = t[1]
                        t_container = t[2]
                        if t_name not in exports or not isinstance(exports[t_name], list):
                            for item in t_items.values():
                                t_container[item['full_path']] = item['item']
                            continue
                        
                        for k, item in t_items.items():
                            if k in exports[t_name]:
                                t_container[item['full_path']] = item['item']


    def _async_wrap(self, func):
        """Wrap a sync function into an async function
            
            This is a decorator which wraps a synchronous function into an
            asynchronous function. The synchronous function will be executed
            in a separate thread pool.
            
            Args:
                func: The synchronous function to wrap.
            
            Returns:
                The asynchronous function.
        """
        @wraps(func)
        async def run(*args, loop=None, executor=None, **kwargs):
            if loop is None:
                loop = asyncio.get_event_loop()
            p_func = partial(func, *args, **kwargs)
            return await loop.run_in_executor(executor, p_func)
        return run
    
    
    async def call(self, item: RpcRequestItem) -> RpcResponseItem:
        """Call a method
            
            Args:
                item (RpcRequestItem): The request item.
            
            Returns:
                RpcResponseItem: The response item.
        """
        if item.method not in self.methods:
            err_msg = f"Method '{item.method}' not found"
            logging.error(err_msg)
            return RpcResponseItem(
                success=False,
                message=err_msg
            )
        
        args = item.args if item.args is not None else []
        kwargs = item.kwargs if item.kwargs is not None else {}
        
        try:
            response = await self.methods[item.method](*args, **kwargs)
        except Exception as err:
            # Send back exception if called function throws an error
            err_msg = json.dumps(str(err))
            logging.error(err_msg)
            return RpcResponseItem(
                success=False,
                message=err_msg
            )
        
        if isinstance(response, RpcResponseItem):
            return response
        
        if response is None or isinstance(response, (str, int, float, bool)):
            return RpcResponseItem(
                success=True,
                payload=response
            )
        try:
            return RpcResponseItem(
                success=True,
                payload=json.dumps(response)
            )
        except Exception as err:
            # Send back exception if response is not serializable
            err_msg = json.dumps(str(err))
            logging.error(err_msg)
            return RpcResponseItem(
                success=False,
                message=err_msg
            )
    
    
    async def get(self, id: str | None = None) -> RpcResponseItem:
        """Get a constant
            
            Args:
                item (str | None): The request item.
            
            Returns:
                RpcResponseItem: The response item.
        """
        if id is None:
            err_msg = f"Data id missing"
            logging.error(err_msg)
            return RpcResponseItem(
                success=False,
                message=err_msg
            )
        if id not in self.constants:
            err_msg = f"Data '{id}' not found"
            logging.error(err_msg)
            return RpcResponseItem(
                success=False,
                message=err_msg
            )
        
        try:
            response = self.constants[id]
        except Exception as err:
            # Send back exception if called function throws an error
            err_msg = json.dumps(str(err))
            logging.error(err_msg)
            return RpcResponseItem(
                success=False,
                message=err_msg
            )
        
        if isinstance(response, RpcResponseItem):
            return response
        
        if response is None or isinstance(response, (str, int, float, bool)):
            return RpcResponseItem(
                success=True,
                payload=response
            )
        try:
            return RpcResponseItem(
                success=True,
                payload=json.dumps(response)
            )
        except Exception as err:
            # Send back exception if response is not serializable
            err_msg = json.dumps(str(err))
            logging.error(err_msg)
            return RpcResponseItem(
                success=False,
                message=err_msg
            )
    
    
    async def echo(self, msg: typing.Any = None) -> RpcResponseItem:
        """Echo a message
            
            Args:
                msg (typing.Any): The message to echo.
            
            Returns:
                RpcResponseItem: The response item.
        """
        return RpcResponseItem(
            success=True,
            payload=msg
        )
    
    
    async def ping(self) -> RpcResponseItem:
        """Ping/Pong
            
            Returns:
                RpcResponseItem: The response item.
        """
        return RpcResponseItem(
            success=True
        )


