#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ========================================================================================
# 
# Copyright 2023 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import os
import sys
import types
import typing

import logging
import getopt
import time
import datetime
import uuid

import re
import json

import csv


from utils import (
    RE_TRUE,
    RE_FALSE,
    RE_NONE,
    RE_NEGATED,
    RE_NUMBER,
    RE_INT,
    RE_FLOAT,
    RE_PRIMITIVE,
    RE_FRACTION,
    RE_SPLIT_FRACTION,
    RE_JSON_OBJECT_START,
    RE_JSON_OBJECT_END,
    RE_JSON_ARRAY_START,
    RE_JSON_ARRAY_END,
    RE_COMMA_SEPARATOR,
    RE_COMMA_TRAILING,
    RE_NEWLINE_SINGLE,
    RE_NEWLINE_MULTI,
    RE_WHITESPACE,
    RE_WHITESPACE_START,
    RE_WHITESPACE_END,
    
    UTILS_FUNCTION_MAP,
    
    to_int,
    to_float,
    to_abs,
    to_list,
    to_msecs,
    frames_to_msecs,
    is_empty,
    str_is_empty,
    to_date,
    to_time,
    to_datetime,
    date_to_str,
    time_to_str,
    datetime_to_str
)

from trp_config import TrpConfig


RE_CSV = re.compile(r'\.csv$', re.I)

TRANSFORMERS_MAP = {
    'string': lambda val: str(val),
    'int': lambda val: to_int(val),
    'float': lambda val: to_float(val),
    'duration': lambda val: to_msecs(val),
    'to_msecs': lambda val: to_msecs(val),
    'frames_to_msecs': lambda val: frames_to_msecs(val),
    'list': lambda val: to_list(val),
    'date': lambda val: to_date(val),
    'time': lambda val: to_time(val),
    'datetime': lambda val, fmt = None: to_datetime(val, fmt if fmt else '%Y-%m-%d %H:%M:%S'),
    'abs': lambda val: to_abs(val)
}


class StatementTransformer:
    def __init__(self, config: TrpConfig | str | None = None) -> None:
        self.config = None
        if isinstance(config, TrpConfig):
            self.config = config
        elif isinstance(config, str):
            if os.path.isdir(config):
                cnf_file = os.path.join(config, 'statement_transformer.ini')
                if not os.path.isfile(cnf_file):
                    cnf_file = os.path.join(config, 'etc', 'statement_transformer.ini')
                    if not os.path.isfile(cnf_file):
                        cnf_file = os.path.join(config, 'config', 'statement_transformer.ini')
                        if not os.path.isfile(cnf_file):
                            raise Exception('No config provided')
                self.config = TrpConfig({
                    'defaults': {
                        'dir': config,
                        'file': 'statement_transformer.defaults.ini'
                    },
                    'files': [cnf_file]
                })
            elif os.path.isfile(config):
                self.config = TrpConfig({
                    'files': [config]
                })
        
        if not self.config:
            raise Exception('No config provided')
        
        #print('config:', self.config)
        #print('config.sections:', self.config.sections)
        #print('config.get(mapping):', self.config.get('mapping'))
        #print('config.get(mapping).options:', self.config.get('mapping').options)
        
        self.target_dir = self.config.get('data').get('target_dir', '/tmp')
        self.delimiter = self.config.get('data').get('delimiter', ';')
        self.quote_char = self.config.get('data').get('quote_char', '"')
        self.encoding = self.config.get('data').get('encoding', 'utf-8')
        self.first_contains_names = self.config.get('data').get('first_contains_names', True)
        
        self.mapping_type = self.config.get('mapping').get('type', 'columns')
        self.mappings = self.config.get('mapping').get('items', [])
        self.num_mappings = len(self.mappings)
        self.target_names = self.config.get('mapping').get('target_names', [])
        
        if self.num_mappings == 0:
            raise Exception('No input mappings provided')
        if len(self.target_names) == 0:
            raise Exception('No target mappings provided')
        
        self.mappings_dict = {}
        self.mappings_names = []
        for mapping in self.mappings:
            self.mappings_dict[mapping['name']] = mapping
            self.mappings_names.append(mapping['name'])

    
    def transform(self, src: str, trg: str | None = None):
        if not src:
            raise Exception('No source file provided')
        
        src_head, src_tail = os.path.split(src)
        if not src_tail:
            raise Exception('No source file provided')
        
        if not RE_CSV.search(src_tail):
            raise Exception('Wrong format. Only CSV files are supported')
        
        src_tail_body, src_tail_ext = os.path.splitext(src_tail)
        
        trg_file_name = f'{src_tail_body}.transformed{src_tail_ext}'
        trg_file_path = os.path.join(src_head, trg_file_name)
        
        container = None
        with open(
            os.path.join(src_head, src_tail),
            'r',
            newline='', encoding=self.encoding
        ) as fh:
            reader = csv.reader(fh, delimiter=self.delimiter, quotechar=self.quote_char)
            row_num = 0
            for row in reader:
                if self.first_contains_names and row_num == 0:
                    row_num += 1
                    continue
                
                cache_row = []
                cache_dict = {}
                col_num = 0
                for col in row:
                    if col_num >= self.num_mappings:
                        continue
                    
                    mapping = self.mappings[col_num]
                    col_num += 1
                    
                    result = self._transform_value(col, mapping)
                    obj = {
                        'value': result,
                        'mapping': mapping
                    }
                    cache_row.append(obj)
                    cache_dict[mapping['name']] = obj
                
                
                
                result_container = {}
                col_num = 0
                for col in cache_row:
                    val = col['value']
                    mapping = col['mapping']
                    col_num += 1
                    
                    if 'target' not in mapping or not mapping['target']:
                        continue
                    
                    target_name = mapping['target']
                    
                    if isinstance(target_name, str):
                        result_container[target_name] = self._get_value_with_default(val, mapping, cache_dict)
                        continue
                    
                    if isinstance(target_name, list):
                        for target in target_name:
                            t_name = target['name']
                            if 'condition' in target and target['condition']:
                                if target['condition'] not in UTILS_FUNCTION_MAP:
                                    result_container[t_name] = self._get_value_with_default('', mapping, cache_dict)
                                    continue
                                
                                if not UTILS_FUNCTION_MAP[target['condition']](val):
                                    result_container[t_name] = self._get_value_with_default('', mapping, cache_dict)
                                    continue
                            
                            if (
                                'transform' in target and
                                target['transform'] and
                                target['transform'] in TRANSFORMERS_MAP
                            ):
                                val = TRANSFORMERS_MAP[target['transform']](val)
                            
                            result_container[t_name] = self._get_value_with_default(val, mapping, cache_dict)
                        continue
                
                row_container = []
                for n in self.target_names:
                    if n in result_container:
                        row_container.append(result_container[n])
                    else:
                        row_container.append(None)
                
                print('result_row:', row_container)
    
    
    
    def _transform_value(self, val, format):
        p_type = format['type']
        if p_type == 'datetime':
            return datetime_to_str(
                to_datetime(val),
                format['format'] if 'format' in format and format['format'] else '%Y-%m-%d %H:%M:%S'
            )
        if p_type == 'date':
            return date_to_str(
                to_date(val),
                format['format'] if 'format' in format and format['format'] else '%Y-%m-%d'
            )
        if p_type == 'time':
            return time_to_str(
                to_time(val),
                format['format'] if 'format' in format and format['format'] else '%H:%M:%S'
            )
        if p_type in TRANSFORMERS_MAP:
            return TRANSFORMERS_MAP[p_type](val)
        
        return val
    
    
    def _get_value_with_default(self, val, mapping, cache_dict):
        if val is not None and val != '':
            return val
        
        if not isinstance(mapping['default'], list):
            return mapping['default']
        
        for d in mapping['default']:
            if isinstance(d, dict):
                if (
                    'ref' not in d or
                    d['ref'] not in cache_dict
                ):
                    continue
                c_val = cache_dict[d['ref']]['value']
                if is_empty(c_val):
                    continue
                
                return c_val
            
            return d

        return val




#########
# Testing
#
if __name__ == '__main__':
    
    transformer = StatementTransformer(
        '/home/bx/Workspace/fairkom/frappe/ERPNext/StatementTransformer'
    )
    
    transformer.transform(
        '/home/bx/Workspace/fairkom/frappe/ERPNext/umsaetze-girokonto_AT204571000351001042_EUR_2023-11-07_13-11-08.csv'
    )


