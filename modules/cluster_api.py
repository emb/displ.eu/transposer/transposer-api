# ========================================================================================
# 
# Copyright 2023 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import os
import sys
import types
import typing

import logging

import asyncio
from fastapi import FastAPI, HTTPException
from pydantic import BaseModel

import trp_config


# Response models

class ActivityItem(BaseModel):
    success: bool = True
    message: str | None = None
    payload: bool = False


class ServersItemPayload(BaseModel):
    active: bool = False
    servers: list[str] = []

class ServersItem(BaseModel):
    success: bool = False
    message: str | None = None
    payload: ServersItemPayload | None = None



# Test health of cluster

class ClusterApi:
    __slots__ = [
        'config',
        'active_brokers_data_path'
    ]
    
    def __init__(self, config: trp_config.TrpConfig) -> None:
        """Initialize ClusterApi
            
            Args:
                config (trp_config.TrpConfig): Configuration
        """
        self.config = config
        self.active_brokers_data_path = os.getenv(
            'ACTIVE_BROKERS_DATA_PATH',
            config.get('data').get(
                'active_brokers_data_path',
                'active_brokers.txt'
            )
        )
    
    
    async def get_brokers(self) -> ServersItem:
        """Get list of active brokers
        
            Returns:
                ServersItem: List of active brokers
        """
        if not os.path.isfile(self.active_brokers_data_path):
            logging.error('File not found')
            #raise HTTPException(status_code=404, detail="File not found")
            return ServersItem(
                success=False,
                message='File not found'
            )
        
        with open(self.active_brokers_data_path, 'r') as file:
            brokers = file.read().splitlines()
            return ServersItem(
                success=True,
                payload=ServersItemPayload(
                    active=len(brokers) > 0,
                    servers=brokers
                )
            )
    
    
    async def is_active(self) -> ActivityItem:
        """Test if cluster is active
            
            Returns:
                ActivityItem: Cluster status
        """
        try:
            result = await self.get_brokers()
            if not result.success:
                return ActivityItem(
                    success=False,
                    message=result.message
                )
            return ActivityItem(
                payload=result.payload.active
            )
        except HTTPException as err:
            return ActivityItem(
                success=False,
                message=err.detail
            )


