#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ========================================================================================
# 
# Copyright 2023 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================
# FastAPI based API for the Transposer Cluster

import os
import sys

BIN_DIR		         = os.path.dirname(os.path.realpath(__file__))
PROJECT_DIR	         = os.path.realpath(os.path.join(BIN_DIR, '../'))
CONFIG_DIR	         = os.path.realpath(os.path.join(PROJECT_DIR, 'etc'))
MODULES_DIR	         = os.path.realpath(os.path.join(PROJECT_DIR, 'modules'))
METHODS_DIR	         = os.path.realpath(os.path.join(PROJECT_DIR, 'methods'))
ROUTERS_DIR	         = os.path.realpath(os.path.join(PROJECT_DIR, 'routers'))
LOG_DIR		         = os.path.realpath(os.path.join(PROJECT_DIR, 'var/log'))
WORK_DIR	         = os.path.realpath(os.path.join(PROJECT_DIR, 'var/run'))
TMP_DIR	             = os.path.realpath(os.path.join(PROJECT_DIR, 'var/tmp'))

SHARED_LIB_DIR    = os.path.realpath(os.path.join(PROJECT_DIR, '../shared-lib'))
SHARED_MODULES_DIR   = os.path.realpath(os.path.join(SHARED_LIB_DIR, 'modules'))
SHARED_METHODS_DIR   = os.path.realpath(os.path.join(SHARED_LIB_DIR, 'methods'))
SHARED_ROUTERS_DIR   = os.path.realpath(os.path.join(SHARED_LIB_DIR, 'routers'))

CUSTOM_METHODS_DIRS = []
CUSTOM_ROUTERS_DIRS = []

# Add sub directories of a given path to the search path.
# Necessary to allow easy install of modules, adapters, etc.
# by simply cloning them into their respective target dirs.
# Only directories that are directly inside 'path', that do
# NOT contain an __init__.py file (thus are not packages),
# are added.
def add_sub_search_dirs(path: str, container: list | None = None) -> None:
    for root, dirs, files in os.walk(path):
        for d in dirs:
            if d == 'tests':
                continue
            if d.startswith('.'):
                continue
            if d.startswith('_'):
                continue
            s_path = os.path.join(root, d)
            for s_root, s_dirs, s_files in os.walk(s_path):
                if '__init__.py' in s_files:
                    break
                sys.path.insert(0, s_path)
                if isinstance(container, list):
                    container.append(s_path)
                break
        break

# Add a list of paths to the search path.
def add_search_dirs(paths: list, methods_container: list | None = None, routers_container: list | None = None) -> None:
    for modules_path, methods_path, routers_path in paths:
        if os.path.isdir(modules_path):
            sys.path.insert(0, modules_path)
            add_sub_search_dirs(modules_path)
        if os.path.isdir(methods_path):
            sys.path.insert(0, methods_path)
            methods_container.append(methods_path)
            add_sub_search_dirs(methods_path, methods_container)
        if os.path.isdir(routers_path):
            sys.path.insert(0, routers_path)
            routers_container.append(routers_path)
            add_sub_search_dirs(routers_path, routers_container)

# Add search paths
add_search_dirs([
    (MODULES_DIR, METHODS_DIR, ROUTERS_DIR),
    (SHARED_MODULES_DIR, SHARED_METHODS_DIR, SHARED_ROUTERS_DIR),
    ('/var/lib/transposer/modules', '/var/lib/transposer/methods', '/var/lib/transposer/routers'),
    (
        os.path.expanduser('~/.local/lib/transposer/modules'),
        os.path.expanduser('~/.local/lib/transposer/methods'),
        os.path.expanduser('~/.local/lib/transposer/routers')
    )
], CUSTOM_METHODS_DIRS, CUSTOM_ROUTERS_DIRS)


#print('sys.path:', sys.path)


import types
import typing
import shutil
import importlib

import logging
import getopt

import re
import json
from pathlib import Path
from dotenv import load_dotenv

import asyncio
from fastapi import (
    Cookie,
    Depends,
    FastAPI,
    HTTPException,
    Query,
    Request,
    Security,
    WebSocket,
    WebSocketException,
    WebSocketDisconnect,
    status
)
from fastapi.security import OAuth2PasswordBearer
from fastapi.responses import HTMLResponse
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates

from fastapi_third_party_auth import (
    Auth,
    GrantType,
    KeycloakIDToken
)

from pydantic import BaseModel
import uvicorn

import trp_config
from utils import (
    RE_TRUE,
    RE_FALSE,
    RE_NONE,
    RE_NEGATED,
    RE_NUMBER,
    RE_INT,
    RE_FLOAT,
    RE_PRIMITIVE,
    RE_FRACTION,
    RE_SPLIT_FRACTION,
    RE_JSON_OBJECT_START,
    RE_JSON_OBJECT_END,
    RE_JSON_ARRAY_START,
    RE_JSON_ARRAY_END,
    RE_COMMA_SEPARATOR,
    RE_COMMA_TRAILING,
    RE_NEWLINE_SINGLE,
    RE_NEWLINE_MULTI,
    RE_WHITESPACE,
    RE_WHITESPACE_START,
    RE_WHITESPACE_END,
    RE_STARTSWITH_SLASH,
    RE_STARTSWITH_TILDE,
    
    get_uuid,
    is_true
)

#from datapool import *



### Help
def show_help():
    print("""Usage: cluster_api [OPTIONS]

-h  --help             Print this help
-c  --config           Path to config file
""")
    sys.exit(0)





# Get CLI options
getopt_types = {
    '-h': {
        'name': 'help',
        'convert': lambda val: True
    },
    '--help': {
        'name': 'help',
        'convert': lambda val: True
    },
    
    '-c': {
        'name': 'config',
        'convert': lambda val: val if isinstance(val, str) else None
    },
    '--config': {
        'name': 'config',
        'convert': lambda val: val if isinstance(val, str) else None
    }
}

try:
    opts, args = getopt.getopt(sys.argv[1:], 'hc:', [
        'help',
        'config='
    ])
except getopt.GetoptError:
    show_help()

opts = { getopt_types[x[0]]['name']:getopt_types[x[0]]['convert'](x[1]) for x in opts };
if 'help' in opts:
    show_help()

# Load .env
dotenv_path = os.path.join(PROJECT_DIR, '.env')
if os.path.isfile(dotenv_path):
    load_dotenv(dotenv_path)

# Load config

# Add all 'ini' files inside sub directories ending in '.d'
def add_sub_config_files(path: str, container: list | None = None) -> None:
    for root, dirs, files in os.walk(path):
        for f in files:
            if not f.endswith('.ini'):
                continue
            if f.startswith('.'):
                continue
            if f.startswith('_'):
                continue
            f_path = os.path.join(root, f)
            container.append(f_path)
        break

config_name = os.getenv('TRANSPOSER_API__CONFIG_NAME', 'config')
config_file = os.getenv('TRANSPOSER_API__CONFIG', None)

# Walk config directories and add all 'ini' files.
# Walk sub directories ending in '.d' and add all 'ini' files
# in there as well.
def add_config_files(paths: list, container: list | None = None) -> None:
    for c_path, c_dir in paths:
        if os.path.isfile(c_path):
            container.append(c_path)
        if os.path.isdir(c_dir):
            add_sub_config_files(c_dir, container)

# Get config dirs
config_dirs = []
tmp = [
    CONFIG_DIR,
    os.path.join(CONFIG_DIR, 'config.ini.d'),
    '/etc/transposer/transposer-api',
    f'/etc/transposer/transposer-api/{config_name}.ini.d',
    os.path.expanduser(f'~/.config/transposer/transposer-api'),
    os.path.expanduser(f'~/.config/transposer/transposer-api/{config_name}.ini.d')
]
for p in tmp:
    if os.path.isdir(p):
        config_dirs.append(p)

# Add configs
config_files = []
tmp = [
    (os.path.join(CONFIG_DIR, 'config.ini'),
        os.path.join(CONFIG_DIR, 'config.ini.d')),
    (f'/etc/transposer/transposer-api/{config_name}.ini',
        f'/etc/transposer/transposer-api/{config_name}.ini.d'),
    (os.path.expanduser(f'~/.config/transposer/transposer-api/{config_name}.ini'),
        os.path.expanduser(f'~/.config/transposer/transposer-api/{config_name}.ini.d'))
]
if 'config' in opts and opts['config']:
    tmp.append((opts['config'], f'{opts["config"]}.d'))
if config_file:
    tmp.append((config_file, f'{config_file}.d'))
add_config_files(tmp, config_files)

#print('CONFIG_DIR:', CONFIG_DIR)
#print('config_dirs:', config_dirs)

# Load configs
config = trp_config.TrpConfig({
    'defaults': {
        'dir': CONFIG_DIR,
        'file': 'defaults.ini',
        'data': {
            'appdirs': {
                'bin': BIN_DIR,
                'project': PROJECT_DIR,
                'config': CONFIG_DIR,
                'modules': MODULES_DIR,
                'methods': METHODS_DIR,
                'routers': ROUTERS_DIR,
                'log': LOG_DIR,
                'work': WORK_DIR,
                'tmp': TMP_DIR,
                
                'shared_project': SHARED_LIB_DIR,
                'shared_modules': SHARED_MODULES_DIR,
                'shared_methods': SHARED_METHODS_DIR,
                'shared_routers': SHARED_ROUTERS_DIR,
                
                'methods_dirs': json.dumps(CUSTOM_METHODS_DIRS),
                'routers_dirs': json.dumps(CUSTOM_ROUTERS_DIRS),
                
                'config_dirs': json.dumps(config_dirs)
            }
        }
    },
    'files': config_files
}, True, 'TRANSPOSER_API')
    
# Additional methods search paths
if config.has('methods') and config.get('methods').has('search_paths'):
    s_paths = config.get('methods').get('search_paths')
    if isinstance(s_paths, str):
        s_paths = [s_paths]
    s_paths.reverse()
    for p in s_paths:
        if os.path.isdir(p):
            sys.path.insert(0, p)
            CUSTOM_METHODS_DIRS.append(p)
            add_sub_search_dirs(p, CUSTOM_METHODS_DIRS)
    
# Additional routers search paths
if config.has('routers') and config.get('routers').has('search_paths'):
    s_paths = config.get('routers').get('search_paths')
    if isinstance(s_paths, str):
        s_paths = [s_paths]
    s_paths.reverse()
    for p in s_paths:
        if os.path.isdir(p):
            sys.path.insert(0, p)
            CUSTOM_ROUTERS_DIRS.append(p)
            add_sub_search_dirs(p, CUSTOM_ROUTERS_DIRS)

print('config:', config)

# Setup logging
log_tracebacks = False
if config.has('logging'):

    print('config ... logging:', config.get('logging'))

    log_tracebacks = config.get('logging').get('tracebacks', False)
    if config.get('logging').has('filename'):
        section = config.get('logging')

        print('config ... logging ... section:', section)
        print('config ... logging ... level:', section.get('level', 'ERROR'))
        print('config ... logging ... level:', getattr(logging, section.get('level', 'ERROR'), logging.ERROR))
        print('config ... logging ... level:', logging.DEBUG)

        f_name = section.get('filename')
        if RE_STARTSWITH_TILDE.match(f_name):
            f_name = os.path.expanduser(f_name)
        elif not RE_STARTSWITH_SLASH.match(f_name):
            f_name = os.path.join(LOG_DIR, f_name)

        print('config ... logging ... f_name:', f_name)

        logging.basicConfig(
            filename=f_name,
            format=section.get('format', '%(asctime)s - %(name)s - %(levelname)s - %(message)s'),
            datefmt=section.get('datefmt', '%Y-%m-%d %H:%M:%S'),
            level=getattr(logging, section.get('level', 'ERROR'), logging.ERROR)
        )



# Try to import all modules in a given directory
def import_modules(
    path: str,
    container: dict,
    app: typing.Any,
    config: trp_config.TrpConfig,
    auth: typing.Any = None
) -> None:
    for root, dirs, files in os.walk(path):
        for f in files:
            if not f.endswith('.py'):
                continue
            if f.startswith('.'):
                continue
            if f.startswith('_'):
                continue
            
            name, ext = os.path.splitext(f)
            if name in container:
                continue
            
            module = None
            try:
                logging.info(f'Import module: {name}')
                module = importlib.import_module(name)
            except Exception as err:
                logging.critical(f'Unable to load module: {str(err)}')
                #if log_tracebacks:
                #    logging.exception(err)
                logging.exception(err)
                continue
            
            container[name] = module
            try:
                module.init(app, config, auth)
                logging.info(f'Import modules: `{name}` import successfull')
            except Exception as err:
                logging.error(f'Import modules: Unable to initialize module `{name}`: {str(err)}')
                continue
        
        break



# Create OAuth2 scheme
oauth2_scheme = OAuth2PasswordBearer(
    tokenUrl='auth/login'
)



# Create FastAPI app
app = FastAPI(
    #dependencies=[Depends(auth)],
)
print('app:', app)


# Mount static files directory
static_dir = os.path.join(PROJECT_DIR, 'assets')
if config.has('html') and config.get('html').has('static_dir'):
    t_dir = config.get('html').get('static_dir')
    if t_dir.startswith('~'):
        t_dir = os.path.expanduser(t_dir)
    if not t_dir.startswith('/'):
        t_dir = os.path.join(PROJECT_DIR, t_dir)
    if os.path.isdir(t_dir):
        static_dir = t_dir
app.mount('/static', StaticFiles(directory=static_dir), name='static')

# Load routes
routers = {}
for p in CUSTOM_ROUTERS_DIRS:
    import_modules(p, routers, app, config, oauth2_scheme)
for r_key in routers:
    try:
        app.include_router(routers[r_key].router)
        logging.info(f'Load router: `{r_key}` OK')
    except Exception as err:
        logging.warning(f'Load router: {str(err)}. Trying to use `get_router` method.')
        try:
            app.include_router(routers[r_key].get_router())
            logging.info(f'Load router: `{r_key}` OK')
        except Exception as err:
            logging.error(f'Load router: {str(err)}')
            pass





#########
# MAIN
#
async def main(config: trp_config.TrpConfig):
    port = config.get('api').get('port', 5000)
    log_level = config.get('logging').get('level', 'error').lower()
    
    print(f"Starting server on port {port}")
    print(f"    with log level '{log_level}'")
    
    config = uvicorn.Config(
        'transposer_api:app',
        port=port,
        log_level=log_level
    )
    server = uvicorn.Server(config)
    await server.serve()



if __name__ == '__main__':
    # Start API
    try:
        asyncio.run(main(config))
    except KeyboardInterrupt:
        print('Shutting down')
    except Exception as err:
        print(f'Error: {str(err)}')
        if log_tracebacks:
            logging.exception(err)
    finally:
        print('Bye.')
        sys.exit(0)

