# Transposer API

*Transposer API* provides an internal API and scripts for:
* Maintenance and healthchecks
* Datastore management
* RPC and data services



## Dependencies

*Transposer API* relies on the *Shared Lib* project.

### Python

Install modules via the `requirements.txt` file with

```bash
pip install -r requirements.txt
```


## Setup

### S3

To handle *S3* storage [AWS Boto3](https://pypi.org/project/boto3/) is used. For detailed information about *Boto3*, please refer to its [documentation](https://boto3.amazonaws.com/v1/documentation/api/latest/index.html).



## REST

Call `<host>:<port>/doc`, e.g. `http://127.0.0.1:5000/doc`, in a brower to get to the API documentation.



## GraphQL

**TBD**



## RPC Server

*RPC Server* serves methods and constants defined in modules and allows them to be called over *REST* and *WebSockets*.

On startup, the server attempts to load all packages and modules from the following directories, in that order:
* &lt;project dir&gt;/methods
* &lt;shared-lib project&gt;/methods
* /var/lib/transposer/methods
* ~/.local/lib/transposer/methods

That means, that modules and their functions and constants, found in later paths will overwrite the ones from earlier ones.

### Modules

To define functions and constants to be called, simply create a module, or a package with modules, in one of the search paths and reload the server. The server then will pick it up, parse it and serve it.

E.g.: `~/.local/lib/transposer/methods/my_module.py`

```python
# define simple functions
def add(a, b):
    return a + b
# simple functions will automatically get wrapped into an async function

# define async functions
async def sub(a, b):
    return a - b
# coroutine-functions will be used as defined

# define a class with methods
class MyClass:
    def my_echo(self, msg):
        return msg
    
    async def my_ping(self):
        return 'Pong'

# define constants
my_constant_1 = 'hello'
my_constant_2 = ['World']

# explicitly set the exports
export = {
    # this will result in only exporting these items
    'methods': [
        'add',
        'sub',
        'MyClass.my_ping'
    ],
    # this will prevent the export of any item
    'constants': [] 
}
# omitting will result in all found items being exported.
# only public items will be exported, meaning
#   * protected (starting with _), or
#   * private (starting with __)
# items will always be ignored.
```

Whatever returned will be wrapped into a response object, with the resulting value as its *payload*.

The response object always carries these fields:
* `success` indicating the success of the operation
* `message` containing the error message, if the operation failed
* `payload` containing the result of the operation

### Requests

#### Methods

To call a **method** a POST request has to be made. The request must contain a JSON body.

**Mandatory fields**
* `method` path to the method to call

**Optional fields**
* `args` positional arguments for the method
* `kwargs` keyword-arguments for the method

The method path is a .-separated string denoting &lt;module&gt;.&lt;method&gt; or &lt;package&gt;.&lt;module&gt;.&lt;method&gt; to the method, starting at the search path as its root.

E.g.: Assuming there is a file `my_module.py` in `~/.local/lib/transposer/methods`, that contains the method `add`, the path to that method would look like `my_module.add`. If the module was part of a package, e.g. `my_package`, the path would extend to `my_package.my_module.add`.


#### Constants

To call a **constant** a GET request has to be made. The path to the requested constant must be appended to the url.

E.g.: To call the `hello` constant, call GET `<host>:<port>/rpc/hello`


#### WebSockets

When using *WebSockets*, calling a method is identical to the REST approach. Instead of the request body, a JSON document is going to be sent. Requesting a constant, also requires sending a JSON document. The only field that is required and should be present in this document, is `id`.

